' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMessageEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMessageEdit))
        Me.MenuFlagsUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsDefault = New System.Windows.Forms.ToolStripMenuItem()
        Me.ButtonOK = New System.Windows.Forms.Button()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.TextBoxComment = New System.Windows.Forms.TextBox()
        Me.ContextMenuStripFlags = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TextBoxData = New System.Windows.Forms.TextBox()
        Me.TextBoxFlags = New System.Windows.Forms.TextBox()
        Me.ComboBoxProtocol = New System.Windows.Forms.ComboBox()
        Me.LabelComment = New System.Windows.Forms.Label()
        Me.LabelDataBytes = New System.Windows.Forms.Label()
        Me.LabelTxFlags = New System.Windows.Forms.Label()
        Me.LabelNetWorkId = New System.Windows.Forms.Label()
        Me.ContextMenuStripFlags.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuFlagsUndo
        '
        Me.MenuFlagsUndo.Name = "MenuFlagsUndo"
        Me.MenuFlagsUndo.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsUndo.Text = "Undo"
        '
        'MenuFlagsSpace1
        '
        Me.MenuFlagsSpace1.Name = "MenuFlagsSpace1"
        Me.MenuFlagsSpace1.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsCut
        '
        Me.MenuFlagsCut.Name = "MenuFlagsCut"
        Me.MenuFlagsCut.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsCut.Text = "Cut"
        '
        'MenuFlagsCopy
        '
        Me.MenuFlagsCopy.Name = "MenuFlagsCopy"
        Me.MenuFlagsCopy.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsCopy.Text = "Copy"
        '
        'MenuFlagsPaste
        '
        Me.MenuFlagsPaste.Name = "MenuFlagsPaste"
        Me.MenuFlagsPaste.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsPaste.Text = "Paste"
        '
        'MenuFlagsSpace2
        '
        Me.MenuFlagsSpace2.Name = "MenuFlagsSpace2"
        Me.MenuFlagsSpace2.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsSelectAll
        '
        Me.MenuFlagsSelectAll.Name = "MenuFlagsSelectAll"
        Me.MenuFlagsSelectAll.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsSelectAll.Text = "Select All"
        '
        'MenuFlagsSpace3
        '
        Me.MenuFlagsSpace3.Name = "MenuFlagsSpace3"
        Me.MenuFlagsSpace3.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsEdit
        '
        Me.MenuFlagsEdit.Name = "MenuFlagsEdit"
        Me.MenuFlagsEdit.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsEdit.Text = "Edit Flags"
        '
        'MenuFlagsClear
        '
        Me.MenuFlagsClear.Name = "MenuFlagsClear"
        Me.MenuFlagsClear.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsClear.Text = "Clear Flags"
        '
        'MenuFlagsDefault
        '
        Me.MenuFlagsDefault.Name = "MenuFlagsDefault"
        Me.MenuFlagsDefault.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsDefault.Text = "Set Default Flags"
        '
        'ButtonOK
        '
        Me.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ButtonOK.Location = New System.Drawing.Point(336, 69)
        Me.ButtonOK.Name = "ButtonOK"
        Me.ButtonOK.Size = New System.Drawing.Size(65, 25)
        Me.ButtonOK.TabIndex = 1
        Me.ButtonOK.Text = "OK"
        Me.ButtonOK.UseVisualStyleBackColor = True
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(408, 69)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(65, 25)
        Me.ButtonCancel.TabIndex = 0
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'TextBoxComment
        '
        Me.TextBoxComment.AcceptsReturn = True
        Me.TextBoxComment.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxComment.Location = New System.Drawing.Point(8, 69)
        Me.TextBoxComment.MaxLength = 0
        Me.TextBoxComment.Name = "TextBoxComment"
        Me.TextBoxComment.Size = New System.Drawing.Size(322, 20)
        Me.TextBoxComment.TabIndex = 5
        '
        'ContextMenuStripFlags
        '
        Me.ContextMenuStripFlags.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuFlagsUndo, Me.MenuFlagsSpace1, Me.MenuFlagsCut, Me.MenuFlagsCopy, Me.MenuFlagsPaste, Me.MenuFlagsSpace2, Me.MenuFlagsSelectAll, Me.MenuFlagsSpace3, Me.MenuFlagsEdit, Me.MenuFlagsClear, Me.MenuFlagsDefault})
        Me.ContextMenuStripFlags.Name = "ContextMenuStripFlags"
        Me.ContextMenuStripFlags.Size = New System.Drawing.Size(162, 198)
        '
        'TextBoxData
        '
        Me.TextBoxData.AcceptsReturn = True
        Me.TextBoxData.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxData.Location = New System.Drawing.Point(208, 21)
        Me.TextBoxData.MaxLength = 0
        Me.TextBoxData.Name = "TextBoxData"
        Me.TextBoxData.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxData.TabIndex = 4
        '
        'TextBoxFlags
        '
        Me.TextBoxFlags.AcceptsReturn = True
        Me.TextBoxFlags.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFlags.Location = New System.Drawing.Point(120, 21)
        Me.TextBoxFlags.MaxLength = 0
        Me.TextBoxFlags.Name = "TextBoxFlags"
        Me.TextBoxFlags.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFlags.TabIndex = 3
        '
        'ComboBoxProtocol
        '
        Me.ComboBoxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxProtocol.Location = New System.Drawing.Point(8, 21)
        Me.ComboBoxProtocol.Name = "ComboBoxProtocol"
        Me.ComboBoxProtocol.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxProtocol.TabIndex = 2
        '
        'LabelComment
        '
        Me.LabelComment.Location = New System.Drawing.Point(8, 53)
        Me.LabelComment.Name = "LabelComment"
        Me.LabelComment.Size = New System.Drawing.Size(89, 17)
        Me.LabelComment.TabIndex = 9
        Me.LabelComment.Text = "Comment:"
        '
        'LabelDataBytes
        '
        Me.LabelDataBytes.Location = New System.Drawing.Point(208, 5)
        Me.LabelDataBytes.Name = "LabelDataBytes"
        Me.LabelDataBytes.Size = New System.Drawing.Size(89, 17)
        Me.LabelDataBytes.TabIndex = 8
        Me.LabelDataBytes.Text = "Data Bytes:"
        '
        'LabelTxFlags
        '
        Me.LabelTxFlags.Location = New System.Drawing.Point(120, 5)
        Me.LabelTxFlags.Name = "LabelTxFlags"
        Me.LabelTxFlags.Size = New System.Drawing.Size(65, 17)
        Me.LabelTxFlags.TabIndex = 7
        Me.LabelTxFlags.Text = "Tx Flags:"
        '
        'LabelNetWorkId
        '
        Me.LabelNetWorkId.Location = New System.Drawing.Point(8, 5)
        Me.LabelNetWorkId.Name = "LabelNetWorkId"
        Me.LabelNetWorkId.Size = New System.Drawing.Size(89, 17)
        Me.LabelNetWorkId.TabIndex = 6
        Me.LabelNetWorkId.Text = "Network ID:"
        '
        'FormMessageEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(479, 99)
        Me.Controls.Add(Me.ButtonOK)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.TextBoxComment)
        Me.Controls.Add(Me.TextBoxData)
        Me.Controls.Add(Me.TextBoxFlags)
        Me.Controls.Add(Me.ComboBoxProtocol)
        Me.Controls.Add(Me.LabelComment)
        Me.Controls.Add(Me.LabelDataBytes)
        Me.Controls.Add(Me.LabelTxFlags)
        Me.Controls.Add(Me.LabelNetWorkId)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormMessageEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Message"
        Me.ContextMenuStripFlags.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents MenuFlagsUndo As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsSpace1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents MenuFlagsCut As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsCopy As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsPaste As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsSpace2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents MenuFlagsSelectAll As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsSpace3 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents MenuFlagsEdit As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsClear As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents MenuFlagsDefault As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ContextMenuStripFlags As System.Windows.Forms.ContextMenuStrip
    Private WithEvents ButtonOK As Button
    Private WithEvents ButtonCancel As Button
    Private WithEvents TextBoxComment As TextBox
    Private WithEvents TextBoxData As TextBox
    Private WithEvents TextBoxFlags As TextBox
    Private WithEvents ComboBoxProtocol As ComboBox
    Private LabelComment As Label
    Private LabelDataBytes As Label
    Private LabelTxFlags As Label
    Private LabelNetWorkId As Label

End Class