﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Globalization
Imports System.IO

Public Module PublicCode
    Public Function PrepareCSV(ByVal value As String) As String
        Dim c As Char
        Dim mustQuote As Boolean
        If String.IsNullOrWhiteSpace(value) Then Return String.Empty
        For i As Integer = 0 To value.Length - 1
            c = value.Chars(i)
            If c = ControlChars.Quote OrElse c = ControlChars.Lf OrElse c = ","c Then mustQuote = True
        Next i
        Dim result As String = value.Replace(ControlChars.Quote, ControlChars.Quote & ControlChars.Quote)
        If mustQuote Then result = ControlChars.Quote & result & ControlChars.Quote
        Return result
    End Function

    Public Function StripSpaces(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then Return String.Empty
        Dim result As New StringBuilder()
        For i As Integer = 0 To value.Length - 1
            If Not Char.IsWhiteSpace(value.Chars(i)) Then result.Append(value.Chars(i))
        Next i
        Return result.ToString
    End Function

    Public Function MustBeHex(ByVal value As String) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(value) Then Return result
        Dim containsAtoF, containsInvalid As Boolean
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value.Chars(i)
            If "AaBbCcDdEeFf".Contains(c) Then containsAtoF = True
            If Not c.IsHex Then containsInvalid = True
        Next i
        result = containsAtoF And Not containsInvalid
        Return result
    End Function

    Public Function MSB(ByVal value As ULong) As Integer
        For i As Integer = 0 To 63
            If (value << i And &H8000000000000000UL) <> 0 Then Return 63 - i
        Next
        Return -1
    End Function

    Public Function PopCount(value As ULong) As Integer
        Const m1 As ULong = &H5555_5555_5555_5555
        Const m2 As ULong = &H3333_3333_3333_3333
        Const m4 As ULong = &H0F0F_0F0F_0F0F_0F0F
        value -= (value >> 1) And m1
        value = (value And m2) + ((value >> 2) And m2)
        value = (value + (value >> 4)) And m4
        value += value >> 8
        value += value >> 16
        value += value >> 32
        Return CInt(value And &H7FUL)
    End Function

#Region "ExtensionMethods"
    Private Const EM_CANUNDO As UInteger = &HC6
    Private Const EM_UNDO As UInteger = &HC7
    Private Const EM_SETSEL As UInteger = &HB1
    Private Const WM_COMMAND As UInteger = &H111
    Private Const WM_CUT As UInteger = &H300
    Private Const WM_COPY As UInteger = &H301
    Private Const WM_PASTE As UInteger = &H302
    Private Const WM_CLEAR As UInteger = &H303
    Private Const WM_UNDO As UInteger = &H304

    Private Function IsEditControl(hWnd As IntPtr) As Boolean
        Dim result As Boolean = False
        If hWnd = IntPtr.Zero Then Return result
        Dim sb As New StringBuilder
        sb.EnsureCapacity(80)
        Dim n As Integer = NativeMethods.GetClassName(hWnd, sb, 80)
        result = sb.ToString.ToLower.Contains("edit") AndAlso n > 0
        Return result
    End Function

    Private ReadOnly SetSelectedTextInternal As Action(Of TextBox, String, Boolean) =
    DirectCast([Delegate].CreateDelegate(GetType(Action(Of TextBox, String, Boolean)),
                                         GetType(TextBox).GetMethod("SetSelectedTextInternal",
                                         BindingFlags.Instance Or BindingFlags.NonPublic)),
                                         Action(Of TextBox, String, Boolean))

    <Extension>
    Public Sub Delete(ByVal target As TextBox)
        If String.IsNullOrEmpty(target?.Text) Then Return
        SetSelectedTextInternal(target, String.Empty, False)
    End Sub

    <Extension>
    Public Sub Undo(ByVal target As ComboBox)
        If target?.DropDownStyle <> ComboBoxStyle.DropDown Then Return
        Dim ptrTextBox As IntPtr = NativeMethods.FindWindowEx(target.Handle, IntPtr.Zero, Nothing, Nothing)
        If ptrTextBox = IntPtr.Zero Then Return
        NativeMethods.SendMessage(ptrTextBox, WM_UNDO, IntPtr.Zero, IntPtr.Zero)
    End Sub

    <Extension>
    Public Sub Cut(ByVal target As ComboBox)
        If target?.DropDownStyle <> ComboBoxStyle.DropDown Then Return
        NativeMethods.SendMessage(target.Handle, WM_CUT, IntPtr.Zero, IntPtr.Zero)
    End Sub

    <Extension>
    Public Sub Copy(ByVal target As ComboBox)
        If target?.DropDownStyle <> ComboBoxStyle.DropDown Then Return
        NativeMethods.SendMessage(target.Handle, WM_COPY, IntPtr.Zero, IntPtr.Zero)
    End Sub

    <Extension>
    Public Sub Paste(ByVal target As ComboBox)
        If target?.DropDownStyle <> ComboBoxStyle.DropDown Then Return
        NativeMethods.SendMessage(target.Handle, WM_PASTE, IntPtr.Zero, IntPtr.Zero)
    End Sub

    <Extension>
    Public Sub Delete(ByVal target As ComboBox)
        If target?.DropDownStyle <> ComboBoxStyle.DropDown Then Return
        target.SelectedText = String.Empty
    End Sub

    <Extension>
    Public Function CanUndo(ByVal target As ComboBox) As Boolean
        If target Is Nothing Then Return False
        Dim result As Boolean = False
        Dim i As Integer = 0
        Dim textBoxHandle As IntPtr = NativeMethods.FindWindowEx(target.Handle, IntPtr.Zero, Nothing, Nothing)
        If textBoxHandle = IntPtr.Zero Then Return result
        If IsEditControl(textBoxHandle) Then i = NativeMethods.SendMessage(textBoxHandle, EM_CANUNDO, IntPtr.Zero, IntPtr.Zero)
        result = i <> 0
        Return result
    End Function

    <StructLayout(LayoutKind.Explicit, Pack:=1)>
    Private Structure Conversion
        <FieldOffset(0)> Public AsByte As Byte
        <FieldOffset(0)> Public AsSByte As SByte
        <FieldOffset(0)> Public AsInt16 As Int16
        <FieldOffset(0)> Public AsUInt16 As UInt16
        <FieldOffset(0)> Public AsInt32 As Int32
        <FieldOffset(0)> Public AsUInt32 As UInt32
        <FieldOffset(0)> Public AsInt64 As Int64
        <FieldOffset(0)> Public AsUInt64 As UInt64
    End Structure

    <Extension>
    Public Function ToByte(ByVal target As String) As Byte
        Dim result As Byte
        Dim isHex As Boolean = False
        target = StripSpaces(target)
        If target.StartsWith("&h", IgnoreCase) OrElse target.StartsWith("0x", IgnoreCase) Then
            target = target.Substring(2)
            isHex = True
        ElseIf MustBeHex(target) Then
            isHex = True
        End If
        If isHex Then
            Byte.TryParse(target, NumberStyles.HexNumber, Nothing, result)
        Else
            Byte.TryParse(target, result)
        End If
        Return result
    End Function

    <Extension>
    Public Function ToUint32(ByVal target As String) As UInt32
        Dim result As UInteger
        Dim isHex As Boolean = False
        target = StripSpaces(target)
        If target.StartsWith("&h", IgnoreCase) OrElse target.StartsWith("0x", IgnoreCase) Then
            target = target.Substring(2)
            isHex = True
        ElseIf MustBeHex(target) Then
            isHex = True
        End If
        If isHex Then
            UInteger.TryParse(target, NumberStyles.HexNumber, Nothing, result)
        Else
            UInteger.TryParse(target, result)
        End If
        Return result
    End Function

    <Extension>
    Public Function ToUint16(ByVal value As UInt32) As UInt16
        Dim convert As Conversion
        convert.AsUInt32 = value
        Return convert.AsUInt16
    End Function

    <Extension>
    Public Function ToInt32(ByVal value As UInt32) As Int32
        Dim convert As Conversion
        convert.AsUInt32 = value
        Return convert.AsInt32
    End Function

    <Extension>
    Public Function ToUint32(ByVal value As Int32) As UInt32
        Dim convert As Conversion
        convert.AsInt32 = value
        Return convert.AsUInt32
    End Function

    <Extension>
    Public Sub SelectAll(ByVal target As ListView)
        If target Is Nothing Then Return
        For Each item As ListViewItem In target.Items
            item.Selected = True
        Next item
    End Sub

    <Extension>
    Public Function IsHex(ByVal target As Char) As Boolean
        Select Case target
            Case "0"c To "9"c, "A"c To "F"c, "a"c To "f"c
                Return True
        End Select
        Return False
    End Function

#End Region
End Module
