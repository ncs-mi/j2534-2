' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.ComponentModel

Public Class FormTxFlags
    Private localFlags As UInteger
    Private localChannel As J2534Channel

    Protected Overrides Sub OnLoad(e As EventArgs)
        SetControlArrays()
        SetCheckCaptions()
        TextBoxFlags.Text = FlagsToText(localFlags)
        SetChecks()
        LabelDesc.Text = String.Empty
        MyBase.OnLoad(e)
    End Sub

    Private Sub SetCheckCaptions()
        For i As Integer = 0 To 5
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE"
        Next i
        CheckBoxFlag(6).Text = "Bit 6 - ISO15765_FRAME_PAD"
        CheckBoxFlag(6).Tag = "0 = no padding" & Environment.NewLine & "1 = pad all flow controlled messages to a full CAN frame using zeroes"
        CheckBoxFlag(7).Text = "Bit 7 - ISO15765_ADDR_TYPE"
        CheckBoxFlag(7).Tag = "0 = no extended address" & Environment.NewLine & "1 = extended address is first byte after the CAN ID"
        CheckBoxFlag(8).Text = "Bit 8 - CAN_29BIT_ID"
        CheckBoxFlag(8).Tag = "0 = 11-bit" & Environment.NewLine & "1 = 29-bit"
        CheckBoxFlag(9).Text = "Bit 9 - WAIT_P3_MIN_ONLY"
        CheckBoxFlag(9).Tag = "0 = Message timing as specified in ISO 14230" & Environment.NewLine &
                           "1 = After a response is received for a physical request, the wait time shall be reduced to P3_MIN"
        CheckBoxFlag(10).Text = "Bit 10 - SW_CAN_HV_TX"
        CheckBoxFlag(10).Tag = "Indicates that the Single Wire CAN message" & Environment.NewLine &
                           "should be transmitted as a high voltage message"
        For i As Integer = 11 To 15
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE"
        Next i
        If localChannel.IsJ1708 Then
            For i As Integer = 16 To 19
                CheckBoxFlag(i).Text = "Bit " & i & " - MSG_PRIORITY_VALUE" & " bit " & i - 16
                CheckBoxFlag(i).Tag = "Message Priority" & Environment.NewLine &
                                   "Valid values 1 thru 8" & Environment.NewLine &
                                   "Value of 0 or greater than 8 will be treated as priority 8"
            Next i
        ElseIf localchannel.IsTP2_0 Then
            CheckBoxFlag(16).Text = "Bit 16 - TP2_0_BROADCAST_MSG"
            CheckBoxFlag(16).Tag = "Send this message as a broadcast which means" & Environment.NewLine &
                           "it will be sent 5 times with an interval of " & Environment.NewLine &
                           "TP2_0_T_BR_INT. The last 2 data bytes will" & Environment.NewLine &
                           "alternate between 0x55 and 0xAA."
            For i As Integer = 17 To 19
                CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE J2534-2"
            Next i
        Else
            For i As Integer = 16 To 19
                CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE J2534-2"
            Next i
        End If
        For i As Integer = 20 To 21
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE J2534-2"
        Next i
        CheckBoxFlag(22).Text = "Bit 22 - SCI_MODE"
        CheckBoxFlag(22).Tag = "0 = Transmit using SCI full duplex mode" & Environment.NewLine & "1 = Transmit using SCI half duplex mode"
        CheckBoxFlag(23).Text = "Bit 23 - SCI_TX_VOLTAGE"
        CheckBoxFlag(23).Tag = "0 = no voltage after message transmit" & Environment.NewLine & "1 = apply 20v after message transmit"
        For i As Integer = 24 To 31
            CheckBoxFlag(i).Text = "Bit " & i & " - Tool Manufacturer Specific"
        Next i
    End Sub

    Private Sub CheckBoxFlag_Click(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        SetText()
    End Sub

    Private Sub CheckBoxFlag_Enter(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.CheckBoxFlag, sender)
        LabelDesc.Text = CheckBoxFlag(index).Text & Environment.NewLine & Convert.ToString(CheckBoxFlag(index).Tag)
    End Sub

    Private Sub SetChecks()
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            CheckBoxFlag(i).Checked = (localFlags And mask) <> 0
        Next i
    End Sub

    Private Sub SetText()
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            If CheckBoxFlag(i).Checked Then
                localFlags = localFlags Or mask
            Else
                localFlags = localFlags And Not mask
            End If
        Next i
        TextBoxFlags.Text = FlagsToText(localFlags)
    End Sub

    Private Sub TextBoxFlags_TextChanged(sender As Object, e As EventArgs) Handles TextBoxFlags.TextChanged
        If Not IsHandleCreated Then Return
        localFlags = TextToFlags(TextBoxFlags.Text)
        SetChecks()
    End Sub

    Private Sub TextBoxFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub ContextMenuStripFlags_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripFlags.Opening
        If TypeOf ContextMenuStripFlags.SourceControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ContextMenuStripFlags.SourceControl, TextBox)
            If thisTextBox.Enabled Then
                thisTextBox.Focus()
                Dim selectedLength As Integer = thisTextBox.SelectionLength
                Dim textLength As Integer = thisTextBox.Text.Length
                MenuFlagsPaste.Enabled = My.Computer.Clipboard.ContainsData(DataFormats.Text)
                MenuFlagsCut.Enabled = selectedLength > 0
                MenuFlagsCopy.Enabled = selectedLength > 0
                MenuFlagsUndo.Enabled = thisTextBox.CanUndo
                MenuFlagsSelectAll.Enabled = selectedLength < textLength
                MenuFlagDefault.Available = localChannel.DefaultTxFlags <> 0
            End If
        End If
    End Sub

    Private Sub MenuFlagClear_Click(sender As Object, e As EventArgs) Handles MenuFlagClear.Click
        localFlags = 0
        SetChecks()
        SetText()
    End Sub

    Private Sub MenuFlagDefault_Click(sender As Object, e As EventArgs) Handles MenuFlagDefault.Click
        TextBoxFlags.Text = FlagsToText(localChannel.DefaultTxFlags)
    End Sub

    Private Sub MenuFlagsCopy_Click(sender As Object, e As EventArgs) Handles MenuFlagsCopy.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Copy()
    End Sub

    Private Sub MenuFlagsCut_Click(sender As Object, e As EventArgs) Handles MenuFlagsCut.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Cut()
    End Sub

    Private Sub MenuFlagsPaste_Click(sender As Object, e As EventArgs) Handles MenuFlagsPaste.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Paste()
    End Sub

    Private Sub MenuFlagsSelectAll_Click(sender As Object, e As EventArgs) Handles MenuFlagsSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).SelectAll()
    End Sub

    Private Sub MenuFlagsUndo_Click(sender As Object, e As EventArgs) Handles MenuFlagsUndo.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Undo()
    End Sub

    Private Sub SetControlArrays()
        InitializeCheckBoxFlag()
        For i As Integer = 0 To CheckBoxFlag.Length - 1
            AddHandler CheckBoxFlag(i).Click, AddressOf CheckBoxFlag_Click
            AddHandler CheckBoxFlag(i).Enter, AddressOf CheckBoxFlag_Enter
        Next i
    End Sub

    Public Overloads Function ShowDialog(ByRef flags As UInteger, ByVal channel As J2534Channel) As DialogResult
        Dim result As DialogResult
        localFlags = flags
        localChannel = channel
        result = Me.ShowDialog
        If result = DialogResult.OK Then flags = localFlags
        Return result
    End Function

End Class