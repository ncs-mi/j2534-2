﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormProtectAddress
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LabelAddress = New System.Windows.Forms.Label()
        Me.TextBoxAddress = New System.Windows.Forms.TextBox()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ButtonSet = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TextBoxIndGroup = New System.Windows.Forms.TextBox()
        Me.TextBoxVehSysInst = New System.Windows.Forms.TextBox()
        Me.TextBoxVehSys = New System.Windows.Forms.TextBox()
        Me.TextBoxFunc = New System.Windows.Forms.TextBox()
        Me.TextBoxFuncInst = New System.Windows.Forms.TextBox()
        Me.TextBoxECUInst = New System.Windows.Forms.TextBox()
        Me.TextBoxMfgCode = New System.Windows.Forms.TextBox()
        Me.TextBoxIdent = New System.Windows.Forms.TextBox()
        Me.LabelIG = New System.Windows.Forms.Label()
        Me.LabelSystemInstance = New System.Windows.Forms.Label()
        Me.LabelSystem = New System.Windows.Forms.Label()
        Me.LabelFunction = New System.Windows.Forms.Label()
        Me.LabelFunctionInstance = New System.Windows.Forms.Label()
        Me.LabelECUInstance = New System.Windows.Forms.Label()
        Me.LabelMfgCode = New System.Windows.Forms.Label()
        Me.LabelIdentityNo = New System.Windows.Forms.Label()
        Me.LabelIGSize = New System.Windows.Forms.Label()
        Me.LabelVehSysInstSize = New System.Windows.Forms.Label()
        Me.LabelVehSysSize = New System.Windows.Forms.Label()
        Me.LabelFuncSize = New System.Windows.Forms.Label()
        Me.LabelFuncInstSize = New System.Windows.Forms.Label()
        Me.LabelECUInstSize = New System.Windows.Forms.Label()
        Me.LabelMfgCodeSize = New System.Windows.Forms.Label()
        Me.LabelIdentSize = New System.Windows.Forms.Label()
        Me.LabelResult = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LabelAddress
        '
        Me.LabelAddress.AutoSize = True
        Me.LabelAddress.Location = New System.Drawing.Point(89, 13)
        Me.LabelAddress.Name = "LabelAddress"
        Me.LabelAddress.Size = New System.Drawing.Size(48, 13)
        Me.LabelAddress.TabIndex = 11
        Me.LabelAddress.Text = "Address:"
        Me.LabelAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextBoxAddress
        '
        Me.TextBoxAddress.Location = New System.Drawing.Point(139, 10)
        Me.TextBoxAddress.Name = "TextBoxAddress"
        Me.TextBoxAddress.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxAddress.TabIndex = 2
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(333, 294)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(65, 25)
        Me.ButtonCancel.TabIndex = 0
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'ButtonSet
        '
        Me.ButtonSet.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ButtonSet.Location = New System.Drawing.Point(262, 294)
        Me.ButtonSet.Name = "ButtonSet"
        Me.ButtonSet.Size = New System.Drawing.Size(65, 25)
        Me.ButtonSet.TabIndex = 1
        Me.ButtonSet.Text = "Set"
        Me.ButtonSet.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(225, 12)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(106, 17)
        Me.CheckBox1.TabIndex = 20
        Me.CheckBox1.Text = "Arbitrary Capable"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'TextBoxIndGroup
        '
        Me.TextBoxIndGroup.Location = New System.Drawing.Point(139, 36)
        Me.TextBoxIndGroup.Name = "TextBoxIndGroup"
        Me.TextBoxIndGroup.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxIndGroup.TabIndex = 3
        '
        'TextBoxVehSysInst
        '
        Me.TextBoxVehSysInst.Location = New System.Drawing.Point(139, 62)
        Me.TextBoxVehSysInst.Name = "TextBoxVehSysInst"
        Me.TextBoxVehSysInst.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxVehSysInst.TabIndex = 4
        '
        'TextBoxVehSys
        '
        Me.TextBoxVehSys.Location = New System.Drawing.Point(139, 88)
        Me.TextBoxVehSys.Name = "TextBoxVehSys"
        Me.TextBoxVehSys.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxVehSys.TabIndex = 5
        '
        'TextBoxFunc
        '
        Me.TextBoxFunc.Location = New System.Drawing.Point(139, 114)
        Me.TextBoxFunc.Name = "TextBoxFunc"
        Me.TextBoxFunc.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxFunc.TabIndex = 6
        '
        'TextBoxFuncInst
        '
        Me.TextBoxFuncInst.Location = New System.Drawing.Point(139, 140)
        Me.TextBoxFuncInst.Name = "TextBoxFuncInst"
        Me.TextBoxFuncInst.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxFuncInst.TabIndex = 7
        '
        'TextBoxECUInst
        '
        Me.TextBoxECUInst.Location = New System.Drawing.Point(139, 166)
        Me.TextBoxECUInst.Name = "TextBoxECUInst"
        Me.TextBoxECUInst.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxECUInst.TabIndex = 8
        '
        'TextBoxMfgCode
        '
        Me.TextBoxMfgCode.Location = New System.Drawing.Point(139, 192)
        Me.TextBoxMfgCode.Name = "TextBoxMfgCode"
        Me.TextBoxMfgCode.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxMfgCode.TabIndex = 9
        '
        'TextBoxIdent
        '
        Me.TextBoxIdent.Location = New System.Drawing.Point(139, 218)
        Me.TextBoxIdent.Name = "TextBoxIdent"
        Me.TextBoxIdent.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxIdent.TabIndex = 10
        '
        'LabelIG
        '
        Me.LabelIG.AutoSize = True
        Me.LabelIG.Location = New System.Drawing.Point(58, 39)
        Me.LabelIG.Name = "LabelIG"
        Me.LabelIG.Size = New System.Drawing.Size(79, 13)
        Me.LabelIG.TabIndex = 12
        Me.LabelIG.Text = "Industry Group:"
        Me.LabelIG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelSystemInstance
        '
        Me.LabelSystemInstance.AutoSize = True
        Me.LabelSystemInstance.Location = New System.Drawing.Point(11, 65)
        Me.LabelSystemInstance.Name = "LabelSystemInstance"
        Me.LabelSystemInstance.Size = New System.Drawing.Size(126, 13)
        Me.LabelSystemInstance.TabIndex = 13
        Me.LabelSystemInstance.Text = "Vehicle System Instance:"
        Me.LabelSystemInstance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelSystem
        '
        Me.LabelSystem.AutoSize = True
        Me.LabelSystem.Location = New System.Drawing.Point(55, 91)
        Me.LabelSystem.Name = "LabelSystem"
        Me.LabelSystem.Size = New System.Drawing.Size(82, 13)
        Me.LabelSystem.TabIndex = 14
        Me.LabelSystem.Text = "Vehicle System:"
        Me.LabelSystem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelFunction
        '
        Me.LabelFunction.AutoSize = True
        Me.LabelFunction.Location = New System.Drawing.Point(86, 117)
        Me.LabelFunction.Name = "LabelFunction"
        Me.LabelFunction.Size = New System.Drawing.Size(51, 13)
        Me.LabelFunction.TabIndex = 15
        Me.LabelFunction.Text = "Function:"
        Me.LabelFunction.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelFunctionInstance
        '
        Me.LabelFunctionInstance.AutoSize = True
        Me.LabelFunctionInstance.Location = New System.Drawing.Point(42, 143)
        Me.LabelFunctionInstance.Name = "LabelFunctionInstance"
        Me.LabelFunctionInstance.Size = New System.Drawing.Size(95, 13)
        Me.LabelFunctionInstance.TabIndex = 16
        Me.LabelFunctionInstance.Text = "Function Instance:"
        Me.LabelFunctionInstance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelECUInstance
        '
        Me.LabelECUInstance.AutoSize = True
        Me.LabelECUInstance.Location = New System.Drawing.Point(61, 169)
        Me.LabelECUInstance.Name = "LabelECUInstance"
        Me.LabelECUInstance.Size = New System.Drawing.Size(76, 13)
        Me.LabelECUInstance.TabIndex = 17
        Me.LabelECUInstance.Text = "ECU Instance:"
        Me.LabelECUInstance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelMfgCode
        '
        Me.LabelMfgCode.AutoSize = True
        Me.LabelMfgCode.Location = New System.Drawing.Point(36, 195)
        Me.LabelMfgCode.Name = "LabelMfgCode"
        Me.LabelMfgCode.Size = New System.Drawing.Size(101, 13)
        Me.LabelMfgCode.TabIndex = 18
        Me.LabelMfgCode.Text = "Manufacturer Code:"
        Me.LabelMfgCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelIdentityNo
        '
        Me.LabelIdentityNo.AutoSize = True
        Me.LabelIdentityNo.Location = New System.Drawing.Point(53, 221)
        Me.LabelIdentityNo.Name = "LabelIdentityNo"
        Me.LabelIdentityNo.Size = New System.Drawing.Size(84, 13)
        Me.LabelIdentityNo.TabIndex = 19
        Me.LabelIdentityNo.Text = "Identity Number:"
        Me.LabelIdentityNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelIGSize
        '
        Me.LabelIGSize.AutoSize = True
        Me.LabelIGSize.Enabled = False
        Me.LabelIGSize.Location = New System.Drawing.Point(225, 39)
        Me.LabelIGSize.Name = "LabelIGSize"
        Me.LabelIGSize.Size = New System.Drawing.Size(56, 13)
        Me.LabelIGSize.TabIndex = 21
        Me.LabelIGSize.Text = "3 bits (0-7)"
        '
        'LabelVehSysInstSize
        '
        Me.LabelVehSysInstSize.AutoSize = True
        Me.LabelVehSysInstSize.Enabled = False
        Me.LabelVehSysInstSize.Location = New System.Drawing.Point(225, 65)
        Me.LabelVehSysInstSize.Name = "LabelVehSysInstSize"
        Me.LabelVehSysInstSize.Size = New System.Drawing.Size(105, 13)
        Me.LabelVehSysInstSize.TabIndex = 22
        Me.LabelVehSysInstSize.Text = "4 bits (0-15, 0x0-0xF)"
        '
        'LabelVehSysSize
        '
        Me.LabelVehSysSize.AutoSize = True
        Me.LabelVehSysSize.Enabled = False
        Me.LabelVehSysSize.Location = New System.Drawing.Point(225, 91)
        Me.LabelVehSysSize.Name = "LabelVehSysSize"
        Me.LabelVehSysSize.Size = New System.Drawing.Size(123, 13)
        Me.LabelVehSysSize.TabIndex = 23
        Me.LabelVehSysSize.Text = "7 bits (0-127, 0x00-0x7F)"
        '
        'LabelFuncSize
        '
        Me.LabelFuncSize.AutoSize = True
        Me.LabelFuncSize.Enabled = False
        Me.LabelFuncSize.Location = New System.Drawing.Point(225, 117)
        Me.LabelFuncSize.Name = "LabelFuncSize"
        Me.LabelFuncSize.Size = New System.Drawing.Size(123, 13)
        Me.LabelFuncSize.TabIndex = 24
        Me.LabelFuncSize.Text = "8 bits (0-255, 0x00-0xFF)"
        '
        'LabelFuncInstSize
        '
        Me.LabelFuncInstSize.AutoSize = True
        Me.LabelFuncInstSize.Enabled = False
        Me.LabelFuncInstSize.Location = New System.Drawing.Point(225, 143)
        Me.LabelFuncInstSize.Name = "LabelFuncInstSize"
        Me.LabelFuncInstSize.Size = New System.Drawing.Size(117, 13)
        Me.LabelFuncInstSize.TabIndex = 25
        Me.LabelFuncInstSize.Text = "5 bits (0-31, 0x00-0x1F)"
        '
        'LabelECUInstSize
        '
        Me.LabelECUInstSize.AutoSize = True
        Me.LabelECUInstSize.Enabled = False
        Me.LabelECUInstSize.Location = New System.Drawing.Point(225, 169)
        Me.LabelECUInstSize.Name = "LabelECUInstSize"
        Me.LabelECUInstSize.Size = New System.Drawing.Size(56, 13)
        Me.LabelECUInstSize.TabIndex = 26
        Me.LabelECUInstSize.Text = "3 bits (0-7)"
        '
        'LabelMfgCodeSize
        '
        Me.LabelMfgCodeSize.AutoSize = True
        Me.LabelMfgCodeSize.Enabled = False
        Me.LabelMfgCodeSize.Location = New System.Drawing.Point(225, 195)
        Me.LabelMfgCodeSize.Name = "LabelMfgCodeSize"
        Me.LabelMfgCodeSize.Size = New System.Drawing.Size(147, 13)
        Me.LabelMfgCodeSize.TabIndex = 27
        Me.LabelMfgCodeSize.Text = "11 bits (0-2047, 0x000-0x7FF)"
        '
        'LabelIdentSize
        '
        Me.LabelIdentSize.AutoSize = True
        Me.LabelIdentSize.Enabled = False
        Me.LabelIdentSize.Location = New System.Drawing.Point(225, 221)
        Me.LabelIdentSize.Name = "LabelIdentSize"
        Me.LabelIdentSize.Size = New System.Drawing.Size(177, 13)
        Me.LabelIdentSize.TabIndex = 28
        Me.LabelIdentSize.Text = "21 bits (0-2097151, 0x00-0x1FFFFF)"
        '
        'LabelResult
        '
        Me.LabelResult.AutoSize = True
        Me.LabelResult.Location = New System.Drawing.Point(92, 257)
        Me.LabelResult.Name = "LabelResult"
        Me.LabelResult.Size = New System.Drawing.Size(212, 13)
        Me.LabelResult.TabIndex = 29
        Me.LabelResult.Text = "0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00"
        '
        'FormProtectAddress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(410, 331)
        Me.Controls.Add(Me.LabelResult)
        Me.Controls.Add(Me.LabelIdentSize)
        Me.Controls.Add(Me.LabelMfgCodeSize)
        Me.Controls.Add(Me.LabelECUInstSize)
        Me.Controls.Add(Me.LabelFuncInstSize)
        Me.Controls.Add(Me.LabelFuncSize)
        Me.Controls.Add(Me.LabelVehSysSize)
        Me.Controls.Add(Me.LabelVehSysInstSize)
        Me.Controls.Add(Me.LabelIGSize)
        Me.Controls.Add(Me.LabelIdentityNo)
        Me.Controls.Add(Me.LabelMfgCode)
        Me.Controls.Add(Me.LabelECUInstance)
        Me.Controls.Add(Me.LabelFunctionInstance)
        Me.Controls.Add(Me.LabelFunction)
        Me.Controls.Add(Me.LabelSystem)
        Me.Controls.Add(Me.LabelSystemInstance)
        Me.Controls.Add(Me.LabelIG)
        Me.Controls.Add(Me.TextBoxIdent)
        Me.Controls.Add(Me.TextBoxMfgCode)
        Me.Controls.Add(Me.TextBoxECUInst)
        Me.Controls.Add(Me.TextBoxFuncInst)
        Me.Controls.Add(Me.TextBoxFunc)
        Me.Controls.Add(Me.TextBoxVehSys)
        Me.Controls.Add(Me.TextBoxVehSysInst)
        Me.Controls.Add(Me.TextBoxIndGroup)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.ButtonSet)
        Me.Controls.Add(Me.TextBoxAddress)
        Me.Controls.Add(Me.LabelAddress)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormProtectAddress"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "J1939 Address Claim"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents LabelAddress As Label
    Private WithEvents TextBoxAddress As TextBox
    Private WithEvents ButtonCancel As Button
    Private WithEvents ButtonSet As Button
    Private WithEvents CheckBox1 As CheckBox
    Private WithEvents TextBoxIndGroup As TextBox
    Private WithEvents TextBoxVehSysInst As TextBox
    Private WithEvents TextBoxVehSys As TextBox
    Private WithEvents TextBoxFunc As TextBox
    Private WithEvents TextBoxFuncInst As TextBox
    Private WithEvents TextBoxECUInst As TextBox
    Private WithEvents TextBoxMfgCode As TextBox
    Private WithEvents TextBoxIdent As TextBox
    Private WithEvents LabelIG As Label
    Private WithEvents LabelSystemInstance As Label
    Private WithEvents LabelSystem As Label
    Private WithEvents LabelFunction As Label
    Private WithEvents LabelFunctionInstance As Label
    Private WithEvents LabelECUInstance As Label
    Private WithEvents LabelMfgCode As Label
    Private WithEvents LabelIdentityNo As Label
    Private WithEvents LabelIGSize As Label
    Private WithEvents LabelVehSysInstSize As Label
    Private WithEvents LabelVehSysSize As Label
    Private WithEvents LabelFuncSize As Label
    Private WithEvents LabelFuncInstSize As Label
    Private WithEvents LabelECUInstSize As Label
    Private WithEvents LabelMfgCodeSize As Label
    Private WithEvents LabelIdentSize As Label
    Private WithEvents LabelResult As Label
End Class
