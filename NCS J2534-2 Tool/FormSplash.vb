' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Public Class FormSplash
    Protected Overrides Sub OnLoad(e As EventArgs)
        LabelVersion.Text = "Version " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor.ToString("00")
        If betaVersion > 0 Then LabelVersion.Text = LabelVersion.Text & " (�" & betaVersion.ToString & ")"
        LabelProductName.Text = My.Application.Info.ProductName
        LabelCompany.Text = My.Application.Info.CompanyName
        LabelCopyright.Text = My.Application.Info.Copyright
        TextBoxLicense.Select(0, 0)
        MyBase.OnLoad(e)
    End Sub

    Private Sub FormSplash_Click(sender As Object, e As EventArgs) Handles MyBase.Click, LabelCompany.Click, LabelCopyright.Click, LabelProductName.Click, LabelVersion.Click, ButtonOK.Click
        Me.Close()
    End Sub

    Private Sub FormSplash_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        If e.KeyChar = KeyChars.Escape Then Me.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        Me.Close()
    End Sub

End Class