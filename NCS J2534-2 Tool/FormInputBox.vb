﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901
'
Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On
Imports System.ComponentModel

Public Class FormInputBox
    Protected Overrides Sub OnLoad(e As EventArgs)
        TextBoxInput.Select()
        TextBoxInput.SelectAll()
        MyBase.OnLoad(e)
    End Sub

    Private Sub ContextMenuStripTextBox_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripTextBox.Opening
        Dim selectedLength As Integer = TextBoxInput.SelectionLength
        Dim textLength As Integer = TextBoxInput.TextLength
        MenuTextBoxPaste.Enabled = Clipboard.ContainsText
        MenuTextBoxCut.Enabled = selectedLength > 0
        MenuTextBoxCopy.Enabled = MenuTextBoxCut.Enabled
        MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled
        MenuTextBoxUndo.Enabled = TextBoxInput.CanUndo
        MenuTextBoxSelectAll.Enabled = selectedLength < textLength
    End Sub

    Private Sub MenuTextBoxCopy_Click(sender As Object, e As EventArgs) Handles MenuTextBoxCopy.Click
        TextBoxInput.Copy()
    End Sub

    Private Sub MenuTextBoxCut_Click(sender As Object, e As EventArgs) Handles MenuTextBoxCut.Click
        TextBoxInput.Cut()
    End Sub

    Private Sub MenuTextBoxDelete_Click(sender As Object, e As EventArgs) Handles MenuTextBoxDelete.Click
        TextBoxInput.Delete
    End Sub

    Private Sub MenuTextBoxPaste_Click(sender As Object, e As EventArgs) Handles MenuTextBoxPaste.Click
        TextBoxInput.Paste()
    End Sub

    Private Sub MenuTextBoxSelectAll_Click(sender As Object, e As EventArgs) Handles MenuTextBoxSelectAll.Click
        TextBoxInput.SelectAll()
    End Sub

    Private Sub MenuTextBoxUndo_Click(sender As Object, e As EventArgs) Handles MenuTextBoxUndo.Click
        TextBoxInput.Undo()
    End Sub

    Public Overloads Function ShowDialog(prompt As String, title As String, defaultResponse As String, positionX As Integer, positionY As Integer) As InputBoxResponse
        Dim result As New InputBoxResponse
        If prompt = "" Then
            LabelPrompt.Text = "Enter a value:"
        Else
            LabelPrompt.Text = prompt
        End If
        If title = "" Then
            Me.Text = My.Application.Info.ProductName
        Else
            Me.Text = title
        End If
        TextBoxInput.Text = defaultResponse
        TextBoxInput.SelectAll()
        If positionX < 0 AndAlso positionY < 0 Then
            Me.StartPosition = FormStartPosition.CenterParent
        Else
            Me.StartPosition = FormStartPosition.Manual
            If positionX >= 0 Then Me.Left = positionX
            If positionY >= 0 Then Me.Top = positionY
        End If
        result.Result = Me.ShowDialog
        If result.Result = DialogResult.OK Then
            result.Response = TextBoxInput.Text
        Else
            result.Response = defaultResponse
        End If
        Return result
    End Function

End Class
