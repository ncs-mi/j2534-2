' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901
'
Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.ComponentModel

Public Class FormMessageEdit
    Private localMessageText As J2534MessageText
    Private referenceMessageText As J2534MessageText
    Private localDevice As J2534Device
    Private settingComboProt As Boolean
    Private updatingMsgEdit As Boolean

    Protected Overrides Sub OnLoad(e As EventArgs)
        settingComboProt = True
        If localDevice IsNot Nothing Then ComboBoxProtocol.Items.AddRange(localDevice.ChannelSet)
        settingComboProt = False
        UpdateMsgEdit()
        MyBase.OnLoad(e)
    End Sub

    Private Sub ComboBoxProtocol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxProtocol.SelectedIndexChanged
        If settingComboProt Then Return
        SetEditMsg()
        UpdateMsgEdit()
    End Sub

    Private Sub TextBoxComment_TextChanged(sender As Object, e As EventArgs) Handles TextBoxComment.TextChanged
        If Not IsHandleCreated Then Return
        If Not updatingMsgEdit Then
            localMessageText.Comment = TextBoxComment.Text
            UpdateButtons()
        End If
    End Sub

    Private Sub TextBoxComment_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxComment.KeyPress
        If e.KeyChar = KeyChars.Cr Then
            localMessageText.Comment = TextBoxComment.Text
            UpdateButtons()
        End If
    End Sub

    Private Sub TextBoxComment_Leave(sender As Object, e As EventArgs) Handles TextBoxComment.Leave
        localMessageText.Comment = TextBoxComment.Text
        UpdateButtons()
    End Sub

    Private Sub TextBoxData_TextChanged(sender As Object, e As EventArgs) Handles TextBoxData.TextChanged
        If Not IsHandleCreated Then Return
        If Not updatingMsgEdit Then
            SetEditMsg()
            UpdateMsgEdit()
        End If
    End Sub

    Private Sub TextBoxData_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxData.KeyPress
        Select Case e.KeyChar
            Case KeyChars.Cr
                SetEditMsg()
                UpdateMsgEdit()
            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo

            Case Else
                If Not e.KeyChar.IsHex Then e.Handled = True
        End Select
    End Sub

    Private Sub TextBoxData_Leave(sender As Object, e As EventArgs) Handles TextBoxData.Leave
        SetEditMsg()
        UpdateMsgEdit()
    End Sub

    Private Sub TextBoxFlags_TextChanged(sender As Object, e As EventArgs) Handles TextBoxFlags.TextChanged
        If Not IsHandleCreated Then Return
        If Not updatingMsgEdit Then
            SetEditMsg()
            UpdateMsgEdit()
        End If
    End Sub

    Private Sub TextBoxFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            SetEditMsg()
            UpdateMsgEdit()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub SetEditMsg()
        localMessageText.Data = TextBoxData.Text
        localMessageText.ProtocolName = ComboBoxProtocol.Text
        localMessageText.TxFlags = TextBoxFlags.Text
    End Sub

    Private Sub UpdateMsgEdit()
        updatingMsgEdit = True
        If Not TextBoxData.Focused Then
            TextBoxData.Text = localMessageText.Data
            TextBoxData.SelectionStart = TextBoxData.Text.Length
        End If
        If Not TextBoxFlags.Focused Then
            TextBoxFlags.Text = localMessageText.TxFlags
            TextBoxFlags.SelectionStart = TextBoxFlags.Text.Length
        End If
        If localMessageText.ProtocolName.Length > 0 Then
            ComboBoxProtocol.Text = localMessageText.ProtocolName
        End If
        If Not TextBoxComment.Focused Then
            TextBoxComment.Text = localMessageText.Comment
            TextBoxComment.SelectionStart = TextBoxComment.Text.Length
        End If
        updatingMsgEdit = False
        UpdateButtons()
    End Sub

    Private Sub UpdateButtons()
        Dim changesExist As Boolean = False
        If referenceMessageText.ProtocolName <> localMessageText.ProtocolName Then changesExist = True
        If referenceMessageText.TxFlags <> localMessageText.TxFlags Then changesExist = True
        If referenceMessageText.Data <> localMessageText.Data Then changesExist = True
        If referenceMessageText.Comment <> localMessageText.Comment Then changesExist = True
        ButtonOK.Enabled = changesExist
        ButtonCancel.Enabled = True
    End Sub

    Private Sub TextFlags_Leave(sender As Object, e As EventArgs) Handles TextBoxFlags.Leave
        SetEditMsg()
        UpdateMsgEdit()
    End Sub

    Private Sub MenuFlagClear_Click(sender As Object, e As EventArgs) Handles MenuFlagsClear.Click
        TextBoxFlags.Text = "0x00000000"
    End Sub

    Private Sub MenuFlagDefault_Click(sender As Object, e As EventArgs) Handles MenuFlagsDefault.Click
        Dim channel As J2534Channel = TryCast(ComboBoxProtocol.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return
        TextBoxFlags.Text = FlagsToText(channel.DefaultTxFlags)
    End Sub

    Private Sub MenuFlagEdit_Click(sender As Object, e As EventArgs) Handles MenuFlagsEdit.Click
        Dim channel As J2534Channel = TryCast(ComboBoxProtocol.SelectedItem, J2534Channel)
        Dim flags As UInteger = TextToFlags(TextBoxFlags.Text)
        If channel Is Nothing Then Return
        If FormTxFlags.ShowDialog(flags, channel) = DialogResult.OK Then TextBoxFlags.Text = FlagsToText(flags)
    End Sub

    Private Sub MenuFlagsCopy_Click(sender As Object, e As EventArgs) Handles MenuFlagsCopy.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Copy()
    End Sub

    Private Sub MenuFlagsCut_Click(sender As Object, e As EventArgs) Handles MenuFlagsCut.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Cut()
    End Sub

    Private Sub MenuFlagsPaste_Click(sender As Object, e As EventArgs) Handles MenuFlagsPaste.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Paste()
    End Sub

    Private Sub MenuFlagsSelectAll_Click(sender As Object, e As EventArgs) Handles MenuFlagsSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).SelectAll()
    End Sub

    Private Sub MenuFlagsUndo_Click(sender As Object, e As EventArgs) Handles MenuFlagsUndo.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Undo()
    End Sub

    Private Sub ContextMenuStripFlags_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripFlags.Opening
        If TypeOf ContextMenuStripFlags.SourceControl Is TextBox Then
            Dim channel As J2534Channel = TryCast(ComboBoxProtocol.SelectedItem, J2534Channel)
            If channel Is Nothing Then Return
            Dim thisTextBox As TextBox = DirectCast(ContextMenuStripFlags.SourceControl, TextBox)
            If thisTextBox.Enabled Then
                thisTextBox.Focus()
                Dim selectedLength As Integer = thisTextBox.SelectionLength
                Dim textLength As Integer = thisTextBox.Text.Length
                MenuFlagsPaste.Enabled = Clipboard.ContainsData(DataFormats.Text)
                MenuFlagsCut.Enabled = selectedLength > 0
                MenuFlagsCopy.Enabled = selectedLength > 0
                MenuFlagsUndo.Enabled = thisTextBox.CanUndo
                MenuFlagsSelectAll.Enabled = (selectedLength < textLength)
                MenuFlagsSpace3.Available = thisTextBox Is TextBoxFlags
                MenuFlagsDefault.Available = thisTextBox Is TextBoxFlags AndAlso channel.DefaultTxFlags <> 0
                MenuFlagsClear.Available = thisTextBox Is TextBoxFlags
                MenuFlagsEdit.Available = thisTextBox Is TextBoxFlags
            End If
        End If
    End Sub

    Public Overloads Function ShowDialog(ByVal messageText As J2534MessageText, ByVal device As J2534Device) As DialogResult
        Dim result As DialogResult
        If device Is Nothing OrElse messageText Is Nothing Then Return result
        localMessageText = messageText
        referenceMessageText = messageText.Clone
        localDevice = device
        result = Me.ShowDialog
        Return result
    End Function

End Class