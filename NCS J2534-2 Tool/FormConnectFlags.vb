' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901
'
Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Public Class FormConnectFlags
    Private localFlags As UInteger

    Protected Overrides Sub OnLoad(e As EventArgs)
        SetControlArrays()
        SetCheckBoxCaptions()
        TextBoxFlags.Text = FlagsToText(localFlags)
        SetCheckBoxes()
        LabelDesc.Text = String.Empty
        MyBase.OnLoad(e)
    End Sub

    Private Sub SetCheckBoxCaptions()
        For i As Integer = 0 To 7
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE"
        Next i
        CheckBoxFlag(8).Text = "Bit 8 - CAN_29BIT_ID"
        CheckBoxFlag(8).Tag = "0 = standard" & Environment.NewLine & "1 = extended"
        CheckBoxFlag(9).Text = "Bit 9 - ISO9141_NO_CHECKSUM"
        CheckBoxFlag(9).Tag = "0 = generate checksum"
        CheckBoxFlag(10).Text = "Bit 10 - Reserved for SAE"
        CheckBoxFlag(11).Text = "Bit 11 - CAN_ID_BOTH"
        CheckBoxFlag(11).Tag = "0 = determined by CAN_29BIT_ID" & Environment.NewLine & "1 = Both standard and 29 bit"
        CheckBoxFlag(12).Text = "Bit 12 - ISO9141_K_LINE_ONLY"
        CheckBoxFlag(12).Tag = "0 = use L and K-lines" & Environment.NewLine & "1 = use K-line only"
        For i As Integer = 13 To 23
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE"
        Next i
        For i As Integer = 24 To 31
            CheckBoxFlag(i).Text = "Bit " & i & " - Tool mfg specific"
        Next i
    End Sub

    Private Sub CheckFlag_Click(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        SetTextBox()
    End Sub

    Private Sub CheckFlag_Enter(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.CheckBoxFlag, sender)
        LabelDesc.Text = CheckBoxFlag(index).Text & Environment.NewLine & Convert.ToString(CheckBoxFlag(index).Tag)
    End Sub

    Private Sub SetCheckBoxes()
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            CheckBoxFlag(i).Checked = (localFlags And mask) <> 0
        Next i
    End Sub

    Private Sub SetTextBox()
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            If CheckBoxFlag(i).Checked Then
                localFlags = localFlags Or mask
            Else
                localFlags = localFlags And Not mask
            End If
        Next i
        TextBoxFlags.Text = FlagsToText(localFlags)
    End Sub

    Private Sub TextBoxFlags_TextChanged(sender As Object, e As EventArgs) Handles TextBoxFlags.TextChanged
        If Not IsHandleCreated Then Return
        localFlags = TextToFlags(TextBoxFlags.Text)
        SetCheckBoxes()
    End Sub

    Private Sub TextBoxFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub SetControlArrays()
        InitializeCheckBoxFlag()
        For i As Integer = 0 To CheckBoxFlag.Length - 1
            AddHandler CheckBoxFlag(i).Click, AddressOf CheckFlag_Click
            AddHandler CheckBoxFlag(i).Enter, AddressOf CheckFlag_Enter
        Next i
    End Sub

    Public Overloads Function ShowDialog(ByRef flags As UInteger) As DialogResult
        Dim result As DialogResult
        localFlags = flags
        result = Me.ShowDialog
        If result = DialogResult.OK Then flags = localFlags
        Return result
    End Function

End Class