﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NCS J2534-2 Tool")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("National Control Systems, Inc.")>
<Assembly: AssemblyProduct("NCS J2534-2 Tool")>
<Assembly: AssemblyCopyright("Copyright © 2017, 2018 National Control Systems, Inc.")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("733e01ae-4fbf-4e78-8d91-95e8f1f8f924")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("2.0.0.0")>
<Assembly: AssemblyFileVersion("2.0.0.0")>
