' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Public Enum LINProtocolId As UInteger
    LIN = &H10101
End Enum

Public Enum LINIoCtlId As UInteger
    CAN_SET_BTR = &H10100
    GET_TIMESTAMP = &H10101
    GET_SERIAL_NUMBER = &H10103
    CAN_SET_ERROR_REPORTING = &H10105
    CAN_SET_INTERNAL_TERMINATION = &H10107
    DEVICE_RESET = &H10200

    LIN_SENDWAKEUP = 0 'unknown?
    LIN_ADD_SCHED = 0
    LIN_GET_SCHED = 0
    LIN_GET_SCHED_SIZE = 0
    LIN_DEL_SCHED = 0
    LIN_ACT_SCHED = 0
    LIN_DEACT_SCHED = 0
    LIN_GET_ACT_SCHED = 0
    LIN_GET_NUM_SCHEDS = 0
    LIN_GET_SCHED_NAMES = 0
    LIN_SET_FLAGS = 0
End Enum

Public Enum LINConfigParameterId As UInteger
    LIN_VERSION = &H10001
End Enum
