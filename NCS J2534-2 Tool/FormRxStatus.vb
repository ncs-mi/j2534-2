' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.ComponentModel

Public Class FormRxStatus
    Private localFlags As UInteger
    Private localProtocolId As UInteger

    Protected Overrides Sub OnLoad(e As EventArgs)
        SetControlArrays()
        SetCheckBoxCaptions()
        TextBoxFlags.Text = FlagsToText(localFlags)
        SetCheckBoxes()
        LabelDesc.Text = String.Empty
        MyBase.OnLoad(e)
    End Sub

    Private Sub SetCheckBoxCaptions()
        CheckBoxFlag(0).Text = "Bit 0 - TX_MSG_TYPE"
        CheckBoxFlag(0).Tag = "Receive Indication/Transmit Loopback" & Environment.NewLine & "1 = transmitted message"
        CheckBoxFlag(1).Text = "Bit 1 - START_OF_MESSAGE"
        CheckBoxFlag(1).Tag = "Indicates first byte of an ISO9141 or ISO14230 message or" & Environment.NewLine & "the first frame of an ISO15765 multi-frame message"
        CheckBoxFlag(2).Text = "Bit 2 - RX_BREAK"
        CheckBoxFlag(2).Tag = "Break Indication Received" & Environment.NewLine & "SAE J2610 and J1850VPW only"
        CheckBoxFlag(3).Text = "Bit 3 - TX_INDICATION"
        CheckBoxFlag(3).Tag = "ISO15765 TxDone" & Environment.NewLine & "1 = TxDone"
        CheckBoxFlag(4).Text = "Bit 4 - ISO15765_PADDING_ERROR"
        CheckBoxFlag(4).Tag = "For ISO15765 a CAN frame was received with less than 8 data bytes."
        CheckBoxFlag(5).Text = "Bit 5 - Reserved for SAE"
        CheckBoxFlag(6).Text = "Bit 6 - Reserved for SAE"
        CheckBoxFlag(7).Text = "Bit 7 - ISO15765_ADDR_TYPE"
        CheckBoxFlag(7).Tag = "0 = no extended address" & Environment.NewLine & "1 = extended address is first byte after the CAN ID"
        CheckBoxFlag(8).Text = "Bit 8 - CAN_29BIT_ID"
        CheckBoxFlag(8).Tag = "0 = 11-bit" & Environment.NewLine & "1 = 29-bit"
        For i As Integer = 9 To 15
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE"
        Next i

        If IsSingleWire(localProtocolId) Then
            CheckBoxFlag(16).Text = "Bit 16 - SW_CAN_HV_RX"
            CheckBoxFlag(16).Tag = "Indicates that the Single Wire CAN message" & Environment.NewLine &
                                "received was high-voltage message." & Environment.NewLine &
                                "0 = Normal Message" & Environment.NewLine &
                                "1 = High-Voltage Message"

            CheckBoxFlag(17).Text = "Bit 17 - SW_CAN_HS_RX"
            CheckBoxFlag(17).Tag = "Indicates that the Single Wire CAN bus has" & Environment.NewLine &
                                "transitioned to High Speed." & Environment.NewLine &
                                "All communication after this event will" & Environment.NewLine &
                                "occur in highspeed mode. The message" & Environment.NewLine &
                                "data in this message is undefined."

            CheckBoxFlag(18).Text = "Bit 18 - SW_CAN_NS_RX"
            CheckBoxFlag(18).Tag = "Indicates that the Single Wire CAN bus has" & Environment.NewLine &
                                "transitioned to Normal Speed." & Environment.NewLine &
                                "All communication after this event will" & Environment.NewLine &
                                "occur in normal-speed mode. The message" & Environment.NewLine &
                                "data in this message is undefined."
        ElseIf IsJ1939(localProtocolId) Then
            CheckBoxFlag(16).Text = "Bit 16 - J1939_ADDRESS_CLAIMED"
            CheckBoxFlag(16).Tag = "Address claimed status indication will be" & Environment.NewLine &
                                "sent when an Address and Name is successfully" & Environment.NewLine &
                                "claimed. The source address claimed will be in " & Environment.NewLine &
                                "Data[0] of the message." & Environment.NewLine &
                                "ExtraDataIndex = 0, DataSize = 1. "

            CheckBoxFlag(17).Text = "Bit 17 - J1939_ADDRESS_LOST"
            CheckBoxFlag(17).Tag = "Address Lost status indication will be sent when" & Environment.NewLine &
                                "a claimed Address and Name has been lost or when" & Environment.NewLine &
                                "an attempt to claim an Address and Name fails. " & Environment.NewLine &
                                "The source address lost will be in Data[0] of the message." & Environment.NewLine &
                                "ExtraDataIndex = 0, DataSize = 1."

            CheckBoxFlag(18).Text = "Bit 18 - Reserved for SAE J2534-2"
            CheckBoxFlag(18).Tag = String.Empty

        ElseIf IsFaultTolerant(localProtocolId) Then
            CheckBoxFlag(16).Text = "Bit 16 - Reserved for SAE J2534-2"
            CheckBoxFlag(16).Tag = String.Empty

            CheckBoxFlag(17).Text = "Bit 17 - LINK_FAULT"
            CheckBoxFlag(17).Tag = "Status bit in a received message that is" & Environment.NewLine &
                                "set when the transceiver has detected a" & Environment.NewLine &
                                "network fault but the device was still able" & Environment.NewLine &
                                "to correctly receive the message."

            CheckBoxFlag(18).Text = "Bit 18 - Reserved for SAE J2534-2"
            CheckBoxFlag(18).Tag = String.Empty

        ElseIf IsAnalog(localProtocolId) Then
            CheckBoxFlag(16).Text = "Bit 16 - OVERFLOW"
            CheckBoxFlag(16).Tag = "Indicates that the input range of the A/D" & Environment.NewLine &
                                "has been exceeded on one or more" & Environment.NewLine &
                                "samples in the received message"

            CheckBoxFlag(17).Text = "Bit 17 - Reserved for SAE J2534-2"
            CheckBoxFlag(17).Tag = String.Empty

            CheckBoxFlag(18).Text = "Bit 18 - Reserved for SAE J2534-2"
            CheckBoxFlag(18).Tag = String.Empty

        ElseIf IsTP2_0(localProtocolId) Then
            CheckBoxFlag(16).Text = "Bit 16 - CONNECTION_ESTABLISHED"
            CheckBoxFlag(16).Tag = "Connection established status" & Environment.NewLine &
                                "indication will be sent when a" & Environment.NewLine &
                                "Connection is successfully" & Environment.NewLine &
                                "established."

            CheckBoxFlag(17).Text = "Bit 17 - CONNECTION_LOST"
            CheckBoxFlag(17).Tag = "Connection Lost status indication will" & Environment.NewLine &
                                "be sent when a established connection has" & Environment.NewLine &
                                "been lost or when an attempt to establish" & Environment.NewLine &
                                "a connection fails."

            CheckBoxFlag(18).Text = "Bit 18 - Reserved for SAE J2534-2"
            CheckBoxFlag(18).Tag = String.Empty

        Else
            For i As Integer = 16 To 18
                CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE J2534-2"
            Next i
        End If

        For i As Integer = 19 To 23
            CheckBoxFlag(i).Text = "Bit " & i & " - Reserved for SAE J2534-2"
        Next i
        For i As Integer = 24 To 31
            CheckBoxFlag(i).Text = "Bit " & i & " - Tool Manufacturer Specific"
        Next i
    End Sub

    Private Sub CheckBoxFlag_Click(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        SetCheckBoxes()
    End Sub

    Private Sub CheckBoxFlag_Enter(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.CheckBoxFlag, sender)
        LabelDesc.Text = CheckBoxFlag(index).Text & Environment.NewLine & CheckBoxFlag(index).Tag.ToString
    End Sub

    Private Sub SetCheckBoxes()
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            CheckBoxFlag(i).Checked = CheckBoxFlag(i).Enabled AndAlso (localFlags And mask) <> 0
        Next i
    End Sub

    Private Sub SetTextBox()
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            If CheckBoxFlag(i).Checked Then
                localFlags = localFlags Or mask
            Else
                localFlags = localFlags And Not mask
            End If
        Next i
        TextBoxFlags.Text = FlagsToText(localFlags)
    End Sub

    Private Sub TextBoxFlags_TextChanged(sender As Object, e As EventArgs) Handles TextBoxFlags.TextChanged
        If Not IsHandleCreated Then Return
        localFlags = TextToFlags(TextBoxFlags.Text)
        SetCheckBoxes()
    End Sub

    Private Sub TextBoxFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub ContextMenuStripTextBox_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripTextBox.Opening
        If TypeOf (ContextMenuStripTextBox.SourceControl) Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ContextMenuStripTextBox.SourceControl, TextBox)
            Dim selectedLength As Integer = thisTextBox.SelectionLength
            Dim textLength As Integer = thisTextBox.TextLength
            MenuTextBoxPaste.Enabled = Clipboard.ContainsText
            MenuTextBoxCut.Enabled = (selectedLength > 0)
            MenuTextBoxCopy.Enabled = MenuTextBoxCut.Enabled
            MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled
            MenuTextBoxUndo.Enabled = thisTextBox.CanUndo
            MenuTextBoxSelectAll.Enabled = (selectedLength < textLength)
        End If
    End Sub

    Private Sub MenuTextBoxCopy_Click(sender As Object, e As EventArgs) Handles MenuTextBoxCopy.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Copy()
    End Sub

    Private Sub MenuTextBoxCut_Click(sender As Object, e As EventArgs) Handles MenuTextBoxCut.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Cut()
    End Sub

    Private Sub MenuTextBoxDelete_Click(sender As Object, e As EventArgs) Handles MenuTextBoxDelete.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Delete
    End Sub

    Private Sub MenuTextBoxPaste_Click(sender As Object, e As EventArgs) Handles MenuTextBoxPaste.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Paste()
    End Sub

    Private Sub MenuTextBoxSelectAll_Click(sender As Object, e As EventArgs) Handles MenuTextBoxSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).SelectAll()
    End Sub

    Private Sub MenuTextBoxUndo_Click(sender As Object, e As EventArgs) Handles MenuTextBoxUndo.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Undo()
    End Sub

    Private Sub SetControlArrays()
        InitializeCheckBoxFlag()
        For i As Integer = 0 To CheckBoxFlag.Length - 1
            AddHandler CheckBoxFlag(i).Click, AddressOf CheckBoxFlag_Click
            AddHandler CheckBoxFlag(i).Enter, AddressOf CheckBoxFlag_Enter
        Next i
    End Sub

    Public Overloads Function ShowDialog(ByRef flags As UInteger, ByVal protocolId As UInteger) As DialogResult
        Dim result As DialogResult
        localFlags = flags
        localProtocolId = protocolId
        result = Me.ShowDialog
        If result = DialogResult.OK Then flags = localFlags
        Return result
    End Function

End Class