﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.Runtime.InteropServices

Public Class J2534_API
    Implements IDisposable
    Private dllPointer As IntPtr

    Public PassThruOpen As PassThruOpenDelegate
    Public PassThruClose As PassThruCloseDelegate
    Public PassThruConnect As PassThruConnectDelegate
    Public PassThruDisconnect As PassThruDisconnectDelegate
    Public PassThruReadMsgs As PassThruReadMsgsDelegate
    Public PassThruWriteMsgs As PassThruWriteMsgsDelegate
    Public PassThruStartPeriodicMsg As PassThruStartPeriodicMsgDelegate
    Public PassThruStopPeriodicMsg As PassThruStopPeriodicMsgDelegate
    Public PassThruStartMsgFilter As PassThruStartMsgFilterDelegate
    Public PassThruStopMsgFilter As PassThruStopMsgFilterDelegate
    Public PassThruSetProgrammingVoltage As PassThruSetProgrammingVoltageDelegate
    Public PassThruReadVersion As PassThruReadVersionDelegate
    Public PassThruGetLastError As PassThruGetLastErrorDelegate
    Public PassThruIoctl As PassThruIoctlDelegate

    Public Property KeyName As String
    Public Property VendorName As String
    Public Property DeviceName As String
    Public Property LibraryName As String
    Public Property ConfigAppPath As String
    Public Property DeviceType As DeviceType
    Public ReadOnly Property DllIsLoaded As Boolean
        Get
            Return dllPointer <> IntPtr.Zero
        End Get
    End Property

    Public Const DLL_NO_ERROR As Integer = &H0
    Public Const ERR_NO_PTOPEN As Integer = &H1
    Public Const ERR_NO_PTCLOSE As Integer = &H2
    Public Const ERR_NO_PTCONNECT As Integer = &H4
    Public Const ERR_NO_PTDISCONNECT As Integer = &H8
    Public Const ERR_NO_PTREADMSGS As Integer = &H10
    Public Const ERR_NO_PTWRITEMSGS As Integer = &H20
    Public Const ERR_NO_PTSTARTPERIODICMSG As Integer = &H40
    Public Const ERR_NO_PTSTOPPERIODICMSG As Integer = &H80
    Public Const ERR_NO_PTSTARTMSGFILTER As Integer = &H100
    Public Const ERR_NO_PTSTOPMSGFILTER As Integer = &H200
    Public Const ERR_NO_PTSETPROGRAMMINGVOLTAGE As Integer = &H400
    Public Const ERR_NO_PTREADVERSION As Integer = &H800
    Public Const ERR_NO_PTGETLASTERROR As Integer = &H1000
    Public Const ERR_NO_PTIOCTL As Integer = &H2000
    Public Const ERR_NO_FUNCTIONS As Integer = &H3FFF
    Public Const ERR_NO_DLL As Integer = -1
    Public Const ERR_WRONG_DLL_VER As Integer = -2
    Public Const ERR_FUNC_MISSING As Integer = -3

    Public Function LoadJ2534dll() As Integer
        Dim result As Integer = ERR_NO_DLL
        If String.IsNullOrWhiteSpace(_LibraryName) Then Return result
        If dllPointer <> IntPtr.Zero Then Return result

        dllPointer = NativeMethods.LoadLibrary(_LibraryName)
        If dllPointer = IntPtr.Zero Then Return result

        Dim passThruOpenPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruOpen")
        Dim passThruClosePointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruClose")
        Dim passThruConnectPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruConnect")
        Dim passThruDisconnectPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruDisconnect")
        Dim passThruReadMsgsPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruReadMsgs")
        Dim passThruWriteMsgsPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruWriteMsgs")
        Dim passThruStartPeriodicMsgPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruStartPeriodicMsg")
        Dim passThruStopPeriodicMsgPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruStopPeriodicMsg")
        Dim passThruStartMsgFilterPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruStartMsgFilter")
        Dim passThruStopMsgFilterPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruStopMsgFilter")
        Dim passThruSetProgrammingVoltagePointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruSetProgrammingVoltage")
        Dim passThruReadVersionPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruReadVersion")
        Dim passThruGetLastErrorPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruGetLastError")
        Dim passThruIoctlPointer As IntPtr = NativeMethods.GetProcAddress(dllPointer, "PassThruIoctl")
        result = DLL_NO_ERROR

        If passThruOpenPointer = IntPtr.Zero Then result = result Or ERR_NO_PTOPEN
        If passThruClosePointer = IntPtr.Zero Then result = result Or ERR_NO_PTCLOSE
        If passThruConnectPointer = IntPtr.Zero Then result = result Or ERR_NO_PTCONNECT
        If passThruDisconnectPointer = IntPtr.Zero Then result = result Or ERR_NO_PTDISCONNECT
        If passThruReadMsgsPointer = IntPtr.Zero Then result = result Or ERR_NO_PTREADMSGS
        If passThruWriteMsgsPointer = IntPtr.Zero Then result = result Or ERR_NO_PTWRITEMSGS
        If passThruStartPeriodicMsgPointer = IntPtr.Zero Then result = result Or ERR_NO_PTSTARTPERIODICMSG
        If passThruStopPeriodicMsgPointer = IntPtr.Zero Then result = result Or ERR_NO_PTSTOPPERIODICMSG
        If passThruStartMsgFilterPointer = IntPtr.Zero Then result = result Or ERR_NO_PTSTARTMSGFILTER
        If passThruStopMsgFilterPointer = IntPtr.Zero Then result = result Or ERR_NO_PTSTOPMSGFILTER
        If passThruSetProgrammingVoltagePointer = IntPtr.Zero Then result = result Or ERR_NO_PTSETPROGRAMMINGVOLTAGE
        If passThruReadVersionPointer = IntPtr.Zero Then result = result Or ERR_NO_PTREADVERSION
        If passThruGetLastErrorPointer = IntPtr.Zero Then result = result Or ERR_NO_PTGETLASTERROR
        If passThruIoctlPointer = IntPtr.Zero Then result = result Or ERR_NO_PTIOCTL
        If result <> DLL_NO_ERROR Then Return result

        PassThruOpen = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruOpenPointer, GetType(PassThruOpenDelegate)), PassThruOpenDelegate)
        PassThruClose = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruClosePointer, GetType(PassThruCloseDelegate)), PassThruCloseDelegate)
        PassThruConnect = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruConnectPointer, GetType(PassThruConnectDelegate)), PassThruConnectDelegate)
        PassThruDisconnect = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruDisconnectPointer, GetType(PassThruDisconnectDelegate)), PassThruDisconnectDelegate)
        PassThruReadMsgs = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruReadMsgsPointer, GetType(PassThruReadMsgsDelegate)), PassThruReadMsgsDelegate)
        PassThruWriteMsgs = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruWriteMsgsPointer, GetType(PassThruWriteMsgsDelegate)), PassThruWriteMsgsDelegate)
        PassThruStartPeriodicMsg = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruStartPeriodicMsgPointer, GetType(PassThruStartPeriodicMsgDelegate)), PassThruStartPeriodicMsgDelegate)
        PassThruStopPeriodicMsg = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruStopPeriodicMsgPointer, GetType(PassThruStopPeriodicMsgDelegate)), PassThruStopPeriodicMsgDelegate)
        PassThruStartMsgFilter = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruStartMsgFilterPointer, GetType(PassThruStartMsgFilterDelegate)), PassThruStartMsgFilterDelegate)
        PassThruStopMsgFilter = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruStopMsgFilterPointer, GetType(PassThruStopMsgFilterDelegate)), PassThruStopMsgFilterDelegate)
        PassThruSetProgrammingVoltage = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruSetProgrammingVoltagePointer, GetType(PassThruSetProgrammingVoltageDelegate)), PassThruSetProgrammingVoltageDelegate)
        PassThruReadVersion = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruReadVersionPointer, GetType(PassThruReadVersionDelegate)), PassThruReadVersionDelegate)
        PassThruGetLastError = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruGetLastErrorPointer, GetType(PassThruGetLastErrorDelegate)), PassThruGetLastErrorDelegate)
        PassThruIoctl = DirectCast(Marshal.GetDelegateForFunctionPointer(passThruIoctlPointer, GetType(PassThruIoctlDelegate)), PassThruIoctlDelegate)

        Return result
    End Function

    Public Function UnloadJ2534Dll() As Integer
        Dim result As Integer
        For i As Integer = 0 To Short.MaxValue
            result = NativeMethods.FreeLibrary(dllPointer)
            If result = 0 Then Exit For
        Next i
        dllPointer = IntPtr.Zero
        PassThruOpen = Nothing
        PassThruClose = Nothing
        PassThruConnect = Nothing
        PassThruDisconnect = Nothing
        PassThruReadMsgs = Nothing
        PassThruWriteMsgs = Nothing
        PassThruStartPeriodicMsg = Nothing
        PassThruStopPeriodicMsg = Nothing
        PassThruStartMsgFilter = Nothing
        PassThruStopMsgFilter = Nothing
        PassThruSetProgrammingVoltage = Nothing
        PassThruReadVersion = Nothing
        PassThruGetLastError = Nothing
        PassThruIoctl = Nothing
        Return result
    End Function

    <DebuggerStepThrough>
    Public Overrides Function ToString() As String
        Return _KeyName
    End Function

    Private disposedValue As Boolean ' To detect redundant calls
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                'dispose managed state (managed objects).
            End If
            UnloadJ2534Dll()
        End If
        disposedValue = True
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class

