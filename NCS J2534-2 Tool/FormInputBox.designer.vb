﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormInputBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ButtonOK = New System.Windows.Forms.Button()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.LabelPrompt = New System.Windows.Forms.Label()
        Me.TextBoxInput = New System.Windows.Forms.TextBox()
        Me.ContextMenuStripTextBox = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MenuTextBoxUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuTextBoxCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuTextBoxSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripTextBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonOK
        '
        Me.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ButtonOK.Location = New System.Drawing.Point(283, 12)
        Me.ButtonOK.Name = "ButtonOK"
        Me.ButtonOK.Size = New System.Drawing.Size(75, 23)
        Me.ButtonOK.TabIndex = 0
        Me.ButtonOK.Text = "OK"
        Me.ButtonOK.UseVisualStyleBackColor = True
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(283, 41)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
        Me.ButtonCancel.TabIndex = 1
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'LabelPrompt
        '
        Me.LabelPrompt.AutoSize = True
        Me.LabelPrompt.Location = New System.Drawing.Point(6, 9)
        Me.LabelPrompt.Name = "LabelPrompt"
        Me.LabelPrompt.Size = New System.Drawing.Size(66, 13)
        Me.LabelPrompt.TabIndex = 2
        Me.LabelPrompt.Text = "LabelPrompt"
        '
        'TextBoxInput
        '
        Me.TextBoxInput.Location = New System.Drawing.Point(9, 87)
        Me.TextBoxInput.Name = "TextBoxInput"
        Me.TextBoxInput.Size = New System.Drawing.Size(349, 20)
        Me.TextBoxInput.TabIndex = 3
        '
        'ContextMenuStripTextBox
        '
        Me.ContextMenuStripTextBox.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuTextBoxUndo, Me.MenuTextBoxSeparator1, Me.MenuTextBoxCut, Me.MenuTextBoxCopy, Me.MenuTextBoxPaste, Me.MenuTextBoxDelete, Me.MenuTextBoxSeparator2, Me.MenuTextBoxSelectAll})
        Me.ContextMenuStripTextBox.Name = "ContextMenuStripTextBox"
        Me.ContextMenuStripTextBox.Size = New System.Drawing.Size(123, 148)
        '
        'MenuTextBoxUndo
        '
        Me.MenuTextBoxUndo.Name = "MenuTextBoxUndo"
        Me.MenuTextBoxUndo.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxUndo.Text = "Undo"
        '
        'MenuTextBoxSeparator1
        '
        Me.MenuTextBoxSeparator1.Name = "MenuTextBoxSeparator1"
        Me.MenuTextBoxSeparator1.Size = New System.Drawing.Size(119, 6)
        '
        'MenuTextBoxCut
        '
        Me.MenuTextBoxCut.Name = "MenuTextBoxCut"
        Me.MenuTextBoxCut.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxCut.Text = "Cut"
        '
        'MenuTextBoxCopy
        '
        Me.MenuTextBoxCopy.Name = "MenuTextBoxCopy"
        Me.MenuTextBoxCopy.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxCopy.Text = "Copy"
        '
        'MenuTextBoxPaste
        '
        Me.MenuTextBoxPaste.Name = "MenuTextBoxPaste"
        Me.MenuTextBoxPaste.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxPaste.Text = "Paste"
        '
        'MenuTextBoxDelete
        '
        Me.MenuTextBoxDelete.Name = "MenuTextBoxDelete"
        Me.MenuTextBoxDelete.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxDelete.Text = "Delete"
        '
        'MenuTextBoxSeparator2
        '
        Me.MenuTextBoxSeparator2.Name = "MenuTextBoxSeparator2"
        Me.MenuTextBoxSeparator2.Size = New System.Drawing.Size(119, 6)
        '
        'MenuTextBoxSelectAll
        '
        Me.MenuTextBoxSelectAll.Name = "MenuTextBoxSelectAll"
        Me.MenuTextBoxSelectAll.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxSelectAll.Text = "Select All"
        '
        'FormInputBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(370, 119)
        Me.Controls.Add(Me.TextBoxInput)
        Me.Controls.Add(Me.LabelPrompt)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.ButtonOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormInputBox"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "FormInputBox"
        Me.ContextMenuStripTextBox.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents ButtonOK As Button
    Private WithEvents ButtonCancel As Button
    Private LabelPrompt As Label
    Private WithEvents TextBoxInput As TextBox
    Private WithEvents ContextMenuStripTextBox As ContextMenuStrip
    Private WithEvents MenuTextBoxUndo As ToolStripMenuItem
    Private WithEvents MenuTextBoxSeparator1 As ToolStripSeparator
    Private WithEvents MenuTextBoxCut As ToolStripMenuItem
    Private WithEvents MenuTextBoxCopy As ToolStripMenuItem
    Private WithEvents MenuTextBoxPaste As ToolStripMenuItem
    Private WithEvents MenuTextBoxDelete As ToolStripMenuItem
    Private WithEvents MenuTextBoxSeparator2 As ToolStripSeparator
    Private WithEvents MenuTextBoxSelectAll As ToolStripMenuItem
End Class
