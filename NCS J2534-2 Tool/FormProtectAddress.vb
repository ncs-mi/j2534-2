﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Public Class FormProtectAddress
    Dim localSetup As J1939AddressClaimSetup
    Dim resultBytes(8) As Byte

    Protected Overrides Sub OnLoad(e As EventArgs)
        UpdateResult()
        UpdateLabel()
        MyBase.OnLoad(e)
    End Sub

    Private Sub UpdateResult()
        resultBytes = BuildJ1939Name(localSetup)
        UpdateLabel()
    End Sub

    Private Sub UpdateLabel()
        LabelResult.Text = "0x" & resultBytes(8).ToString("X2") &
                          " 0x" & resultBytes(7).ToString("X2") &
                          " 0x" & resultBytes(6).ToString("X2") &
                          " 0x" & resultBytes(5).ToString("X2") &
                          " 0x" & resultBytes(4).ToString("X2") &
                          " 0x" & resultBytes(3).ToString("X2") &
                          " 0x" & resultBytes(2).ToString("X2") &
                          " 0x" & resultBytes(1).ToString("X2") &
                          " 0x" & resultBytes(0).ToString("X2")
    End Sub

    Private Sub TextBoxAddress_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxAddress.KeyPress,
                                                                                          TextBoxIndGroup.KeyPress,
                                                                                          TextBoxVehSysInst.KeyPress,
                                                                                          TextBoxVehSys.KeyPress,
                                                                                          TextBoxFunc.KeyPress,
                                                                                          TextBoxFuncInst.KeyPress,
                                                                                          TextBoxECUInst.KeyPress,
                                                                                          TextBoxMfgCode.KeyPress,
                                                                                          TextBoxIdent.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxAddress_TextChanged(sender As Object, e As EventArgs) Handles TextBoxAddress.TextChanged
        localSetup.Address = TextBoxAddress.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxIndGroup_TextChanged(sender As Object, e As EventArgs) Handles TextBoxIndGroup.TextChanged
        localSetup.IndustryGroup = TextBoxIndGroup.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxVehSysInst_TextChanged(sender As Object, e As EventArgs) Handles TextBoxVehSysInst.TextChanged
        localSetup.VehicleSystemInstance = TextBoxVehSysInst.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxVehSys_TextChanged(sender As Object, e As EventArgs) Handles TextBoxVehSys.TextChanged
        localSetup.VehicleSystemId = TextBoxVehSys.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxFunc_TextChanged(sender As Object, e As EventArgs) Handles TextBoxFunc.TextChanged
        localSetup.FunctionId = TextBoxFunc.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxFuncInst_TextChanged(sender As Object, e As EventArgs) Handles TextBoxFuncInst.TextChanged
        localSetup.FunctionInstance = TextBoxFuncInst.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxECUInst_TextChanged(sender As Object, e As EventArgs) Handles TextBoxECUInst.TextChanged
        localSetup.ECUInstance = TextBoxECUInst.Text.ToByte
        UpdateResult()
    End Sub

    Private Sub TextBoxMfgCode_TextChanged(sender As Object, e As EventArgs) Handles TextBoxMfgCode.TextChanged
        localSetup.MfgCode = TextBoxMfgCode.Text.ToUint32
        UpdateResult()
    End Sub

    Private Sub TextBoxIdent_TextChanged(sender As Object, e As EventArgs) Handles TextBoxIdent.TextChanged
        localSetup.Identity = TextBoxIdent.Text.ToUint32
        UpdateResult()
    End Sub

    Private Sub CheckBox1_Click(sender As Object, e As EventArgs) Handles CheckBox1.Click
        localSetup.ArbitraryCapable = CheckBox1.Checked
        UpdateResult()
    End Sub

    Public Overloads Function ShowDialog(ByVal setup As J1939AddressClaimSetup) As DialogResult
        localSetup = setup
        Return Me.ShowDialog
    End Function

End Class

