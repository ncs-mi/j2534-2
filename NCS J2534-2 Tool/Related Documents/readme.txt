﻿NCS Tool for J2534-2 Compliant Devices
Copyright © 2018 National Control Systems, Inc.

This tool serves two purposes:
 - To provide functioning examples of J2534 dll calls in Visual Basic .NET
 - To provide a tool for basic testing of various modules in the lab
 
The application is based on SAE J2534-1 dated 12-2004 and J2534-2 dated 10-2010
 
The tool is based on a similar tool for J2534-1 compliant devices released
in 2005. Some significant changes include:
 - It is now possible to load more than one J2534 dll at a time, even from
   different manufacturers
 - Analog subsystems are supported
 - While it is no longer possible to save and load entire device config files,
   filters and periodic messages may be saved and loaded
 - The tool is almost completely device neutral
 
Notes on the code:

Only J2534-1 dlls with proper registry entries are supported. If you have a dll
that you would like to load, it would not be hard to add a feature such as the
ability to browse for a dll to load.

Only one pin switched channel per protocol is supported.

GET_PROTOCOL_INFO is included but largely unused. The function GetChannelInfo
can be built upon. I found that the responses to the ioctls varied so much just
in the few devices I tested, I did not delve much further into it. 

The basis for adding manufacturer specific support is implemented to some
degree to allow for LIN on a DG Netbridge. This can be expanded upon to 
provide more mfg specific support.

Some features have never been tested. For instance, five baud init. I have no 
modules to test it on.

I hope you find this tool and the source code useful. Please feel free to
provide any feedback for improvements, added features, or even coding
suggestions.

National Control Systems, Inc.
10737 Hamburg Rd.
Hamburg, MI 48139
810-231-2901