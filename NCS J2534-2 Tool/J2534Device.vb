﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.Runtime.InteropServices
Imports System.Text
Imports NCS_J2534_2_Tool.J2534IoCtlId
Imports NCS_J2534_2_Tool.J2534ConfigParameterId
Imports NCS_J2534_2_Tool.J2534GetDeviceInfoParameter
Imports NCS_J2534_2_Tool.J2534GetProtocolInfoParameter


Public NotInheritable Class J2534Device
    Implements IDisposable
    Public Property ChannelSet As J2534Channel() = {}
    Public Property Setup As New J2534Setup With {.MessageReadRate = 10, .TextBoxOutFlags = "0x00000000", .TextBoxMessageOut = "00 00"}
    Public Property Analog As New J2534Analog
    Public Property API As J2534_API
    Public Property Name As String
    Public ReadOnly Property DeviceId As UInteger
    Public Property Version As String
    Public Property Description As String
    Public Property Connected As Boolean
    Public Property Online As Boolean
    Public Property MessageInCount As ULong
    Public Property MessageOutCount As ULong
    Public Property SerialNo As UInteger
    Public ReadOnly Property SupportedProtocols As String
        Get
            Dim result As New StringBuilder
            Dim n As Integer = _ChannelSet.Length - 1
            For index As Integer = 0 To n
                result.Append(_ChannelSet(index).Name)
                Dim protocolId As UInteger = _ChannelSet(index).ProtocolId
                Dim first As UInteger = ProtocolIdToChannelNo(protocolId)
                If first > 0 Then
                    Dim last As UInteger
                    Dim root As UInteger = protocolId And &HFF80UI
                    Dim j As UInteger = 0
                    Dim i As Integer = index
                    While i <= n AndAlso (_ChannelSet(i).ProtocolId And &HFF80UI) = root
                        last = first + j
                        j += 1UI
                        i += 1
                    End While
                    If last > first Then
                        index = i
                        result.Append(" to " & last.ToString)
                    End If
                End If
                If index < n Then result.Append(", ")
            Next index
            Return result.ToString
        End Get
    End Property
    Public ReadOnly Property MaxChannel As Integer
        Get
            Return _ChannelSet.Length - 1
        End Get
    End Property
    Public ReadOnly Property Channel(ByVal index As Integer) As J2534Channel
        Get
            If index >= 0 AndAlso index < _ChannelSet.Length Then Return _ChannelSet(index)
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Channel(ByVal protocolId As UInteger) As J2534Channel
        Get
            For index As Integer = 0 To MaxChannel
                If _ChannelSet(index).ProtocolId = protocolId Then Return _ChannelSet(index)
            Next index
            Return Nothing
        End Get
    End Property
    Public ReadOnly Property Channel(ByVal name As String) As J2534Channel
        Get
            Dim result As J2534Channel = Nothing
            For index As Integer = 0 To MaxChannel
                If _ChannelSet(index).Name = name Then Return _ChannelSet(index)
            Next index
            Return result
        End Get
    End Property
    Public ReadOnly Property ChannelIndex(ByVal protocolId As UInteger) As Integer
        Get
            For index As Integer = 0 To MaxChannel
                If _ChannelSet(index).ProtocolId = protocolId Then Return index
            Next index
            Return -1
        End Get
    End Property
    Public ReadOnly Property ChannelIndex(ByVal name As String) As Integer
        Get
            For index As Integer = 0 To MaxChannel
                If _ChannelSet(index).Name = name Then Return index
            Next index
            Return -1
        End Get
    End Property
    Public ReadOnly Property ChannelIndex(ByVal channel As J2534Channel) As Integer
        Get
            For index As Integer = 0 To MaxChannel
                If _ChannelSet(index) Is channel Then Return index
            Next index
            Return -1
        End Get
    End Property
    Public ReadOnly Property HasChannel(ByVal name As String) As Boolean
        Get
            For i As Integer = 0 To MaxChannel
                If _ChannelSet(i).Name = name Then Return True
            Next i
            Return False
        End Get
    End Property
    Public ReadOnly Property HasChannel(ByVal protocolId As UInteger) As Boolean
        Get
            For i As Integer = 0 To MaxChannel
                If _ChannelSet(i).ProtocolId = protocolId Then Return True
            Next i
            Return False
        End Get
    End Property

    Public ListViewMessageIn As New ListView
    Public ListViewMessageOut As New ListView
    Public TimerMessageReceive As New Timer

    Public Sub SetDeviceId(value As UInteger)
        _DeviceId = value
    End Sub

    Private Sub BuildListViewObjects()
        ListViewMessageIn.Font = New Font("Courier New", 9)
        ListViewMessageIn.Name = "ListViewMessageIn"
        ListViewMessageIn.UseCompatibleStateImageBehavior = False
        ListViewMessageIn.View = View.Details
        ListViewMessageIn.FullRowSelect = True
        ListViewMessageIn.HideSelection = False
        ListViewMessageIn.SmallImageList = New ImageList()
        ListViewMessageIn.SmallImageList.Images.Add(My.Resources.inArrow)
        ListViewMessageIn.SmallImageList.Images.Add(My.Resources.outArrow)
        If ListViewMessageIn.Columns.Count = 0 Then
            ListViewMessageIn.Columns.Add("Timestamp", "Timestamp", 120)
            ListViewMessageIn.Columns.Add("Network ID", "Network ID", 103)
            ListViewMessageIn.Columns.Add("Rx Status", "Rx Status", 82)
            ListViewMessageIn.Columns.Add("Data Bytes", "Data Bytes")
        End If

        ListViewMessageOut.Font = New Font("Courier New", 9)
        ListViewMessageOut.Name = "ListViewMessageOut"
        ListViewMessageOut.UseCompatibleStateImageBehavior = False
        ListViewMessageOut.View = View.Details
        ListViewMessageOut.FullRowSelect = True
        ListViewMessageOut.HideSelection = False
        ListViewMessageOut.MultiSelect = False
        If ListViewMessageOut.Columns.Count = 0 Then
            ListViewMessageOut.Columns.Add("Network ID", "Network ID", 103)
            ListViewMessageOut.Columns.Add("Tx Flags", "Tx Flags", 82)
            ListViewMessageOut.Columns.Add("Data Bytes", "Data Bytes")
            ListViewMessageOut.Columns.Add("Comments", "Comments")
        End If
    End Sub

    Public Sub BuildChannels()
        BuildChannels(Me)
    End Sub
    Private Shared Sub BuildChannels(ByVal device As J2534Device)
        If device Is Nothing Then Return
        Dim ChannelSet As New List(Of J2534Channel)

        Dim deviceInfo As SPARAM = GetDeviceInfo(device, SERIAL_NUMBER)
        If deviceInfo.Supported = J2534DiscoveryResult.SUPPORTED Then device.SerialNo = deviceInfo.Value

        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1850VPW))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1850PWM))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ISO9141))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ISO14230))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.CAN))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SW_CAN))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.FT_CAN))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ISO15765))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SW_ISO15765))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.FT_ISO15765))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_A_ENGINE))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_A_TRANS))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_B_ENGINE))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_B_TRANS))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J2610))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.GM_UART))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ECHO_BYTE))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.HONDA_DIAGH))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1939))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1708))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.TP2_0))
        ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.Analog))

        If device.API.DeviceType = DeviceType.Netbridge Then
            Dim channel As New J2534Channel(J2534ProtocolId.LIN)
            GetChannelInfo(device, channel)
            ChannelSet.Add(channel)
        End If

        device.ChannelSet = ChannelSet.ToArray
        device.Analog.BatteryReading = 0
        device.Analog.PinReading = 0
        device.Analog.PinSetting = 5000
        For i As Integer = 0 To 15
            device.Analog.Pin(i) = New PinInfo With {.Mode = PinMode.Open, .Selected = False, .CanBeSet = False, .CanBeShorted = False}
        Next i
        GetProgVoltagePinInfo(device)

    End Sub

    Private Shared Function GetProtocolSupport(ByVal device As J2534Device, ByVal builder As ChannelSetupParameters) As J2534Channel()
        Dim result As New List(Of J2534Channel)
        If device Is Nothing Then Return {}

        Dim deviceInfo As SPARAM = GetDeviceInfo(device, builder.SupportParameter)
        If deviceInfo.Supported = J2534DiscoveryResult.SUPPORTED Then
            If deviceInfo.SS > 0 AndAlso builder.BaseId.HasValue Then 'Base Channel
                Dim channel As New J2534Channel(builder.BaseId.Value)
                GetChannelInfo(device, channel)
                result.Add(channel)
            End If

            If deviceInfo.RR > 0 AndAlso builder.PinSwitchedId.HasValue Then 'Pin Switched Channels
                Dim channel As New J2534Channel(builder.PinSwitchedId.Value)
                Dim parameters As New List(Of J2534ConfigParameter)
                parameters.AddRange(channel.ConfParameter)
                If builder.J1962PinsParameter.HasValue Then
                    channel.J1962Pins = GetPinInfo(device, builder.J1962PinsParameter.Value)
                    If channel.J1962Pins.Length > 0 Then parameters.Add(New J2534ConfigParameter(J1962_PINS))
                End If
                If builder.J1939PinsParameter.HasValue Then
                    channel.J1939Pins = GetPinInfo(device, builder.J1939PinsParameter.Value)
                    If channel.J1939Pins.Length > 0 Then parameters.Add(New J2534ConfigParameter(J1939_PINS))
                End If
                If builder.J1708PinsParameter.HasValue Then
                    channel.J1708Pins = GetPinInfo(device, builder.J1708PinsParameter.Value)
                    If channel.J1708Pins.Length > 0 Then parameters.Add(New J2534ConfigParameter(J1708_PINS))
                End If
                channel.ConfParameter = parameters.ToArray
                GetChannelInfo(device, channel)
                result.Add(channel)
            End If

            Dim maxChannel As Integer = deviceInfo.QQ - 1  'CHx Channels
            If maxChannel >= 0 AndAlso builder.FirstCHxId.HasValue Then
                For i As Integer = 0 To maxChannel
                    Dim protocolId As J2534ProtocolId = DirectCast(builder.FirstCHxId.Value + i.ToUint32, J2534ProtocolId)
                    Dim channel As New J2534Channel(protocolId)
                    GetChannelInfo(device, channel)
                    result.Add(channel)
                Next i
            End If
        End If
        Return result.ToArray
    End Function
    Private Shared Function GetProtocolSupport(ByVal device As J2534Device, ByVal builder As ChannelSetupParametersK) As J2534Channel()
        Dim result As New List(Of J2534Channel)
        If device Is Nothing Then Return {}

        Dim deviceInfo As SPARAM = GetDeviceInfo(device, builder.SupportParameter)
        If deviceInfo.Supported = J2534DiscoveryResult.SUPPORTED Then
            If deviceInfo.SS > 0 AndAlso builder.BaseId.HasValue Then 'Base Channel
                Dim channel As New J2534Channel(builder.BaseId.Value)
                GetChannelInfo(device, channel)
                result.Add(channel)
            End If

            If deviceInfo.RR > 0 AndAlso builder.PinSwitchedId.HasValue Then 'Pin Switched Channels
                Dim channel As New J2534Channel(builder.PinSwitchedId.Value)
                Dim parameters As New List(Of J2534ConfigParameter)
                If builder.J1962_K_PinsParameter.HasValue AndAlso builder.J1962_L_PinsParameter.HasValue Then
                    channel.J1962Pins = GetPinInfo(device, builder.J1962_K_PinsParameter.Value, builder.J1962_L_PinsParameter.Value)
                    If channel.J1962Pins.Length > 0 Then parameters.Add(New J2534ConfigParameter(J1962_PINS))
                End If
                If builder.J1939_K_PinsParameter.HasValue AndAlso builder.J1939_L_PinsParameter.HasValue Then
                    channel.J1939Pins = GetPinInfo(device, builder.J1939_K_PinsParameter.Value, builder.J1939_L_PinsParameter.Value)
                    If channel.J1939Pins.Length > 0 Then parameters.Add(New J2534ConfigParameter(J1939_PINS))
                End If
                channel.ConfParameter = parameters.ToArray
                GetChannelInfo(device, channel)
                result.Add(channel)
            End If

            Dim maxChannel As Integer = deviceInfo.QQ - 1 'CHx Channels
            If maxChannel >= 0 AndAlso builder.FirstCHxId.HasValue Then
                For i As Integer = 0 To maxChannel
                    Dim protocolId As J2534ProtocolId = DirectCast(builder.FirstCHxId.Value + i.ToUint32, J2534ProtocolId)
                    Dim channel As New J2534Channel(protocolId)
                    GetChannelInfo(device, channel)
                    result.Add(channel)
                Next i
            End If
        End If
        Return result.ToArray
    End Function
    Private Shared Function GetProtocolSupport(ByVal device As J2534Device, ByVal builder As ChannelSetupParametersAnalog) As J2534Channel()
        Dim result As New List(Of J2534Channel)
        Dim deviceInfo As SPARAM = GetDeviceInfo(device, ANALOG_IN_SUPPORTED)
        If deviceInfo.Supported = J2534DiscoveryResult.SUPPORTED Then
            Dim maxChannel As Integer = deviceInfo.QQ - 1
            If maxChannel >= 0 Then
                Dim protocolInfo As SPARAM
                For i As Integer = 0 To maxChannel
                    Dim protocolId As J2534ProtocolId = DirectCast(builder.FirstCHxId.Value + i.ToUint32, J2534ProtocolId)
                    Dim channel As New J2534Channel(protocolId) With {.AnalogSubsystem = New J2534AnalogSubsystem}
                    protocolInfo = GetProtocolInfo(device, protocolId, MAX_AD_ACTIVE_CHANNELS)
                    If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then
                        'this is a kludge to support a box that reports MAX_AD_ACTIVE_CHANNELS
                        'as an integer rather than a bitmap. The kludge will fail if the box reports an integer 7
                        'for example, where this will assume it is a bitmap and return 3 for the 3 bits
                        Select Case protocolInfo.Value
                            Case 1, 3, 7, 15, > 31 'the bits are packed or it's not valid as an integer
                                channel.AnalogSubsystem.MaxChannel = PopCount(protocolInfo.Value).ToUint32
                            Case Else
                                channel.AnalogSubsystem.MaxChannel = protocolInfo.Value
                        End Select
                    End If
                    protocolInfo = GetProtocolInfo(device, protocolId, MAX_AD_SAMPLE_RATE)
                    If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.AnalogSubsystem.MaxSampleRate = protocolInfo.Value
                    GetChannelInfo(device, channel)
                    result.Add(channel)
                Next i
            End If
        End If
        Return result.ToArray
    End Function

    Private Shared Sub GetChannelInfo(ByVal device As J2534Device, ByRef channel As J2534Channel)
        'Most of this is unused, but it's here to be expanded and made use of
        Dim protocolInfo As SPARAM
        protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_RX_BUFFER_SIZE)
        If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.MaxRXBufferSize = protocolInfo.Value

        protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_PASS_FILTER)
        If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.MaxPassFilter = protocolInfo.Value

        protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_BLOCK_FILTER)
        If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.MaxBlockFilter = protocolInfo.Value

        protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_FLOW_CONTROL_FILTER)
        If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.MaxFlowFilter = protocolInfo.Value

        protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_PERIODIC_MSGS)
        If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.MaxPeriodicMessage = protocolInfo.Value

        protocolInfo = GetProtocolInfo(device, channel.ProtocolId, FIVE_BAUD_MOD_SUPPORTED)
        If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then channel.FiveBaudMod = protocolInfo.Value

        'Different boxes report suppordated data rates differently. I didn't find the below code
        'useful but chose to leave it as a comment.
        'For i As Integer = 0 To 1000000
        '    protocolInfo = GetProtocolInfo(device, channel.ProtocolId, DESIRED_DATA_RATE, i.ToUint32)
        '    If protocolInfo.Supported = J2534DiscoveryResult.SUPPORTED Then
        '        'Do something with the info
        '    End If
        'Next i
    End Sub

    Private Shared Function GetProgVoltagePinInfo(ByVal device As J2534Device) As Integer
        If device Is Nothing OrElse device.API.PassThruIoctl Is Nothing Then Return -1
        Dim paramStruct(1) As SPARAM
        Dim paramList As SPARAM_LIST
        Dim pinTest As PinMap
        device.Analog.Pin(0).CanBeSet = True
        For pinNo As Integer = 1 To 15
            paramList.NumOfParams = 2
            paramStruct(0).Parameter = SHORT_TO_GND_J1962
            pinTest.LLLL = 1US << pinNo - 1
            pinTest.HHHH = 0
            paramStruct(0).Value = pinTest.Value
            paramStruct(1).Parameter = PGM_VOLTAGE_J1962
            pinTest.HHHH = pinTest.LLLL
            pinTest.LLLL = 0
            paramStruct(1).Value = pinTest.Value
            Dim pinsInfoSize As Integer = Marshal.SizeOf(GetType(SPARAM))
            Dim pinsInfoSizePointer As IntPtr = Marshal.AllocHGlobal(pinsInfoSize * paramStruct.Length)
            For i As Integer = 0 To paramStruct.Length - 1
                Marshal.StructureToPtr(paramStruct(i), pinsInfoSizePointer + i * pinsInfoSize, True)
            Next i
            paramList.ParamPtr = pinsInfoSizePointer.ToInt32
            Dim paramListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(paramList))
            Marshal.StructureToPtr(paramList, paramListPointer, True)
            Dim result As Integer = device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32)
            For i As Integer = 0 To paramStruct.Length - 1
                paramStruct(i) = DirectCast(Marshal.PtrToStructure(pinsInfoSizePointer + i * pinsInfoSize, GetType(SPARAM)), SPARAM)
            Next i
            If result = J2534Result.STATUS_NOERROR Then
                device.Analog.Pin(pinNo).CanBeShorted = paramStruct(0).Supported = J2534DiscoveryResult.SUPPORTED
                device.Analog.Pin(pinNo).CanBeSet = paramStruct(1).Supported = J2534DiscoveryResult.SUPPORTED
            End If
            Marshal.FreeHGlobal(paramListPointer)
            Marshal.FreeHGlobal(pinsInfoSizePointer)
        Next pinNo
        Return 0
    End Function

    Private Shared Function GetDeviceInfo(ByVal device As J2534Device, ByVal parameter As UInteger) As SPARAM
        Dim result As SPARAM
        result.Parameter = parameter
        Dim paramList As SPARAM_LIST
        If device Is Nothing OrElse device.API.PassThruIoctl Is Nothing Then Return result
        paramList.NumOfParams = 1
        Dim resultPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(result))
        Marshal.StructureToPtr(result, resultPointer, True)
        paramList.ParamPtr = resultPointer.ToInt32
        Dim paramListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(paramList))
        Marshal.StructureToPtr(paramList, paramListPointer, True)
        device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32)
        result = DirectCast(Marshal.PtrToStructure(resultPointer, GetType(SPARAM)), SPARAM)
        Marshal.FreeHGlobal(paramListPointer)
        Marshal.FreeHGlobal(resultPointer)
        Return result
    End Function

    Private Shared Function GetProtocolInfo(ByVal device As J2534Device, ByVal protocolId As UInteger, ByVal parameter As J2534GetProtocolInfoParameter) As SPARAM
        Return GetProtocolInfo(device, protocolId, parameter, 0)
    End Function
    Private Shared Function GetProtocolInfo(ByVal device As J2534Device, ByVal protocolId As UInteger, ByVal parameter As J2534GetProtocolInfoParameter, ByVal input As UInteger) As SPARAM
        Dim result As SPARAM
        result.Parameter = parameter
        result.Value = input
        Dim paramList As SPARAM_LIST
        If device Is Nothing OrElse device.API.PassThruIoctl Is Nothing Then Return result
        paramList.NumOfParams = 1
        Dim resultPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(result))
        Marshal.StructureToPtr(result, resultPointer, True)
        paramList.ParamPtr = resultPointer.ToInt32
        Dim paramListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(paramList))
        Marshal.StructureToPtr(paramList, paramListPointer, True)
        Dim gh As GCHandle = GCHandle.Alloc(protocolId, GCHandleType.Pinned)
        Dim ProtocolIdPointer As IntPtr = gh.AddrOfPinnedObject()
        device.API.PassThruIoctl(device.DeviceId, GET_PROTOCOL_INFO, ProtocolIdPointer.ToInt32, paramListPointer.ToInt32)
        result = DirectCast(Marshal.PtrToStructure(resultPointer, GetType(SPARAM)), SPARAM)
        Marshal.FreeHGlobal(paramListPointer)
        Marshal.FreeHGlobal(resultPointer)
        gh.Free()
        Return result
    End Function

    Private Shared Function GetPinInfo(ByVal device As J2534Device, ByVal parameter As UInteger) As UInteger()
        Dim paramStruct As SPARAM
        paramStruct.Parameter = parameter
        Dim paramList As SPARAM_LIST
        Dim pins As UInteger() = {}
        Dim pinTest As PinMap
        Dim pin As PinMap
        If device Is Nothing OrElse device.API.PassThruIoctl Is Nothing Then Return pins
        For pin.HiPin = 1 To 15
            For pin.LoPin = 0 To 15 'inner loop from zero for single wire
                pinTest.HHHH = (1UI << pin.HiPin - 1).ToUint16
                pinTest.LLLL = (1UI << pin.LoPin - 1).ToUint16
                paramStruct.Value = pinTest.Value
                paramList.NumOfParams = 1
                Dim paramStructPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(paramStruct))
                Marshal.StructureToPtr(paramStruct, paramStructPointer, True)
                paramList.ParamPtr = paramStructPointer.ToInt32
                Dim paramListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(paramList))
                Marshal.StructureToPtr(paramList, paramListPointer, True)
                Dim result As Integer = device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32)
                paramStruct = DirectCast(Marshal.PtrToStructure(paramStructPointer, GetType(SPARAM)), SPARAM)
                If result = J2534Result.STATUS_NOERROR Then
                    If paramStruct.Supported = J2534DiscoveryResult.SUPPORTED Then
                        Dim n As Integer = pins.Length
                        ReDim Preserve pins(n)
                        pins(n) = pin.Value
                    End If
                End If
                Marshal.FreeHGlobal(paramListPointer)
                Marshal.FreeHGlobal(paramStructPointer)
            Next pin.LoPin
        Next pin.HiPin
        Return pins
    End Function
    Private Shared Function GetPinInfo(ByVal device As J2534Device, ByVal parameterK As UInteger, ByVal parameterL As UInteger) As UInteger()
        Dim paramStruct(1) As SPARAM
        paramStruct(0).Parameter = parameterK
        paramStruct(0).Value = 0
        paramStruct(1).Parameter = parameterL
        paramStruct(1).Value = 0
        Dim paramList As SPARAM_LIST
        Dim pins As UInteger() = {}
        Dim pinTest As PinMap
        Dim pin As PinMap
        Dim pinK, pinL As UInteger
        If device Is Nothing OrElse device.API.PassThruIoctl Is Nothing Then Return pins
        For pin.HiPin = 1 To 15
            For pin.LoPin = 0 To 15 'inner loop from zero for single wire
                pinTest.HHHH = (1UI << pin.HiPin - 1).ToUint16
                pinTest.LLLL = (1UI << pin.LoPin - 1).ToUint16
                paramStruct(0).Value = pinTest.Value
                paramStruct(1).Value = pinTest.Value
                paramList.NumOfParams = 2
                Dim pinsInfoSize As Integer = Marshal.SizeOf(GetType(SPARAM))
                Dim pinsInfoSizePointer As IntPtr = Marshal.AllocHGlobal(pinsInfoSize * paramStruct.Length)
                For i As Integer = 0 To paramStruct.Length - 1
                    Marshal.StructureToPtr(paramStruct(i), pinsInfoSizePointer + i * pinsInfoSize, True)
                Next i
                paramList.ParamPtr = pinsInfoSizePointer.ToInt32
                Dim paramListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(paramList))
                Marshal.StructureToPtr(paramList, paramListPointer, True)
                Dim result As Integer = device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32)
                For i As Integer = 0 To paramStruct.Length - 1
                    paramStruct(i) = DirectCast(Marshal.PtrToStructure(pinsInfoSizePointer + i * pinsInfoSize, GetType(SPARAM)), SPARAM)
                Next i
                If result = J2534Result.STATUS_NOERROR Then
                    If paramStruct(0).Supported = J2534DiscoveryResult.SUPPORTED OrElse
                       paramStruct(1).Supported = J2534DiscoveryResult.SUPPORTED Then
                        If paramStruct(0).Supported = J2534DiscoveryResult.SUPPORTED Then pinK = pin.Value And &HFF00UI
                        If paramStruct(1).Supported = J2534DiscoveryResult.SUPPORTED Then pinL = pin.Value And &HFFUI
                        Dim n As Integer = pins.Length
                        ReDim Preserve pins(n)
                        pins(n) = pinK Or pinL
                    End If
                End If
                Marshal.FreeHGlobal(paramListPointer)
                Marshal.FreeHGlobal(pinsInfoSizePointer)
            Next pin.LoPin
        Next pin.HiPin
        Return pins
    End Function

    <DebuggerStepThrough>
    Public Overrides Function ToString() As String
        Return _Name
    End Function

    Public Sub New()
        BuildListViewObjects()
    End Sub

    Private disposedValue As Boolean
    Protected Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ListViewMessageIn.Dispose()
                ListViewMessageOut.Dispose()
                TimerMessageReceive.Dispose()
                'Don't dispose of the API here
            End If
        End If
        disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
    End Sub

End Class
