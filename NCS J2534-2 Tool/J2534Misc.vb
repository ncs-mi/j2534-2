' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901
'
Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.Runtime.InteropServices
Imports System.Collections.Concurrent
Imports NCS_J2534_2_Tool.J2534ConfigParameterId

Public Module J2534Constants 'todo rename
    Public Const MAX_FILTER As Integer = 9
    Public Const MAX_PM As Integer = 9
    Public Const MAX_FUNC_MSG As Integer = 31
    Public Const MAX_ANALOG_CH As Integer = 31

    Public Const MIN_CH As UInteger = 1
    Public Const MAX_CH As UInteger = 128
    Public Const MAX_IN As UInteger = 32
End Module

Public Module J2534Collections 'todo rename
    Public FilterEditBuffer As J2534Filter()
    Public PeriodicMessageEditBuffer As J2534PeriodicMessage()
    Public J2534_APIs As J2534_API() = {}
    Public J2534Devices As New ConcurrentDictionary(Of Integer, J2534Device)
End Module

Public Class ChannelSetup
    Public Shared ReadOnly J1850VPW As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.J1850VPW_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.J1850VPW_PS_J1962,
        .BaseId = J2534ProtocolId.J1850VPW,
        .PinSwitchedId = J2534ProtocolId.J1850VPW_PS,
        .FirstCHxId = J2534ProtocolId.J1850VPW_CH1
    }

    Public Shared ReadOnly J1850PWM As New ChannelSetupParametersK With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.J1850PWM_SUPPORTED,
        .J1962_K_PinsParameter = J2534GetDeviceInfoParameter.J1850PWM_PS_J1962,
        .BaseId = J2534ProtocolId.J1850PWM,
        .PinSwitchedId = J2534ProtocolId.J1850PWM_PS,
        .FirstCHxId = J2534ProtocolId.J1850PWM_CH1
    }

    Public Shared ReadOnly ISO9141 As New ChannelSetupParametersK With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.ISO9141_SUPPORTED,
        .J1962_K_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_K_LINE_J1962,
        .J1962_L_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_L_LINE_J1962,
        .J1939_K_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_K_LINE_J1939,
        .J1939_L_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_L_LINE_J1939,
        .BaseId = J2534ProtocolId.ISO9141,
        .PinSwitchedId = J2534ProtocolId.ISO9141_PS,
        .FirstCHxId = J2534ProtocolId.ISO9141_CH1
    }

    Public Shared ReadOnly ISO14230 As New ChannelSetupParametersK With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.ISO14230_SUPPORTED,
        .J1962_K_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_K_LINE_J1962,
        .J1962_L_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_L_LINE_J1962,
        .J1939_K_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_K_LINE_J1939,
        .J1939_L_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_L_LINE_J1939,
        .BaseId = J2534ProtocolId.ISO14230,
        .PinSwitchedId = J2534ProtocolId.ISO14230_PS,
        .FirstCHxId = J2534ProtocolId.ISO14230_CH1
    }

    Public Shared ReadOnly CAN As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.CAN_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.CAN_PS_J1962,
        .BaseId = J2534ProtocolId.CAN,
        .PinSwitchedId = J2534ProtocolId.CAN_PS,
        .FirstCHxId = J2534ProtocolId.CAN_CH1
    }

    Public Shared ReadOnly SW_CAN As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.SW_CAN_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.SW_CAN_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.SW_CAN_PS,
        .FirstCHxId = J2534ProtocolId.SW_CAN_CAN_CH1
    }

    Public Shared ReadOnly FT_CAN As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.FT_CAN_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.FT_CAN_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.FT_CAN_PS,
        .FirstCHxId = J2534ProtocolId.FT_CAN_CH1
    }

    Public Shared ReadOnly ISO15765 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.ISO15765_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.ISO15765_PS_J1962,
        .BaseId = J2534ProtocolId.ISO15765,
        .PinSwitchedId = J2534ProtocolId.ISO15765_PS,
        .FirstCHxId = J2534ProtocolId.ISO15765_CH1
    }

    Public Shared ReadOnly SW_ISO15765 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.SW_ISO15765_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.SW_ISO15765_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.SW_ISO15765_PS,
        .FirstCHxId = J2534ProtocolId.SW_CAN_ISO15765_CH1
    }

    Public Shared ReadOnly FT_ISO15765 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.FT_ISO15765_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.FT_ISO15765_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.FT_ISO15765_PS,
        .FirstCHxId = J2534ProtocolId.FT_ISO15765_CH1
    }

    Public Shared ReadOnly SCI_A_ENGINE As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.SCI_A_ENGINE_SUPPORTED,
        .BaseId = J2534ProtocolId.SCI_A_ENGINE
    }

    Public Shared ReadOnly SCI_A_TRANS As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.SCI_A_TRANS_SUPPORTED,
        .BaseId = J2534ProtocolId.SCI_A_TRANS
    }

    Public Shared ReadOnly SCI_B_ENGINE As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.SCI_B_ENGINE_SUPPORTED,
        .BaseId = J2534ProtocolId.SCI_B_ENGINE
    }

    Public Shared ReadOnly SCI_B_TRANS As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.SCI_B_TRANS_SUPPORTED,
        .BaseId = J2534ProtocolId.SCI_B_TRANS
    }

    Public Shared ReadOnly J2610 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.J2610_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.J2610_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.J2610_PS,
        .FirstCHxId = J2534ProtocolId.J2610_CH1
    }

    Public Shared ReadOnly GM_UART As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.GM_UART_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.GM_UART_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.GM_UART_PS,
        .FirstCHxId = J2534ProtocolId.GM_UART_CH1
    }

    Public Shared ReadOnly ECHO_BYTE As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.UART_ECHO_BYTE_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.UART_ECHO_BYTE_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.UART_ECHO_BYTE_PS,
        .FirstCHxId = J2534ProtocolId.ECHO_BYTE_CH1
    }

    Public Shared ReadOnly HONDA_DIAGH As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.HONDA_DIAGH_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.HONDA_DIAGH_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.HONDA_DIAGH_PS,
        .FirstCHxId = J2534ProtocolId.HONDA_DIAGH_CH1
    }

    Public Shared ReadOnly J1939 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.J1939_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.J1939_PS_J1962,
        .J1939PinsParameter = J2534GetDeviceInfoParameter.J1939_PS_J1939,
        .PinSwitchedId = J2534ProtocolId.J1939_PS,
        .FirstCHxId = J2534ProtocolId.J1939_CH1
    }

    Public Shared ReadOnly J1708 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.J1708_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.J1708_PS_J1962,
        .J1939PinsParameter = J2534GetDeviceInfoParameter.J1708_PS_J1939,
        .J1708PinsParameter = J2534GetDeviceInfoParameter.J1708_PS_J1708,
        .PinSwitchedId = J2534ProtocolId.J1708_PS,
        .FirstCHxId = J2534ProtocolId.J1708_CH1
    }

    Public Shared ReadOnly TP2_0 As New ChannelSetupParameters With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.TP2_0_SUPPORTED,
        .J1962PinsParameter = J2534GetDeviceInfoParameter.TP2_0_PS_J1962,
        .PinSwitchedId = J2534ProtocolId.TP2_0_PS,
        .FirstCHxId = J2534ProtocolId.TP2_0_CH1
    }

    Public Shared ReadOnly Analog As New ChannelSetupParametersAnalog With
    {
        .SupportParameter = J2534GetDeviceInfoParameter.ANALOG_IN_SUPPORTED,
        .FirstCHxId = J2534ProtocolId.ANALOG_IN_1
    }

    Private Sub New()
    End Sub
End Class

Public Class J2534BaudRates
    Public Shared Function J1850VPW() As UInteger()
        Dim result(1) As UInteger
        result(0) = 10400
        result(1) = 41600
        Return result
    End Function

    Public Shared Function J1850PWM() As UInteger()
        Dim result(1) As UInteger
        result(0) = 41600
        result(1) = 83300
        Return result
    End Function

    Public Shared Function ISO9141_14230() As UInteger()
        Dim result(16) As UInteger
        result(0) = 4800
        result(1) = 9600
        result(2) = 9615
        result(3) = 9800
        result(4) = 10000
        result(5) = 10400
        result(6) = 10870
        result(7) = 11905
        result(8) = 12500
        result(9) = 13158
        result(10) = 13889
        result(11) = 14706
        result(12) = 15625
        result(13) = 19200
        result(14) = 38400
        result(15) = 57600
        result(16) = 115200
        Return result
    End Function

    Public Shared Function CAN_ISO() As UInteger()
        Dim result(9) As UInteger
        result(0) = 31250
        result(1) = 50000 'DG Netbridge
        result(2) = 62500
        result(3) = 83333
        result(4) = 95200
        result(5) = 125000
        result(6) = 250000
        result(7) = 500000
        result(8) = 666667 'DG Netbridge
        result(9) = 1000000
        Return result
    End Function

    Public Shared Function SW_CAN_ISO() As UInteger()
        Dim result(1) As UInteger
        result(0) = 33333
        result(1) = 83333
        Return result
    End Function

    Public Shared Function FT_CAN_ISO() As UInteger()
        Dim result(2) As UInteger
        result(0) = 50000
        result(1) = 95238
        result(2) = 125000
        Return result
    End Function

    Public Shared Function J2610() As UInteger()
        Dim result(2) As UInteger
        result(0) = 7812 'J2534-1 7812.5
        result(1) = 62500 'J2534-1
        result(2) = 125000
        Return result
    End Function

    Public Shared Function GM_UART() As UInteger()
        Dim result(0) As UInteger
        result(0) = 8192
        Return result
    End Function

    Public Shared Function UART_ECHO_BYTE() As UInteger()
        Dim result(0) As UInteger
        result(0) = 9600
        Return result
    End Function

    Public Shared Function HONDA_DIAGH() As UInteger()
        Dim result(0) As UInteger
        result(0) = 9600
        Return result
    End Function

    Public Shared Function J1939() As UInteger()
        Dim result(3) As UInteger
        result(0) = 125000
        result(1) = 250000
        result(2) = 500000
        result(3) = 666667
        Return result
    End Function

    Public Shared Function J1708() As UInteger()
        Dim result(1) As UInteger
        result(0) = 9600
        result(1) = 19200
        Return result
    End Function

    Public Shared Function TP2_0() As UInteger()
        Dim result(5) As UInteger
        result(0) = 83333
        result(1) = 125000
        result(2) = 250000
        result(3) = 500000
        result(4) = 666667 'DG Netbridge
        result(5) = 1000000
        Return result
    End Function

    Public Shared Function LIN() As UInteger()
        Dim result(3) As UInteger 'DG
        result(0) = 4800
        result(1) = 9600
        result(2) = 19200
        result(3) = 20000
        Return result
    End Function

    Private Sub New()
    End Sub
End Class

Public Class J2534ConfigParameters
    Public Shared Function J1850VPW() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK)
        }
        Return result.ToArray
    End Function

    Public Shared Function J1850PWM() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(NODE_ADDRESS),
            New J2534ConfigParameter(NETWORK_LINE)
        }
        Return result.ToArray
    End Function

    Public Shared Function ISO9141() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(P1_MAX),
            New J2534ConfigParameter(P3_MIN),
            New J2534ConfigParameter(P4_MIN),
            New J2534ConfigParameter(W0),
            New J2534ConfigParameter(W1),
            New J2534ConfigParameter(W2),
            New J2534ConfigParameter(W3),
            New J2534ConfigParameter(W4),
            New J2534ConfigParameter(TIDLE),
            New J2534ConfigParameter(TINIL),
            New J2534ConfigParameter(TWUP),
            New J2534ConfigParameter(PARITY),
            New J2534ConfigParameter(DATA_BITS),
            New J2534ConfigParameter(FIVE_BAUD_MOD)
        }
        Return result.ToArray
    End Function

    Public Shared Function ISO14230() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(P1_MAX),
            New J2534ConfigParameter(P3_MIN),
            New J2534ConfigParameter(P4_MIN),
            New J2534ConfigParameter(W0),
            New J2534ConfigParameter(W1),
            New J2534ConfigParameter(W2),
            New J2534ConfigParameter(W3),
            New J2534ConfigParameter(W4),
            New J2534ConfigParameter(W5),
            New J2534ConfigParameter(TIDLE),
            New J2534ConfigParameter(TINIL),
            New J2534ConfigParameter(TWUP),
            New J2534ConfigParameter(PARITY),
            New J2534ConfigParameter(DATA_BITS),
            New J2534ConfigParameter(FIVE_BAUD_MOD)
        }
        Return result.ToArray
    End Function

    Public Shared Function CAN() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH)
        }
        Return result.ToArray
    End Function

    Public Shared Function ISO15765() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH),
            New J2534ConfigParameter(ISO15765_BS),
            New J2534ConfigParameter(ISO15765_STMIN),
            New J2534ConfigParameter(BS_TX),
            New J2534ConfigParameter(STMIN_TX),
            New J2534ConfigParameter(ISO15765_WFT_MAX),
            New J2534ConfigParameter(CAN_MIXED_FORMAT)
        }
        Return result.ToArray
    End Function

    Public Shared Function SW_CAN() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH),
            New J2534ConfigParameter(SW_CAN_HS_DATA_RATE),
            New J2534ConfigParameter(SW_CAN_SPEEDCHANGE_ENABLE),
            New J2534ConfigParameter(SW_CAN_RES_SWITCH)
        }
        Return result.ToArray
    End Function

    Public Shared Function FT_CAN() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH)
        }
        Return result.ToArray
    End Function

    Public Shared Function SW_ISO15765() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH),
            New J2534ConfigParameter(ISO15765_BS),
            New J2534ConfigParameter(ISO15765_STMIN),
            New J2534ConfigParameter(BS_TX),
            New J2534ConfigParameter(STMIN_TX),
            New J2534ConfigParameter(ISO15765_WFT_MAX),
            New J2534ConfigParameter(CAN_MIXED_FORMAT),
            New J2534ConfigParameter(SW_CAN_HS_DATA_RATE),
            New J2534ConfigParameter(SW_CAN_SPEEDCHANGE_ENABLE),
            New J2534ConfigParameter(SW_CAN_RES_SWITCH)
        }
        Return result.ToArray
    End Function

    Public Shared Function FT_ISO15765() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH),
            New J2534ConfigParameter(ISO15765_BS),
            New J2534ConfigParameter(ISO15765_STMIN),
            New J2534ConfigParameter(BS_TX),
            New J2534ConfigParameter(STMIN_TX),
            New J2534ConfigParameter(ISO15765_WFT_MAX),
            New J2534ConfigParameter(CAN_MIXED_FORMAT)
        }
        Return result.ToArray
    End Function

    Public Shared Function J2610() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(T1_MAX),
            New J2534ConfigParameter(T2_MAX),
            New J2534ConfigParameter(T3_MAX),
            New J2534ConfigParameter(T4_MAX),
            New J2534ConfigParameter(T5_MAX)
        }
        Return result.ToArray
    End Function

    Public Shared Function GM_UART() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK)
        }
        Return result.ToArray
    End Function

    Public Shared Function UART_ECHO_BYTE() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(UEB_T0_MIN),
            New J2534ConfigParameter(UEB_T1_MAX),
            New J2534ConfigParameter(UEB_T2_MAX),
            New J2534ConfigParameter(UEB_T3_MAX),
            New J2534ConfigParameter(UEB_T4_MIN),
            New J2534ConfigParameter(UEB_T5_MAX),
            New J2534ConfigParameter(UEB_T6_MAX),
            New J2534ConfigParameter(UEB_T7_MIN),
            New J2534ConfigParameter(UEB_T7_MAX),
            New J2534ConfigParameter(UEB_T9_MIN)
        }
        Return result.ToArray
    End Function

    Public Shared Function HONDA_DIAGH() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(P1_MAX),
            New J2534ConfigParameter(P3_MIN),
            New J2534ConfigParameter(P4_MIN)
        }
        Return result.ToArray
    End Function

    Public Shared Function J1939() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH),
            New J2534ConfigParameter(J1939_T1),
            New J2534ConfigParameter(J1939_T2),
            New J2534ConfigParameter(J1939_T3),
            New J2534ConfigParameter(J1939_T4),
            New J2534ConfigParameter(J1939_BRDCST_MIN_DELAY)
        }
        Return result.ToArray
    End Function

    Public Shared Function J1708() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK)
        }
        Return result.ToArray
    End Function

    Public Shared Function TP2_0() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(BIT_SAMPLE_POINT),
            New J2534ConfigParameter(SYNC_JUMP_WIDTH),
            New J2534ConfigParameter(TP2_0_T_BR_INT),
            New J2534ConfigParameter(TP2_0_T_E),
            New J2534ConfigParameter(TP2_0_MNTC),
            New J2534ConfigParameter(TP2_0_T_CTA),
            New J2534ConfigParameter(TP2_0_MNCT),
            New J2534ConfigParameter(TP2_0_MNTB),
            New J2534ConfigParameter(TP2_0_MNT),
            New J2534ConfigParameter(TP2_0_T_WAIT),
            New J2534ConfigParameter(TP2_0_T1),
            New J2534ConfigParameter(TP2_0_T3),
            New J2534ConfigParameter(TP2_0_IDENTIFER),
            New J2534ConfigParameter(TP2_0_RXIDPASSIVE)
        }
        Return result.ToArray
    End Function

    Public Shared Function Analog() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(ACTIVE_CHANNELS, False),
            New J2534ConfigParameter(SAMPLE_RATE, False),
            New J2534ConfigParameter(SAMPLES_PER_READING, False),
            New J2534ConfigParameter(READINGS_PER_MSG, False),
            New J2534ConfigParameter(AVERAGING_METHOD, False),
            New J2534ConfigParameter(SAMPLE_RESOLUTION, True),
            New J2534ConfigParameter(INPUT_RANGE_LOW, True),
            New J2534ConfigParameter(INPUT_RANGE_HIGH, True)
        }
        Return result.ToArray
    End Function

    Public Shared Function LIN() As J2534ConfigParameter()
        Dim result As New List(Of J2534ConfigParameter) From
        {
            New J2534ConfigParameter(DATA_RATE),
            New J2534ConfigParameter(LOOPBACK),
            New J2534ConfigParameter(LIN_VERSION)
        }
        Return result.ToArray
    End Function

End Class

Public Delegate Function PassThruOpenDelegate(ByVal pName As Int32, ByRef DeviceId As UInt32) As J2534Result
Public Delegate Function PassThruCloseDelegate(ByVal deviceId As UInt32) As J2534Result
Public Delegate Function PassThruConnectDelegate(ByVal deviceId As UInt32, ByVal protocolId As J2534ProtocolId, ByVal flags As J2534ConnectFlags, ByVal baudRate As UInt32, ByRef channelId As UInt32) As J2534Result
Public Delegate Function PassThruDisconnectDelegate(ByVal channelId As UInt32) As J2534Result
Public Delegate Function PassThruReadMsgsDelegate(ByVal channelId As UInt32, ByVal pMsg As Int32, ByRef pNumMsgs As Int32, ByVal timeout As UInt32) As J2534Result
Public Delegate Function PassThruWriteMsgsDelegate(ByVal channelId As UInt32, ByVal pMsg As Int32, ByRef pNumMsgs As Int32, ByVal timeout As UInt32) As J2534Result
Public Delegate Function PassThruStartPeriodicMsgDelegate(ByVal channelId As UInt32, ByVal pMsg As Int32, ByRef MsgId As UInt32, ByVal timeInterval As UInt32) As J2534Result
Public Delegate Function PassThruStopPeriodicMsgDelegate(ByVal channelId As UInt32, ByVal MsgId As UInt32) As J2534Result
Public Delegate Function PassThruStartMsgFilterDelegate(ByVal channelId As UInt32, ByVal filterType As J2534FilterType, ByVal pMaskMsg As Int32, ByVal pPattMsg As Int32, ByVal pFlowControlMsg As Int32, ByRef MsgId As UInt32) As J2534Result
Public Delegate Function PassThruStopMsgFilterDelegate(ByVal channelId As UInt32, ByVal MsgId As UInt32) As J2534Result
Public Delegate Function PassThruSetProgrammingVoltageDelegate(ByVal deviceId As UInt32, ByVal Pin As UInt32, ByVal Voltage As UInt32) As J2534Result
Public Delegate Function PassThruReadVersionDelegate(ByVal deviceId As UInt32, ByVal pFWVer As Byte(), ByVal pDLLVer As Byte(), ByVal pAPIVer As Byte()) As J2534Result
Public Delegate Function PassThruGetLastErrorDelegate(ByVal rrrStr As Byte()) As J2534Result
Public Delegate Function PassThruIoctlDelegate(ByVal channelId As UInt32, ByVal ioctlId As UInt32, ByVal pInput As Int32, ByVal pOutput As Int32) As J2534Result

<StructLayout(LayoutKind.Explicit, Pack:=1)>
Public Structure SPARAM 'Basically a J2534-2 SPARAM with explicit values for GET_DEVICE_INFO outputs
    <FieldOffset(0)> Dim Parameter As UInteger
    <FieldOffset(4)> Dim SS As Byte
    <FieldOffset(5)> Dim RR As Byte
    <FieldOffset(6)> Dim QQ As Byte
    <FieldOffset(7)> Dim PP As Byte
    <FieldOffset(4)> Dim Value As UInteger
    <FieldOffset(8)> Dim Supported As UInteger
End Structure

<StructLayout(LayoutKind.Explicit, Pack:=1)>
Public Structure PinMap
    <FieldOffset(0)> Dim Value As UInteger
    <FieldOffset(0)> Dim LLLL As UShort
    <FieldOffset(2)> Dim HHHH As UShort
    <FieldOffset(0)> Dim LoPin As Byte
    <FieldOffset(1)> Dim HiPin As Byte
End Structure

' This enum can be expanded to support features particular to a given box.
Public Enum DeviceType
    Unknown = 0
    Other = 1
    Netbridge = 2 'DG
End Enum

<Flags>
Public Enum MessageCompareResults
    None = 0
    ProtocolMismatch = 1
    RxStatusMismatch = 2
    TxFlagsMismatch = 4
    TimestampMismatch = 8
    DataSizeMismatch = 16
    ExtraDataMismatch = 32
    DataMismatch = 64
End Enum

<Flags>
Public Enum ConfigChanges
    None = 0
    ValueMismatch = 1
    TextExists = 2
End Enum

Public Class J2534MessageText
    Public Property ProtocolName As String
    Public Property RxStatus As String
    Public Property TxFlags As String
    Public Property Timestamp As String
    Public Property Data As String
    Public Property ExtraData As String
    Public Property DataASCII As String
    Public Property Comment As String
    Public Property Tx As Boolean
    Public Function Clone() As J2534MessageText
        Dim result As J2534MessageText = DirectCast(Me.MemberwiseClone(), J2534MessageText)
        Return result
    End Function
End Class

Public Class J2534FilterText
    Public Property MaskMessageText As New J2534MessageText
    Public Property PatternMessageText As New J2534MessageText
    Public Property FlowMessageText As New J2534MessageText
    Public Property MessageId As UInteger
    Public Property FilterType As String = String.Empty
    Public Property Enabled As Boolean
End Class

Public Class J2534Filter
    Public MaskMessage As PASSTHRU_MSG = PASSTHRU_MSG.CreateInstance()
    Public PatternMessage As PASSTHRU_MSG = PASSTHRU_MSG.CreateInstance()
    Public FlowMessage As PASSTHRU_MSG = PASSTHRU_MSG.CreateInstance()
    Public Property MessageId As UInteger
    Public Property FilterType As J2534FilterType
    Public Property Enabled As Boolean

    Public Function IsEmpty() As Boolean
        Dim result As Boolean = False
        If Me.MaskMessage.DataSize = 0 AndAlso
           Me.PatternMessage.DataSize = 0 AndAlso
           Me.FlowMessage.DataSize = 0 AndAlso
           Me.FilterType = 0 AndAlso
           Not _Enabled Then 'filter is empty
            result = True
        End If
        Return result
    End Function

    Public Function Clone() As J2534Filter
        Dim result As J2534Filter = DirectCast(Me.MemberwiseClone(), J2534Filter)
        result.MaskMessage = Me.MaskMessage.Clone
        result.PatternMessage = Me.PatternMessage.Clone
        result.FlowMessage = Me.FlowMessage.Clone
        Return result
    End Function
End Class

Public Class J2534PeriodicMessageText
    Public Property MessageText As New J2534MessageText
    Public Property Interval As UInteger
    Public Property MessageId As UInteger
    Public Property Enabled As Boolean
End Class

Public Class J2534PeriodicMessage
    Public Message As PASSTHRU_MSG = PASSTHRU_MSG.CreateInstance()
    Public Property Interval As UInteger
    Public Property MessageId As UInteger
    Public Property Enabled As Boolean

    Public Function IsEmpty() As Boolean
        Dim result As Boolean = False
        If Me.Message.DataSize = 0 AndAlso
           Not Me.Enabled Then
            result = True
        End If
        Return result
    End Function

    Public Function Clone() As J2534PeriodicMessage
        Dim result As J2534PeriodicMessage = DirectCast(Me.MemberwiseClone(), J2534PeriodicMessage)
        result.Message = Me.Message.Clone
        Return result
    End Function
End Class

Public Class J2534ConfigParameter
    Public Property Id As J2534ConfigParameterId
    Public Property Value As UInteger
    Public Property NewText As String
    Public Property IsReadOnly As Boolean

    Public Sub New(ByVal id As J2534ConfigParameterId)
        Me.New(id, False)
    End Sub
    Public Sub New(ByVal id As J2534ConfigParameterId, ByVal [readOnly] As Boolean)
        _Id = id
        _IsReadOnly = [readOnly]
    End Sub
End Class

Public Class J2534FunctionalMessage
    Public Property Value As Byte
    Public Property NewText As String = String.Empty
End Class

Public Class J2534Setup
    Public Property SelectedChannelName As String
    Public Property MessageChannelName As String
    Public Property ConnectChannelName As String
    Public Property TextBoxOutFlags As String
    Public Property TextBoxMessageOut As String
    Public Property GetMessages As Boolean
    Public Property MessageReadRate As Integer
End Class

Public Class J2534ConnectOptions
    Public Property Channel As J2534Channel
    Public Property BaudRate As UInteger
    Public Property Connector As UInteger
    Public Property Pins As UInteger
    Public Property ConnectFlags As J2534ConnectFlags
    Public Property MixedMode As Boolean
End Class

Public Class PinInfo
    Public Property Selected As Boolean
    Public Property CanBeShorted As Boolean
    Public Property CanBeSet As Boolean
    Public Property Mode As PinMode
End Class

Public Enum PinMode
    Open = 0
    Volt = 1
    Shorted = 2
End Enum

Public Class J2534AnalogSubsystem
    Public Property AnalogIn As Integer()
    Public Property RangeLow As Integer
    Public Property RangeHigh As Integer
    Public Property ActiveChannels As UInteger
    Public Property ReadingsPerMessage As UInteger
    Public Property MaxChannel As UInteger
    Public Property MaxSampleRate As UInteger
    Public Sub New()
        ReDim _AnalogIn(31)
    End Sub
End Class

Public Class J2534Analog
    Public Property Pin As PinInfo()
    Public Property PinReading As UInteger
    Public Property BatteryReading As UInteger
    Public Property PinSetting As UInteger
    Public Sub New()
        ReDim _Pin(15)
    End Sub
End Class

Public Class J1939AddressClaimSetup
    Public Property Address As Byte
    Public Property Identity As UInteger
    Public Property MfgCode As UInteger
    Public Property ECUInstance As Byte
    Public Property FunctionInstance As Byte
    Public Property FunctionId As Byte
    Public Property VehicleSystemId As Byte
    Public Property VehicleSystemInstance As Byte
    Public Property IndustryGroup As Byte
    Public Property ArbitraryCapable As Boolean


    'masks for J1939 address claim
    Public Const J1939_NAME_MASK_IDENTITY As UInteger = &H00_1F_FF_FF
    Public Const J1939_NAME_MASK_MFG_CODE As UInteger = &HFF_E0_00_00UI 'unused
    Public Const J1939_NAME_MASK_ECU_INST As Byte = &B0000_0111
    'Public Const J1939_NAME_MASK_FUNC_INST As Byte = &B1111_1000
    'Public Const J1939_NAME_MASK_VEH_SYS As Byte = &B1111_1110
    Public Const J1939_NAME_MASK_VEH_SYS_INST As Byte = &B0000_1111
    Public Const J1939_NAME_MASK_IND_GRP As Byte = &B0111_0000
    Public Const J1939_NAME_MASK_ARB As Byte = &B1000_0000
End Class

Public Class ChannelSetupParameters
    Public Property SupportParameter As J2534GetDeviceInfoParameter = 0
    Public Property J1962PinsParameter As J2534GetDeviceInfoParameter?
    Public Property J1939PinsParameter As J2534GetDeviceInfoParameter?
    Public Property J1708PinsParameter As J2534GetDeviceInfoParameter?
    Public Property BaseId As J2534ProtocolId?
    Public Property PinSwitchedId As J2534ProtocolId?
    Public Property FirstCHxId As J2534ProtocolId?
End Class

Public Class ChannelSetupParametersK
    Public Property SupportParameter As J2534GetDeviceInfoParameter = 0
    Public Property J1962_K_PinsParameter As J2534GetDeviceInfoParameter?
    Public Property J1962_L_PinsParameter As J2534GetDeviceInfoParameter?
    Public Property J1939_K_PinsParameter As J2534GetDeviceInfoParameter?
    Public Property J1939_L_PinsParameter As J2534GetDeviceInfoParameter?
    Public Property BaseId As J2534ProtocolId?
    Public Property PinSwitchedId As J2534ProtocolId?
    Public Property FirstCHxId As J2534ProtocolId?
End Class

Public Class ChannelSetupParametersAnalog
    Public Property SupportParameter As J2534GetDeviceInfoParameter = 0
    Public Property FirstCHxId As J2534ProtocolId?
End Class