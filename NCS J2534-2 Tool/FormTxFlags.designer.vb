' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormTxFlags
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormTxFlags))
        Me.TextBoxFlags = New System.Windows.Forms.TextBox()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.CheckBoxFlag0 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag1 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag2 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag3 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag4 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag5 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag6 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag7 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag8 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag9 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag10 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag11 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag12 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag13 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag14 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag15 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag16 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag17 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag18 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag19 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag20 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag21 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag22 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag23 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag24 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag25 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag26 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag27 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag28 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag29 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag30 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFlag31 = New System.Windows.Forms.CheckBox()
        Me.SetButton = New System.Windows.Forms.Button()
        Me.LabelDesc = New System.Windows.Forms.Label()
        Me.ContextMenuStripFlags = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MenuFlagsUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagDefault = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripFlags.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBoxFlags
        '
        Me.TextBoxFlags.AcceptsReturn = True
        Me.TextBoxFlags.Location = New System.Drawing.Point(312, 288)
        Me.TextBoxFlags.MaxLength = 0
        Me.TextBoxFlags.Name = "TextBoxFlags"
        Me.TextBoxFlags.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFlags.TabIndex = 35
        '
        'Cancel_Button
        '
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(367, 312)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(65, 25)
        Me.Cancel_Button.TabIndex = 0
        Me.Cancel_Button.Text = "Cancel"
        Me.Cancel_Button.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag0
        '
        Me.CheckBoxFlag0.AutoSize = True
        Me.CheckBoxFlag0.Location = New System.Drawing.Point(8, 24)
        Me.CheckBoxFlag0.Name = "CheckBoxFlag0"
        Me.CheckBoxFlag0.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag0.TabIndex = 2
        Me.CheckBoxFlag0.Text = "CheckBoxFlag(0)"
        Me.CheckBoxFlag0.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag1
        '
        Me.CheckBoxFlag1.AutoSize = True
        Me.CheckBoxFlag1.Location = New System.Drawing.Point(8, 40)
        Me.CheckBoxFlag1.Name = "CheckBoxFlag1"
        Me.CheckBoxFlag1.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag1.TabIndex = 3
        Me.CheckBoxFlag1.Text = "CheckBoxFlag(1)"
        Me.CheckBoxFlag1.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag2
        '
        Me.CheckBoxFlag2.AutoSize = True
        Me.CheckBoxFlag2.Location = New System.Drawing.Point(8, 56)
        Me.CheckBoxFlag2.Name = "CheckBoxFlag2"
        Me.CheckBoxFlag2.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag2.TabIndex = 4
        Me.CheckBoxFlag2.Text = "CheckBoxFlag(2)"
        Me.CheckBoxFlag2.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag3
        '
        Me.CheckBoxFlag3.AutoSize = True
        Me.CheckBoxFlag3.Location = New System.Drawing.Point(8, 72)
        Me.CheckBoxFlag3.Name = "CheckBoxFlag3"
        Me.CheckBoxFlag3.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag3.TabIndex = 5
        Me.CheckBoxFlag3.Text = "CheckBoxFlag(3)"
        Me.CheckBoxFlag3.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag4
        '
        Me.CheckBoxFlag4.AutoSize = True
        Me.CheckBoxFlag4.Location = New System.Drawing.Point(8, 88)
        Me.CheckBoxFlag4.Name = "CheckBoxFlag4"
        Me.CheckBoxFlag4.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag4.TabIndex = 6
        Me.CheckBoxFlag4.Text = "CheckBoxFlag(4)"
        Me.CheckBoxFlag4.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag5
        '
        Me.CheckBoxFlag5.AutoSize = True
        Me.CheckBoxFlag5.Location = New System.Drawing.Point(8, 104)
        Me.CheckBoxFlag5.Name = "CheckBoxFlag5"
        Me.CheckBoxFlag5.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag5.TabIndex = 7
        Me.CheckBoxFlag5.Text = "CheckBoxFlag(5)"
        Me.CheckBoxFlag5.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag6
        '
        Me.CheckBoxFlag6.AutoSize = True
        Me.CheckBoxFlag6.Location = New System.Drawing.Point(8, 120)
        Me.CheckBoxFlag6.Name = "CheckBoxFlag6"
        Me.CheckBoxFlag6.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag6.TabIndex = 8
        Me.CheckBoxFlag6.Text = "CheckBoxFlag(6)"
        Me.CheckBoxFlag6.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag7
        '
        Me.CheckBoxFlag7.AutoSize = True
        Me.CheckBoxFlag7.Location = New System.Drawing.Point(8, 136)
        Me.CheckBoxFlag7.Name = "CheckBoxFlag7"
        Me.CheckBoxFlag7.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag7.TabIndex = 9
        Me.CheckBoxFlag7.Text = "CheckBoxFlag(7)"
        Me.CheckBoxFlag7.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag8
        '
        Me.CheckBoxFlag8.AutoSize = True
        Me.CheckBoxFlag8.Location = New System.Drawing.Point(8, 152)
        Me.CheckBoxFlag8.Name = "CheckBoxFlag8"
        Me.CheckBoxFlag8.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag8.TabIndex = 10
        Me.CheckBoxFlag8.Text = "CheckBoxFlag(8)"
        Me.CheckBoxFlag8.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag9
        '
        Me.CheckBoxFlag9.AutoSize = True
        Me.CheckBoxFlag9.Location = New System.Drawing.Point(8, 168)
        Me.CheckBoxFlag9.Name = "CheckBoxFlag9"
        Me.CheckBoxFlag9.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxFlag9.TabIndex = 11
        Me.CheckBoxFlag9.Text = "CheckBoxFlag(9)"
        Me.CheckBoxFlag9.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag10
        '
        Me.CheckBoxFlag10.AutoSize = True
        Me.CheckBoxFlag10.Location = New System.Drawing.Point(8, 184)
        Me.CheckBoxFlag10.Name = "CheckBoxFlag10"
        Me.CheckBoxFlag10.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag10.TabIndex = 12
        Me.CheckBoxFlag10.Text = "CheckBoxFlag(10)"
        Me.CheckBoxFlag10.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag11
        '
        Me.CheckBoxFlag11.AutoSize = True
        Me.CheckBoxFlag11.Location = New System.Drawing.Point(8, 200)
        Me.CheckBoxFlag11.Name = "CheckBoxFlag11"
        Me.CheckBoxFlag11.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag11.TabIndex = 13
        Me.CheckBoxFlag11.Text = "CheckBoxFlag(11)"
        Me.CheckBoxFlag11.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag12
        '
        Me.CheckBoxFlag12.AutoSize = True
        Me.CheckBoxFlag12.Location = New System.Drawing.Point(8, 216)
        Me.CheckBoxFlag12.Name = "CheckBoxFlag12"
        Me.CheckBoxFlag12.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag12.TabIndex = 14
        Me.CheckBoxFlag12.Text = "CheckBoxFlag(12)"
        Me.CheckBoxFlag12.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag13
        '
        Me.CheckBoxFlag13.AutoSize = True
        Me.CheckBoxFlag13.Location = New System.Drawing.Point(8, 232)
        Me.CheckBoxFlag13.Name = "CheckBoxFlag13"
        Me.CheckBoxFlag13.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag13.TabIndex = 15
        Me.CheckBoxFlag13.Text = "CheckBoxFlag(13)"
        Me.CheckBoxFlag13.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag14
        '
        Me.CheckBoxFlag14.AutoSize = True
        Me.CheckBoxFlag14.Location = New System.Drawing.Point(8, 248)
        Me.CheckBoxFlag14.Name = "CheckBoxFlag14"
        Me.CheckBoxFlag14.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag14.TabIndex = 16
        Me.CheckBoxFlag14.Text = "CheckBoxFlag(14)"
        Me.CheckBoxFlag14.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag15
        '
        Me.CheckBoxFlag15.AutoSize = True
        Me.CheckBoxFlag15.Location = New System.Drawing.Point(8, 264)
        Me.CheckBoxFlag15.Name = "CheckBoxFlag15"
        Me.CheckBoxFlag15.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag15.TabIndex = 17
        Me.CheckBoxFlag15.Text = "CheckBoxFlag(15)"
        Me.CheckBoxFlag15.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag16
        '
        Me.CheckBoxFlag16.AutoSize = True
        Me.CheckBoxFlag16.Location = New System.Drawing.Point(228, 24)
        Me.CheckBoxFlag16.Name = "CheckBoxFlag16"
        Me.CheckBoxFlag16.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag16.TabIndex = 18
        Me.CheckBoxFlag16.Text = "CheckBoxFlag(16)"
        Me.CheckBoxFlag16.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag17
        '
        Me.CheckBoxFlag17.AutoSize = True
        Me.CheckBoxFlag17.Location = New System.Drawing.Point(228, 40)
        Me.CheckBoxFlag17.Name = "CheckBoxFlag17"
        Me.CheckBoxFlag17.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag17.TabIndex = 19
        Me.CheckBoxFlag17.Text = "CheckBoxFlag(17)"
        Me.CheckBoxFlag17.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag18
        '
        Me.CheckBoxFlag18.AutoSize = True
        Me.CheckBoxFlag18.Location = New System.Drawing.Point(228, 56)
        Me.CheckBoxFlag18.Name = "CheckBoxFlag18"
        Me.CheckBoxFlag18.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag18.TabIndex = 20
        Me.CheckBoxFlag18.Text = "CheckBoxFlag(18)"
        Me.CheckBoxFlag18.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag19
        '
        Me.CheckBoxFlag19.AutoSize = True
        Me.CheckBoxFlag19.Location = New System.Drawing.Point(228, 72)
        Me.CheckBoxFlag19.Name = "CheckBoxFlag19"
        Me.CheckBoxFlag19.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag19.TabIndex = 21
        Me.CheckBoxFlag19.Text = "CheckBoxFlag(19)"
        Me.CheckBoxFlag19.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag20
        '
        Me.CheckBoxFlag20.AutoSize = True
        Me.CheckBoxFlag20.Location = New System.Drawing.Point(228, 88)
        Me.CheckBoxFlag20.Name = "CheckBoxFlag20"
        Me.CheckBoxFlag20.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag20.TabIndex = 22
        Me.CheckBoxFlag20.Text = "CheckBoxFlag(20)"
        Me.CheckBoxFlag20.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag21
        '
        Me.CheckBoxFlag21.AutoSize = True
        Me.CheckBoxFlag21.Location = New System.Drawing.Point(228, 104)
        Me.CheckBoxFlag21.Name = "CheckBoxFlag21"
        Me.CheckBoxFlag21.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag21.TabIndex = 23
        Me.CheckBoxFlag21.Text = "CheckBoxFlag(21)"
        Me.CheckBoxFlag21.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag22
        '
        Me.CheckBoxFlag22.AutoSize = True
        Me.CheckBoxFlag22.Location = New System.Drawing.Point(228, 120)
        Me.CheckBoxFlag22.Name = "CheckBoxFlag22"
        Me.CheckBoxFlag22.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag22.TabIndex = 24
        Me.CheckBoxFlag22.Text = "CheckBoxFlag(22)"
        Me.CheckBoxFlag22.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag23
        '
        Me.CheckBoxFlag23.AutoSize = True
        Me.CheckBoxFlag23.Location = New System.Drawing.Point(228, 136)
        Me.CheckBoxFlag23.Name = "CheckBoxFlag23"
        Me.CheckBoxFlag23.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag23.TabIndex = 25
        Me.CheckBoxFlag23.Text = "CheckBoxFlag(23)"
        Me.CheckBoxFlag23.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag24
        '
        Me.CheckBoxFlag24.AutoSize = True
        Me.CheckBoxFlag24.Location = New System.Drawing.Point(228, 152)
        Me.CheckBoxFlag24.Name = "CheckBoxFlag24"
        Me.CheckBoxFlag24.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag24.TabIndex = 26
        Me.CheckBoxFlag24.Text = "CheckBoxFlag(24)"
        Me.CheckBoxFlag24.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag25
        '
        Me.CheckBoxFlag25.AutoSize = True
        Me.CheckBoxFlag25.Location = New System.Drawing.Point(228, 168)
        Me.CheckBoxFlag25.Name = "CheckBoxFlag25"
        Me.CheckBoxFlag25.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag25.TabIndex = 27
        Me.CheckBoxFlag25.Text = "CheckBoxFlag(25)"
        Me.CheckBoxFlag25.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag26
        '
        Me.CheckBoxFlag26.AutoSize = True
        Me.CheckBoxFlag26.Location = New System.Drawing.Point(228, 184)
        Me.CheckBoxFlag26.Name = "CheckBoxFlag26"
        Me.CheckBoxFlag26.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag26.TabIndex = 28
        Me.CheckBoxFlag26.Text = "CheckBoxFlag(26)"
        Me.CheckBoxFlag26.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag27
        '
        Me.CheckBoxFlag27.AutoSize = True
        Me.CheckBoxFlag27.Location = New System.Drawing.Point(228, 200)
        Me.CheckBoxFlag27.Name = "CheckBoxFlag27"
        Me.CheckBoxFlag27.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag27.TabIndex = 29
        Me.CheckBoxFlag27.Text = "CheckBoxFlag(27)"
        Me.CheckBoxFlag27.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag28
        '
        Me.CheckBoxFlag28.AutoSize = True
        Me.CheckBoxFlag28.Location = New System.Drawing.Point(228, 216)
        Me.CheckBoxFlag28.Name = "CheckBoxFlag28"
        Me.CheckBoxFlag28.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag28.TabIndex = 30
        Me.CheckBoxFlag28.Text = "CheckBoxFlag(28)"
        Me.CheckBoxFlag28.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag29
        '
        Me.CheckBoxFlag29.AutoSize = True
        Me.CheckBoxFlag29.Location = New System.Drawing.Point(228, 232)
        Me.CheckBoxFlag29.Name = "CheckBoxFlag29"
        Me.CheckBoxFlag29.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag29.TabIndex = 31
        Me.CheckBoxFlag29.Text = "CheckBoxFlag(29)"
        Me.CheckBoxFlag29.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag30
        '
        Me.CheckBoxFlag30.AutoSize = True
        Me.CheckBoxFlag30.Location = New System.Drawing.Point(228, 248)
        Me.CheckBoxFlag30.Name = "CheckBoxFlag30"
        Me.CheckBoxFlag30.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag30.TabIndex = 32
        Me.CheckBoxFlag30.Text = "CheckBoxFlag(30)"
        Me.CheckBoxFlag30.UseVisualStyleBackColor = True
        '
        'CheckBoxFlag31
        '
        Me.CheckBoxFlag31.AutoSize = True
        Me.CheckBoxFlag31.Location = New System.Drawing.Point(228, 264)
        Me.CheckBoxFlag31.Name = "CheckBoxFlag31"
        Me.CheckBoxFlag31.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxFlag31.TabIndex = 33
        Me.CheckBoxFlag31.Text = "CheckBoxFlag(31)"
        Me.CheckBoxFlag31.UseVisualStyleBackColor = True
        '
        'SetButton
        '
        Me.SetButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.SetButton.Location = New System.Drawing.Point(312, 312)
        Me.SetButton.Name = "SetButton"
        Me.SetButton.Size = New System.Drawing.Size(49, 25)
        Me.SetButton.TabIndex = 1
        Me.SetButton.Text = "Set"
        Me.SetButton.UseVisualStyleBackColor = True
        '
        'LabelDesc
        '
        Me.LabelDesc.BackColor = System.Drawing.SystemColors.Window
        Me.LabelDesc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelDesc.Location = New System.Drawing.Point(8, 288)
        Me.LabelDesc.Name = "LabelDesc"
        Me.LabelDesc.Size = New System.Drawing.Size(298, 57)
        Me.LabelDesc.TabIndex = 34
        '
        'ContextMenuStripFlags
        '
        Me.ContextMenuStripFlags.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuFlagsUndo, Me.MenuFlagsSpace1, Me.MenuFlagsCut, Me.MenuFlagsCopy, Me.MenuFlagsPaste, Me.MenuFlagsSpace2, Me.MenuFlagsSelectAll, Me.MenuFlagsSpace3, Me.MenuFlagClear, Me.MenuFlagDefault})
        Me.ContextMenuStripFlags.Name = "ContextMenuStripFlags"
        Me.ContextMenuStripFlags.Size = New System.Drawing.Size(162, 176)
        '
        'MenuFlagsUndo
        '
        Me.MenuFlagsUndo.Name = "MenuFlagsUndo"
        Me.MenuFlagsUndo.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsUndo.Text = "Undo"
        '
        'MenuFlagsSpace1
        '
        Me.MenuFlagsSpace1.Name = "MenuFlagsSpace1"
        Me.MenuFlagsSpace1.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsCut
        '
        Me.MenuFlagsCut.Name = "MenuFlagsCut"
        Me.MenuFlagsCut.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsCut.Text = "Cut"
        '
        'MenuFlagsCopy
        '
        Me.MenuFlagsCopy.Name = "MenuFlagsCopy"
        Me.MenuFlagsCopy.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsCopy.Text = "Copy"
        '
        'MenuFlagsPaste
        '
        Me.MenuFlagsPaste.Name = "MenuFlagsPaste"
        Me.MenuFlagsPaste.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsPaste.Text = "Paste"
        '
        'MenuFlagsSpace2
        '
        Me.MenuFlagsSpace2.Name = "MenuFlagsSpace2"
        Me.MenuFlagsSpace2.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsSelectAll
        '
        Me.MenuFlagsSelectAll.Name = "MenuFlagsSelectAll"
        Me.MenuFlagsSelectAll.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsSelectAll.Text = "Select All"
        '
        'MenuFlagsSpace3
        '
        Me.MenuFlagsSpace3.Name = "MenuFlagsSpace3"
        Me.MenuFlagsSpace3.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagClear
        '
        Me.MenuFlagClear.Name = "MenuFlagClear"
        Me.MenuFlagClear.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagClear.Text = "Clear Flags"
        '
        'MenuFlagDefault
        '
        Me.MenuFlagDefault.Name = "MenuFlagDefault"
        Me.MenuFlagDefault.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagDefault.Text = "Set Default Flags"
        '
        'FormTxFlags
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(444, 352)
        Me.Controls.Add(Me.TextBoxFlags)
        Me.Controls.Add(Me.Cancel_Button)
        Me.Controls.Add(Me.CheckBoxFlag0)
        Me.Controls.Add(Me.CheckBoxFlag1)
        Me.Controls.Add(Me.CheckBoxFlag2)
        Me.Controls.Add(Me.CheckBoxFlag3)
        Me.Controls.Add(Me.CheckBoxFlag4)
        Me.Controls.Add(Me.CheckBoxFlag5)
        Me.Controls.Add(Me.CheckBoxFlag6)
        Me.Controls.Add(Me.CheckBoxFlag7)
        Me.Controls.Add(Me.CheckBoxFlag8)
        Me.Controls.Add(Me.CheckBoxFlag9)
        Me.Controls.Add(Me.CheckBoxFlag10)
        Me.Controls.Add(Me.CheckBoxFlag11)
        Me.Controls.Add(Me.CheckBoxFlag12)
        Me.Controls.Add(Me.CheckBoxFlag13)
        Me.Controls.Add(Me.CheckBoxFlag14)
        Me.Controls.Add(Me.CheckBoxFlag15)
        Me.Controls.Add(Me.CheckBoxFlag16)
        Me.Controls.Add(Me.CheckBoxFlag17)
        Me.Controls.Add(Me.CheckBoxFlag18)
        Me.Controls.Add(Me.CheckBoxFlag19)
        Me.Controls.Add(Me.CheckBoxFlag20)
        Me.Controls.Add(Me.CheckBoxFlag21)
        Me.Controls.Add(Me.CheckBoxFlag22)
        Me.Controls.Add(Me.CheckBoxFlag23)
        Me.Controls.Add(Me.CheckBoxFlag24)
        Me.Controls.Add(Me.CheckBoxFlag25)
        Me.Controls.Add(Me.CheckBoxFlag26)
        Me.Controls.Add(Me.CheckBoxFlag27)
        Me.Controls.Add(Me.CheckBoxFlag28)
        Me.Controls.Add(Me.CheckBoxFlag29)
        Me.Controls.Add(Me.CheckBoxFlag30)
        Me.Controls.Add(Me.CheckBoxFlag31)
        Me.Controls.Add(Me.SetButton)
        Me.Controls.Add(Me.LabelDesc)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormTxFlags"
        Me.Text = "TxFlags"
        Me.ContextMenuStripFlags.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Sub InitializeCheckBoxFlag()
        ReDim CheckBoxFlag(31)
        Me.CheckBoxFlag(0) = CheckBoxFlag0
        Me.CheckBoxFlag(1) = CheckBoxFlag1
        Me.CheckBoxFlag(2) = CheckBoxFlag2
        Me.CheckBoxFlag(3) = CheckBoxFlag3
        Me.CheckBoxFlag(4) = CheckBoxFlag4
        Me.CheckBoxFlag(5) = CheckBoxFlag5
        Me.CheckBoxFlag(6) = CheckBoxFlag6
        Me.CheckBoxFlag(7) = CheckBoxFlag7
        Me.CheckBoxFlag(8) = CheckBoxFlag8
        Me.CheckBoxFlag(9) = CheckBoxFlag9
        Me.CheckBoxFlag(10) = CheckBoxFlag10
        Me.CheckBoxFlag(11) = CheckBoxFlag11
        Me.CheckBoxFlag(12) = CheckBoxFlag12
        Me.CheckBoxFlag(13) = CheckBoxFlag13
        Me.CheckBoxFlag(14) = CheckBoxFlag14
        Me.CheckBoxFlag(15) = CheckBoxFlag15
        Me.CheckBoxFlag(16) = CheckBoxFlag16
        Me.CheckBoxFlag(17) = CheckBoxFlag17
        Me.CheckBoxFlag(18) = CheckBoxFlag18
        Me.CheckBoxFlag(19) = CheckBoxFlag19
        Me.CheckBoxFlag(20) = CheckBoxFlag20
        Me.CheckBoxFlag(21) = CheckBoxFlag21
        Me.CheckBoxFlag(22) = CheckBoxFlag22
        Me.CheckBoxFlag(23) = CheckBoxFlag23
        Me.CheckBoxFlag(24) = CheckBoxFlag24
        Me.CheckBoxFlag(25) = CheckBoxFlag25
        Me.CheckBoxFlag(26) = CheckBoxFlag26
        Me.CheckBoxFlag(27) = CheckBoxFlag27
        Me.CheckBoxFlag(28) = CheckBoxFlag28
        Me.CheckBoxFlag(29) = CheckBoxFlag29
        Me.CheckBoxFlag(30) = CheckBoxFlag30
        Me.CheckBoxFlag(31) = CheckBoxFlag31
    End Sub

    Private WithEvents TextBoxFlags As System.Windows.Forms.TextBox
    Private WithEvents Cancel_Button As System.Windows.Forms.Button
    Private CheckBoxFlag As System.Windows.Forms.CheckBox()
    Private CheckBoxFlag0 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag1 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag2 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag3 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag4 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag5 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag6 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag7 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag8 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag9 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag10 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag11 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag12 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag13 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag14 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag15 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag16 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag17 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag18 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag19 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag20 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag21 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag22 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag23 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag24 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag25 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag26 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag27 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag28 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag29 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag30 As System.Windows.Forms.CheckBox
    Private CheckBoxFlag31 As System.Windows.Forms.CheckBox
    Private LabelDesc As System.Windows.Forms.Label
    Private WithEvents SetButton As System.Windows.Forms.Button
    Private WithEvents ContextMenuStripFlags As ContextMenuStrip
    Private WithEvents MenuFlagsUndo As ToolStripMenuItem
    Private WithEvents MenuFlagsSpace1 As ToolStripSeparator
    Private WithEvents MenuFlagsCut As ToolStripMenuItem
    Private WithEvents MenuFlagsCopy As ToolStripMenuItem
    Private WithEvents MenuFlagsPaste As ToolStripMenuItem
    Private WithEvents MenuFlagsSpace2 As ToolStripSeparator
    Private WithEvents MenuFlagsSelectAll As ToolStripMenuItem
    Private WithEvents MenuFlagsSpace3 As ToolStripSeparator
    Private WithEvents MenuFlagClear As ToolStripMenuItem
    Private WithEvents MenuFlagDefault As ToolStripMenuItem
End Class