' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.Globalization
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Text
Imports Microsoft.Win32
Imports NCS_J2534_2_Tool.J2534ProtocolId
Imports NCS_J2534_2_Tool.J2534IoCtlId
Imports NCS_J2534_2_Tool.J2534ConfigParameterId
Imports NCS_J2534_2_Tool.J1939AddressClaimSetup

Public Module J2534Code
    Public Function GetRegistryPassThruSupportInfo() As Integer
        Dim j As Integer = -1
        Dim key As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\PassThruSupport.04.04")
        If key Is Nothing Then key = Registry.LocalMachine.OpenSubKey("Software\Wow6432Node\PassThruSupport.04.04")
        If key IsNot Nothing Then
            j = J2534_APIs.Length - 1
            For Each keyname As String In key.GetSubKeyNames()
                Dim subKey As RegistryKey = key.OpenSubKey(keyname)
                If subKey IsNot Nothing Then
                    j += 1
                    ReDim Preserve J2534_APIs(j)
                    J2534_APIs(j) = New J2534_API With
                    {
                        .KeyName = keyname,
                        .DeviceName = subKey.GetValue("Name").ToString,
                        .LibraryName = subKey.GetValue("FunctionLibrary").ToString,
                        .VendorName = subKey.GetValue("Vendor").ToString,
                        .ConfigAppPath = subKey.GetValue("ConfigApplication").ToString
                    }

                    Select Case J2534_APIs(j).DeviceName
                        Case "Netbridge"
                            J2534_APIs(j).DeviceType = DeviceType.Netbridge

                        Case Else
                            J2534_APIs(j).DeviceType = DeviceType.Other

                    End Select
                    subKey.Close()
                End If
            Next keyname
            key.Close()
        End If
        Return j + 1
    End Function

    Public Sub SetArrays()

        ReDim FilterEditBuffer(MAX_FILTER)
        For i As Integer = 0 To MAX_FILTER
            FilterEditBuffer(i) = New J2534Filter
        Next i

        ReDim PeriodicMessageEditBuffer(MAX_PM)
        For i As Integer = 0 To MAX_FILTER
            PeriodicMessageEditBuffer(i) = New J2534PeriodicMessage
        Next i

    End Sub

    Public Function CompareMessages(ByVal base As PASSTHRU_MSG, ByVal compare As PASSTHRU_MSG) As MessageCompareResults
        Dim result As MessageCompareResults
        If base.ProtocolId <> compare.ProtocolId Then result = result Or MessageCompareResults.ProtocolMismatch
        If base.RxStatus <> compare.RxStatus Then result = result Or MessageCompareResults.RxStatusMismatch
        If base.TxFlags <> compare.TxFlags Then result = result Or MessageCompareResults.TxFlagsMismatch
        If base.Timestamp <> compare.Timestamp Then result = result Or MessageCompareResults.TimestampMismatch
        If base.DataSize <> compare.DataSize Then result = result Or MessageCompareResults.DataSizeMismatch
        If base.ExtraDataIndex <> compare.ExtraDataIndex Then result = result Or MessageCompareResults.ExtraDataMismatch
        If base.DataSize <> compare.DataSize Then
            result = result Or MessageCompareResults.DataMismatch
        ElseIf base.Data Is Nothing OrElse compare.Data Is Nothing Then
            result = result Or MessageCompareResults.DataMismatch
        ElseIf base.Data.Length < 4128 Or compare.Data.Length < 4128 Then
            result = result Or MessageCompareResults.DataMismatch
        Else
            Dim n As Integer = base.DataSize.ToInt32 - 1
            If n > 4127 Then n = 4127
            For i As Integer = 0 To n
                If base.Data(i) <> compare.Data(i) Then
                    result = result Or MessageCompareResults.DataMismatch
                    Exit For
                End If
            Next i
        End If
        Return result
    End Function

    Public Sub FlagsKeyPress(sender As Object, e As KeyPressEventArgs)
        Dim thisTextBox As TextBox = TryCast(sender, TextBox)
        If thisTextBox Is Nothing Then Return
        Dim unSelected As String = thisTextBox.Text.Remove(thisTextBox.SelectionStart, thisTextBox.SelectionLength)
        Select Case e.KeyChar
            Case "X"c, "x"c
                If unSelected.IndexOfAny("&HhXx".ToCharArray) >= 0 Then
                    e.Handled = True
                End If
                If Not (thisTextBox.Text.StartsWith("0") AndAlso thisTextBox.SelectionStart = 1) Then e.Handled = True

            Case "&"c
                If unSelected.Contains("x"c) OrElse unSelected.Contains("X"c) OrElse
                   thisTextBox.SelectionStart <> 0 Then e.Handled = True

            Case "H"c, "h"c
                If unSelected.IndexOfAny("HhXx".ToCharArray) >= 0 OrElse Not (thisTextBox.Text.StartsWith("&") AndAlso thisTextBox.SelectionStart = 1) Then
                    e.Handled = True
                End If

            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo


            Case Else
                If Not (e.KeyChar.IsHex OrElse e.KeyChar = " "c) Then e.Handled = True

        End Select
    End Sub

    Public Function CanMakePeriodicMessage(ByVal device As J2534Device, ByVal protocolName As String) As Boolean
        Dim result As Boolean = False
        If device Is Nothing Then Return result
        Dim protocolId As UInteger = NameToProtocolId(protocolName)
        Dim channel As J2534Channel = device.Channel(protocolId)
        If channel Is Nothing Then Return False
        Dim channelISO As J2534Channel = Nothing
        If channel.IsCAN AndAlso Not channel.Connected Then
            channelISO = CAN_ChannelToISO_Channel(device, channel)
        End If
        If channel IsNot Nothing AndAlso channel.Connected Then
            result = True
        ElseIf channelISO IsNot Nothing AndAlso channelISO.Connected AndAlso channelISO.MixedMode Then
            result = True
        End If
        Return result
    End Function

    Public Function NameToFilterType(ByVal name As String) As J2534FilterType
        Select Case name
            Case "Pass"
                Return J2534FilterType.PASS_FILTER
            Case "Block"
                Return J2534FilterType.BLOCK_FILTER
            Case "Flow"
                Return J2534FilterType.FLOW_CONTROL_FILTER
            Case Else
                Return J2534FilterType.NO_FILTER
        End Select
    End Function

    Public Function FilterTypeToName(ByVal filterType As J2534FilterType) As String
        Select Case filterType
            Case J2534FilterType.PASS_FILTER
                Return "Pass"
            Case J2534FilterType.BLOCK_FILTER
                Return "Block"
            Case J2534FilterType.FLOW_CONTROL_FILTER
                Return "Flow"
            Case Else
                Return "None"
        End Select
    End Function

    Public Function IsISO15765(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case ISO15765, ISO15765_PS, SW_ISO15765_PS, FT_ISO15765_PS, ISO15765_CH1 To ISO15765_CH128,
                 SW_CAN_ISO15765_CH1 To SW_CAN_ISO15765_CH128,
                 FT_ISO15765_CH1 To FT_ISO15765_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsCAN(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case CAN, CAN_PS, SW_CAN_PS, FT_CAN_PS, CAN_CH1 To CAN_CH128,
                 SW_CAN_CAN_CH1 To SW_CAN_CAN_CH128, FT_CAN_CH1 To FT_CAN_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsJ1708(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case J1708_PS, J1708_CH1 To J1708_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsJ1939(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case J1939_PS, J1939_CH1 To J1939_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsISO9141(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case ISO9141, ISO9141_PS, ISO9141_CH1 To ISO9141_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsISO14230(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case ISO14230, ISO14230_PS, ISO14230_CH1 To ISO14230_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsJ1850VPW(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case J2534ProtocolId.J1850VPW, J1850VPW_PS, J1850VPW_CH1 To J1850VPW_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsJ1850PWM(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case J2534ProtocolId.J1850PWM, J1850PWM_PS, J1850PWM_CH1 To J1850PWM_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsTP2_0(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case TP2_0_PS, TP2_0_CH1 To TP2_0_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsSingleWire(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case SW_CAN_PS, SW_CAN_CAN_CH1 To SW_CAN_CAN_CH128, SW_ISO15765_PS, SW_CAN_ISO15765_CH1 To SW_CAN_ISO15765_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsFaultTolerant(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case FT_CAN_PS, FT_CAN_CH1 To FT_CAN_CH128, FT_ISO15765_PS, FT_ISO15765_CH1 To FT_ISO15765_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsPinSwitched(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case J1850VPW_PS To FT_ISO15765_PS
                result = True
        End Select
        Return result
    End Function

    Public Function IsCHx(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case CAN_CH1 To TP2_0_CH128
                result = True
        End Select
        Return result
    End Function

    Public Function IsAnalog(ByVal protocolId As UInteger) As Boolean
        Dim result As Boolean = False
        Select Case protocolId
            Case ANALOG_IN_1 To ANALOG_IN_32
                result = True
        End Select
        Return result
    End Function

    Public Sub ProcessAnalogMessage(ByVal device As J2534Device, ByVal channel As J2534Channel, ByVal message As PASSTHRU_MSG)
        If channel Is Nothing Then Return
        GetConfigParameter(device, channel, ACTIVE_CHANNELS)
        GetConfigParameter(device, channel, INPUT_RANGE_LOW)
        GetConfigParameter(device, channel, INPUT_RANGE_HIGH)
        GetConfigParameter(device, channel, READINGS_PER_MSG)

        Dim openChannels As Integer = PopCount(channel.AnalogSubsystem.ActiveChannels)
        Dim iterations As Integer = CInt(channel.AnalogSubsystem.ReadingsPerMessage)
        If openChannels * iterations > 1032 Then iterations = 1032 \ openChannels
        Dim u As Integer
        'the outer 'j' loop doesn't do anything, but it's how you would get all the messages
        'with READINGS_PER_MESSAGE set higher than 1 if you needed them
        For j As Integer = 1 To iterations
            For i As Integer = 0 To 31
                If (channel.AnalogSubsystem.ActiveChannels And 1& << i) <> 0 Then
                    channel.AnalogSubsystem.AnalogIn(i) = BitConverter.ToInt32(message.Data, u)
                    u += 4
                End If
            Next i
        Next j
    End Sub

    <Extension>
    Public Function ToMessageText(ByVal target As PASSTHRU_MSG) As J2534MessageText
        Dim result As New J2534MessageText
        Dim dataBuilder, ExtraDataBuilder, AsciiBuilder As New StringBuilder

        result.ProtocolName = ProtocolIdToName(target.ProtocolId)
        result.RxStatus = FlagsToText(target.RxStatus)
        result.TxFlags = FlagsToText(target.TxFlags)
        result.Timestamp = TimestampToString(target.Timestamp)
        For i As Integer = 0 To CInt(target.DataSize - 1)
            Dim c As Byte = target.Data(i)
            Dim hex As String = c.ToString("X2")
            If i < target.ExtraDataIndex Then
                dataBuilder.Append(hex)
                dataBuilder.Append(" ")
                If c > 31 AndAlso c < 127 Then
                    AsciiBuilder.Append("'")
                    AsciiBuilder.Append(Chr(c))
                    AsciiBuilder.Append("' ")
                Else
                    AsciiBuilder.Append(hex)
                    AsciiBuilder.Append(" ")
                End If
            Else
                ExtraDataBuilder.Append(hex)
                ExtraDataBuilder.Append(" ")
            End If
        Next i
        result.Data = dataBuilder.ToString.TrimEnd
        result.ExtraData = ExtraDataBuilder.ToString.TrimEnd
        result.DataASCII = AsciiBuilder.ToString.TrimEnd
        result.Tx = (target.RxStatus And J2534RxStatusFlags.TX_MSG_TYPE) <> 0
        Return result
    End Function

    <Extension>
    Public Function ToMessage(ByVal target As J2534MessageText) As PASSTHRU_MSG
        If target Is Nothing Then Return Nothing
        Dim result As PASSTHRU_MSG = PASSTHRU_MSG.CreateInstance()
        If String.IsNullOrWhiteSpace(target.ProtocolName) Then
            result.ProtocolId = 0
        Else
            result.ProtocolId = NameToProtocolId(target.ProtocolName)
        End If
        result.TxFlags = TextToFlags(target.TxFlags)
        Dim msgData As String = StripSpaces(target.Data)
        msgData = msgData.Replace("[", "")
        msgData = msgData.Replace("]", "")
        Dim dataIndex As Integer = 0
        Dim dataByte As Byte = 0
        For i As Integer = 0 To msgData.Length - 1 Step 2
            Dim hexPair As String = msgData.Substring(i, Math.Min(2, msgData.Length - i))
            Byte.TryParse(hexPair, NumberStyles.HexNumber, Nothing, dataByte)
            result.Data(dataIndex) = dataByte
            dataIndex += 1
        Next i
        result.DataSize = dataIndex.ToUint32
        result.ExtraDataIndex = result.DataSize
        Return result
    End Function

    Public Function FlagsToText(ByVal flags As UInteger) As String
        Return "0x" & flags.ToString("X8")
    End Function

    Public Function TextToFlags(ByVal text As String) As UInteger
        Dim result As UInteger = 0
        If String.IsNullOrWhiteSpace(text) Then
            result = 0
        Else
            If text.StartsWith("&h", IgnoreCase) OrElse text.StartsWith("0x", IgnoreCase) Then text = text.Substring(2)
            If text.StartsWith("&") Then text = text.Substring(1)
            UInteger.TryParse(text, NumberStyles.HexNumber, Nothing, result)
        End If
        Return result
    End Function

    Public Function GetMessageMask(ByVal message As PASSTHRU_MSG) As PASSTHRU_MSG
        Dim result As PASSTHRU_MSG = message.Clone
        For i As Integer = 0 To CInt(message.DataSize - 1)
            result.Data(i) = Byte.MaxValue
        Next i
        Return result
    End Function

    Public Function TimestampToString(ByVal timestamp As UInteger) As String
        Dim uSec As UInteger = timestamp Mod 1000000UI
        Dim sec As UInteger = timestamp \ 1000000UI Mod 60UI
        Dim min As UInteger = timestamp \ 60000000UI
        Return min.ToString("00") & ":" & sec.ToString("00") & "." & uSec.ToString("000000")
    End Function

    Public Function ProtocolIdToName(ByVal protocolId As J2534ProtocolId) As String
        Dim result As String = "none"
        Dim isCHxId As Boolean = IsCHx(protocolId)
        Dim isAnalogId As Boolean = IsAnalog(protocolId)
        Dim channel As UInteger = protocolId And &H7FUI
        Dim rootProtocolId As J2534ProtocolId = DirectCast(If(isCHxId OrElse isAnalogId, protocolId And &HFF80UI, protocolId), J2534ProtocolId)
        If [Enum].IsDefined(GetType(J2534ProtocolId), rootProtocolId) Then result = rootProtocolId.ToString()
        If isCHxId Then
            result = result.Replace("_CH1", "_CH" & channel + 1)
        ElseIf isAnalogId Then
            result = result.Replace("_IN_1", "_IN_" & channel + 1)
        End If
        Return result
    End Function

    Public Function ProtocolIdToChannelNo(ByVal protocolId As UInteger) As UInteger
        Dim result As UInteger = 0
        If IsCHx(protocolId) OrElse IsAnalog(protocolId) Then result = (protocolId And &H7FUI) + 1UI
        Return result
    End Function

    Public Function NameToProtocolId(ByVal protocolName As String) As J2534ProtocolId
        Dim result As J2534ProtocolId = 0
        If String.IsNullOrWhiteSpace(protocolName) Then Return result
        Dim offset As UInteger = UInteger.MaxValue
        Dim lookup As String = String.Empty
        Dim chLoc As Integer = protocolName.LastIndexOf("_CH")
        Dim inLoc As Integer = protocolName.LastIndexOf("_IN_")
        If chLoc > 0 Then
            lookup = protocolName.Substring(0, chLoc + 3) & "1"
            UInteger.TryParse(protocolName.Substring(chLoc + 3), offset)
            If offset < MIN_CH OrElse offset > MAX_CH Then Return result
            offset -= 1UI
        ElseIf inLoc > 0 Then
            lookup = protocolName.Substring(0, inLoc + 4) & "1"
            UInteger.TryParse(protocolName.Substring(inLoc + 4), offset)
            If offset < MIN_CH OrElse offset > MAX_IN Then Return result
            offset -= 1UI
        Else
            lookup = protocolName
            offset = 0
        End If
        Dim test As J2534ProtocolId = Nothing
        If [Enum].TryParse(lookup, True, test) Then
            result = DirectCast(test + offset, J2534ProtocolId)
        End If
        Return result
    End Function

    Public Function CAN_ChannelToISO_Channel(ByVal device As J2534Device, ByVal channel As J2534Channel) As J2534Channel
        Dim result As J2534Channel = Nothing
        If device Is Nothing OrElse channel Is Nothing Then Return result

        Select Case channel.ProtocolId
            Case CAN
                result = device.Channel(ISO15765)

            Case CAN_PS
                result = device.Channel(ISO15765_PS)

            Case SW_CAN_PS
                result = device.Channel(SW_ISO15765_PS)

            Case FT_CAN_PS
                result = device.Channel(FT_ISO15765_PS)

            Case CAN_CH1 To CAN_CH128
                result = device.Channel(ISO15765_CH1 + channel.ProtocolId - CAN_CH1)

            Case SW_CAN_CAN_CH1 To SW_CAN_CAN_CH128
                result = device.Channel(SW_CAN_ISO15765_CH1 + channel.ProtocolId - SW_CAN_CAN_CH1)

            Case FT_CAN_CH1 To FT_CAN_CH128
                result = device.Channel(FT_ISO15765_CH1 + channel.ProtocolId - FT_CAN_CH1)

        End Select

        Return result
    End Function

    Public Function ISO_ChannelToCAN_Channel(ByVal device As J2534Device, ByVal channel As J2534Channel) As J2534Channel
        Dim result As J2534Channel = Nothing
        If device Is Nothing OrElse channel Is Nothing Then Return result

        Select Case channel.ProtocolId
            Case ISO15765
                result = device.Channel(CAN)

            Case ISO15765_PS
                result = device.Channel(CAN_PS)

            Case SW_ISO15765_PS
                result = device.Channel(SW_CAN_PS)

            Case FT_ISO15765_PS
                result = device.Channel(FT_CAN_PS)

            Case ISO15765_CH1 To ISO15765_CH128
                result = device.Channel(CAN_CH1 + channel.ProtocolId - ISO15765_CH1)

            Case SW_CAN_ISO15765_CH1 To SW_CAN_ISO15765_CH128
                result = device.Channel(SW_CAN_CAN_CH1 + channel.ProtocolId - SW_CAN_ISO15765_CH1)

            Case FT_ISO15765_CH1 To FT_ISO15765_CH128
                result = device.Channel(FT_CAN_CH1 + channel.ProtocolId - FT_ISO15765_CH1)

        End Select

        Return result
    End Function

    Public Function CAN_IdToISO_Id(ByVal protocolId As UInteger) As UInteger
        Dim result As UInteger = 0
        Select Case protocolId
            Case CAN
                result = ISO15765

            Case CAN_PS
                result = ISO15765_PS

            Case SW_CAN_PS
                result = SW_ISO15765_PS

            Case FT_CAN_PS
                result = FT_ISO15765_PS

            Case CAN_CH1 To CAN_CH128
                result = ISO15765_CH1 + protocolId - CAN_CH1

            Case SW_CAN_CAN_CH1 To SW_CAN_CAN_CH128
                result = SW_CAN_ISO15765_CH1 + protocolId - SW_CAN_CAN_CH1

            Case FT_ISO15765_CH1 To FT_ISO15765_CH128
                result = FT_ISO15765_CH1 + protocolId - FT_CAN_CH1

        End Select
        Return result
    End Function

    Public Function ISO_IdToCAN_Id(ByVal protocolId As UInteger) As J2534ProtocolId
        Dim result As J2534ProtocolId = 0
        Select Case protocolId
            Case ISO15765
                result = CAN

            Case ISO15765_PS
                result = CAN_PS

            Case SW_ISO15765_PS
                result = SW_CAN_PS

            Case FT_ISO15765_PS
                result = FT_CAN_PS

            Case ISO15765_CH1 To ISO15765_CH128
                result = DirectCast(CAN_CH1 + protocolId - ISO15765_CH1, J2534ProtocolId)

            Case SW_CAN_ISO15765_CH1 To SW_CAN_ISO15765_CH128
                result = DirectCast(SW_CAN_CAN_CH1 + protocolId - SW_CAN_ISO15765_CH1, J2534ProtocolId)

            Case FT_ISO15765_CH1 To FT_ISO15765_CH128
                result = DirectCast(FT_CAN_CH1 + protocolId - FT_ISO15765_CH1, J2534ProtocolId)

        End Select
        Return result
    End Function

    Public Function NameToDevice(ByVal name As String) As J2534Device
        Dim result As J2534Device = Nothing
        For Each Box As KeyValuePair(Of Integer, J2534Device) In J2534Devices
            If Box.Value.Name = name Then
                result = Box.Value
                Exit For
            End If
        Next Box
        Return result
    End Function

    Public Sub FixFilterMask(ByVal mask As PASSTHRU_MSG, ByVal pattern As PASSTHRU_MSG)
        If mask.DataSize = 0 AndAlso pattern.DataSize > 0 Then
            mask = GetMessageMask(pattern)
        End If
        Dim n As Integer = CInt(mask.DataSize) - 1
        For i As Integer = CInt(pattern.DataSize) To n
            pattern.Data(i) = 0
        Next i
        For i As Integer = 0 To n
            pattern.Data(i) = pattern.Data(i) And mask.Data(i)
        Next i
        n = CInt(pattern.DataSize) - 1
        For i As Integer = CInt(mask.DataSize) To n
            pattern.Data(i) = 0
        Next i
        pattern.DataSize = mask.DataSize
        pattern.ExtraDataIndex = pattern.DataSize
    End Sub

    Public Sub DisplayMessage(ByVal device As J2534Device, ByVal messageText As J2534MessageText)
        If device Is Nothing Then Return
        Dim data As String = messageText.Data
        If If(messageText?.ExtraData?.Length, 0) > 0 Then
            If If(data?.Length, 0) > 0 Then data &= " "
            data &= "[" & messageText.ExtraData & "]"
        End If

        Dim item As ListViewItem = device.ListViewMessageIn.Items.Add(messageText.Timestamp)
        If messageText.Tx Then
            item.ImageIndex = 1 'out
            If device.MessageOutCount = ULong.MaxValue Then device.MessageOutCount = 0
            device.MessageOutCount += 1UI
        Else
            item.ImageIndex = 0 'in
            If device.MessageInCount = ULong.MaxValue Then device.MessageInCount = 0
            device.MessageInCount += 1UI
        End If

        item.SubItems.Add(messageText.ProtocolName)
        item.SubItems.Add(messageText.RxStatus)
        item.SubItems.Add(data)
        If Not String.IsNullOrWhiteSpace(messageText.DataASCII) Then item.SubItems(3).Tag = messageText.DataASCII
        If device.ListViewMessageIn.Items.Count > 10000 Then device.ListViewMessageIn.Items.RemoveAt(0)
        If device.ListViewMessageOut.Items.Count > 10000 Then device.ListViewMessageOut.Items.RemoveAt(0)

    End Sub

    Public Function GetListViewParentDevice(ByVal thisListView As ListView) As J2534Device
        For Each kvp As KeyValuePair(Of Integer, J2534Device) In J2534Devices
            If thisListView Is kvp.Value.ListViewMessageIn OrElse thisListView Is kvp.Value.ListViewMessageOut Then Return kvp.Value
        Next kvp
        Return Nothing
    End Function

    Public Function GetTimerParentDevice(ByVal thisTimer As Timer) As J2534Device
        For Each kvp As KeyValuePair(Of Integer, J2534Device) In J2534Devices
            If thisTimer Is kvp.Value.TimerMessageReceive Then Return kvp.Value
        Next kvp
        Return Nothing
    End Function

    Public Function GetVersionNumericPart(ByVal value As String) As String
        Dim result As String = String.Empty
        Dim n As Integer = If(value?.Length, 0)
        For i As Integer = 0 To n - 1
            Dim c As Char = value.Chars(i)
            If c = "."c OrElse Char.IsDigit(c) Then 'this is a kludge for a box that returns a story about its FW version
                result &= c
            Else
                Exit For
            End If
        Next i
        Dim versions As String() = Split(result, ".")
        result = String.Empty
        For Each version As String In versions
            Dim d As Double
            Double.TryParse(version, d)
            result &= d & "."
        Next version
        Return result.TrimEnd("."c)
    End Function

    Public Function IsSendable(ByVal device As J2534Device, ByVal channel As J2534Channel) As Boolean
        If channel Is Nothing Then Return False
        If channel.Connected Then Return True
        If channel.IsCAN Then
            Dim channelISO As J2534Channel = CAN_ChannelToISO_Channel(device, channel)
            If channelISO IsNot Nothing AndAlso channelISO.MixedMode Then Return True
        End If
        Return False
    End Function

    Public Function SetConfigParameter(ByVal device As J2534Device, ByVal channel As J2534Channel, ByVal parameterId As J2534ConfigParameterId, ByVal value As UInteger) As J2534Result
        Dim result As J2534Result = DirectCast(-1, J2534Result)
        If device?.API?.PassThruIoctl Is Nothing OrElse channel Is Nothing Then Return result
        Dim parameterIndex As Integer = channel.ParameterIndex(parameterId)
        If parameterIndex < 0 Then Return result
        Dim config As SCONFIG
        Dim configList As SCONFIG_LIST
        config.Parameter = parameterId
        config.Value = value
        configList.NumOfParams = 1

        Dim configPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(config))
        Marshal.StructureToPtr(config, configPointer, True)
        configList.ConfigPtr = configPointer.ToInt32
        Dim configListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(configList))
        Marshal.StructureToPtr(configList, configListPointer, True)
        result = device.API.PassThruIoctl(channel.ChannelId, SET_CONFIG, configListPointer.ToInt32, 0)
        config = DirectCast(Marshal.PtrToStructure(configPointer, GetType(SCONFIG)), SCONFIG)
        Marshal.FreeHGlobal(configListPointer)
        Marshal.FreeHGlobal(configPointer)
        If result = J2534Result.STATUS_NOERROR AndAlso parameterId = CAN_MIXED_FORMAT AndAlso channel.IsISO15765 Then
            channel.MixedMode = config.Value <> 0
            ClearAllFilters(device, channel)
        End If

        Return result
    End Function

    Public Function ClearAllFilters(ByVal device As J2534Device, ByVal channel As J2534Channel) As Integer
        If device?.API?.PassThruIoctl Is Nothing OrElse channel Is Nothing Then Return -1
        Dim result As J2534Result = device.API.PassThruIoctl(channel.ChannelId, CLEAR_MSG_FILTERS, 0, 0)
        FormJ2534.ShowResult(device, result, "PassThruIoctl - CLEAR_MSG_FILTERS")
        For i As Integer = 0 To MAX_FILTER
            channel.Filter(i).FilterType = 0
            channel.Filter(i).MaskMessage.Clear()
            channel.Filter(i).PatternMessage.Clear()
            channel.Filter(i).FlowMessage.Clear()
            channel.Filter(i).FlowMessage.TxFlags = channel.DefaultTxFlags
            channel.Filter(i).MessageId = 0
            channel.Filter(i).Enabled = False
        Next i
        FormJ2534.SetFilterControls()
        FormJ2534.UpdateFilterEdit()
        Return result
    End Function

    Public Function ClearAllPeriodicMessages(ByVal device As J2534Device, ByVal channel As J2534Channel) As J2534Result
        If device?.API?.PassThruIoctl Is Nothing OrElse channel Is Nothing Then Return DirectCast(-1, J2534Result)
        Dim result As J2534Result = device.API.PassThruIoctl(channel.ChannelId, CLEAR_PERIODIC_MSGS, 0, 0)
        FormJ2534.ShowResult(device, result, "PassThruIoctl - CLEAR_PERIODIC_MSGS")
        For i As Integer = 0 To MAX_PM
            channel.PeriodicMessage(i).Message.Clear()
            channel.PeriodicMessage(i).Message.TxFlags = channel.DefaultTxFlags
            channel.PeriodicMessage(i).Interval = 100
            channel.PeriodicMessage(i).MessageId = 0
            channel.PeriodicMessage(i).Enabled = False
        Next i
        FormJ2534.SetPeriodicMessageControls(device)
        FormJ2534.UpdatePeriodicMessageEdit()
        Return result
    End Function

    Public Function ReadBatteryVoltage(ByVal device As J2534Device) As J2534Result
        If device?.API?.PassThruIoctl Is Nothing OrElse Not device.Connected Then Return DirectCast(-1, J2534Result)
        Dim gh As GCHandle = GCHandle.Alloc(device.Analog.BatteryReading, GCHandleType.Pinned)
        Dim readingPointer As IntPtr = gh.AddrOfPinnedObject()
        Dim result As J2534Result = device.API.PassThruIoctl(device.DeviceId, READ_VBATT, 0, readingPointer.ToInt32)
        device.Analog.BatteryReading = DirectCast(gh.Target, UInteger)
        gh.Free()
        Return result
    End Function

    Public Function ReadProgrammingVoltage(ByVal device As J2534Device) As J2534Result
        If device?.API?.PassThruIoctl Is Nothing OrElse Not device.Connected Then Return DirectCast(-1, J2534Result)
        Dim gh As GCHandle = GCHandle.Alloc(device.Analog.PinReading, GCHandleType.Pinned)
        Dim readingPointer As IntPtr = gh.AddrOfPinnedObject()
        Dim result As J2534Result = device.API.PassThruIoctl(device.DeviceId, READ_PROG_VOLTAGE, 0, readingPointer.ToInt32)
        device.Analog.PinReading = DirectCast(gh.Target, UInteger)
        gh.Free()
        Return result
    End Function

    Public Function GetConfigParameter(ByVal device As J2534Device, ByVal channel As J2534Channel, ByVal parameterId As UInteger) As J2534Result
        If device?.API?.PassThruIoctl Is Nothing OrElse channel Is Nothing Then Return DirectCast(-1, J2534Result)
        Dim parameterIndex As Integer = channel.ParameterIndex(parameterId)
        Dim result As J2534Result = DirectCast(-1, J2534Result)
        If parameterIndex < 0 Then Return result
        Dim config As SCONFIG
        Dim configList As SCONFIG_LIST
        config.Parameter = parameterId
        configList.NumOfParams = 1

        Dim configPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(config))
        Marshal.StructureToPtr(config, configPointer, True)
        configList.ConfigPtr = configPointer.ToInt32
        Dim configListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(configList))
        Marshal.StructureToPtr(configList, configListPointer, True)
        result = device.API.PassThruIoctl(channel.ChannelId, GET_CONFIG, configListPointer.ToInt32, 0)
        config = DirectCast(Marshal.PtrToStructure(configPointer, GetType(SCONFIG)), SCONFIG)
        Marshal.FreeHGlobal(configListPointer)
        Marshal.FreeHGlobal(configPointer)
        If result = J2534Result.STATUS_NOERROR AndAlso parameterId = CAN_MIXED_FORMAT AndAlso channel.IsISO15765 Then
            channel.MixedMode = config.Value <> 0
        End If
        If result = J2534Result.STATUS_NOERROR Then
            channel.ConfParameter(parameterIndex).Value = config.Value
            Select Case config.Parameter
                Case DATA_RATE
                    channel.BaudRate = config.Value

                Case ACTIVE_CHANNELS
                    channel.AnalogSubsystem.ActiveChannels = config.Value

                Case INPUT_RANGE_LOW
                    channel.AnalogSubsystem.RangeLow = config.Value.ToInt32

                Case INPUT_RANGE_HIGH
                    channel.AnalogSubsystem.RangeHigh = config.Value.ToInt32

                Case READINGS_PER_MSG
                    channel.AnalogSubsystem.ReadingsPerMessage = config.Value

            End Select
        End If

        Return result
    End Function

    Public Function PrepFunctionalAddressArray(ByVal values As Byte()) As Byte()
        If values Is Nothing Then Return {}
        Dim result As New List(Of Byte) With {.Capacity = 64}
        For Each value As Byte In values
            If value <> 0 AndAlso Not result.Contains(value) Then result.Add(value)
        Next value
        Return result.ToArray
    End Function

    Public Function BuildJ1939Name(ByVal setup As J1939AddressClaimSetup) As Byte()
        If setup Is Nothing Then Return {}
        Dim mfgCodeId As UInteger
        Dim result(8) As Byte
        result(0) = setup.Address
        mfgCodeId = setup.MfgCode << 21
        mfgCodeId = mfgCodeId Or (setup.Identity And J1939_NAME_MASK_IDENTITY)
        Array.Copy(BitConverter.GetBytes(mfgCodeId), 0, result, 1, 4)
        result(5) = setup.FunctionInstance << 3
        result(5) = result(5) Or (setup.ECUInstance And J1939_NAME_MASK_ECU_INST)
        result(6) = setup.FunctionId
        result(7) = setup.VehicleSystemId << 1
        result(8) = setup.IndustryGroup << 4 And J1939_NAME_MASK_IND_GRP
        result(8) = result(8) Or (setup.VehicleSystemInstance And J1939_NAME_MASK_VEH_SYS_INST)
        If setup.ArbitraryCapable Then result(8) = result(8) Or J1939_NAME_MASK_ARB
        Return result
    End Function

    Public Function ClaimJ1939Address(ByVal device As J2534Device, ByVal channel As J2534Channel, ByVal setup As J1939AddressClaimSetup) As J2534Result
        Dim result As J2534Result = DirectCast(-1, J2534Result)
        Dim bytes As Byte() = BuildJ1939Name(setup)
        If device?.API?.PassThruIoctl Is Nothing OrElse channel Is Nothing Then Return result
        Dim inputMsg As SBYTE_ARRAY
        inputMsg.NumOfBytes = 9
        Dim bytesPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(bytes(0)) * bytes.Length)
        Marshal.Copy(bytes, 0, bytesPointer, bytes.Length)
        inputMsg.BytePtr = bytesPointer.ToInt32
        Dim inputMsgPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(inputMsg))
        Marshal.StructureToPtr(inputMsg, inputMsgPointer, True)
        result = device.API.PassThruIoctl(channel.ChannelId, PROTECT_J1939_ADDR, inputMsgPointer.ToInt32, 0)
        Marshal.FreeHGlobal(bytesPointer)
        Marshal.FreeHGlobal(inputMsgPointer)
        FormJ2534.ShowResult(device, result, "PassThruIoctl - PROTECT_J1939_ADDR")
        Return result
    End Function

End Module