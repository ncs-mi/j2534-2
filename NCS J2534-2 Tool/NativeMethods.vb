﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Explicit On
Option Infer Off
Option Strict On

Imports System.Runtime.InteropServices

Public NotInheritable Class NativeMethods
    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode)>', BestFitMapping:=False, ThrowOnUnmappableChar:=True)>
    Friend Shared Function LoadLibrary(ByVal libName As String) As IntPtr
    End Function

    <DllImport("kernel32.dll")>
    Friend Shared Function FreeLibrary(ByVal hModule As IntPtr) As Integer
    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Ansi, BestFitMapping:=False, ThrowOnUnmappableChar:=True)>
    Friend Shared Function GetProcAddress(ByVal hModule As IntPtr, ByVal lpProcName As String) As IntPtr
    End Function

    <DllImport("User32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Friend Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As UInt32, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Int32
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Friend Shared Function FindWindowEx(ByVal hwndParent As IntPtr, ByVal hwndChildAfter As IntPtr, ByVal lpszClass As String, ByVal lpszWindow As String) As IntPtr
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto, BestFitMapping:=False, ThrowOnUnmappableChar:=True)>
    Friend Shared Function GetClassName(ByVal hWnd As IntPtr, ByVal lpClassName As System.Text.StringBuilder, ByVal nMaxCount As Int32) As Int32
    End Function

    Private Sub New()
    End Sub
End Class
