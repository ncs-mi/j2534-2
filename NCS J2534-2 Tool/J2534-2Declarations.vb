' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.Runtime.InteropServices

Public Enum J2534ProtocolId As UInteger
    J1850VPW = 1
    J1850PWM = 2
    ISO9141 = 3
    ISO14230 = 4
    CAN = 5
    ISO15765 = 6
    SCI_A_ENGINE = 7
    SCI_A_TRANS = 8
    SCI_B_ENGINE = 9
    SCI_B_TRANS = 10

    'J2534-2 Pin Switched Protocol IDs
    J1850VPW_PS = &H8000
    J1850PWM_PS = &H8001
    ISO9141_PS = &H8002
    ISO14230_PS = &H8003
    CAN_PS = &H8004
    ISO15765_PS = &H8005
    J2610_PS = &H8006
    SW_ISO15765_PS = &H8007
    SW_CAN_PS = &H8008
    GM_UART_PS = &H8009
    UART_ECHO_BYTE_PS = &H800A
    HONDA_DIAGH_PS = &H800B
    J1939_PS = &H800C
    J1708_PS = &H800D
    TP2_0_PS = &H800E
    FT_CAN_PS = &H800F
    FT_ISO15765_PS = &H8010

    CAN_CH1 = &H9000
    CAN_CH128 = &H907F
    J1850VPW_CH1 = &H9080
    J1850VPW_CH128 = &H90FF
    J1850PWM_CH1 = &H9100
    J1850PWM_CH128 = &H917F
    ISO9141_CH1 = &H9180
    ISO9141_CH128 = &H91FF
    ISO14230_CH1 = &H9200
    ISO14230_CH128 = &H927F
    ISO15765_CH1 = &H9280
    ISO15765_CH128 = &H92FF
    SW_CAN_CAN_CH1 = &H9300
    SW_CAN_CAN_CH128 = &H937F
    SW_CAN_ISO15765_CH1 = &H9380
    SW_CAN_ISO15765_CH128 = &H93FF
    J2610_CH1 = &H9400
    J2610_CH128 = &H947F
    FT_CAN_CH1 = &H9480
    FT_CAN_CH128 = &H94FF
    FT_ISO15765_CH1 = &H9500
    FT_ISO15765_CH128 = &H957F
    GM_UART_CH1 = &H9580
    GM_UART_CH128 = &H95FF
    ECHO_BYTE_CH1 = &H9600
    ECHO_BYTE_CH128 = &H967F
    HONDA_DIAGH_CH1 = &H9680
    HONDA_DIAGH_CH128 = &H96FF
    J1939_CH1 = &H9700
    J1939_CH128 = &H977F
    J1708_CH1 = &H9780
    J1708_CH128 = &H97FF
    TP2_0_CH1 = &H9800
    TP2_0_CH128 = &H987F

    ANALOG_IN_1 = &HC000
    ANALOG_IN_32 = &HC01F

    LIN = LINProtocolId.LIN
End Enum

Public Enum J2534IoCtlId As UInteger
    GET_CONFIG = &H1
    SET_CONFIG = &H2
    READ_VBATT = &H3
    FIVE_BAUD_INIT = &H4
    FAST_INIT = &H5
    CLEAR_TX_BUFFER = &H7
    CLEAR_RX_BUFFER = &H8
    CLEAR_PERIODIC_MSGS = &H9
    CLEAR_MSG_FILTERS = &HA
    CLEAR_FUNCT_MSG_LOOKUP_TABLE = &HB
    ADD_TO_FUNCT_MSG_LOOKUP_TABLE = &HC
    DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE = &HD
    READ_PROG_VOLTAGE = &HE
    ' J2534-2
    SW_CAN_HS = &H8000 '-2
    SW_CAN_NS = &H8001 '-2
    SET_POLL_RESPONSE = &H8002
    BECOME_MASTER = &H8003
    START_REPEAT_MESSAGE = &H8004
    QUERY_REPEAT_MESSAGE = &H8005
    STOP_REPEAT_MESSAGE = &H8006
    GET_DEVICE_CONFIG = &H8007
    SET_DEVICE_CONFIG = &H8008
    PROTECT_J1939_ADDR = &H8009
    REQUEST_CONNECTION = &H800A
    TEARDOWN_CONNECTION = &H800B
    GET_DEVICE_INFO = &H800C
    GET_PROTOCOL_INFO = &H800D
End Enum

Public Enum J2534ConfigParameterId As UInteger
    DATA_RATE = &H1
    LOOPBACK = &H3
    NODE_ADDRESS = &H4
    NETWORK_LINE = &H5
    P1_MIN = &H6 'Don't use
    P1_MAX = &H7
    P2_MIN = &H8 'Don't use
    P2_MAX = &H9 'Don't use
    P3_MIN = &HA
    P3_MAX = &HB 'Don't use
    P4_MIN = &HC
    P4_MAX = &HD 'Don't use
    W1 = &HE
    W2 = &HF
    W3 = &H10
    W4 = &H11
    W5 = &H12
    TIDLE = &H13
    TINIL = &H14
    TWUP = &H15
    PARITY = &H16
    BIT_SAMPLE_POINT = &H17
    SYNC_JUMP_WIDTH = &H18
    W0 = &H19
    T1_MAX = &H1A
    T2_MAX = &H1B

    T4_MAX = &H1C
    T5_MAX = &H1D
    ISO15765_BS = &H1E
    ISO15765_STMIN = &H1F
    '----------------------------
    DATA_BITS = &H20
    FIVE_BAUD_MOD = &H21
    BS_TX = &H22
    STMIN_TX = &H23
    T3_MAX = &H24
    ISO15765_WFT_MAX = &H25

    CAN_MIXED_FORMAT = &H8000
    SW_CAN_HS_DATA_RATE = &H8010
    SW_CAN_SPEEDCHANGE_ENABLE = &H8011
    SW_CAN_RES_SWITCH = &H8012
    ACTIVE_CHANNELS = &H8020
    SAMPLE_RATE = &H8021
    SAMPLES_PER_READING = &H8022
    READINGS_PER_MSG = &H8023
    AVERAGING_METHOD = &H8024
    SAMPLE_RESOLUTION = &H8025
    INPUT_RANGE_LOW = &H8026
    INPUT_RANGE_HIGH = &H8027

    UEB_T0_MIN = &H8028
    UEB_T1_MAX = &H8029
    UEB_T2_MAX = &H802A
    UEB_T3_MAX = &H802B
    UEB_T4_MIN = &H802C
    UEB_T5_MAX = &H802D
    UEB_T6_MAX = &H802E
    UEB_T7_MIN = &H802F
    UEB_T7_MAX = &H8030
    UEB_T9_MIN = &H8031

    ' Pin selection
    J1962_PINS = &H8001 'OBD-II
    J1939_PINS = &H803D 'Newer Heavy Truck
    J1708_PINS = &H803E 'Older Heavy Truck

    J1939_T1 = &H803F
    J1939_T2 = &H8040
    J1939_T3 = &H8041
    J1939_T4 = &H8042
    J1939_BRDCST_MIN_DELAY = &H8043

    TP2_0_T_BR_INT = &H8044
    TP2_0_T_E = &H8045
    TP2_0_MNTC = &H8046
    TP2_0_T_CTA = &H8047
    TP2_0_MNCT = &H8048
    TP2_0_MNTB = &H8049
    TP2_0_MNT = &H804A
    TP2_0_T_WAIT = &H804B
    TP2_0_T1 = &H804C
    TP2_0_T3 = &H804D
    TP2_0_IDENTIFER = &H804E
    TP2_0_RXIDPASSIVE = &H804F

    NON_VOLATILE_STORE_1 = &HC001
    NON_VOLATILE_STORE_2 = &HC002
    NON_VOLATILE_STORE_3 = &HC003
    NON_VOLATILE_STORE_4 = &HC004
    NON_VOLATILE_STORE_5 = &HC005
    NON_VOLATILE_STORE_6 = &HC006
    NON_VOLATILE_STORE_7 = &HC007
    NON_VOLATILE_STORE_8 = &HC008
    NON_VOLATILE_STORE_9 = &HC009
    NON_VOLATILE_STORE_10 = &HC00A

    LIN_VERSION = LINConfigParameterId.LIN_VERSION 'DG
End Enum

Public Enum J2534GetDeviceInfoParameter As UInteger
    SERIAL_NUMBER = &H1
    J1850PWM_SUPPORTED = &H2
    J1850VPW_SUPPORTED = &H3
    ISO9141_SUPPORTED = &H4
    ISO14230_SUPPORTED = &H5
    CAN_SUPPORTED = &H6
    ISO15765_SUPPORTED = &H7
    SCI_A_ENGINE_SUPPORTED = &H8
    SCI_A_TRANS_SUPPORTED = &H9
    SCI_B_ENGINE_SUPPORTED = &HA
    SCI_B_TRANS_SUPPORTED = &HB
    SW_ISO15765_SUPPORTED = &HC
    SW_CAN_SUPPORTED = &HD
    GM_UART_SUPPORTED = &HE
    UART_ECHO_BYTE_SUPPORTED = &HF
    HONDA_DIAGH_SUPPORTED = &H10
    J1939_SUPPORTED = &H11
    J1708_SUPPORTED = &H12
    TP2_0_SUPPORTED = &H13
    J2610_SUPPORTED = &H14
    ANALOG_IN_SUPPORTED = &H15
    MAX_NON_VOLATILE_STORAGE = &H16
    SHORT_TO_GND_J1962 = &H17
    PGM_VOLTAGE_J1962 = &H18
    J1850PWM_PS_J1962 = &H19
    J1850VPW_PS_J1962 = &H1A
    ISO9141_PS_K_LINE_J1962 = &H1B
    ISO14230_PS_K_LINE_J1962 = &H1C
    ISO9141_PS_L_LINE_J1962 = &H1D
    ISO14230_PS_L_LINE_J1962 = &H1E
    CAN_PS_J1962 = &H1F
    ISO15765_PS_J1962 = &H20
    SW_CAN_PS_J1962 = &H21
    SW_ISO15765_PS_J1962 = &H22
    GM_UART_PS_J1962 = &H23
    UART_ECHO_BYTE_PS_J1962 = &H24
    HONDA_DIAGH_PS_J1962 = &H25
    J1939_PS_J1962 = &H26
    J1708_PS_J1962 = &H27
    TP2_0_PS_J1962 = &H28
    J2610_PS_J1962 = &H29
    J1939_PS_J1939 = &H2A
    J1708_PS_J1939 = &H2B
    ISO9141_PS_K_LINE_J1939 = &H2C
    ISO9141_PS_L_LINE_J1939 = &H2D
    ISO14230_PS_K_LINE_J1939 = &H2E
    ISO14230_PS_L_LINE_J1939 = &H2F
    J1708_PS_J1708 = &H30
    FT_CAN_SUPPORTED = &H31
    FT_ISO15765_SUPPORTED = &H32
    FT_CAN_PS_J1962 = &H33
    FT_ISO15765_PS_J1962 = &H34
    J1850PWM_SIMULTANEOUS = &H35
    J1850VPW_SIMULTANEOUS = &H36
    ISO9141_SIMULTANEOUS = &H37
    ISO14230_SIMULTANEOUS = &H38
    CAN_SIMULTANEOUS = &H39
    ISO15765_SIMULTANEOUS = &H3A
    SCI_A_ENGINE_SIMULTANEOUS = &H3B
    SCI_A_TRANS_SIMULTANEOUS = &H3C
    SCI_B_ENGINE_SIMULTANEOUS = &H3D
    SCI_B_TRANS_SIMULTANEOUS = &H3E
    SW_ISO15765_SIMULTANEOUS = &H3F
    SW_CAN_SIMULTANEOUS = &H40
    GM_UART_SIMULTANEOUS = &H41
    UART_ECHO_BYTE_SIMULTANEOUS = &H42
    HONDA_DIAGH_SIMULTANEOUS = &H43
    J1939_SIMULTANEOUS = &H44
    J1708_SIMULTANEOUS = &H45
    TP2_0_SIMULTANEOUS = &H46
    J2610_SIMULTANEOUS = &H47
    ANALOG_IN_SIMULTANEOUS = &H48
    PART_NUMBER = &H49
    FT_CAN_SIMULTANEOUS = &H4A
    FT_ISO15765_SIMULTANEOUS = &H4B
End Enum

Public Enum J2534GetProtocolInfoParameter As UInteger
    MAX_RX_BUFFER_SIZE = &H1
    MAX_PASS_FILTER = &H2
    MAX_BLOCK_FILTER = &H3
    MAX_FILTER_MSG_LENGTH = &H4
    MAX_PERIODIC_MSGS = &H5
    MAX_PERIODIC_MSG_LENGTH = &H6
    DESIRED_DATA_RATE = &H7
    MAX_REPEAT_MESSAGING = &H8
    MAX_REPEAT_MESSAGING_LENGTH = &H9
    NETWORK_LINE_SUPPORTED = &HA
    MAX_FUNCT_MSG_LOOKUP = &HB
    PARITY_SUPPORTED = &HC
    DATA_BITS_SUPPORTED = &HD
    FIVE_BAUD_MOD_SUPPORTED = &HE
    L_LINE_SUPPORTED = &HF
    CAN_11_29_IDS_SUPPORTED = &H10
    CAN_MIXED_FORMAT_SUPPORTED = &H11
    MAX_FLOW_CONTROL_FILTER = &H12
    MAX_ISO15765_WFT_MAX = &H13
    MAX_AD_ACTIVE_CHANNELS = &H14
    MAX_AD_SAMPLE_RATE = &H15
    MAX_AD_SAMPLES_PER_READING = &H16
    AD_SAMPLE_RESOLUTION = &H17
    AD_INPUT_RANGE_LOW = &H18
    AD_INPUT_RANGE_HIGH = &H19
    RESOURCE_GROUP = &H1A
    TIMESTAMP_RESOLUTION = &H1B
End Enum

Public Enum J2534Result As Integer
    STATUS_NOERROR = &H0
    ERR_NOT_SUPPORTED = &H1
    ERR_INVALID_CHANNEL_ID = &H2
    ERR_INVALID_PROTOCOL_ID = &H3
    ERR_NULL_PARAMETER = &H4
    ERR_INVALID_IOCTL_VALUE = &H5
    ERR_INVALID_FLAGS = &H6
    ERR_FAILED = &H7
    ERR_DEVICE_NOT_CONNECTED = &H8
    ERR_TIMEOUT = &H9
    ERR_INVALID_MSG = &HA
    ERR_INVALID_TIME_INTERVAL = &HB
    ERR_EXCEEDED_LIMIT = &HC
    ERR_INVALID_MSG_ID = &HD
    ERR_DEVICE_IN_USE = &HE
    ERR_INVALID_IOCTL_ID = &HF
    ERR_BUFFER_EMPTY = &H10
    ERR_BUFFER_FULL = &H11
    ERR_BUFFER_OVERFLOW = &H12
    ERR_PIN_INVALID = &H13
    ERR_CHANNEL_IN_USE = &H14
    ERR_MSG_PROTOCOL_ID = &H15
    ERR_INVALID_FILTER_ID = &H16
    ERR_NO_FLOW_CONTROL = &H17
    ERR_NOT_UNIQUE = &H18
    ERR_INVALID_BAUDRATE = &H19
    ERR_INVALID_DEVICE_ID = &H1A

    ERR_INVALID_IOCTL_PARAM_ID = &H1E
    ERR_VOLTAGE_IN_USE = &H1F
    ERR_PIN_IN_USE = &H20

    ERR_ADDRESS_NOT_CLAIMED = &H10000
    ERR_NO_CONNECTION_ESTABLISHED = &H10001
    ERR_RESOURCE_IN_USE = &H10002
End Enum

Public Enum J2534VoltageValue As UInteger
    SHORT_TO_GROUND = &HFFFFFFFEUI
    VOLTAGE_OFF = &HFFFFFFFFUI
End Enum

Public Enum J2534Parity As UInteger
    NO_PARITY = 0
    ODD_PARITY = 1
    EVEN_PARITY = 2
End Enum

Public Enum J2534MixedMode As UInteger
    CAN_MIXED_FORMAT_OFF = 0
    CAN_MIXED_FORMAT_ON = 1
    CAN_MIXED_FORMAT_ALL_FRAMES = 2 ' Not supported
End Enum

Public Enum J2534DiscoveryResult As UInteger
    NOT_SUPPORTED = 0
    SUPPORTED = 1
End Enum

Public Enum J2534AnalogAveraging As UInteger
    SIMPLE_AVERAGE = &H0 ' Simple arithmetic mean
    MAX_LIMIT_AVERAGE = &H1 ' Choose the biggest value
    MIN_LIMIT_AVERAGE = &H2 ' Choose the lowest value
    MEDIAN_AVERAGE = &H3 ' Choose the arithmetic median
End Enum

<Flags>
Public Enum J2534ConnectFlags As UInteger
    CAN_29BIT_ID = &H100
    ISO9141_NO_CHECKSUM = &H200
    NO_CHECKSUM = &H200
    CHECKSUM_DISABLED = &H200
    CAN_ID_BOTH = &H800
    ISO9141_K_LINE_ONLY = &H1000
End Enum

<Flags>
Public Enum J2534RxStatusFlags As UInteger
    TX_MSG_TYPE = &H1
    START_OF_MESSAGE = &H2
    RX_BREAK = &H4
    TX_INDICATION = &H8
    ISO15765_PADDING_ERROR = &H10
    ISO15765_ADDR_TYPE = &H80
    CAN_29BIT_ID = &H100&
    SW_CAN_HV_RX = &H10000
    SW_CAN_HS_RX = &H20000
    SW_CAN_NS_RX = &H40000
    OVERFLOW = &H10000
    CONNECTION_ESTABLISHED = &H10000
    CONNECTION_LOST = &H20000
End Enum

<Flags>
Public Enum J2534TxFlags As UInteger
    ISO15765_FRAME_PAD = &H40
    ISO15765_ADDR_TYPE = &H80
    CAN_29BIT_ID = &H100
    WAIT_P3_MIN_ONLY = &H200
    SW_CAN_HV_TX = &H400
    SCI_MODE = &H400000
    SCI_TX_VOLTAGE = &H800000
End Enum

Public Enum J2534FilterType As UInteger
    NO_FILTER = 0
    PASS_FILTER = &H1
    BLOCK_FILTER = &H2
    FLOW_CONTROL_FILTER = &H3
End Enum

<StructLayout(LayoutKind.Sequential, Pack:=1)>
Public Structure PASSTHRU_MSG
    Public ProtocolId As J2534ProtocolId
    Public RxStatus As UInt32
    Public TxFlags As UInt32
    Public Timestamp As UInt32
    Public DataSize As UInt32
    Public ExtraDataIndex As UInt32
    <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4128)>
    Public Data As Byte()

    Public Shared Function CreateInstance() As PASSTHRU_MSG
        Dim result As New PASSTHRU_MSG()
        ReDim result.Data(4127)
        Array.Clear(result.Data, 0, 4128)
        Return result
    End Function

    Public Function Clone() As PASSTHRU_MSG
        Dim result As PASSTHRU_MSG = Me
        If Data Is Nothing OrElse Data.Length <> 4128 Then ReDim Data(4127)
        Array.Copy(Me.Data, result.Data, 4128)
        Return result
    End Function

    Public Sub Clear()
        ProtocolId = 0
        RxStatus = 0
        TxFlags = 0
        Timestamp = 0
        DataSize = 0
        ExtraDataIndex = 0
        If Data Is Nothing OrElse Data.Length <> 4128 Then ReDim Data(4127)
        Array.Clear(Data, 0, 4128)
    End Sub

End Structure

<StructLayout(LayoutKind.Sequential)>
Public Structure SCONFIG
    Dim Parameter As UInt32
    Dim Value As UInt32
End Structure

<StructLayout(LayoutKind.Sequential)>
Public Structure SCONFIG_LIST
    Dim NumOfParams As UInt32
    Dim ConfigPtr As Int32
End Structure

<StructLayout(LayoutKind.Sequential)>
Public Structure SBYTE_ARRAY
    Dim NumOfBytes As UInt32
    Dim BytePtr As Int32
End Structure

'This is a plain J2534-2 SPARAM. see J2534Misc.SPARAM for a better version
'<StructLayout(LayoutKind.Sequential)>
'Public Structure SPARAM
'    Dim Parameter As UInt32
'    Dim Value As UInt32
'    Dim Supported As UInt32
'End Structure

<StructLayout(LayoutKind.Sequential)>
Public Structure SPARAM_LIST
    Dim NumOfParams As UInt32
    Dim ParamPtr As Int32
End Structure





