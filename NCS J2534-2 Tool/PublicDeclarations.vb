﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Module PublicDeclarations
    Public Const betaVersion As Integer = 0
    Public Const IgnoreCase As StringComparison = StringComparison.OrdinalIgnoreCase
End Module

Public NotInheritable Class KeyChars
    Public Const Back As Char = Chr(8)
    Public Const Copy As Char = Chr(3)
    Public Const Paste As Char = Chr(22)
    Public Const Cut As Char = Chr(24)
    Public Const Undo As Char = Chr(26)
    Public Const Escape As Char = Chr(27)
    Public Const Cr As Char = Chr(13)
    Public Const Lf As Char = Chr(10)
    Private Sub New()
    End Sub
End Class

Public Class InputBoxResponse
    Public Property Response As String = String.Empty
    Public Property Result As DialogResult
End Class