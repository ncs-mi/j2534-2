﻿' NCS J2534-2 Tool
' Copyright © 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901
'
Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports NCS_J2534_2_Tool.J2534ProtocolId

Public Class J2534Channel
    Public Property ChannelId As UInteger
    Public Property Connected As Boolean
    Public Property BaudRate As UInteger
    Public Property ConnectFlags As UInteger
    Public Property MixedMode As Boolean
    Public Property MaxRXBufferSize As UInteger
    Public Property MaxPassFilter As UInteger
    Public Property MaxBlockFilter As UInteger
    Public Property MaxFlowFilter As UInteger
    Public Property MaxPeriodicMessage As UInteger
    Public Property FiveBaudMod As UInteger
    Public Property Name As String
    Public Property ProtocolId As J2534ProtocolId
    Public Property BaudRates As UInteger() = {}
    Public Property DefaultBaudRateIndex As Integer
    Public Property DefaultTxFlags As J2534TxFlags
    Public Property DefaultConnectFlags As J2534ConnectFlags
    Public Property SelectedPins As UInteger
    Public Property Connector As UInteger
    Public Property J1962Pins As UInteger() = {}
    Public Property J1939Pins As UInteger() = {}
    Public Property J1708Pins As UInteger() = {}
    Public Property ConfParameter As J2534ConfigParameter() = {}
    Public Property AnalogSubsystem As J2534AnalogSubsystem
    Public Property FunctionalMessage As J2534FunctionalMessage()
    Public Property Filter As J2534Filter()
    Public Property PeriodicMessage As J2534PeriodicMessage()
    Public ReadOnly Property PeriodicMessagesExist() As Boolean
        Get
            Dim result As Boolean = False
            If _PeriodicMessage Is Nothing Then Return result
            For i As Integer = 0 To MAX_PM
                If Not _PeriodicMessage(i).IsEmpty Then result = True
            Next i
            Return result
        End Get
    End Property
    Public ReadOnly Property FiltersExist() As Boolean
        Get
            Dim result As Boolean = False
            If _Filter Is Nothing Then Return result
            For i As Integer = 0 To MAX_FILTER
                If Not _Filter(i).IsEmpty Then result = True
            Next i
            Return result
        End Get
    End Property
    Public ReadOnly Property ParameterIndex(ByVal parameterId As UInteger) As Integer
        Get
            Dim result As Integer = -1
            For i As Integer = 0 To _ConfParameter.Length - 1
                If parameterId = _ConfParameter(i).Id Then
                    result = i
                    Exit For
                End If
            Next i
            Return result
        End Get
    End Property
    Public ReadOnly Property NextFilter As Integer
        Get
            Dim result As Integer = -1
            If _IsAnalog Then Return result
            For i As Integer = 0 To MAX_FILTER
                If Not _Filter(i).Enabled Then
                    If _Filter(i).PatternMessage.DataSize = 0 AndAlso
                       _Filter(i).MaskMessage.DataSize = 0 AndAlso
                       _Filter(i).FlowMessage.DataSize = 0 AndAlso
                       _Filter(i).FilterType = 0 Then
                        result = i
                        Exit For
                    End If
                End If
            Next i
            Return result
        End Get
    End Property
    Public ReadOnly Property NextPeriodicMessage As Integer
        Get
            Dim result As Integer = -1
            If _IsAnalog Then Return result
            For i As Integer = 0 To MAX_PM
                If Not _PeriodicMessage(i).Enabled Then
                    If _PeriodicMessage(i).Message.DataSize = 0 Then
                        result = i
                        Exit For
                    End If
                End If
            Next i
            Return result
        End Get
    End Property
    Public ReadOnly Property IsISO15765 As Boolean
    Public ReadOnly Property IsCAN As Boolean
    Public ReadOnly Property IsJ1708 As Boolean
    Public ReadOnly Property IsJ1939 As Boolean
    Public ReadOnly Property IsISO9141 As Boolean
    Public ReadOnly Property IsISO14230 As Boolean
    Public ReadOnly Property IsJ1850VPW As Boolean
    Public ReadOnly Property IsJ1850PWM As Boolean
    Public ReadOnly Property IsTP2_0 As Boolean
    Public ReadOnly Property IsSingleWire As Boolean
    Public ReadOnly Property IsFaultTolerant As Boolean
    Public ReadOnly Property IsPinSwitched As Boolean
    Public ReadOnly Property IsCHx As Boolean
    Public ReadOnly Property IsAnalog As Boolean

    <DebuggerStepThrough>
    Public Overrides Function ToString() As String
        Return _Name
    End Function

    Public Sub New(id As J2534ProtocolId)
        _ProtocolId = id
        _Name = ProtocolIdToName(Me.ProtocolId)
        Select Case _ProtocolId

            Case J1850VPW, J1850VPW_PS, J1850VPW_CH1 To J1850VPW_CH128
                _BaudRates = J2534BaudRates.J1850VPW()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.J1850VPW()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsJ1850VPW = True

            Case ISO9141, ISO9141_PS, ISO9141_CH1 To ISO9141_CH128
                _BaudRates = J2534BaudRates.ISO9141_14230()
                _DefaultBaudRateIndex = 5
                _ConfParameter = J2534ConfigParameters.ISO9141()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsISO9141 = True

            Case ISO14230, ISO14230_PS, ISO14230_CH1 To ISO14230_CH128
                _BaudRates = J2534BaudRates.ISO9141_14230()
                _DefaultBaudRateIndex = 5
                _ConfParameter = J2534ConfigParameters.ISO14230()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsISO14230 = True

            Case J1850PWM, J1850PWM_PS, J1850PWM_CH1 To J1850PWM_CH128
                _BaudRates = J2534BaudRates.J1850PWM()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.J1850PWM()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsJ1850PWM = True
                ReDim _FunctionalMessage(MAX_FUNC_MSG)
                For i As Integer = 0 To MAX_FUNC_MSG
                    _FunctionalMessage(i) = New J2534FunctionalMessage
                Next i

            Case CAN, CAN_PS, CAN_CH1 To CAN_CH128
                _BaudRates = J2534BaudRates.CAN_ISO()
                _DefaultBaudRateIndex = 7
                _ConfParameter = J2534ConfigParameters.CAN()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsCAN = True

            Case SW_CAN_PS, SW_CAN_CAN_CH1 To SW_CAN_CAN_CH128
                _BaudRates = J2534BaudRates.SW_CAN_ISO()
                _DefaultBaudRateIndex = 1
                _ConfParameter = J2534ConfigParameters.SW_CAN()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsCAN = True
                _IsSingleWire = True

            Case FT_CAN_PS, FT_CAN_CH1 To FT_CAN_CH128
                _BaudRates = J2534BaudRates.FT_CAN_ISO()
                _DefaultBaudRateIndex = 2
                _ConfParameter = J2534ConfigParameters.FT_CAN()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsCAN = True
                _IsFaultTolerant = True

            Case ISO15765, ISO15765_PS, ISO15765_CH1 To ISO15765_CH128
                _BaudRates = J2534BaudRates.CAN_ISO()
                _DefaultBaudRateIndex = 7
                _ConfParameter = J2534ConfigParameters.ISO15765()
                _DefaultTxFlags = J2534TxFlags.ISO15765_FRAME_PAD
                _DefaultConnectFlags = &H0
                _IsISO15765 = True

            Case SW_ISO15765_PS, SW_CAN_ISO15765_CH1 To SW_CAN_ISO15765_CH128
                _BaudRates = J2534BaudRates.SW_CAN_ISO()
                _DefaultBaudRateIndex = 1
                _ConfParameter = J2534ConfigParameters.SW_ISO15765()
                _DefaultTxFlags = J2534TxFlags.ISO15765_FRAME_PAD
                _DefaultConnectFlags = &H0
                _IsISO15765 = True
                _IsSingleWire = True

            Case FT_ISO15765_PS, FT_ISO15765_CH1 To FT_ISO15765_CH128
                _BaudRates = J2534BaudRates.FT_CAN_ISO()
                _DefaultBaudRateIndex = 2
                _ConfParameter = J2534ConfigParameters.FT_ISO15765()
                _DefaultTxFlags = J2534TxFlags.ISO15765_FRAME_PAD
                _DefaultConnectFlags = &H0
                _IsISO15765 = True
                _IsFaultTolerant = True

            Case SCI_A_ENGINE, SCI_A_TRANS, SCI_B_ENGINE, SCI_B_TRANS
                _BaudRates = J2534BaudRates.J2610()
                _DefaultBaudRateIndex = 2
                _ConfParameter = J2534ConfigParameters.J2610()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0

            Case J2610_PS, J2610_CH1 To J2610_CH128
                _BaudRates = J2534BaudRates.J2610()
                _DefaultBaudRateIndex = 2
                _ConfParameter = J2534ConfigParameters.J2610()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0

            Case GM_UART_PS, GM_UART_CH1 To GM_UART_CH128
                _BaudRates = J2534BaudRates.GM_UART()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.GM_UART()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0

            Case UART_ECHO_BYTE_PS, ECHO_BYTE_CH1 To ECHO_BYTE_CH128
                _BaudRates = J2534BaudRates.UART_ECHO_BYTE()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.UART_ECHO_BYTE()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0

            Case HONDA_DIAGH_PS, HONDA_DIAGH_CH1 To HONDA_DIAGH_CH128
                _BaudRates = J2534BaudRates.HONDA_DIAGH()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.HONDA_DIAGH()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0

            Case J1939_PS, J1939_CH1 To J1939_CH128
                _BaudRates = J2534BaudRates.J1939()
                _DefaultBaudRateIndex = 1
                _ConfParameter = J2534ConfigParameters.J1939()
                _DefaultTxFlags = J2534TxFlags.CAN_29BIT_ID
                _DefaultConnectFlags = J2534ConnectFlags.CAN_29BIT_ID
                _IsJ1939 = True

            Case J1708_PS, J1708_CH1 To J1708_CH128
                _BaudRates = J2534BaudRates.J1708()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.J1708()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsJ1708 = True

            Case TP2_0_PS, TP2_0_CH1 To TP2_0_CH128
                _BaudRates = J2534BaudRates.TP2_0()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.TP2_0()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsTP2_0 = True

            Case ANALOG_IN_1 To ANALOG_IN_32
                _BaudRates = {}
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.Analog()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0
                _IsAnalog = True

            Case LIN
                _BaudRates = J2534BaudRates.LIN()
                _DefaultBaudRateIndex = 0
                _ConfParameter = J2534ConfigParameters.LIN()
                _DefaultTxFlags = &H0
                _DefaultConnectFlags = &H0

        End Select

        Select Case _ProtocolId
            Case J1850VPW_PS To FT_ISO15765_PS
                _IsPinSwitched = True

            Case CAN_CH1 To TP2_0_CH128
                _IsCHx = True

        End Select

        ReDim _Filter(MAX_FILTER)
        For i As Integer = 0 To MAX_FILTER
            _Filter(i) = New J2534Filter
            _Filter(i).FlowMessage.TxFlags = _DefaultTxFlags
        Next i

        ReDim _PeriodicMessage(MAX_PM)
        For i As Integer = 0 To MAX_PM
            _PeriodicMessage(i) = New J2534PeriodicMessage
            _PeriodicMessage(i).Message.TxFlags = _DefaultTxFlags
        Next i


    End Sub
End Class
