' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

Option Compare Text
Option Explicit On
Option Infer Off
Option Strict On

Imports System.ComponentModel
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Xml
Imports NCS_J2534_2_Tool.J2534ProtocolId
Imports NCS_J2534_2_Tool.J2534IoCtlId
Imports NCS_J2534_2_Tool.J2534ConfigParameterId


Public Class FormJ2534
    Private selectedDevice As J2534Device
    Private settingPeriodicMessages As Boolean
    Private settingFilters As Boolean
    Private settingTextBoxAnalogRate As Boolean
    Private settingTextBoxActiveChannelConfig As Boolean
    Private WithEvents TextBoxAnalogRateConfig As TextBox
    Private WithEvents TextBoxActiveChannelConfig As TextBox
    Private configChannels As UInteger
    Private configRate As UInteger
    Private PeriodicMessageCheckBoxHit(MAX_PM) As Boolean

    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim title As String = My.Application.Info.ProductName & " version " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor.ToString("00")
        If betaVersion > 0 Then title &= " (�" & betaVersion & ") "
        Me.Text = title
        SetControlArrays()
        SetArrays()
        SetMiscControls()
        ClearFilterControls()
        ClearPeriodicMessageControls()
        SetConfigControls()
        SetInitControls()
        SetFunctionalMessageLookupTableControls()
        InitComboBoxAPI()
        InitializeEditMenu()
        FileMenuInit()
        UpdateComboBoxAvailableDevice()
        SetVoltControls()
        UpdateTabs()
        ComboBoxDevice.Enabled = False
        LabelComboDevice.Enabled = ComboBoxDevice.Enabled
        TabControl1.SelectedIndex = 0
        MyBase.OnLoad(e)
    End Sub

    Protected Overrides Sub OnClosed(e As EventArgs)
        For Each device As J2534Device In J2534Devices.Values
            If device.Connected Then CloseJ2534Device(device)
        Next device
        For Each API As J2534_API In J2534_APIs
            API.Dispose()
        Next API
        MyBase.OnClosed(e)
    End Sub

    Private Sub SetMiscControls()
        ComboBoxDevice.Visible = True
        ComboBoxDevice.Text = String.Empty
        TextBoxReadRate.Text = String.Empty
        TextBoxTimeOut.Text = "0"
        TextBoxMessageOut.Text = String.Empty
        TextBoxOutFlags.Text = String.Empty
        TextBoxConnectFlags.Text = String.Empty
        TextBoxConnectFlags.Enabled = False
        LabelConnectFlags.Enabled = False
        For i As Integer = 0 To MAX_FILTER
            ComboBoxFilterType(i).Items.Add("None")
            ComboBoxFilterType(i).Items.Add("Pass")
            ComboBoxFilterType(i).Items.Add("Block")
            ComboBoxFilterType(i).Items.Add("Flow")
            ComboBoxFilterType(i).SelectedIndex = 0
        Next i
        SetMessageControls()
        LabelJ2534Info.Text = String.Empty
        LabelProtSupport.Text = String.Empty
    End Sub

    Private Sub UpdateDevice()
        Dim newDevice As J2534Device = TryCast(ComboBoxAvailableDevice.SelectedItem, J2534Device)
        If selectedDevice IsNot Nothing AndAlso selectedDevice IsNot newDevice Then HandleOutgoingDevice(selectedDevice)
        UpdateDeviceSetup(selectedDevice)
        If newDevice IsNot Nothing AndAlso selectedDevice IsNot newDevice Then
            HandleIncomingDevice(newDevice)
        End If
        selectedDevice = newDevice
        If newDevice IsNot Nothing Then
            ToolStripStatusLabel1.Text = "In: " & newDevice.MessageInCount & "     Out: " & newDevice.MessageOutCount
        Else
            ToolStripStatusLabel1.Text = String.Empty
        End If
        UpdateMessageControls()
        UpdateConnectControls()
        UpdateComboBoxAvailableChannel(newDevice.Setup.SelectedChannelName)
    End Sub

    Private Sub HandleOutgoingDevice(ByVal device As J2534Device)
        If device IsNot Nothing Then
            device.ListViewMessageIn.Visible = False
            RemoveHandler device.ListViewMessageIn.MouseMove, AddressOf ListViewMessageIn_MouseMove
            RemoveHandler device.ListViewMessageIn.MouseLeave, AddressOf ListViewMessageIn_MouseLeave
            RemoveHandler device.ListViewMessageIn.SizeChanged, AddressOf ListViewMessageIn_SizeChanged
            device.ListViewMessageIn.ContextMenuStrip = Nothing
            If device.ListViewMessageIn.Parent Is TabPageMessages Then
                TabPageMessages.Controls.Remove(device.ListViewMessageIn)
            End If

            device.ListViewMessageOut.Visible = False
            RemoveHandler device.ListViewMessageOut.Leave, AddressOf ListViewMessageOut_Leave
            RemoveHandler device.ListViewMessageOut.SelectedIndexChanged, AddressOf ListViewMessageOut_SelectedIndexChanged
            RemoveHandler device.ListViewMessageOut.Enter, AddressOf ListViewMessageOut_Enter
            RemoveHandler device.ListViewMessageOut.SizeChanged, AddressOf ListViewMessageOut_SizeChanged
            device.ListViewMessageOut.ContextMenuStrip = Nothing
            If device.ListViewMessageOut.Parent Is TabPageMessages Then
                TabPageMessages.Controls.Remove(device.ListViewMessageOut)
            End If
        End If
        UpdateDeviceInfo(device)
        UpdateComboBoxConnectChannel(Nothing)
    End Sub

    Private Sub UpdateDeviceSetup(ByVal device As J2534Device)
        If device IsNot Nothing Then
            Integer.TryParse(TextBoxReadRate.Text, device.Setup.MessageReadRate)
            device.Setup.SelectedChannelName = ComboBoxAvailableChannel.Text
            device.Setup.MessageChannelName = ComboBoxMessageChannel.Text
            device.Setup.ConnectChannelName = ComboBoxConnectChannel.Text
            device.Setup.TextBoxOutFlags = TextBoxOutFlags.Text
            device.Setup.TextBoxMessageOut = TextBoxMessageOut.Text
        End If
    End Sub

    Private Sub HandleIncomingDevice(ByVal device As J2534Device)
        If device.ListViewMessageIn.Parent Is Nothing Then
            TabPageMessages.Controls.Add(device.ListViewMessageIn)
        End If
        device.ListViewMessageIn.Visible = True
        device.ListViewMessageIn.Enabled = True
        AddHandler device.ListViewMessageIn.MouseMove, AddressOf ListViewMessageIn_MouseMove
        AddHandler device.ListViewMessageIn.MouseLeave, AddressOf ListViewMessageIn_MouseLeave
        AddHandler device.ListViewMessageIn.SizeChanged, AddressOf ListViewMessageIn_SizeChanged
        device.ListViewMessageIn.Location = LvInLocator.Location
        device.ListViewMessageIn.Size = LvInLocator.Size
        device.ListViewMessageIn.BringToFront()
        device.ListViewMessageIn.ContextMenuStrip = ContextMenuStripMessageIn

        If device.ListViewMessageOut.Parent Is Nothing Then
            TabPageMessages.Controls.Add(device.ListViewMessageOut)
        End If
        device.ListViewMessageOut.Visible = True
        device.ListViewMessageOut.Enabled = True
        AddHandler device.ListViewMessageOut.Leave, AddressOf ListViewMessageOut_Leave
        AddHandler device.ListViewMessageOut.SelectedIndexChanged, AddressOf ListViewMessageOut_SelectedIndexChanged
        AddHandler device.ListViewMessageOut.Enter, AddressOf ListViewMessageOut_Enter
        AddHandler device.ListViewMessageOut.SizeChanged, AddressOf ListViewMessageOut_SizeChanged
        device.ListViewMessageOut.Location = LvOutLocator.Location
        device.ListViewMessageOut.Size = LvOutLocator.Size
        device.ListViewMessageOut.BringToFront()
        device.ListViewMessageOut.ContextMenuStrip = ContextMenuStripMessageOut

        TextBoxReadRate.Text = device.Setup.MessageReadRate.ToString("000")
        UpdateComboBoxAvailableChannel(device.Setup.SelectedChannelName)
        UpdateComboBoxMessageChannel(device.Setup.MessageChannelName)
        TextBoxOutFlags.Text = device.Setup.TextBoxOutFlags
        TextBoxMessageOut.Text = device.Setup.TextBoxMessageOut
        For i As Integer = 0 To RadioButtonPin.Length - 1
            If Not RadioButtonPin(i) Is Nothing Then
                RadioButtonPin(i).Tag = device.Analog.Pin(i).Mode
                RadioButtonPin(i).Checked = device.Analog.Pin(i).Selected
            End If
        Next i
        UpdateComboBoxConnectChannel(device)
        UpdateDeviceInfo(device)
        UpdateDeviceLabels()
        SetVoltControls()
        For Each box As KeyValuePair(Of Integer, J2534Device) In J2534Devices
            If box.Value IsNot device Then
                box.Value.ListViewMessageIn.Visible = False
                box.Value.ListViewMessageOut.Visible = False
            End If
        Next box
    End Sub

    Private Shared Function GetAvailableDeviceIndex() As Integer
        Dim result As Integer = Integer.MaxValue
        For i As Integer = 1 To Integer.MaxValue
            If Not J2534Devices.ContainsKey(i) Then
                result = i
                Exit For
            End If
            If i = Integer.MaxValue Then Exit For
        Next i
        Return result
    End Function

    Private Shared Sub RemoveBox(ByVal device As J2534Device)
        If Not J2534Devices.Values.Contains(device) Then Return
        Dim index As Integer = -1
        For Each kvp As KeyValuePair(Of Integer, J2534Device) In J2534Devices
            If kvp.Value Is device Then
                index = kvp.Key
                Exit For
            End If
        Next kvp
        J2534Devices.TryRemove(index, Nothing)
        device.Dispose()
    End Sub

    Private Sub ButtonOpenBox_Click(sender As Object, e As EventArgs) Handles ButtonOpenBox.Click
        If ComboBoxAPI.SelectedItem Is Nothing Then Return
        Dim item As J2534_API = TryCast(ComboBoxAPI.SelectedItem, J2534_API)
        If item Is Nothing Then Return
        If ComboBoxDevice.Text.Length > 0 Then
            Dim index As Integer = GetAvailableDeviceIndex()
            Dim device As New J2534Device With {.Name = ComboBoxDevice.Text, .API = item}
            If J2534Devices.TryAdd(index, device) Then
                If OpenJ2534Device(device) Then
                    UpdateComboBoxAvailableDevice()
                    UpdateComboBoxDevice()
                    ComboBoxAvailableDevice.Text = ComboBoxDevice.Text
                    UpdateDevice()
                End If
            End If
        End If
    End Sub

    Private Sub ButtonCloseBox_Click(sender As Object, e As EventArgs) Handles ButtonCloseBox.Click
        Dim device As J2534Device = NameToDevice(ComboBoxDevice.Text)
        If device IsNot Nothing Then CloseJ2534Device(device)
        UpdateComboBoxAvailableDevice()
        UpdateComboBoxDevice()
    End Sub

    Private Sub ButtonConnect_Click(sender As Object, e As EventArgs) Handles ButtonConnect.Click
        Dim options As New J2534ConnectOptions
        Dim pinsetIndex As Integer
        options.Channel = TryCast(ComboBoxConnectChannel.SelectedItem, J2534Channel)
        If options.Channel Is Nothing Then Return
        UInteger.TryParse(ComboBoxBaudRate.Text, options.BaudRate)
        options.ConnectFlags = DirectCast(TextToFlags(TextBoxConnectFlags.Text), J2534ConnectFlags)
        options.MixedMode = CheckBoxMixedMode.Checked
        If options.Channel.IsPinSwitched Then
            pinsetIndex = ComboBoxPins.SelectedIndex
            Select Case ComboBoxConnector.Text
                Case "J1962"
                    options.Connector = J1962_PINS
                    If pinsetIndex >= 0 Then options.Pins = options.Channel.J1962Pins(pinsetIndex)

                Case "J1939"
                    options.Connector = J1939_PINS
                    If pinsetIndex >= 0 Then options.Pins = options.Channel.J1939Pins(pinsetIndex)

                Case "J1708"
                    options.Connector = J1708_PINS
                    If pinsetIndex >= 0 Then options.Pins = options.Channel.J1708Pins(pinsetIndex)

            End Select

        End If
        ConnectChannel(selectedDevice, options)
        ComboBoxConnectChannel.Focus()
    End Sub

    Private Sub ButtonDisconnect_Click(sender As Object, e As EventArgs) Handles ButtonDisconnect.Click
        Dim channel As J2534Channel = TryCast(ComboBoxConnectChannel.SelectedItem, J2534Channel)
        DisconnectChannel(selectedDevice, channel)
    End Sub

    Private Sub ButtonClearRx_Click(sender As Object, e As EventArgs) Handles ButtonClearRx.Click
        If selectedDevice?.API?.PassThruIoctl Is Nothing Then Return
        For Each channel As J2534Channel In selectedDevice.ChannelSet
            If channel.Connected Then
                Dim result As J2534Result = selectedDevice.API.PassThruIoctl(channel.ChannelId, CLEAR_RX_BUFFER, 0, 0)
                ShowResult(selectedDevice, result, "PassThruIoctl - CLEAR_RX_BUFFER")
            End If
        Next channel
    End Sub

    Private Sub ButtonClearTx_Click(sender As Object, e As EventArgs) Handles ButtonClearTx.Click
        If selectedDevice?.API?.PassThruIoctl Is Nothing Then Return
        For Each channel As J2534Channel In selectedDevice.ChannelSet
            If channel.Connected Then
                Dim result As J2534Result = selectedDevice.API.PassThruIoctl(channel.ChannelId, CLEAR_TX_BUFFER, 0, 0)
                ShowResult(selectedDevice, result, "PassThruIoctl - CLEAR_TX_BUFFER")
            End If
        Next channel
    End Sub

    Private Sub ButtonExecute5BInit_Click(sender As Object, e As EventArgs) Handles ButtonExecute5BInit.Click
        If selectedDevice?.API?.PassThruIoctl Is Nothing Then Return
        Dim inputMessage, outputMessage As SBYTE_ARRAY
        Dim ecuAddress(0) As Byte
        Dim keyWord(1) As Byte
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not (channel.IsISO9141 OrElse channel.IsISO14230) Then
            SetInitControls()
            Return
        End If
        ecuAddress(0) = TextBox5BInitECU.Text.ToByte
        inputMessage.NumOfBytes = 1
        Dim ecuAddressPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(ecuAddress(0)) * ecuAddress.Length)
        Marshal.Copy(ecuAddress, 0, ecuAddressPointer, ecuAddress.Length)
        inputMessage.BytePtr = ecuAddressPointer.ToInt32
        Dim inputMessagePointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(inputMessage))
        Marshal.StructureToPtr(inputMessage, inputMessagePointer, True)
        outputMessage.NumOfBytes = 2
        Dim keyWordPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(keyWord(0)) * keyWord.Length)
        Marshal.Copy(keyWord, 0, keyWordPointer, keyWord.Length)
        outputMessage.BytePtr = keyWordPointer.ToInt32
        Dim outputMessagePointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(outputMessage))
        Marshal.StructureToPtr(outputMessage, outputMessagePointer, True)
        Dim result As J2534Result = selectedDevice.API.PassThruIoctl(channel.ChannelId, FIVE_BAUD_INIT, inputMessagePointer.ToInt32, outputMessagePointer.ToInt32)
        Marshal.Copy(keyWordPointer, keyWord, 0, keyWord.Length)
        Marshal.FreeHGlobal(ecuAddressPointer)
        Marshal.FreeHGlobal(inputMessagePointer)
        Marshal.FreeHGlobal(keyWordPointer)
        Marshal.FreeHGlobal(outputMessagePointer)

        ShowResult(selectedDevice, result, "PassThruIoctl - FIVE_BAUD_INIT")
        If result = J2534Result.STATUS_NOERROR Then
            Label5BKeyWord0.Text = "0x" & keyWord(0).ToString("X2")
            Label5BKeyWord1.Text = "0x" & keyWord(1).ToString("X2")
        Else
            Label5BKeyWord0.Text = String.Empty
            Label5BKeyWord1.Text = String.Empty
        End If
    End Sub

    Private Sub ButtonExecuteFastInit_Click(sender As Object, e As EventArgs) Handles ButtonExecuteFastInit.Click
        If selectedDevice?.API?.PassThruIoctl Is Nothing Then Return
        Dim initMessageText As New J2534MessageText

        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not (channel.IsISO9141 OrElse channel.IsISO14230) Then
            SetInitControls()
            Return
        End If
        initMessageText.Data = TextBoxFIMessage.Text
        initMessageText.ProtocolName = ComboBoxAvailableChannel.Text
        initMessageText.TxFlags = TextBoxFIFlags.Text

        Dim inputMessage As PASSTHRU_MSG = initMessageText.ToMessage
        Dim inputPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(inputMessage))
        Marshal.StructureToPtr(inputMessage, inputPointer, True)

        Dim outputMessage As PASSTHRU_MSG = PASSTHRU_MSG.CreateInstance()
        Dim outputPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(outputMessage))
        Marshal.StructureToPtr(outputMessage, outputPointer, True)

        Dim result As J2534Result = selectedDevice.API.PassThruIoctl(channel.ChannelId, FAST_INIT, inputPointer.ToInt32, outputPointer.ToInt32)

        outputMessage = DirectCast(Marshal.PtrToStructure(outputPointer, GetType(PASSTHRU_MSG)), PASSTHRU_MSG)

        Marshal.FreeHGlobal(inputPointer)
        Marshal.FreeHGlobal(outputPointer)

        ShowResult(selectedDevice, result, "PassThruIoctl - FAST_INIT")
        If result = J2534Result.STATUS_NOERROR Then
            Dim respMessageText As J2534MessageText = outputMessage.ToMessageText
            LabelFIResponse.Text = respMessageText.Data
            LabelFIRxStatus.Text = respMessageText.RxStatus
        Else
            LabelFIResponse.Text = String.Empty
            LabelFIRxStatus.Text = String.Empty
        End If

    End Sub

    Private Sub ButtonApplyFilter_Click(sender As Object, e As EventArgs) Handles ButtonApplyFilter.Click
        If selectedDevice?.API?.PassThruStopMsgFilter Is Nothing Then Return
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return
        For i As Integer = 0 To MAX_FILTER
            If FilterEditBuffer(i).FilterType = J2534FilterType.PASS_FILTER OrElse
               FilterEditBuffer(i).FilterType = J2534FilterType.BLOCK_FILTER Then
                If IsISO15765(FilterEditBuffer(i).MaskMessage.ProtocolId) Then
                    FilterEditBuffer(i).MaskMessage.ProtocolId = ISO_IdToCAN_Id(FilterEditBuffer(i).MaskMessage.ProtocolId)
                    FilterEditBuffer(i).PatternMessage.ProtocolId = FilterEditBuffer(i).MaskMessage.ProtocolId
                End If
            End If
            FixFilterMask(FilterEditBuffer(i).MaskMessage, FilterEditBuffer(i).PatternMessage)
            If channel.Connected Then
                If FilterEditBuffer(i).Enabled Then
                    If FilterChangesPresent(i) Then
                        Dim result As J2534Result = selectedDevice.API.PassThruStopMsgFilter(channel.ChannelId, FilterEditBuffer(i).MessageId)
                        If result = J2534Result.STATUS_NOERROR Then FilterEditBuffer(i).Enabled = False
                        ShowResult(selectedDevice, result, "PassThruStopMsgFilter")
                    End If
                End If
                If (Not FilterEditBuffer(i).Enabled) AndAlso FilterEditBuffer(i).FilterType <> J2534FilterType.NO_FILTER Then
                    Dim maskPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(FilterEditBuffer(i).MaskMessage))
                    Marshal.StructureToPtr(FilterEditBuffer(i).MaskMessage, maskPointer, True)
                    Dim patternPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(FilterEditBuffer(i).PatternMessage))
                    Marshal.StructureToPtr(FilterEditBuffer(i).PatternMessage, patternPointer, True)
                    Dim flowPointer As IntPtr = IntPtr.Zero
                    If FilterEditBuffer(i).FilterType = J2534FilterType.FLOW_CONTROL_FILTER Then
                        flowPointer = Marshal.AllocHGlobal(Marshal.SizeOf(FilterEditBuffer(i).FlowMessage))
                        Marshal.StructureToPtr(FilterEditBuffer(i).FlowMessage, flowPointer, True)
                    End If
                    Dim result As J2534Result = selectedDevice.API.PassThruStartMsgFilter(channel.ChannelId, FilterEditBuffer(i).FilterType, maskPointer.ToInt32, patternPointer.ToInt32, flowPointer.ToInt32, FilterEditBuffer(i).MessageId)
                    Marshal.FreeHGlobal(maskPointer)
                    Marshal.FreeHGlobal(patternPointer)
                    Marshal.FreeHGlobal(flowPointer)
                    If result = J2534Result.STATUS_NOERROR Then
                        FilterEditBuffer(i).Enabled = True
                    Else
                        FilterEditBuffer(i).FilterType = J2534FilterType.NO_FILTER
                    End If
                    ShowResult(selectedDevice, result, "PassThruStartMsgFilter")
                End If
            End If
            channel.Filter(i) = FilterEditBuffer(i).Clone
        Next i
        SetFilterControls()
        UpdateFilterEdit()
    End Sub

    Private Sub ButtonCancelFilter_Click(sender As Object, e As EventArgs) Handles ButtonCancelFilter.Click
        SetFilterControls()
        UpdateFilterEdit()
    End Sub

    Private Sub ButtonClearAllFilter_Click(sender As Object, e As EventArgs) Handles ButtonClearAllFilter.Click
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        ClearAllFilters(selectedDevice, channel)
    End Sub

    Private Sub ButtonCreatePassFilter_Click(sender As Object, e As EventArgs) Handles ButtonCreatePassFilter.Click
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        Dim channelForFlags As J2534Channel = channel
        If channel Is Nothing Then Return
        If channel.IsISO15765 Then channelForFlags = ISO_ChannelToCAN_Channel(selectedDevice, channel)

        Dim nextFilter As Integer = channel.NextFilter
        If nextFilter < 0 Then Return
        Select Case channel.ProtocolId
            Case J2534ProtocolId.J1850VPW, J1850VPW_PS, J1850VPW_CH1 To J1850VPW_CH128,
                 ISO9141, ISO9141_PS, ISO9141_CH1 To ISO9141_CH128,
                 ISO14230, ISO14230_PS, ISO14230_CH1 To ISO14230_CH128,
                 SCI_A_ENGINE, SCI_A_TRANS, SCI_B_ENGINE, SCI_B_TRANS,
                 J1708_PS, J1708_CH1 To J1708_CH128
                TextBoxFilterMask(nextFilter).Text = "00"
                TextBoxFilterPatt(nextFilter).Text = "00"

            Case J2534ProtocolId.J1850PWM, J1850PWM_PS, J1850PWM_CH1 To J1850PWM_CH128,
                 HONDA_DIAGH_PS, HONDA_DIAGH_CH1 To HONDA_DIAGH_CH128
                TextBoxFilterMask(nextFilter).Text = "00 00 00"
                TextBoxFilterPatt(nextFilter).Text = "00 00 00"

            Case J1939_PS, J1939_CH1 To J1939_CH128
                TextBoxFilterMask(nextFilter).Text = "00 00 00 00 00"
                TextBoxFilterPatt(nextFilter).Text = "00 00 00 00 00"

            Case Else
                TextBoxFilterMask(nextFilter).Text = "00 00 00 00"
                TextBoxFilterPatt(nextFilter).Text = "00 00 00 00"

        End Select
        If channelForFlags IsNot Nothing Then
            TextBoxFilterFlags(nextFilter).Text = FlagsToText(channelForFlags.DefaultTxFlags)
        Else
            TextBoxFilterFlags(nextFilter).Text = "0x00000000"
        End If
        ComboBoxFilterType(nextFilter).Text = "Pass"
        SetFilterEditBuffer(nextFilter)
        UpdateFilterEdit()
    End Sub

    Private Sub ButtonApplyFunctionalMessages_Click(sender As Object, e As EventArgs) Handles ButtonApplyFunctionalMessages.Click
        If selectedDevice?.API?.PassThruIoctl Is Nothing Then Return
        Dim functionalAddresses(MAX_FUNC_MSG) As Byte
        Dim addrByte As Byte

        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not channel.IsJ1850PWM Then
            SetFunctionalMessageLookupTableControls()
            Return
        End If
        For i As Integer = 0 To MAX_FUNC_MSG
            addrByte = TextBoxFunctionalMessage(i).Text.ToByte
            If channel.FunctionalMessage(i).Value <> 0 Then
                If channel.FunctionalMessage(i).Value <> addrByte Then
                    DeleteFromFunctionalMessageLookupTable(channel.FunctionalMessage(i).Value)
                    CheckBoxFunctionalMessageDelete(i).Checked = False
                End If
                If CheckBoxFunctionalMessageDelete(i).Checked Then
                    TextBoxFunctionalMessage(i).Text = String.Empty
                    addrByte = 0
                    DeleteFromFunctionalMessageLookupTable(channel.FunctionalMessage(i).Value)
                End If
            End If
            functionalAddresses(i) = addrByte
        Next i

        functionalAddresses = PrepFunctionalAddressArray(functionalAddresses)

        Dim result As J2534Result = J2534Result.STATUS_NOERROR
        If functionalAddresses.Length > 0 Then
            Dim message As SBYTE_ARRAY
            message.NumOfBytes = functionalAddresses.Length.ToUint32
            Dim funcAddrPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(functionalAddresses(0)) * functionalAddresses.Length)
            Marshal.Copy(functionalAddresses, 0, funcAddrPointer, functionalAddresses.Length)
            message.BytePtr = funcAddrPointer.ToInt32

            Dim pointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(message))
            Marshal.StructureToPtr(message, pointer, True)

            result = selectedDevice.API.PassThruIoctl(channel.ChannelId, ADD_TO_FUNCT_MSG_LOOKUP_TABLE, pointer.ToInt32, 0)

            Marshal.Copy(funcAddrPointer, functionalAddresses, 0, functionalAddresses.Length)
            Marshal.FreeHGlobal(funcAddrPointer)
            Marshal.FreeHGlobal(pointer)
        End If

        If result = J2534Result.STATUS_NOERROR Then
            For i As Integer = 0 To MAX_FUNC_MSG
                channel.FunctionalMessage(i).Value = If(i < functionalAddresses.Length, functionalAddresses(i), CByte(0))
                If channel.FunctionalMessage(i).Value <> 0 Then
                    channel.FunctionalMessage(i).NewText = "0x" & functionalAddresses(i).ToString("X2")
                Else
                    channel.FunctionalMessage(i).NewText = String.Empty
                End If
            Next i
        End If
        ShowResult(selectedDevice, result, "PassThruIoctl - ADD_TO_FUNCT_MSG_LOOKUP_TABLE")
        SetFunctionalMessageLookupTableControls()
    End Sub

    Private Sub ButtonCancelFunctionalMessages_Click(sender As Object, e As EventArgs) Handles ButtonCancelFunctionalMessages.Click
        SetFunctionalMessageLookupTableControls()
    End Sub

    Private Sub ButtonClearAllFunctionalMessagesClick(sender As Object, e As EventArgs) Handles ButtonClearAllFunctionalMessages.Click
        If selectedDevice?.API?.PassThruIoctl Is Nothing Then Return
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not channel.IsJ1850PWM Then
            SetFunctionalMessageLookupTableControls()
            Return
        End If
        Dim result As J2534Result = selectedDevice.API.PassThruIoctl(channel.ChannelId, CLEAR_FUNCT_MSG_LOOKUP_TABLE, 0, 0)
        ShowResult(selectedDevice, result, "PassThruIoctl - CLEAR_FUNCT_MSG_LOOKUP_TABLE")
        If result = J2534Result.STATUS_NOERROR Then
            For i As Integer = 0 To MAX_FUNC_MSG
                channel.FunctionalMessage(i).Value = 0
                channel.FunctionalMessage(i).NewText = String.Empty
            Next i
        End If
        SetFunctionalMessageLookupTableControls()
    End Sub

    Private Sub ButtonLoadDLL_Click(sender As Object, e As EventArgs) Handles ButtonLoadDLL.Click
        Dim API As J2534_API = TryCast(ComboBoxAPI.SelectedItem, J2534_API)
        If API IsNot Nothing Then
            API.LoadJ2534dll()
            ButtonLoadDLL.Enabled = Not API.DllIsLoaded
        End If
        UpdateComboBoxDevice()
    End Sub

    Private Sub ButtonApplyPeriodicMessages_Click(sender As Object, e As EventArgs) Handles ButtonApplyPeriodicMessages.Click
        If selectedDevice?.API?.PassThruStopPeriodicMsg Is Nothing Then Return
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return
        For index As Integer = 0 To MAX_PM
            If channel.Connected Then
                If PeriodicMessageEditBuffer(index).Enabled Then
                    If PeriodicMessageChangesPresent(index) Then
                        Dim result As J2534Result = selectedDevice.API.PassThruStopPeriodicMsg(channel.ChannelId, PeriodicMessageEditBuffer(index).MessageId)
                        If result = J2534Result.STATUS_NOERROR Then PeriodicMessageEditBuffer(index).Enabled = False
                        ShowResult(selectedDevice, result, "PassThruStopPeriodicMsg")
                    End If
                End If
                If Not PeriodicMessageEditBuffer(index).Enabled Then
                    If CheckBoxPeriodicMessageEnable(index).Checked Then
                        Dim pointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(PeriodicMessageEditBuffer(index).Message))
                        Marshal.StructureToPtr(PeriodicMessageEditBuffer(index).Message, pointer, True)
                        Dim result As J2534Result = selectedDevice.API.PassThruStartPeriodicMsg(channel.ChannelId, pointer.ToInt32, PeriodicMessageEditBuffer(index).MessageId, PeriodicMessageEditBuffer(index).Interval)
                        Marshal.FreeHGlobal(pointer)
                        If result = J2534Result.STATUS_NOERROR Then PeriodicMessageEditBuffer(index).Enabled = True
                        ShowResult(selectedDevice, result, "PassThruStartPeriodicMsg")
                    End If
                End If
            End If
            channel.PeriodicMessage(index) = PeriodicMessageEditBuffer(index).Clone
        Next index
        SetPeriodicMessageControls(selectedDevice)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub ButtonCancelPeriodicMessages_Click(sender As Object, e As EventArgs) Handles ButtonCancelPeriodicMessages.Click
        SetPeriodicMessageControls(selectedDevice)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub ButtonClearAllPeriodicMessages_Click(sender As Object, e As EventArgs) Handles ButtonClearAllPeriodicMessages.Click
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        ClearAllPeriodicMessages(selectedDevice, channel)
    End Sub

    Private Sub ButtonReceive_Click(sender As Object, e As EventArgs) Handles ButtonReceive.Click
        If selectedDevice Is Nothing Then Return
        selectedDevice.Setup.GetMessages = Not selectedDevice.Setup.GetMessages
        selectedDevice.TimerMessageReceive.Enabled = selectedDevice.Setup.GetMessages
        GetMessages(selectedDevice)
        UpdateMessageControls()
    End Sub

    Private Sub ButtonClearList_Click(sender As Object, e As EventArgs) Handles ButtonClearList.Click
        selectedDevice.ListViewMessageIn.Items.Clear()
        UpdateMessageControls()
    End Sub

    Private Sub ButtonSend_Click(sender As Object, e As EventArgs) Handles ButtonSend.Click
        Dim message As PASSTHRU_MSG
        If selectedDevice?.API?.PassThruWriteMsgs Is Nothing Then
            UpdateButtonSend()
            Return
        End If
        If TypeOf ButtonSend.Tag Is ListView Then
            message = GetListViewOutMessageText.ToMessage
        Else
            Dim messageText As New J2534MessageText With {.Data = TextBoxMessageOut.Text, .ProtocolName = ComboBoxMessageChannel.Text, .TxFlags = TextBoxOutFlags.Text}
            message = messageText.ToMessage
        End If
        Dim channel As J2534Channel = selectedDevice.Channel(message.ProtocolId)
        If channel Is Nothing Then Return
        If channel.IsCAN AndAlso CheckBoxPadMessage.Checked Then
            If message.DataSize < 12 Then
                For i As Integer = CInt(message.DataSize) To 11
                    message.Data(i) = 0
                Next i
                message.DataSize = 12
                message.ExtraDataIndex = 12
            End If
        End If
        Dim channelId As UInteger = channel.ChannelId
        If channel.IsCAN AndAlso Not channel.Connected Then
            Dim channelISO As J2534Channel = CAN_ChannelToISO_Channel(selectedDevice, channel)
            If channelISO IsNot Nothing Then
                If channelISO.Connected AndAlso channelISO.MixedMode Then channelId = channelISO.ChannelId
            End If
        End If

        Dim pointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(message))
        Marshal.StructureToPtr(message, pointer, True)
        Dim timeout As UInteger
        UInteger.TryParse(TextBoxTimeOut.Text, timeout)
        Dim count As Integer = 1
        Dim result As J2534Result = selectedDevice.API.PassThruWriteMsgs(channelId, pointer.ToInt32, count, timeout)
        Marshal.FreeHGlobal(pointer)
        ShowResult(selectedDevice, result, "PassThruWriteMsgs")

        UpdateButtonSend()
    End Sub

    Private Sub ButtonClaimJ1939Address_Click(sender As Object, e As EventArgs) Handles ButtonClaimJ1939Address.Click
        If String.IsNullOrWhiteSpace(ComboBoxAvailableChannel.Text) OrElse selectedDevice Is Nothing Then Return
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        Dim setup As New J1939AddressClaimSetup
        If FormProtectAddress.ShowDialog(setup) = DialogResult.OK Then ClaimJ1939Address(selectedDevice, channel, setup)
        FormProtectAddress.Dispose()
    End Sub

    Private Sub ButtonSetConfig_Click(sender As Object, e As EventArgs) Handles ButtonSetConfig.Click
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return

        For i As Integer = 0 To channel.ConfParameter.Length - 1
            If Not channel.ConfParameter(i).IsReadOnly Then
                Dim parameterId As J2534ConfigParameterId = channel.ConfParameter(i).Id
                If ConfigChangesPresent(i) <> 0 Then
                    Dim value As UInteger = GetConfigValue(channel, parameterId)
                    Dim result As J2534Result = SetConfigParameter(selectedDevice, channel, parameterId, value)
                    ShowResult(selectedDevice, result, "PassThruIoctl - SET_CONFIG " & channel.ConfParameter(i).Id.ToString)
                End If
            End If
        Next i

        SetConfigControls()
    End Sub

    Private Sub ButtonClearConfig_Click(sender As Object, e As EventArgs) Handles ButtonClearConfig.Click
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return
        For i As Integer = 0 To TextBoxParamVal.Length - 1
            If TextBoxParamVal(i) IsNot Nothing Then
                TextBoxParamVal(i).Text = String.Empty
                If i < channel.ConfParameter.Length Then channel.ConfParameter(i).NewText = String.Empty
            End If
        Next i
        SetConfigControls()
    End Sub

    Private Sub ButtonSetVoltage_Click(sender As Object, e As EventArgs) Handles ButtonSetVoltage.Click
        SetProgrammingVoltage(selectedDevice)
    End Sub

    Private Sub ButtonReadVolt_Click(sender As Object, e As EventArgs) Handles ButtonReadVolt.Click
        Dim result As J2534Result = ReadProgrammingVoltage(selectedDevice)
        If selectedDevice.Analog.PinReading < 1000 Then
            LabelVoltRead.Text = selectedDevice.Analog.PinReading & " mVDC"
        Else
            LabelVoltRead.Text = selectedDevice.Analog.PinReading / 1000 & " VDC"
        End If
        ShowResult(selectedDevice, result, "PassThruIoctl - READ_PROG_VOLTAGE")
    End Sub

    Private Sub ButtonReadBatt_Click(sender As Object, e As EventArgs) Handles ButtonReadBatt.Click
        Dim result As J2534Result = ReadBatteryVoltage(selectedDevice)
        If selectedDevice.Analog.BatteryReading < 1000 Then
            LabelBattRead.Text = selectedDevice.Analog.BatteryReading & " mVDC"
        Else
            LabelBattRead.Text = selectedDevice.Analog.BatteryReading / 1000 & " VDC"
        End If
        ShowResult(selectedDevice, result, "PassThruIoctl - READ_VBATT")
    End Sub

    Private Sub CheckBoxFunctionalMessageDelete_Click(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        UpdateFunctionalMessageLookupTableEdit()
    End Sub

    Private Sub CheckBoxPeriodicMessageEnable_Click(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.CheckBoxPeriodicMessageEnable, sender)
        If index < 0 Then Return
        PeriodicMessageCheckBoxHit(index) = True
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub CheckBoxCH_Click(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays 
        SetChannelText()
    End Sub

    Private Sub ComboBoxAnalogChannel_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ComboBoxAnalogChannel.SelectionChangeCommitted
        ShowAnalogInputs(selectedDevice)
    End Sub

    Private Sub ComboBoxAvailableChannel_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ComboBoxAvailableChannel.SelectionChangeCommitted
        UpdateTabs()
        If TabControl1.SelectedTab IsNot TabPageConnect Then ComboBoxAvailableChannel.Focus()
        ComboBoxMessageChannel.Text = ComboBoxAvailableChannel.Text
    End Sub

    Private Sub ComboBoxAvailableDevice_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ComboBoxAvailableDevice.SelectionChangeCommitted
        UpdateDevice()
        UpdateTabs()
        If TabControl1.SelectedTab IsNot TabPageConnect Then ComboBoxAvailableDevice.Focus()
    End Sub

    Private Sub ComboBoxMessageChannel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMessageChannel.SelectedIndexChanged
        UpdateButtonSend()
        UpdateButtonClaimJ1939Address()
        ComboBoxAvailableChannel.Text = ComboBoxMessageChannel.Text
        Dim channel As J2534Channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
        If channel IsNot Nothing Then TextBoxOutFlags.Text = FlagsToText(channel.DefaultTxFlags)
    End Sub

    Private Sub ComboBoxDevice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxDevice.SelectedIndexChanged
        UpdateDeviceLabels()
    End Sub

    Private Sub ComboBoxDevice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBoxDevice.KeyPress
        If e.KeyChar = KeyChars.Cr Then UpdateDeviceLabels()
    End Sub

    Private Sub ComboBoxDevice_TextChanged(sender As Object, e As EventArgs) Handles ComboBoxDevice.TextChanged
        UpdateDeviceLabels()
    End Sub

    Private Sub ComboBoxAPI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxAPI.SelectedIndexChanged
        UpdateAPILabels()
        UpdateComboBoxDevice()
    End Sub

    Private Sub ComboBoxBaudRate_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBoxBaudRate.KeyPress
        Select Case e.KeyChar
            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo

            Case Else
                e.Handled = Not Char.IsDigit(e.KeyChar)
        End Select
    End Sub

    Private Sub ComboBoxFilterType_SelectionChangeCommitted(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.ComboBoxFilterType, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub ComboBoxConnectChannel_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ComboBoxConnectChannel.SelectionChangeCommitted
        UpdateConnectControls()
    End Sub

    Private Sub ComboBoxConnector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxConnector.SelectedIndexChanged
        UpdateComboBoxPins(TryCast(ComboBoxConnectChannel.SelectedItem, J2534Channel))
    End Sub

    Private Sub ComboBoxPeriodicMessageChannel_SelectionChangeCommitted(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.ComboBoxPeriodicMessageChannel, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
        UpdatePeriodicMessageCheck(index)
    End Sub

    Private Sub ComboBoxPeriodicMessageChannel_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.ComboBoxPeriodicMessageChannel, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
        If ActiveControl IsNot CheckBoxPeriodicMessageEnable(index) Then UpdatePeriodicMessageCheck(index)
    End Sub

    Private Sub ComboBoxAnalogRateMode_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ComboBoxAnalogRateMode.SelectionChangeCommitted
        SetRateText()
    End Sub

    Private Sub ListViewMessageIn_SizeChanged(sender As Object, e As EventArgs) 'Event Handler added in HandleIncomingDevice
        Dim thisListView As ListView = DirectCast(sender, ListView)
        Try
            thisListView.Columns("Data Bytes").Width = thisListView.Width - 320 ' started As 292
        Catch
        End Try
    End Sub

    Private Sub ListViewMessageIn_MouseMove(sender As Object, e As MouseEventArgs) 'Event Handler added in HandleIncomingDevice
        Static position As New Point(-1, -1)
        Dim thisListView As ListView = DirectCast(sender, ListView)
        If position = MousePosition Then Return  ' to avoid annoying flickering
        Dim info As ListViewHitTestInfo = thisListView.HitTest(thisListView.PointToClient(MousePosition))
        If info.SubItem IsNot Nothing AndAlso info.SubItem.Tag IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(info.SubItem.Text) Then
            ToolTipMessageIn.Show(info.SubItem.Tag.ToString, info.Item.ListView, info.Item.ListView.PointToClient(Cursor.Position + New Size(1, 25)))
        Else
            ToolTipMessageIn.Hide(Me)
        End If
        position = MousePosition
    End Sub

    Private Sub ListViewMessageIn_MouseLeave(sender As Object, e As EventArgs) 'Event Handler added in HandleIncomingDevice
        ToolTipMessageIn.Hide(Me)
    End Sub

    Private Sub ListViewMessageOut_Enter(sender As Object, e As EventArgs) 'Event Handler added in HandleIncomingDevice
        UpdateButtonSend()
    End Sub

    Private Sub ListViewMessageOut_SelectedIndexChanged(sender As Object, e As EventArgs) 'Event Handler added in HandleIncomingDevice
        UpdateButtonSend()
    End Sub

    Private Sub ListViewMessageOut_Leave(sender As Object, e As EventArgs) 'Event Handler added in HandleIncomingDevice
        UpdateButtonSend()
    End Sub

    Private Sub ListViewMessageOut_SizeChanged(sender As Object, e As EventArgs) 'Event Handler added in HandleIncomingDevice
        Dim thisListView As ListView = DirectCast(sender, ListView)
        Try
            thisListView.Columns("Data Bytes").Width = (thisListView.Width - 190) * 2 \ 3 'started as 185
            thisListView.Columns("Comments").Width = (thisListView.Width - 190) \ 3
        Catch
        End Try
    End Sub

    Private Sub InitializeEditMenu()
        MenuEditUndo.Enabled = False
        MenuEditCut.Enabled = False
        MenuEditCopy.Enabled = False
        MenuEditCopy.Available = True
        MenuEditCopyLine.Available = False
        MenuEditMsgInCopyData.Available = False
        MenuEditPaste.Enabled = False
        MenuEditDelete.Enabled = False
        MenuEditMsgInMakeFilter.Available = False
        MenuEditMsgOutEditMessage.Available = False
        MenuEditMsgOutAddMessage.Available = False
        MenuEditMsgOutCopyToScratchPad.Available = False
        MenuEditScratchAddToOutgoingMessageSet.Available = False
        MenuEditMsgOutMakePeriodicMessage.Available = False
        MenuEditSelectAll.Enabled = False
        MenuEditClear.Enabled = False
        MenuEditFlagsEdit.Available = False
        MenuEditFlagsClear.Available = False
        MenuEditFlagsSetDefault.Available = False
        MenuEditRxStatus.Available = False
    End Sub

    Private Sub ConfigureEditMenu()
        If TypeOf ActiveControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ActiveControl, TextBox)
            Dim selectedLength As Integer = thisTextBox.SelectionLength
            Dim textLength As Integer = thisTextBox.Text.Length
            MenuEditUndo.Enabled = thisTextBox.CanUndo
            MenuEditCut.Enabled = selectedLength > 0
            MenuEditCopy.Enabled = MenuEditCut.Enabled
            MenuEditCopyLine.Enabled = False
            MenuEditMsgInCopyData.Enabled = False
            MenuEditPaste.Enabled = Clipboard.ContainsText
            MenuEditDelete.Enabled = MenuEditCut.Enabled
            MenuEditMsgInMakeFilter.Enabled = False
            MenuEditMsgOutEditMessage.Enabled = False
            MenuEditMsgOutAddMessage.Enabled = False
            MenuEditSelectAll.Enabled = selectedLength < textLength
            MenuEditClear.Enabled = textLength > 0
            If thisTextBox Is TextBoxMessageOut Then
                MenuEditScratchAddToOutgoingMessageSet.Available = ComboBoxMessageChannel.SelectedIndex >= 0
            ElseIf thisTextBox Is TextBoxOutFlags Then
                MenuEditFlagsEdit.Available = True
                MenuEditFlagsClear.Available = True
            ElseIf thisTextBox Is TextBoxConnectFlags Then
                MenuEditFlagsEdit.Available = True
                MenuEditFlagsClear.Available = True
                MenuEditFlagsSetDefault.Available = False
            ElseIf thisTextBox.Name.StartsWith("_TextBoxFilterFlags_") OrElse thisTextBox.Name.StartsWith("_TextBoxPeriodicMessageFlags_") Then
                MenuEditFlagsEdit.Available = True
                MenuEditFlagsClear.Available = True
            End If
            Dim flags As UInteger = GetDefaultFlags(selectedDevice, thisTextBox)
            MenuEditFlagsSetDefault.Available = flags <> 0
        ElseIf TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            Dim device As J2534Device = GetListViewParentDevice(thisListView)
            MenuEditUndo.Enabled = False
            MenuEditCut.Enabled = False
            MenuEditPaste.Enabled = False
            MenuEditCopy.Enabled = False
            Dim itemCount As Integer = thisListView.Items.Count
            Dim selectedCount As Integer = thisListView.SelectedItems.Count
            If thisListView Is ListViewResults Then
                MenuEditCopyLine.Enabled = selectedCount > 0
                MenuEditCopyLine.Text = If(selectedCount > 1, "Copy Selected", "Copy Line")
                MenuEditCopyLine.Available = True
                MenuEditDelete.Enabled = selectedCount > 0
                MenuEditSelectAll.Enabled = selectedCount < itemCount
                MenuEditClear.Enabled = itemCount > 0
                MenuEditCopy.Available = False
            ElseIf device IsNot Nothing AndAlso thisListView Is device.ListViewMessageIn Then
                MenuEditCopyLine.Enabled = selectedCount > 0
                MenuEditCopyLine.Text = If(selectedCount > 1, "Copy Selected", "Copy Line")
                MenuEditCopyLine.Available = True
                MenuEditMsgInCopyData.Enabled = selectedCount = 1
                MenuEditMsgInCopyData.Available = True
                Dim selectedChannel As J2534Channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
                Dim channel As J2534Channel = Nothing
                If selectedCount > 0 Then channel = device.Channel(thisListView.SelectedItems(0).SubItems(1).Text)
                Dim nextFilter As Integer = If(channel?.NextFilter, -1)
                MenuEditDelete.Enabled = selectedCount > 0
                MenuEditMsgInMakeFilter.Enabled = selectedCount = 1 AndAlso (nextFilter >= 0) AndAlso selectedChannel Is channel
                MenuEditSelectAll.Enabled = selectedCount < itemCount
                MenuEditClear.Enabled = itemCount > 0
                MenuEditRxStatus.Enabled = selectedCount = 1
                MenuEditCopy.Available = False
                MenuEditMsgInMakeFilter.Available = True
                MenuEditRxStatus.Available = True
            ElseIf device IsNot Nothing AndAlso thisListView Is selectedDevice.ListViewMessageOut Then
                MenuEditMsgOutEditMessage.Enabled = selectedCount > 0
                MenuEditMsgOutEditMessage.Available = True
                MenuEditDelete.Enabled = MenuEditMsgOutEditMessage.Enabled
                MenuEditMsgOutCopyToScratchPad.Enabled = MenuEditMsgOutEditMessage.Enabled
                Dim selectedChannel As J2534Channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
                Dim channel As J2534Channel = Nothing
                If selectedCount > 0 Then channel = device.Channel(thisListView.SelectedItems(0).Text)
                Dim nextPM As Integer = If(channel?.NextPeriodicMessage, -1)
                MenuEditMsgOutMakePeriodicMessage.Enabled = MenuEditMsgOutEditMessage.Enabled AndAlso nextPM >= 0 AndAlso CanMakePeriodicMessage(device, thisListView.SelectedItems(0).Text) AndAlso selectedChannel Is channel
                MenuEditSelectAll.Enabled = False
                MenuEditClear.Enabled = itemCount > 0
                MenuEditMsgOutAddMessage.Enabled = True
                MenuEditMsgOutAddMessage.Available = True
                MenuEditMsgOutCopyToScratchPad.Available = True
                MenuEditMsgOutMakePeriodicMessage.Available = True
            End If
        ElseIf TypeOf ActiveControl Is ComboBox Then
            Dim thisCombo As ComboBox = DirectCast(ActiveControl, ComboBox)
            If thisCombo.DropDownStyle <> ComboBoxStyle.DropDown Then
                Return
            End If
            Dim selectedLength As Integer = thisCombo.SelectionLength
            Dim textLength As Integer = thisCombo.Text.Length
            MenuEditUndo.Enabled = thisCombo.CanUndo
            MenuEditCut.Enabled = selectedLength > 0
            MenuEditCopy.Enabled = MenuEditCut.Enabled
            MenuEditCopyLine.Enabled = False
            MenuEditMsgInCopyData.Enabled = False
            MenuEditPaste.Enabled = Clipboard.ContainsText
            MenuEditDelete.Enabled = MenuEditCut.Enabled
            MenuEditMsgInMakeFilter.Enabled = False
            MenuEditMsgOutEditMessage.Enabled = False
            MenuEditMsgOutAddMessage.Enabled = False
            MenuEditSelectAll.Enabled = selectedLength < textLength
            MenuEditClear.Enabled = textLength > 0
        End If
    End Sub

    Private Sub MenuEdit_DropDownOpening(sender As Object, e As EventArgs) Handles MenuEdit.DropDownOpening
        InitializeEditMenu()
        ConfigureEditMenu()
    End Sub

    Private Sub MenuEditClear_Click(sender As Object, e As EventArgs) Handles MenuEditClear.Click
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).Clear()
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).SelectAll()
            DirectCast(ActiveControl, ComboBox).Delete()
        ElseIf TypeOf ActiveControl Is ListView Then
            DirectCast(ActiveControl, ListView).Items.Clear()
        End If
    End Sub

    Private Sub MenuEditCopy_Click(sender As Object, e As EventArgs) Handles MenuEditCopy.Click
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).Copy()
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).Copy()
        End If
    End Sub

    Private Sub MenuEditCut_Click(sender As Object, e As EventArgs) Handles MenuEditCut.Click
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).Cut()
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).Cut()
        End If
    End Sub

    Private Sub MenuEditDelete_Click(sender As Object, e As EventArgs) Handles MenuEditDelete.Click
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).Delete
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).Delete
        ElseIf TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) OrElse thisListView Is ListViewResults Then
                For Each item As ListViewItem In thisListView.SelectedItems
                    thisListView.Items.Remove(item)
                Next
            End If
        End If
    End Sub

    Private Sub MenuEditFlagsClear_Click(sender As Object, e As EventArgs) Handles MenuEditFlagsClear.Click
        If TypeOf ActiveControl Is TextBox Then ActiveControl.Text = "0x00000000"
    End Sub

    Private Sub MenuEditFlagsSetDefault_Click(sender As Object, e As EventArgs) Handles MenuEditFlagsSetDefault.Click
        If TypeOf ActiveControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ActiveControl, TextBox)
            thisTextBox.Text = FlagsToText(GetDefaultFlags(selectedDevice, thisTextBox))
        End If
    End Sub

    Private Sub MenuEditFlagsEdit_Click(sender As Object, e As EventArgs) Handles MenuEditFlagsEdit.Click
        If TypeOf ActiveControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ActiveControl, TextBox)
            Dim flags As UInteger = TextToFlags(thisTextBox.Text)
            If thisTextBox Is TextBoxConnectFlags Then
                If FormConnectFlags.ShowDialog(flags) = DialogResult.OK Then thisTextBox.Text = FlagsToText(flags)
                FormConnectFlags.Dispose()
            Else
                Dim channel As J2534Channel
                If thisTextBox Is TextBoxOutFlags Then
                    channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
                Else
                    channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
                End If
                If channel IsNot Nothing Then
                    If FormTxFlags.ShowDialog(flags, channel) = DialogResult.OK Then thisTextBox.Text = FlagsToText(flags)
                    FormTxFlags.Dispose()
                End If
            End If
        End If
    End Sub

    Private Sub MenuEditMsgInCopyData_Click(sender As Object, e As EventArgs) Handles MenuEditMsgInCopyData.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then
                If thisListView.SelectedItems.Count > 0 Then
                    Clipboard.SetText(thisListView.SelectedItems(0).SubItems(3).Text)
                End If
            End If
        End If
    End Sub

    Private Sub MenuEditCopyLine_Click(sender As Object, e As EventArgs) Handles MenuEditCopyLine.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then
                MsgInCopyLine(thisListView)
            ElseIf thisListView Is ListViewResults Then
                ResultsCopyLine(thisListView)
            End If
        End If
    End Sub

    Private Sub MenuEditMsgInMakeFilter_Click(sender As Object, e As EventArgs) Handles MenuEditMsgInMakeFilter.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then MakeFilterFromIncomingMessage(thisListView)
        End If
    End Sub

    Private Sub MenuEditMsgOutAddMessage_Click(sender As Object, e As EventArgs) Handles MenuEditMsgOutAddMessage.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then
                Dim messageText As J2534MessageText = GetNewMessageTextFromDialog()
                If Not String.IsNullOrWhiteSpace(messageText.ProtocolName) Then
                    AddMessageToOutgoingListView(thisListView, messageText)
                End If
            End If
        End If
    End Sub

    Private Sub MenuEditMsgOutEditMessage_Click(sender As Object, e As EventArgs) Handles MenuEditMsgOutEditMessage.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then ModifySelectedOutgoingMessage(thisListView)
        End If
    End Sub

    Private Sub MenuEditMsgOutCopyToScratchPad_Click(sender As Object, e As EventArgs) Handles MenuEditMsgOutCopyToScratchPad.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then CopyOutgoingMessageToScratchPad(thisListView)
        End If
    End Sub

    Private Sub MenuEditMsgOutMakePeriodicMessage_Click(sender As Object, e As EventArgs) Handles MenuEditMsgOutMakePeriodicMessage.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then MakePeriodicFromOutgoingMessage(thisListView)
        End If
    End Sub

    Private Sub MenuEditScratchAddToOutgoingMessageSet_Click(sender As Object, e As EventArgs) Handles MenuEditScratchAddToOutgoingMessageSet.Click
        If selectedDevice IsNot Nothing AndAlso selectedDevice.ListViewMessageOut IsNot Nothing Then
            Dim messageText As J2534MessageText = GetScratchPadMessageText()
            AddMessageToOutgoingListView(selectedDevice.ListViewMessageOut, messageText)
        End If
    End Sub

    Private Sub MenuEditPaste_Click(sender As Object, e As EventArgs) Handles MenuEditPaste.Click
        If Not Clipboard.ContainsText Then Return
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).Paste()
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).Paste()
        End If
    End Sub

    Private Sub MenuEditRxStatus_Click(sender As Object, e As EventArgs) Handles MenuEditRxStatus.Click
        If TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) AndAlso thisListView.SelectedItems.Count > 0 Then
                Dim protocolId As UInteger = NameToProtocolId(thisListView.SelectedItems(0).SubItems(1).Text)
                Dim flags As UInteger = TextToFlags(thisListView.SelectedItems(0).SubItems(2).Text)
                FormRxStatus.ShowDialog(flags, protocolId)
                FormRxStatus.Dispose()
            End If
        End If
    End Sub

    Private Sub MenuEditSelectAll_Click(sender As Object, e As EventArgs) Handles MenuEditSelectAll.Click
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).SelectAll()
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).SelectAll()
        ElseIf TypeOf ActiveControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ActiveControl, ListView)
            If String.Equals(thisListView.Name, "ListViewMessageIn", IgnoreCase) OrElse thisListView Is ListViewResults Then
                thisListView.SelectAll
            End If
        End If
    End Sub

    Private Sub MenuEditUndo_Click(sender As Object, e As EventArgs) Handles MenuEditUndo.Click
        If TypeOf ActiveControl Is TextBox Then
            DirectCast(ActiveControl, TextBox).Undo()
        ElseIf TypeOf ActiveControl Is ComboBox Then
            DirectCast(ActiveControl, ComboBox).Undo()
        End If
    End Sub

    Private Sub FileMenuInit()
        MenuFileMsgOutLoad.Available = False
        MenuFilePeriodicMessageLoad.Available = False
        MenuFileFilterLoad.Available = False
        MenuFileSpace1.Available = False

        MenuFileMsgInSaveSelected.Available = False
        MenuFileMsgInSaveAll.Available = False
        MenuFileMsgOutSave.Available = False
        MenuFilePeriodicMessageSave.Available = False
        MenuFileFilterSave.Available = False
        MenuFileSpace2.Available = False
    End Sub

    Private Sub SetFileMenus()
        Dim boxExists As Boolean = selectedDevice IsNot Nothing
        If TabControl1.SelectedTab Is TabPageMessages Then
            MenuFileMsgInSaveAll.Available = True
            MenuFileMsgInSaveAll.Enabled = boxExists AndAlso selectedDevice.ListViewMessageIn.Items.Count > 0
            MenuFileMsgInSaveSelected.Available = True
            MenuFileMsgInSaveSelected.Enabled = boxExists AndAlso selectedDevice.ListViewMessageIn.SelectedItems.Count > 0
            MenuFileMsgInSaveSelected.Text = If(boxExists AndAlso selectedDevice.ListViewMessageIn.SelectedItems.Count > 1,
                                        "Save Selected Incoming Messages",
                                        "Save Selected Incoming Message")
            MenuFileMsgOutLoad.Available = True
            MenuFileMsgOutLoad.Enabled = boxExists
            MenuFileMsgOutSave.Available = True
            MenuFileMsgOutSave.Enabled = boxExists AndAlso selectedDevice.ListViewMessageOut.Items.Count > 0
        ElseIf TabControl1.SelectedTab Is TabPagePeriodicMessages Then
            MenuFilePeriodicMessageLoad.Available = True
            MenuFilePeriodicMessageLoad.Enabled = TextBoxPeriodicMessage(0).Enabled
            Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
            MenuFilePeriodicMessageSave.Available = True
            MenuFilePeriodicMessageSave.Enabled = channel IsNot Nothing AndAlso channel.PeriodicMessagesExist
        ElseIf TabControl1.SelectedTab Is TabPageFilters Then
            MenuFileFilterLoad.Available = True
            MenuFileFilterLoad.Enabled = TextBoxFilterMask(0).Enabled
            Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
            MenuFileFilterSave.Available = True
            MenuFileFilterSave.Available = channel IsNot Nothing AndAlso channel.FiltersExist
        End If
        MenuFileSpace1.Available = MenuFileFilterLoad.Available OrElse
                                   MenuFilePeriodicMessageLoad.Available OrElse
                                   MenuFileMsgOutLoad.Available
        MenuFileSpace2.Available = MenuFileFilterSave.Available OrElse
                                   MenuFilePeriodicMessageSave.Available OrElse
                                   MenuFileMsgInSaveAll.Available OrElse
                                   MenuFileMsgInSaveSelected.Available OrElse
                                   MenuFileMsgOutSave.Available
    End Sub

    Private Sub MenuFile_DropDownOpening(sender As Object, e As EventArgs) Handles MenuFile.DropDownOpening
        FileMenuInit()
        SetFileMenus()
    End Sub

    Private Sub MenuFileMsgInSaveAll_Click(sender As Object, e As EventArgs) Handles MenuFileMsgInSaveAll.Click
        If selectedDevice Is Nothing Then Return
        Dim itemList(selectedDevice.ListViewMessageIn.Items.Count - 1) As ListViewItem
        selectedDevice.ListViewMessageIn.Items.CopyTo(itemList, 0)
        SaveListViewItemListToFile(itemList)
    End Sub

    Private Sub MenuFileMsgInSaveSelected_Click(sender As Object, e As EventArgs) Handles MenuFileMsgInSaveSelected.Click
        If selectedDevice Is Nothing Then Return
        Dim itemList(selectedDevice.ListViewMessageIn.SelectedItems.Count - 1) As ListViewItem
        selectedDevice.ListViewMessageIn.SelectedItems.CopyTo(itemList, 0)
        SaveListViewItemListToFile(itemList)
    End Sub

    Private Sub MenuFileMsgOutLoad_Click(sender As Object, e As EventArgs) Handles MenuFileMsgOutLoad.Click
        If selectedDevice Is Nothing Then Return
        LoadOutgoingMessageListFromFile(selectedDevice.ListViewMessageOut)
    End Sub

    Private Sub MenuFileMsgOutSave_Click(sender As Object, e As EventArgs) Handles MenuFileMsgOutSave.Click
        If selectedDevice Is Nothing Then Return
        SaveOutgoingMessageListToFile(selectedDevice.ListViewMessageOut)
    End Sub

    Private Sub MenuFileFilterLoad_Click(sender As Object, e As EventArgs) Handles MenuFileFilterLoad.Click
        LoadFiltersFromFile()
    End Sub

    Private Sub MenuFileFilterSave_Click(sender As Object, e As EventArgs) Handles MenuFileFilterSave.Click
        SaveFiltersToFile()
    End Sub

    Private Sub MenuFilePeriodicMessageLoad_Click(sender As Object, e As EventArgs) Handles MenuFilePeriodicMessageLoad.Click
        LoadPeriodicMessagesFromFile()
    End Sub

    Private Sub MenuFilePeriodicMessageSave_Click(sender As Object, e As EventArgs) Handles MenuFilePeriodicMessageSave.Click
        SavePeriodicMessagesToFile()
    End Sub

    Private Sub MenuFileExit_Click(sender As Object, e As EventArgs) Handles MenuFileExit.Click
        Me.Close()
    End Sub

    Private Sub ContextMenuStripFlags_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripFlags.Opening
        If TypeOf ContextMenuStripFlags.SourceControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ContextMenuStripFlags.SourceControl, TextBox)
            Dim flags As UInteger = 0
            If selectedDevice IsNot Nothing Then flags = GetDefaultFlags(selectedDevice, thisTextBox)
            If thisTextBox.Enabled Then
                thisTextBox.Focus()
                MenuFlagsCut.Enabled = thisTextBox.SelectionLength > 0
                MenuFlagsCopy.Enabled = MenuFlagsCut.Enabled
                MenuFlagsUndo.Enabled = thisTextBox.CanUndo
                MenuFlagsPaste.Enabled = Clipboard.ContainsText
                MenuFlagsDelete.Enabled = MenuFlagsCut.Enabled
                MenuFlagsSelectAll.Enabled = thisTextBox.SelectionLength < thisTextBox.Text.Length
                MenuFlagsEdit.Enabled = True
                MenuFlagsClear.Enabled = True
                MenuFlagsSetDefault.Available = flags <> 0
            End If
        End If
    End Sub

    Private Sub MenuFlagsClear_Click(sender As Object, e As EventArgs) Handles MenuFlagsClear.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Text = "0x00000000"
    End Sub

    Private Sub MenuFlagsSetDefault_Click(sender As Object, e As EventArgs) Handles MenuFlagsSetDefault.Click
        If selectedDevice Is Nothing Then Return
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(thisMenuStrip.SourceControl, TextBox)
            thisTextBox.Text = FlagsToText(GetDefaultFlags(selectedDevice, thisTextBox))
        End If
    End Sub

    Private Sub MenuFlagsEdit_Click(sender As Object, e As EventArgs) Handles MenuFlagsEdit.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(thisMenuStrip.SourceControl, TextBox)
            Dim flags As UInteger = TextToFlags(thisTextBox.Text)
            If thisTextBox Is TextBoxConnectFlags Then
                If FormConnectFlags.ShowDialog(flags) = DialogResult.OK Then thisTextBox.Text = FlagsToText(flags)
                FormConnectFlags.Dispose()
            Else
                Dim channel As J2534Channel
                If thisTextBox Is TextBoxOutFlags Then
                    channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
                Else
                    channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
                End If
                If FormTxFlags.ShowDialog(flags, channel) = DialogResult.OK Then thisTextBox.Text = FlagsToText(flags)
                FormTxFlags.Dispose()
            End If
        End If
    End Sub

    Private Sub MenuFlagsCopy_Click(sender As Object, e As EventArgs) Handles MenuFlagsCopy.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Copy()
    End Sub

    Private Sub MenuFlagsCut_Click(sender As Object, e As EventArgs) Handles MenuFlagsCut.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Cut()
    End Sub

    Private Sub MenuFlagsDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles MenuFlagsDelete.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Delete
    End Sub

    Private Sub MenuFlagsPaste_Click(ByVal sender As Object, ByVal e As EventArgs) Handles MenuFlagsPaste.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Paste()
    End Sub

    Private Sub MenuFlagsSelectAll_Click(sender As Object, e As EventArgs) Handles MenuFlagsSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).SelectAll()
    End Sub

    Private Sub MenuFlagsUndo_Click(sender As Object, e As EventArgs) Handles MenuFlagsUndo.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then DirectCast(thisMenuStrip.SourceControl, TextBox).Undo()
    End Sub

    Private Sub MenuHelpAbout_Click(sender As Object, e As EventArgs) Handles MenuHelpAbout.Click
        FormSplash.Show()
        FormSplash.Timer1.Enabled = False
        FormSplash.ButtonOK.Visible = True
    End Sub

    Private Sub ContextMenuStripMsgIn_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripMessageIn.Opening
        If TypeOf ContextMenuStripMessageIn.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(ContextMenuStripMessageIn.SourceControl, ListView)
            Dim device As J2534Device = GetListViewParentDevice(thisListView)
            If device Is Nothing Then Return
            Dim selectedCount As Integer = thisListView.SelectedItems.Count
            Dim itemCount As Integer = thisListView.Items.Count
            MenuMsgInSelectAll.Enabled = selectedCount < itemCount
            MenuMsgInCopyLine.Enabled = selectedCount > 0
            MenuMsgInCopyLine.Text = If(selectedCount > 1, "Copy Selected", "Copy Line")
            MenuMsgInCopyData.Enabled = selectedCount = 1
            MenuMsgInRxStatus.Enabled = selectedCount = 1
            Dim channel As J2534Channel = Nothing
            Dim selectedChannel As J2534Channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
            If selectedCount > 0 Then channel = device.Channel(thisListView.SelectedItems(0).SubItems(1).Text)
            Dim nextFilter As Integer = If(channel?.NextFilter, -1)
            MenuMsgInMakeFilter.Enabled = selectedCount = 1 AndAlso nextFilter >= 0 AndAlso selectedChannel Is channel
            MenuMsgInSaveAll.Enabled = itemCount > 0
            MenuMsgInSaveSelected.Enabled = selectedCount > 0
            MenuMsgInClear.Enabled = itemCount > 0
        End If
    End Sub

    Private Sub MenuMsgInClear_Click(sender As Object, e As EventArgs) Handles MenuMsgInClear.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then DirectCast(thisMenuStrip.SourceControl, ListView).Items.Clear()
    End Sub

    Private Sub MenuMsgInCopyData_Click(sender As Object, e As EventArgs) Handles MenuMsgInCopyData.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.SelectedItems.Count = 1 Then
                Clipboard.SetText(thisListView.SelectedItems(0).SubItems(3).Text)
            End If
        End If
    End Sub

    Private Sub MenuMsgInCopyLine_Click(sender As Object, e As EventArgs) Handles MenuMsgInCopyLine.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then MsgInCopyLine(thisListView)
        End If
    End Sub

    Private Sub MenuMsgInMakeFilter_Click(sender As Object, e As EventArgs) Handles MenuMsgInMakeFilter.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then MakeFilterFromIncomingMessage(thisListView)
        End If
    End Sub

    Private Sub MenuMsgInRxStatus_Click(sender As Object, e As EventArgs) Handles MenuMsgInRxStatus.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) AndAlso thisListView.SelectedItems.Count > 0 Then
                Dim protocolId As UInteger = NameToProtocolId(thisListView.SelectedItems(0).SubItems(1).Text)
                Dim flags As UInteger = TextToFlags(thisListView.SelectedItems(0).SubItems(2).Text)
                FormRxStatus.ShowDialog(flags, protocolId)
                FormRxStatus.Dispose()
            End If
        End If
    End Sub

    Private Sub MenuMsgInSaveAll_Click(sender As Object, e As EventArgs) Handles MenuMsgInSaveAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then
                Dim itemList(thisListView.Items.Count - 1) As ListViewItem
                thisListView.Items.CopyTo(itemList, 0)
                SaveListViewItemListToFile(itemList)
            End If
        End If
    End Sub

    Private Sub MenuMsgInSaveSelected_Click(sender As Object, e As EventArgs) Handles MenuMsgInSaveSelected.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then
                Dim itemList(thisListView.SelectedItems.Count - 1) As ListViewItem
                thisListView.SelectedItems.CopyTo(itemList, 0)
                SaveListViewItemListToFile(itemList)
            End If
        End If
    End Sub

    Private Sub MenuMsgInSelectAll_Click(sender As Object, e As EventArgs) Handles MenuMsgInSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageIn", IgnoreCase) Then thisListView.SelectAll
        End If
    End Sub

    Private Sub ContextMenuStripMsgOut_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripMessageOut.Opening
        If TypeOf ContextMenuStripMessageOut.SourceControl IsNot ListView Then Return
        Dim thisListView As ListView = DirectCast(ContextMenuStripMessageOut.SourceControl, ListView)
        Dim device As J2534Device = GetListViewParentDevice(thisListView)
        If device Is Nothing Then Return
        Dim itemCount As Integer = thisListView.Items.Count
        Dim selectedCount As Integer = thisListView.SelectedItems.Count
        Dim selectionOK As Boolean = selectedCount = 1
        MenuMsgOutEditMessage.Enabled = selectionOK
        MenuMsgOutDelete.Enabled = selectedCount > 0
        MenuMsgOutCopyToScratchPad.Enabled = selectionOK
        Dim selectedChannel As J2534Channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
        Dim channel As J2534Channel = Nothing
        If selectionOK Then channel = device.Channel(thisListView.SelectedItems(0).Text)
        Dim nextPM As Integer = If(channel?.NextPeriodicMessage, -1)
        MenuMsgOutMakePeriodicMessage.Enabled = selectionOK AndAlso nextPM >= 0 AndAlso
                                                CanMakePeriodicMessage(device, thisListView.SelectedItems(0).Text) AndAlso
                                                selectedChannel Is channel
        MenuMsgOutAddMessage.Enabled = True
        MenuMsgOutSave.Enabled = itemCount > 0
        MenuMsgOutLoad.Enabled = True
        MenuMsgOutClear.Enabled = itemCount > 0
    End Sub

    Private Sub MenuMsgOutClear_Click(sender As Object, e As EventArgs) Handles MenuMsgOutClear.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then thisListView.Items.Clear()
        End If
    End Sub

    Private Sub MenuMsgOutDelete_Click(sender As Object, e As EventArgs) Handles MenuMsgOutDelete.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then
                For Each item As ListViewItem In thisListView.SelectedItems
                    thisListView.Items.Remove(item)
                Next item
            End If
        End If
    End Sub

    Private Sub MenuMsgOutAddMessage_Click(sender As Object, e As EventArgs) Handles MenuMsgOutAddMessage.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then
                Dim messageText As J2534MessageText = GetNewMessageTextFromDialog()
                If Not String.IsNullOrWhiteSpace(messageText.ProtocolName) Then
                    AddMessageToOutgoingListView(thisListView, messageText)
                End If
            End If
        End If
    End Sub

    Private Sub MenuMsgOutLoad_Click(sender As Object, e As EventArgs) Handles MenuMsgOutLoad.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then LoadOutgoingMessageListFromFile(thisListView)
        End If
    End Sub

    Private Sub MenuMsgOutEditMessage_Click(sender As Object, e As EventArgs) Handles MenuMsgOutEditMessage.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then ModifySelectedOutgoingMessage(thisListView)
        End If
    End Sub

    Private Sub MenuMsgOutMakePeriodicMessage_Click(sender As Object, e As EventArgs) Handles MenuMsgOutMakePeriodicMessage.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then MakePeriodicFromOutgoingMessage(thisListView)
        End If
    End Sub

    Private Sub MenuMsgOutSave_Click(sender As Object, e As EventArgs) Handles MenuMsgOutSave.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then SaveOutgoingMessageListToFile(thisListView)
        End If
    End Sub

    Private Sub MenuMsgOutCopyToScratchPad_Click(sender As Object, e As EventArgs) Handles MenuMsgOutCopyToScratchPad.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView.Name.Equals("ListViewMessageOut", IgnoreCase) Then CopyOutgoingMessageToScratchPad(thisListView)
        End If
    End Sub

    Private Sub ContextMenuStripScratchPad_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripScratchPad.Opening
        If Not TextBoxMessageOut.Enabled Then Return
        Dim selectedLenth As Integer = TextBoxMessageOut.SelectionLength
        Dim textLength As Integer = TextBoxMessageOut.TextLength
        MenuScratchPadPaste.Enabled = Clipboard.ContainsText
        MenuScratchPadCut.Enabled = selectedLenth > 0
        MenuScratchPadCopy.Enabled = MenuScratchPadCut.Enabled
        MenuScratchPadDelete.Enabled = MenuScratchPadCut.Enabled
        MenuScratchPadUndo.Enabled = TextBoxMessageOut.CanUndo
        MenuScratchPadSelectAll.Enabled = selectedLenth < textLength
        MenuScratchPadAddToOutgoingMessageSet.Available = ComboBoxMessageChannel.SelectedIndex >= 0
    End Sub

    Private Sub MenuScratchPadCopy_Click(sender As Object, e As EventArgs) Handles MenuScratchPadCopy.Click
        TextBoxMessageOut.Copy()
    End Sub

    Private Sub MenuScratchPadCut_Click(sender As Object, e As EventArgs) Handles MenuScratchPadCut.Click
        TextBoxMessageOut.Cut()
    End Sub

    Private Sub MenuScratchPadDelete_Click(sender As Object, e As EventArgs) Handles MenuScratchPadDelete.Click
        TextBoxMessageOut.Delete
    End Sub

    Private Sub MenuScratchPadPaste_Click(sender As Object, e As EventArgs) Handles MenuScratchPadPaste.Click
        TextBoxMessageOut.Paste()
    End Sub

    Private Sub MenuScratchPadSelectAll_Click(sender As Object, e As EventArgs) Handles MenuScratchPadSelectAll.Click
        TextBoxMessageOut.SelectAll()
    End Sub

    Private Sub MenuScratchPadUndo_Click(sender As Object, e As EventArgs) Handles MenuScratchPadUndo.Click
        TextBoxMessageOut.Undo()
    End Sub

    Private Sub MenuScratchPadAddToOutgoingMessageSet_Click(sender As Object, e As EventArgs) Handles MenuScratchPadAddToOutgoingMessageSet.Click
        If selectedDevice IsNot Nothing AndAlso selectedDevice.ListViewMessageOut IsNot Nothing Then
            Dim messageText As J2534MessageText = GetScratchPadMessageText()
            AddMessageToOutgoingListView(selectedDevice.ListViewMessageOut, messageText)
        End If
    End Sub

    Private Sub ContextMenuStripTextBox_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripTextBox.Opening
        If TypeOf ContextMenuStripTextBox.SourceControl Is TextBox Then
            Dim thisTextBox As TextBox = DirectCast(ContextMenuStripTextBox.SourceControl, TextBox)
            Dim selectedLength As Integer = thisTextBox.SelectionLength
            Dim textLength As Integer = thisTextBox.TextLength
            MenuTextBoxPaste.Enabled = Clipboard.ContainsText AndAlso Not thisTextBox.ReadOnly
            MenuTextBoxCopy.Enabled = (selectedLength > 0)
            MenuTextBoxCut.Enabled = MenuTextBoxCopy.Enabled AndAlso Not thisTextBox.ReadOnly
            MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled
            MenuTextBoxUndo.Enabled = thisTextBox.CanUndo
            MenuTextBoxSelectAll.Enabled = (selectedLength < textLength)
        ElseIf TypeOf ContextMenuStripTextBox.SourceControl Is ComboBox Then
            Dim thisComboBox As ComboBox = DirectCast(ContextMenuStripTextBox.SourceControl, ComboBox)
            If thisComboBox.DropDownStyle <> ComboBoxStyle.DropDown Then
                e.Cancel = True
                Return
            End If
            Dim selectedLength As Integer = thisComboBox.SelectionLength
            Dim textLength As Integer = thisComboBox.Text.Length
            MenuTextBoxPaste.Enabled = Clipboard.ContainsText
            MenuTextBoxCut.Enabled = (selectedLength > 0)
            MenuTextBoxCopy.Enabled = MenuTextBoxCut.Enabled
            MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled
            MenuTextBoxUndo.Enabled = thisComboBox.CanUndo
            MenuTextBoxSelectAll.Enabled = (selectedLength < textLength)
        End If
    End Sub

    Private Sub MenuTextBoxCopy_Click(sender As Object, e As EventArgs) Handles MenuTextBoxCopy.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            DirectCast(thisMenuStrip.SourceControl, TextBox).Copy()
        ElseIf TypeOf thisMenuStrip.SourceControl Is ComboBox Then
            DirectCast(thisMenuStrip.SourceControl, ComboBox).Copy()
        End If
    End Sub

    Private Sub MenuTextBoxCut_Click(sender As Object, e As EventArgs) Handles MenuTextBoxCut.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            DirectCast(thisMenuStrip.SourceControl, TextBox).Cut()
        ElseIf TypeOf thisMenuStrip.SourceControl Is ComboBox Then
            DirectCast(thisMenuStrip.SourceControl, ComboBox).Cut()
        End If
    End Sub

    Private Sub MenuTextBoxDelete_Click(sender As Object, e As EventArgs) Handles MenuTextBoxDelete.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            DirectCast(thisMenuStrip.SourceControl, TextBox).Delete
        ElseIf TypeOf thisMenuStrip.SourceControl Is ComboBox Then
            DirectCast(thisMenuStrip.SourceControl, ComboBox).Delete()
        End If
    End Sub

    Private Sub MenuTextBoxPaste_Click(sender As Object, e As EventArgs) Handles MenuTextBoxPaste.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            DirectCast(thisMenuStrip.SourceControl, TextBox).Paste()
        ElseIf TypeOf thisMenuStrip.SourceControl Is ComboBox Then
            DirectCast(thisMenuStrip.SourceControl, ComboBox).Paste()
        End If
    End Sub

    Private Sub MenuTextBoxSelectAll_Click(sender As Object, e As EventArgs) Handles MenuTextBoxSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            DirectCast(thisMenuStrip.SourceControl, TextBox).SelectAll()
        ElseIf TypeOf thisMenuStrip.SourceControl Is ComboBox Then
            DirectCast(thisMenuStrip.SourceControl, ComboBox).SelectAll()
        End If
    End Sub

    Private Sub MenuTextBoxUndo_Click(sender As Object, e As EventArgs) Handles MenuTextBoxUndo.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is TextBox Then
            DirectCast(thisMenuStrip.SourceControl, TextBox).Undo()
        ElseIf TypeOf thisMenuStrip.SourceControl Is ComboBox Then
            DirectCast(thisMenuStrip.SourceControl, ComboBox).Undo()
        End If
    End Sub

    Private Sub RadioButtonPin_CheckedChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim pinButton As RadioButton = TryCast(sender, RadioButton)
        If pinButton Is Nothing Then Return
        If pinButton.Checked Then UpdatePinControls(selectedDevice)
    End Sub

    Private Sub MenuDeviceUndo_Click(sender As Object, e As EventArgs)
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ComboBox Then DirectCast(thisMenuStrip.SourceControl, ComboBox).Undo()
    End Sub

    Private Sub MenuDeviceCut_Click(sender As Object, e As EventArgs)
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ComboBox Then DirectCast(thisMenuStrip.SourceControl, ComboBox).Cut()
    End Sub

    Private Sub MenuDeviceCopy_Click(sender As Object, e As EventArgs)
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ComboBox Then DirectCast(thisMenuStrip.SourceControl, ComboBox).Copy()
    End Sub

    Private Sub MenuDevicePaste_Click(sender As Object, e As EventArgs)
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ComboBox Then DirectCast(thisMenuStrip.SourceControl, ComboBox).Paste()
    End Sub

    Private Sub MenuDeviceDelete_Click(sender As Object, e As EventArgs)
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ComboBox Then DirectCast(thisMenuStrip.SourceControl, ComboBox).Delete()
    End Sub

    Private Sub MenuDeviceSelectAll_Click(sender As Object, e As EventArgs)
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ComboBox Then DirectCast(thisMenuStrip.SourceControl, ComboBox).SelectAll()
    End Sub

    Private Sub ContextMenuStripResults_Opening(sender As Object, e As CancelEventArgs) Handles ContextMenuStripResults.Opening
        If TypeOf ContextMenuStripResults.SourceControl IsNot ListView Then Return
        Dim thisListView As ListView = DirectCast(ContextMenuStripResults.SourceControl, ListView)
        Dim selectedCount As Integer = thisListView.SelectedItems.Count
        Dim itemCount As Integer = thisListView.Items.Count
        MenuResultsSelectAll.Enabled = selectedCount < itemCount
        MenuResultsCopyLine.Enabled = selectedCount > 0
        MenuResultsCopyLine.Text = If(selectedCount > 1, "Copy Selected", "Copy Line")
        MenuResultsDelete.Enabled = selectedCount > 0
        MenuResultsClear.Enabled = itemCount > 0
    End Sub

    Private Sub MenuResultsCopyLine_Click(sender As Object, e As EventArgs) Handles MenuResultsCopyLine.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            If thisListView Is ListViewResults Then ResultsCopyLine(thisListView)
        End If
    End Sub

    Private Sub MenuResultsDelete_Click(sender As Object, e As EventArgs) Handles MenuResultsDelete.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then
            Dim thisListView As ListView = DirectCast(thisMenuStrip.SourceControl, ListView)
            For Each item As ListViewItem In thisListView.SelectedItems
                thisListView.Items.Remove(item)
            Next
        End If
    End Sub

    Private Sub MenuResultsSelectAll_Click(sender As Object, e As EventArgs) Handles MenuResultsSelectAll.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then DirectCast(thisMenuStrip.SourceControl, ListView).SelectAll
    End Sub

    Private Sub MenuResultsClear_Click(sender As Object, e As EventArgs) Handles MenuResultsClear.Click
        Dim thisMenuStrip As ContextMenuStrip = TryCast(DirectCast(sender, ToolStripMenuItem).Owner, ContextMenuStrip)
        If thisMenuStrip Is Nothing Then Return
        If TypeOf thisMenuStrip.SourceControl Is ListView Then DirectCast(thisMenuStrip.SourceControl, ListView).Items.Clear()
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        UpdateTabs()
    End Sub

    Private Sub TimerMessageReceive_Tick(sender As Object, e As EventArgs) 'Eventhandler set in OpenJ2534Device
        Dim device As J2534Device = GetTimerParentDevice(DirectCast(sender, Timer))
        If device IsNot Nothing AndAlso device.Setup.GetMessages Then GetMessages(device)
    End Sub

    Private Sub TextBox5BInit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox5BInitECU.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxFIFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFIFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxFilterFlags_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingFilters Then Return
        Dim index As Integer = Array.IndexOf(TextBoxFilterFlags, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterFlags_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            Dim index As Integer = Array.IndexOf(TextBoxPeriodicMessageFlags, sender)
            If index < 0 Then Return
            SetFilterEditBuffer(index)
            UpdateFilterEdit()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxFilterFlags_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(TextBoxFilterFlags, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterFlow_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingFilters Then Return
        Dim index As Integer = Array.IndexOf(TextBoxFilterFlow, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterFlow_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim index As Integer = Array.IndexOf(TextBoxFilterFlow, sender)
                SetFilterEditBuffer(index)
                UpdateFilterEdit()
            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo

            Case Else
                If Not e.KeyChar.IsHex Then e.Handled = True
        End Select
    End Sub

    Private Sub TextBoxFilterFlow_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(TextBoxFilterFlow, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterMask_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingFilters Then Return
        Dim index As Integer = Array.IndexOf(TextBoxFilterMask, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterMask_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim index As Integer = Array.IndexOf(TextBoxFilterMask, sender)
                SetFilterEditBuffer(index)
                UpdateFilterEdit()
            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo

            Case Else
                If Not e.KeyChar.IsHex Then e.Handled = True
        End Select
    End Sub

    Private Sub TextBoxFilterMask_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(TextBoxFilterMask, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterPatt_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingFilters Then Return
        Dim index As Integer = Array.IndexOf(TextBoxFilterPatt, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFilterPatt_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim index As Integer = Array.IndexOf(TextBoxFilterPatt, sender)
                SetFilterEditBuffer(index)
                UpdateFilterEdit()
            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo

            Case Else
                If Not e.KeyChar.IsHex Then e.Handled = True
        End Select
    End Sub

    Private Sub TextBoxFilterPatt_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.TextBoxFilterPatt, sender)
        If index < 0 Then Return
        SetFilterEditBuffer(index)
        UpdateFilterEdit()
    End Sub

    Private Sub TextBoxFIMessage_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxFIMessage.KeyPress
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim index As Integer = Array.IndexOf(TextBoxFilterPatt, sender)
                SetFilterEditBuffer(index)
                UpdateFilterEdit()

            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo


            Case Else
                If Not e.KeyChar.IsHex Then e.Handled = True

        End Select
    End Sub

    Private Sub TextBoxConnectFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxConnectFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxOutFlags_Enter(sender As Object, e As EventArgs) Handles TextBoxOutFlags.Enter
        UpdateButtonSend()
    End Sub

    Private Sub TextBoxOutFlags_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxOutFlags.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxOutFlags_Leave(sender As Object, e As EventArgs) Handles TextBoxOutFlags.Leave
        UpdateButtonSend()
    End Sub

    Private Sub TextBoxFunctionalMessage_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        UpdateFunctionalMessageLookupTableEdit()
    End Sub

    Private Sub TextBoxFunctionalMessage_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            UpdateFunctionalMessageLookupTableEdit()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxFunctionalMessage_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        UpdateFunctionalMessageLookupTableEdit()
    End Sub

    Private Sub TextBoxMessageOut_Enter(sender As Object, e As EventArgs) Handles TextBoxMessageOut.Enter
        UpdateButtonSend()
    End Sub

    Private Sub TextBoxMessageOut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxMessageOut.KeyPress
        Select Case e.KeyChar
            Case KeyChars.Cr
                UpdateButtonSend()
            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo

            Case Else
                If Not (e.KeyChar.IsHex OrElse e.KeyChar = " "c) Then e.Handled = True
        End Select
    End Sub

    Private Sub TextBoxMessageOut_Leave(sender As Object, e As EventArgs) Handles TextBoxMessageOut.Leave
        UpdateButtonSend()
    End Sub

    Private Sub TextBoxParamVal_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.TextBoxParamVal, sender)
        If index < 0 Then Return
        SetEditConfig(index)
        UpdateConfigEdit()
    End Sub

    Private Sub TextBoxParamVal_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            Dim index As Integer = Array.IndexOf(Me.TextBoxParamVal, sender)
            SetEditConfig(index)
            UpdateConfigEdit()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxParamVal_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.TextBoxParamVal, sender)
        If index < 0 Then Return
        SetEditConfig(index)
        UpdateConfigEdit()
    End Sub

    Private Sub TextBoxAnalogRateConfig_TextChanged(sender As Object, e As EventArgs) Handles TextBoxAnalogRateConfig.TextChanged
        UpdateRateControls()
    End Sub

    Private Sub TextBoxAnalogRate_TextChanged(sender As Object, e As EventArgs) Handles TextBoxAnalogRate.TextChanged
        If settingTextBoxAnalogRate Then Return
        SetRateText()
    End Sub

    Private Sub TextBoxAnalogRateConfig_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxAnalogRateConfig.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            UpdateRateControls()
            SetRateText()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxActiveChannelConfig_TextChanged(sender As Object, e As EventArgs) Handles TextBoxActiveChannelConfig.TextChanged
        configChannels = TextBoxActiveChannelConfig.Text.ToUint32
        If Not settingTextBoxActiveChannelConfig Then UpdateChannelCheckBoxes()
    End Sub

    Private Sub TextBoxActiveChannelConfig_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxActiveChannelConfig.KeyPress
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            UpdateChannelCheckBoxes()
            SetChannelText()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxPeriodicMessageFlags_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingPeriodicMessages Then Return
        Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessageFlags, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub TextBoxPeriodicMessageFlags_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        If TypeOf sender IsNot TextBox Then Return
        If e.KeyChar = KeyChars.Cr Then
            Dim index As Integer = Array.IndexOf(TextBoxPeriodicMessageFlags, sender)
            SetPeriodicMessageEditBuffer(index)
            UpdatePeriodicMessageEdit()
            e.Handled = True
        Else
            FlagsKeyPress(sender, e)
        End If
    End Sub

    Private Sub TextBoxPeriodicMessageFlags_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessageFlags, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub TextBoxPeriodicMessageInterval_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingPeriodicMessages Then Return
        Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessageInterval, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub TextBoxPeriodicMessageInterval_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim index As Integer = Array.IndexOf(TextBoxPeriodicMessageInterval, sender)
                SetPeriodicMessageEditBuffer(index)
                UpdatePeriodicMessageEdit()

            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo


            Case Else
                e.Handled = Not Char.IsDigit(e.KeyChar)

        End Select
    End Sub

    Private Sub TextBoxPeriodicMessageInterval_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessageInterval, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
    End Sub

    Private Sub TextBoxPeriodicMessage_TextChanged(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        If settingPeriodicMessages Then Return
        Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessage, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
        UpdatePeriodicMessageCheck(index)
    End Sub

    Private Sub TextBoxPeriodicMessage_KeyPress(sender As Object, e As KeyPressEventArgs) 'Event Handler added in SetControlArrays
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessage, sender)
                SetPeriodicMessageEditBuffer(index)
                UpdatePeriodicMessageEdit()
                UpdatePeriodicMessageCheck(index)

            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo


            Case Else
                If Not e.KeyChar.IsHex Then e.Handled = True

        End Select
    End Sub

    Private Sub TextBoxPeriodicMessage_Leave(sender As Object, e As EventArgs) 'Event Handler added in SetControlArrays
        Dim index As Integer = Array.IndexOf(Me.TextBoxPeriodicMessage, sender)
        If index < 0 Then Return
        SetPeriodicMessageEditBuffer(index)
        UpdatePeriodicMessageEdit()
        If ActiveControl IsNot CheckBoxPeriodicMessageEnable(index) Then UpdatePeriodicMessageCheck(index)
    End Sub

    Private Sub TextBoxReadRate_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxReadRate.KeyPress
        Select Case e.KeyChar
            Case KeyChars.Cr
                Dim readRate As Integer = 0
                Integer.TryParse(TextBoxReadRate.Text, readRate)
                If selectedDevice IsNot Nothing Then If readRate <> 0 Then selectedDevice.TimerMessageReceive.Interval = readRate

            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo


            Case Else
                e.Handled = Not Char.IsDigit(e.KeyChar)

        End Select
    End Sub

    Private Sub TextBoxTimeOut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxTimeOut.KeyPress
        Select Case e.KeyChar
            Case KeyChars.Cr


            Case KeyChars.Back, KeyChars.Copy, KeyChars.Paste, KeyChars.Cut, KeyChars.Undo


            Case Else
                e.Handled = Not Char.IsDigit(e.KeyChar)

        End Select
    End Sub

    Private Sub TextBoxVoltSetting_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxVoltSetting.KeyPress
        If e.KeyChar = KeyChars.Cr Then
            UpdatePinVoltage()
        End If
        e.Handled = Not Char.IsDigit(e.KeyChar)
    End Sub

    Private Sub TextBoxVoltSetting_Leave(sender As Object, e As EventArgs) Handles TextBoxVoltSetting.Leave
        UpdatePinVoltage()
    End Sub

    Private Sub UpdatePinVoltage()
        Dim pinVoltage As UInteger
        UInteger.TryParse(TextBoxVoltSetting.Text, pinVoltage)
        If pinVoltage < 5000 Then pinVoltage = 5000
        If pinVoltage > 20000 Then pinVoltage = 20000
        TextBoxVoltSetting.Text = pinVoltage.ToString
        If selectedDevice IsNot Nothing Then selectedDevice.Analog.PinSetting = pinVoltage
    End Sub

    Private Sub UpdateComboBoxAvailableChannel()
        UpdateComboBoxAvailableChannel(Nothing)
    End Sub
    Private Sub UpdateComboBoxAvailableChannel(ByVal name As String)
        Dim m As Integer = -1
        If selectedDevice IsNot Nothing Then m = selectedDevice.MaxChannel
        Dim selectedChannel As String = ComboBoxAvailableChannel.Text
        ComboBoxAvailableChannel.Items.Clear()
        For index As Integer = 0 To m
            If selectedDevice.Channel(index).Connected Then ComboBoxAvailableChannel.Items.Add(selectedDevice.Channel(index))
        Next index
        If ComboBoxAvailableChannel.Items.Count > 0 Then
            ComboBoxAvailableChannel.Text = selectedChannel
            If Not String.IsNullOrWhiteSpace(name) Then ComboBoxAvailableChannel.Text = name
            If String.IsNullOrWhiteSpace(ComboBoxAvailableChannel.Text) Then ComboBoxAvailableChannel.SelectedIndex = 0
        End If
        ComboBoxAvailableChannel.Enabled = ComboBoxAvailableChannel.Items.Count > 1
        For i As Integer = 1 To LabelChannel.Length - 1
            LabelChannel(i).Enabled = ComboBoxAvailableChannel.Enabled
        Next i
    End Sub

    Private Sub UpdateComboBoxMessageChannel()
        UpdateComboBoxMessageChannel(Nothing)
    End Sub
    Private Sub UpdateComboBoxMessageChannel(ByVal name As String)
        Dim m As Integer = -1
        If selectedDevice IsNot Nothing Then m = selectedDevice.MaxChannel
        Dim selectedChannel As String = ComboBoxMessageChannel.Text
        ComboBoxMessageChannel.Items.Clear()
        For index As Integer = 0 To m
            Dim channel As J2534Channel = selectedDevice.Channel(index)
            If channel.Connected AndAlso Not channel.IsAnalog Then
                ComboBoxMessageChannel.Items.Add(channel)
            ElseIf channel.IsCAN Then
                Dim channelISO As J2534Channel = CAN_ChannelToISO_Channel(selectedDevice, channel)
                If channelISO IsNot Nothing Then
                    If channelISO.Connected AndAlso channelISO.MixedMode Then
                        ComboBoxMessageChannel.Items.Add(channel)
                    End If
                End If
            End If
        Next index
        If ComboBoxMessageChannel.Items.Count > 0 Then
            ComboBoxMessageChannel.Text = selectedChannel
            If Not String.IsNullOrWhiteSpace(name) Then ComboBoxMessageChannel.Text = name
            If String.IsNullOrWhiteSpace(ComboBoxMessageChannel.Text) Then ComboBoxMessageChannel.SelectedIndex = 0
        End If
        ComboBoxMessageChannel.Enabled = ComboBoxMessageChannel.Items.Count > 1
    End Sub

    Private Sub UpdateComboBoxConnectChannel(ByVal device As J2534Device)
        Dim selectedChannel As String = ComboBoxConnectChannel.Text
        ComboBoxConnectChannel.Items.Clear()
        If device?.ChannelSet IsNot Nothing Then ComboBoxConnectChannel.Items.AddRange(device.ChannelSet)
        If ComboBoxConnectChannel.Items.Count > 0 Then ComboBoxConnectChannel.SelectedIndex = 0
        ComboBoxConnectChannel.Text = selectedChannel
        If Not String.IsNullOrWhiteSpace(device?.Setup?.ConnectChannelName) Then ComboBoxConnectChannel.Text = device.Setup.ConnectChannelName
    End Sub

    Private Sub UpdateConnectControls()
        ComboBoxConnectChannel.Enabled = selectedDevice IsNot Nothing AndAlso selectedDevice.Connected
        Dim channel As J2534Channel = TryCast(ComboBoxConnectChannel.SelectedItem, J2534Channel)
        Dim connected As Boolean = ComboBoxConnectChannel.Enabled AndAlso channel IsNot Nothing AndAlso channel.Connected
        LabelConnectChannel.Enabled = ComboBoxConnectChannel.Enabled
        If channel IsNot Nothing Then
            ButtonConnect.Enabled = ComboBoxConnectChannel.Enabled AndAlso Not connected
            If channel.IsISO15765 Then
                CheckBoxMixedMode.Enabled = ButtonConnect.Enabled
                CheckBoxMixedMode.Checked = channel.MixedMode
            Else
                CheckBoxMixedMode.Enabled = False
                CheckBoxMixedMode.Checked = False
            End If
            UpdateComboBoxBaudRate(channel)
            UpdateComboBoxConnector(channel)
            UpdateComboBoxPins(channel)
        Else
            ButtonConnect.Enabled = False
            CheckBoxMixedMode.Enabled = False
            CheckBoxMixedMode.Checked = False
            ComboBoxBaudRate.Items.Clear()
            ComboBoxBaudRate.Text = String.Empty
            ComboBoxBaudRate.Enabled = False
            TextBoxConnectFlags.Text = String.Empty
            TextBoxConnectFlags.Enabled = False
            LabelConnectFlags.Enabled = False
            ComboBoxConnector.Items.Clear()
            ComboBoxConnector.Enabled = False
            LabelConn.Enabled = False
            ComboBoxPins.Items.Clear()
            ComboBoxPins.Enabled = False
            LabelPins.Enabled = False
        End If
        LabelBaud.Enabled = ComboBoxBaudRate.Enabled
        ButtonDisconnect.Enabled = connected AndAlso ComboBoxConnectChannel.Enabled
    End Sub

    Private Sub UpdateMessageControls()
        UpdateComboBoxMessageChannel()
        SetMessageControls()
        If selectedDevice IsNot Nothing Then
            Dim i As Integer = 0
            Integer.TryParse(TextBoxReadRate.Text, i)
            If i = 0 Then
                selectedDevice.TimerMessageReceive.Enabled = False
            Else
                selectedDevice.TimerMessageReceive.Interval = i
            End If
            If selectedDevice.Setup.GetMessages Then
                ButtonReceive.Text = "Stop"
            Else
                ButtonReceive.Text = "Start"
            End If
        Else
            ButtonReceive.Text = "Start"
        End If
        UpdateButtonSend()
        UpdateButtonClaimJ1939Address()
        ButtonReceive.Enabled = ComboBoxAvailableChannel.Items.Count > 0
        ButtonClearList.Enabled = ButtonReceive.Enabled
    End Sub

    Private Sub UpdateButtonClaimJ1939Address()
        Dim channel As J2534Channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
        If channel IsNot Nothing Then
            ButtonClaimJ1939Address.Visible = channel.IsJ1939
            ButtonClaimJ1939Address.Enabled = ButtonClaimJ1939Address.Visible AndAlso channel.Connected
        Else
            ButtonClaimJ1939Address.Visible = False
            ButtonClaimJ1939Address.Enabled = ButtonClaimJ1939Address.Visible
        End If
    End Sub

    Private Sub UpdateComboBoxBaudRate(ByVal channel As J2534Channel)
        ComboBoxBaudRate.Items.Clear()
        ComboBoxBaudRate.Text = String.Empty
        If selectedDevice IsNot Nothing AndAlso selectedDevice.Connected Then
            Dim connected As Boolean = channel IsNot Nothing AndAlso channel.Connected
            Dim baudRate As UInteger = channel.BaudRate
            Dim flags As UInteger = channel.ConnectFlags
            If (Not connected) AndAlso flags = 0 Then flags = channel.DefaultConnectFlags
            Dim comboIndex As Integer = channel.DefaultBaudRateIndex
            For i As Integer = 0 To channel.BaudRates.Length - 1
                ComboBoxBaudRate.Items.Add(channel.BaudRates(i).ToString)
                If baudRate = channel.BaudRates(i) Then comboIndex = i
            Next i
            If ComboBoxBaudRate.Items.Count > 0 Then
                If connected AndAlso baudRate <> 0 Then
                    ComboBoxBaudRate.Text = baudRate.ToString
                Else
                    If comboIndex >= channel.BaudRates.Length Then
                        comboIndex = channel.BaudRates.Length - 1
                    End If
                    ComboBoxBaudRate.Text = channel.BaudRates(comboIndex).ToString
                End If
            End If
            ComboBoxBaudRate.Enabled = (ComboBoxBaudRate.Items.Count > 1) AndAlso (Not connected)
            If channel?.IsAnalog Then
                TextBoxConnectFlags.Text = String.Empty
                TextBoxConnectFlags.Enabled = False
                LabelConnectFlags.Enabled = False
            Else
                TextBoxConnectFlags.Text = FlagsToText(flags)
                TextBoxConnectFlags.Enabled = Not connected
                LabelConnectFlags.Enabled = TextBoxConnectFlags.Enabled
            End If

        End If
    End Sub

    Private Sub UpdateComboBoxConnector(ByVal channel As J2534Channel)
        ComboBoxConnector.Items.Clear()
        If selectedDevice Is Nothing OrElse channel Is Nothing Then
            ComboBoxConnector.Enabled = False
        Else

            If channel.J1962Pins.Length > 0 Then ComboBoxConnector.Items.Add("J1962")
            If channel.J1939Pins.Length > 0 Then ComboBoxConnector.Items.Add("J1939")
            If channel.J1708Pins.Length > 0 Then ComboBoxConnector.Items.Add("J1708")
            If ComboBoxConnector.Items.Count > 0 Then
                Select Case channel.Connector
                    Case J1962_PINS
                        ComboBoxConnector.Text = "J1962"

                    Case J1939_PINS
                        ComboBoxConnector.Text = "J1939"

                    Case J1708_PINS
                        ComboBoxConnector.Text = "J1708"

                    Case Else
                        ComboBoxConnector.SelectedIndex = 0

                End Select
            End If
            ComboBoxConnector.Enabled = channel.IsPinSwitched AndAlso selectedDevice.Connected AndAlso (Not channel.Connected) AndAlso channel.SelectedPins = 0

        End If
        LabelConn.Enabled = ComboBoxConnector.Enabled
    End Sub

    Private Sub UpdateComboBoxPins(ByVal channel As J2534Channel)
        If channel Is Nothing Then Return
        Dim pins As UInteger() = {}
        Dim setting As Integer = ComboBoxPins.SelectedIndex
        ComboBoxPins.Items.Clear()

        Select Case ComboBoxConnector.Text
            Case "J1962"
                pins = channel.J1962Pins

            Case "J1939"
                pins = channel.J1939Pins

            Case "J1708"
                pins = channel.J1708Pins

        End Select

        For i As Integer = 0 To pins.Length - 1
            ComboBoxPins.Items.Add(PinsToText(pins(i)))
        Next i

        If ComboBoxPins.Items.Count > 0 Then
            If channel.Connected Then
                ComboBoxPins.SelectedIndex = FindPin(pins, channel.SelectedPins)
            ElseIf setting >= 0 AndAlso setting < ComboBoxPins.Items.Count Then
                ComboBoxPins.SelectedIndex = setting
            Else
                ComboBoxPins.SelectedIndex = 0
            End If
        End If
        ComboBoxPins.Enabled = ComboBoxConnector.Enabled
        LabelPins.Enabled = ComboBoxPins.Enabled
    End Sub

    Private Shared Function PinsToText(ByVal pins As UInteger) As String
        Dim pin As New PinMap With {.Value = pins}
        If pin.LoPin = 0 Then
            Return pin.HiPin & " Only"
        Else
            Return pin.HiPin & ", " & pin.LoPin
        End If
    End Function

    Private Shared Function FindPin(ByVal pinSet As UInteger(), ByVal pins As UInteger) As Integer
        Dim result As Integer = 0
        For i As Integer = 0 To pinSet.Length - 1
            If pins = pinSet(i) Then
                result = i
                Exit For
            End If
        Next i
        Return result
    End Function

    Private Sub UpdateButtonSend()
        If ButtonSend.Focused Then Return
        ButtonSend.Enabled = False
        Dim channelIsCan As Boolean = False
        If selectedDevice IsNot Nothing Then
            Dim channel As J2534Channel = Nothing
            If selectedDevice.ListViewMessageOut.Focused Then
                If selectedDevice.ListViewMessageOut.SelectedItems.Count > 0 Then
                    channel = selectedDevice.Channel(selectedDevice.ListViewMessageOut.SelectedItems(0).Text)
                    If IsSendable(selectedDevice, channel) Then
                        ButtonSend.Tag = selectedDevice.ListViewMessageOut
                        ButtonSend.Enabled = True
                    End If
                End If
                ButtonSend.Text = "Send" & Environment.NewLine & "Selected"
            ElseIf Not ButtonSend.Enabled Then
                If ComboBoxMessageChannel.SelectedIndex >= 0 AndAlso TextBoxOutFlags.Text.Length > 0 And TextBoxMessageOut.Text.Length > 0 Then
                    channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
                    If IsSendable(selectedDevice, channel) Then
                        ButtonSend.Tag = TextBoxMessageOut
                        ButtonSend.Enabled = True
                    End If
                End If
                ButtonSend.Text = "Send" & Environment.NewLine & "ScratchPad"
            End If
            channelIsCan = channel IsNot Nothing AndAlso channel.IsCAN
        End If
        CheckBoxPadMessage.Visible = ButtonSend.Enabled AndAlso channelIsCan
    End Sub

    Public Sub GetMessages(ByVal device As J2534Device)
        If device?.API?.PassThruReadMsgs Is Nothing Then Return
        If device.Connected Then
            For index As Integer = 0 To device.MaxChannel
                If device.Channel(index).Connected Then
                    Dim count As Integer = 100
                    Dim size As Integer = Marshal.SizeOf(GetType(PASSTHRU_MSG))
                    Dim pointer As IntPtr = Marshal.AllocHGlobal(size * count)
                    Dim result As J2534Result = device.API.PassThruReadMsgs(device.Channel(index).ChannelId, pointer.ToInt32, count, 0)
                    If result = J2534Result.STATUS_NOERROR OrElse result = J2534Result.ERR_TIMEOUT OrElse result = J2534Result.ERR_BUFFER_OVERFLOW Then
                        If count > 100 Then count = 100
                        For m As Integer = 0 To count - 1
                            Dim message As PASSTHRU_MSG = DirectCast(Marshal.PtrToStructure(pointer + size * m, GetType(PASSTHRU_MSG)), PASSTHRU_MSG)
                            If IsAnalog(message.ProtocolId) Then
                                ProcessAnalogMessage(device, device.Channel(index), message)
                                ShowAnalogInputs(device)
                            End If
                            DisplayMessage(device, message.ToMessageText)
                        Next m
                    End If
                    ShowResult(device, result, "PassThruReadMsgs")
                    Marshal.FreeHGlobal(pointer)
                End If
            Next index
        End If
        ToolStripStatusLabel1.Text = "In: " & selectedDevice.MessageInCount & "     Out: " & selectedDevice.MessageOutCount
    End Sub

    Public Sub SetFilterControls()
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse channel.IsAnalog Then
            ClearFilterControls()
        Else
            Dim messageText As J2534MessageText
            Dim mixedMode As Boolean = channel.MixedMode
            BuildFilterComboBoxes(channel.IsISO15765, mixedMode)
            For i As Integer = 0 To MAX_FILTER
                messageText = channel.Filter(i).MaskMessage.ToMessageText
                TextBoxFilterMask(i).Text = messageText.Data
                messageText = channel.Filter(i).PatternMessage.ToMessageText
                TextBoxFilterPatt(i).Text = messageText.Data
                messageText = channel.Filter(i).FlowMessage.ToMessageText
                TextBoxFilterFlow(i).Text = messageText.Data
                TextBoxFilterFlags(i).Text = messageText.TxFlags
                ComboBoxFilterType(i).Text = FilterTypeToName(channel.Filter(i).FilterType)
                LabelFilterId(i).Text = If(channel.Filter(i).Enabled, "*", "")
                LabelFilter(i).Enabled = True
                TextBoxFilterMask(i).Enabled = True
                TextBoxFilterPatt(i).Enabled = True
                TextBoxFilterFlow(i).Enabled = channel.IsISO15765
                TextBoxFilterFlags(i).Enabled = True
                ComboBoxFilterType(i).Enabled = True
                LabelFilterId(i).Enabled = True
                FilterEditBuffer(i) = channel.Filter(i).Clone
            Next i
            LabelFilterMask.Enabled = True
            LabelFilterPatt.Enabled = True
            LabelFilterFlow.Enabled = channel.IsISO15765
            LabelFilterFlags.Enabled = True
            LabelFilterType.Enabled = True
            LabelFilterIds.Enabled = True
            ButtonClearAllFilter.Enabled = True
            ButtonCreatePassFilter.Visible = ((Not channel.IsISO15765) OrElse mixedMode) AndAlso (channel.NextFilter >= 0)
        End If
    End Sub

    Private Sub ClearFilterControls()
        settingFilters = True
        For i As Integer = 0 To MAX_FILTER
            TextBoxFilterMask(i).Text = String.Empty
            TextBoxFilterPatt(i).Text = String.Empty
            TextBoxFilterFlow(i).Text = String.Empty
            ComboBoxFilterType(i).SelectedIndex = 0
            LabelFilter(i).Enabled = False
            TextBoxFilterFlags(i).Text = String.Empty
            TextBoxFilterMask(i).Enabled = False
            TextBoxFilterPatt(i).Enabled = False
            TextBoxFilterFlow(i).Enabled = False
            TextBoxFilterFlags(i).Enabled = False
            ComboBoxFilterType(i).Enabled = False
            LabelFilterId(i).Text = String.Empty
            LabelFilterId(i).Enabled = False
        Next i
        LabelFilterMask.Enabled = False
        LabelFilterPatt.Enabled = False
        LabelFilterFlow.Enabled = False
        LabelFilterFlags.Enabled = False
        LabelFilterType.Enabled = False
        LabelFilterIds.Enabled = False
        ButtonClearAllFilter.Enabled = False
        ButtonApplyFilter.Enabled = False
        ButtonCancelFilter.Enabled = False
        ButtonCreatePassFilter.Visible = False
        settingFilters = False
    End Sub

    Private Sub SetFilterEditBuffer(ByVal index As Integer)
        Dim maskTxt As New J2534MessageText With
        {
            .Data = TextBoxFilterMask(index).Text,
            .ProtocolName = ComboBoxAvailableChannel.Text,
            .TxFlags = TextBoxFilterFlags(index).Text
        }
        FilterEditBuffer(index).MaskMessage = maskTxt.ToMessage

        Dim pattTxt As New J2534MessageText With
        {
            .Data = TextBoxFilterPatt(index).Text,
            .ProtocolName = ComboBoxAvailableChannel.Text,
            .TxFlags = TextBoxFilterFlags(index).Text
        }
        FilterEditBuffer(index).PatternMessage = pattTxt.ToMessage

        Dim flowTxt As New J2534MessageText With
        {
            .Data = TextBoxFilterFlow(index).Text,
            .ProtocolName = ComboBoxAvailableChannel.Text,
            .TxFlags = TextBoxFilterFlags(index).Text
        }
        FilterEditBuffer(index).FlowMessage = flowTxt.ToMessage

        FilterEditBuffer(index).FilterType = NameToFilterType(ComboBoxFilterType(index).Text)

    End Sub

    Private Function FilterChangesPresent() As Boolean
        Return FilterChangesPresent(-1)
    End Function
    Private Function FilterChangesPresent(ByVal index As Integer) As Boolean
        Dim result As Boolean = False
        Dim loFilter, hiFilter As Integer
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse channel.IsAnalog Then Return result
        If index < 0 Then
            loFilter = 0
            hiFilter = MAX_FILTER
        Else
            loFilter = index
            hiFilter = index
        End If
        Dim compare As MessageCompareResults
        For i As Integer = loFilter To hiFilter
            If channel.Filter(i).FilterType <> FilterEditBuffer(i).FilterType Then result = True
            compare = CompareMessages(channel.Filter(i).MaskMessage, FilterEditBuffer(i).MaskMessage)
            If (compare And Not MessageCompareResults.ProtocolMismatch) <> MessageCompareResults.None Then result = True
            compare = CompareMessages(channel.Filter(i).PatternMessage, FilterEditBuffer(i).PatternMessage)
            If (compare And Not MessageCompareResults.ProtocolMismatch) <> MessageCompareResults.None Then result = True
            compare = CompareMessages(channel.Filter(i).FlowMessage, FilterEditBuffer(i).FlowMessage)
            If (compare And Not MessageCompareResults.ProtocolMismatch) <> MessageCompareResults.None Then result = True
        Next i
        Return result
    End Function

    Public Sub UpdateFilterEdit()
        ButtonApplyFilter.Enabled = FilterChangesPresent()
        ButtonCancelFilter.Enabled = ButtonApplyFilter.Enabled
    End Sub

    Private Sub BuildFilterComboBoxes(ByVal isISO15765 As Boolean, ByVal mixedMode As Boolean)
        For i As Integer = 0 To MAX_FILTER
            ComboBoxFilterType(i).Items.Clear()
            ComboBoxFilterType(i).Items.Add("None")
            If Not isISO15765 OrElse mixedMode Then
                ComboBoxFilterType(i).Items.Add("Pass")
                ComboBoxFilterType(i).Items.Add("Block")
            End If
            If isISO15765 Then
                ComboBoxFilterType(i).Items.Add("Flow")
            End If
            ComboBoxFilterType(i).SelectedIndex = 0
        Next i
    End Sub

    Public Sub SetPeriodicMessageControls(ByVal device As J2534Device)
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse channel.IsAnalog Then
            ClearPeriodicMessageControls()
        Else
            Dim channelCAN As J2534Channel = Nothing
            Dim mixedMode As Boolean = channel.MixedMode AndAlso channel.IsISO15765
            LabelPeriodicMessageChannel.Visible = mixedMode
            If mixedMode Then channelCAN = ISO_ChannelToCAN_Channel(device, channel)
            For i As Integer = 0 To MAX_PM
                Dim messageText As J2534MessageText = channel.PeriodicMessage(i).Message.ToMessageText
                ComboBoxPeriodicMessageChannel(i).Items.Clear()
                If mixedMode AndAlso channelCAN IsNot Nothing Then ComboBoxPeriodicMessageChannel(i).Items.Add(channelCAN)
                ComboBoxPeriodicMessageChannel(i).Items.Add(channel)
                ComboBoxPeriodicMessageChannel(i).Text = messageText.ProtocolName
                If mixedMode Then
                    TextBoxPeriodicMessage(i).Width = 265
                    ComboBoxPeriodicMessageChannel(i).Visible = True
                Else
                    TextBoxPeriodicMessage(i).Width = 377
                    ComboBoxPeriodicMessageChannel(i).Visible = False
                End If
                TextBoxPeriodicMessage(i).Text = messageText.Data
                TextBoxPeriodicMessageFlags(i).Text = messageText.TxFlags
                If channel.PeriodicMessage(i).Interval < 5 OrElse channel.PeriodicMessage(i).Interval > 65535 Then
                    channel.PeriodicMessage(i).Interval = 100
                End If
                TextBoxPeriodicMessageInterval(i).Text = channel.PeriodicMessage(i).Interval.ToString
                LabelPeriodicMessageId(i).Text = channel.PeriodicMessage(i).MessageId.ToString
                CheckBoxPeriodicMessageEnable(i).Checked = channel.PeriodicMessage(i).Enabled
                LabelPeriodicMessage(i).Enabled = True
                TextBoxPeriodicMessage(i).Enabled = True
                TextBoxPeriodicMessageFlags(i).Enabled = True
                TextBoxPeriodicMessageInterval(i).Enabled = True
                LabelPeriodicMessageId(i).Enabled = True
                CheckBoxPeriodicMessageEnable(i).Enabled = True
                PeriodicMessageEditBuffer(i) = channel.PeriodicMessage(i).Clone
                PeriodicMessageCheckBoxHit(i) = False
            Next i
            LabelPMMessage.Enabled = True
            LabelPeriodicMessageFlags.Enabled = True
            LabelPeriodicMessageInterval.Enabled = True
            LabelPeriodicMessageIds.Enabled = True
            LabelPeriodicMessageDelete.Enabled = True
            ButtonClearAllPeriodicMessages.Enabled = True
            CheckBoxPadMessage.Checked = True
        End If
    End Sub

    Private Sub ClearPeriodicMessageControls()
        settingPeriodicMessages = True
        For i As Integer = 0 To MAX_PM
            ComboBoxPeriodicMessageChannel(i).Items.Clear()
            TextBoxPeriodicMessage(i).Text = String.Empty
            CheckBoxPeriodicMessageEnable(i).Checked = False
            LabelPeriodicMessage(i).Enabled = False
            TextBoxPeriodicMessageFlags(i).Text = String.Empty
            TextBoxPeriodicMessage(i).Enabled = False
            TextBoxPeriodicMessageFlags(i).Enabled = False
            TextBoxPeriodicMessageInterval(i).Text = String.Empty
            TextBoxPeriodicMessageInterval(i).Enabled = False
            CheckBoxPeriodicMessageEnable(i).Enabled = False
            LabelPeriodicMessageId(i).Text = "-"
            LabelPeriodicMessageId(i).Enabled = False
            TextBoxPeriodicMessage(i).Width = 377
            ComboBoxPeriodicMessageChannel(i).Visible = False
        Next i
        settingPeriodicMessages = False
        LabelPMMessage.Enabled = False
        LabelPeriodicMessageChannel.Visible = False
        LabelPeriodicMessageFlags.Enabled = False
        LabelPeriodicMessageInterval.Enabled = False
        LabelPeriodicMessageIds.Enabled = False
        LabelPeriodicMessageDelete.Enabled = False
        ButtonClearAllPeriodicMessages.Enabled = False
        ButtonApplyPeriodicMessages.Enabled = False
        ButtonCancelPeriodicMessages.Enabled = False
    End Sub

    Private Sub SetPeriodicMessageEditBuffer(ByVal index As Integer)
        If index < 0 Then Return
        Dim messageText As New J2534MessageText With
        {
            .Data = TextBoxPeriodicMessage(index).Text,
            .TxFlags = TextBoxPeriodicMessageFlags(index).Text
        }
        If String.IsNullOrWhiteSpace(messageText.Data) Then
            messageText.ProtocolName = String.Empty
        Else
            messageText.ProtocolName = ComboBoxPeriodicMessageChannel(index).Text
            If selectedDevice Is Nothing OrElse Not selectedDevice.HasChannel(messageText.ProtocolName) Then
                messageText.ProtocolName = ComboBoxAvailableChannel.Text
            End If
        End If
        PeriodicMessageEditBuffer(index).Message = messageText.ToMessage
        UInteger.TryParse(TextBoxPeriodicMessageInterval(index).Text, PeriodicMessageEditBuffer(index).Interval)
    End Sub

    Private Function PeriodicMessageChangesPresent() As Boolean
        Return PeriodicMessageChangesPresent(-1)
    End Function
    Private Function PeriodicMessageChangesPresent(ByVal index As Integer) As Boolean
        Dim result As Boolean = False
        Dim loPM, hiPM As Integer
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse index > MAX_PM OrElse channel.IsAnalog Then Return result
        If index < 0 Then
            loPM = 0
            hiPM = MAX_PM
        Else
            loPM = index
            hiPM = index
        End If
        For i As Integer = loPM To hiPM
            If channel.PeriodicMessage(i).Interval <> PeriodicMessageEditBuffer(i).Interval Then result = True
            If CompareMessages(channel.PeriodicMessage(i).Message, PeriodicMessageEditBuffer(i).Message) <> 0 Then result = True
            If channel.PeriodicMessage(i).Enabled <> CheckBoxPeriodicMessageEnable(i).Checked Then result = True
        Next i
        Return result
    End Function

    Public Sub UpdatePeriodicMessageEdit()
        ButtonApplyPeriodicMessages.Enabled = PeriodicMessageChangesPresent()
        ButtonCancelPeriodicMessages.Enabled = ButtonApplyPeriodicMessages.Enabled
    End Sub

    Private Sub UpdatePeriodicMessageCheck(ByVal index As Integer)
        If index > MAX_PM OrElse index < 0 Then Return
        If TextBoxPeriodicMessage(index).Text.Length > 0 Then
            If Not PeriodicMessageCheckBoxHit(index) Then CheckBoxPeriodicMessageEnable(index).Checked = True
        Else
            CheckBoxPeriodicMessageEnable(index).Checked = False
        End If
    End Sub

    Private Sub UpdateTabs()
        Dim showComboBoxAvailableProtocol As Boolean = True
        Dim showComboBoxAvailableDevice As Boolean = True
        Dim tab As TabPage = TabControl1.SelectedTab
        If tab Is TabPageConnect Then
            showComboBoxAvailableProtocol = False
            UpdateConnectControls()
        ElseIf tab Is TabPageMessages Then
            UpdateButtonSend()
            showComboBoxAvailableProtocol = False
            UpdateMessageControls()
        ElseIf tab Is TabPagePeriodicMessages Then
            SetPeriodicMessageControls(selectedDevice)
        ElseIf tab Is TabPageFilters Then
            SetFilterControls()
            UpdateFilterEdit()
        ElseIf tab Is TabPageConfig Then
            SetConfigControls()
        ElseIf tab Is TabPageInit Then
            SetInitControls()
        ElseIf tab Is TabPageFunctionalMessages Then
            SetFunctionalMessageLookupTableControls()
        ElseIf tab Is TabPageAnalog Then
            SetVoltControls()
            showComboBoxAvailableProtocol = False
        ElseIf tab Is TabPageResults Then
            showComboBoxAvailableProtocol = False
            showComboBoxAvailableDevice = False
        End If

        ComboBoxAvailableChannel.Parent = tab
        For Each locator As Label In ComboAvailableChannelLocator
            If locator.Parent Is tab Then ComboBoxAvailableChannel.Location = locator.Location
        Next locator
        ComboBoxAvailableChannel.Visible = showComboBoxAvailableProtocol

        For Each locator As Label In ComboAvailableBoxLocator
            If tab Is TabPageConnect Then
                If locator.Parent Is GroupBoxConnect Then ComboBoxAvailableDevice.Location = locator.Location
                ComboBoxAvailableDevice.Parent = GroupBoxConnect
            Else
                If locator.Parent Is tab Then ComboBoxAvailableDevice.Location = locator.Location
                ComboBoxAvailableDevice.Parent = tab
            End If

        Next locator
        ComboBoxAvailableDevice.Visible = showComboBoxAvailableDevice

    End Sub

    Private Sub SetConfigControls()
        If selectedDevice Is Nothing Then
            ClearAndDisableConfigControls()
            Return
        End If
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then
            ClearAndDisableConfigControls()
        Else
            GetConfigParameterValues(selectedDevice, channel)
            ClearAndEnableConfigControls()
            For i As Integer = 0 To channel.ConfParameter.Length - 1
                If channel.ConfParameter(i).Id <> 0 Then
                    LabelParameterName(i).Text = channel.ConfParameter(i).Id.ToString
                    LabelParameterName(i).Visible = True
                    Select Case channel.ConfParameter(i).Id
                        Case NODE_ADDRESS
                            LabelParamVal(i).Text = "0x" & channel.ConfParameter(i).Value.ToString("X2")

                        Case J1962_PINS, J1939_PINS, J1708_PINS
                            LabelParamVal(i).Text = "0x" & channel.ConfParameter(i).Value.ToString("X4")

                        Case ACTIVE_CHANNELS
                            LabelParamVal(i).Text = "0x" & channel.ConfParameter(i).Value.ToString("X8")
                            TextBoxActiveChannelConfig = TextBoxParamVal(i)
                            configChannels = channel.ConfParameter(i).Value
                            UpdateChannelCheckBoxes()

                        Case SAMPLE_RATE
                            LabelParamVal(i).Text = "0x" & channel.ConfParameter(i).Value.ToString("X8")
                            TextBoxAnalogRateConfig = TextBoxParamVal(i)
                            configRate = channel.ConfParameter(i).Value
                            SetRateControls()

                        Case Else
                            LabelParamVal(i).Text = channel.ConfParameter(i).Value.ToString

                    End Select
                    LabelParamVal(i).Visible = True
                    TextBoxParamVal(i).Text = channel.ConfParameter(i).NewText
                    TextBoxParamVal(i).Visible = Not channel.ConfParameter(i).IsReadOnly
                    If i = 12 Then
                        LabelParameters1.Visible = True
                        LabelValues1.Visible = True
                        LabelMod1.Visible = True
                    End If

                End If
            Next i
            If channel.IsAnalog Then
                ShowAnalogConfig()
            End If
            LabelParameters0.Enabled = True
            LabelParameters1.Enabled = True
            LabelValues0.Enabled = True
            LabelValues1.Enabled = True
            LabelMod0.Enabled = True
            LabelMod1.Enabled = True
        End If
        UpdateConfigEdit()
    End Sub

    Private Sub ClearAndEnableConfigControls()
        For Each label As Label In LabelParamVal
            label.Visible = False
        Next label
        For Each label As Label In LabelParameterName
            label.Visible = False
        Next label
        For Each txtBox As TextBox In TextBoxParamVal
            txtBox.Visible = False
        Next txtBox
        LabelParameters0.Enabled = True
        LabelParameters1.Enabled = True
        LabelParameters1.Visible = False
        LabelValues0.Enabled = True
        LabelValues1.Enabled = True
        LabelValues1.Visible = False
        LabelMod0.Enabled = True
        LabelMod1.Enabled = True
        LabelMod1.Visible = False
        ButtonSetConfig.Enabled = True
        ButtonClearConfig.Enabled = True
        TextBoxActiveChannelConfig = Nothing
        TextBoxAnalogRateConfig = Nothing
        HideAnalogConfig()
    End Sub

    Private Sub ClearAndDisableConfigControls()
        For Each item As Label In LabelParamVal
            item.Visible = False
        Next item
        For Each item As Label In LabelParameterName
            item.Visible = False
        Next item
        For Each item As TextBox In TextBoxParamVal
            item.Visible = False
        Next item
        LabelParameters0.Enabled = False
        LabelParameters1.Enabled = False
        LabelParameters1.Visible = False
        LabelValues0.Enabled = False
        LabelValues1.Enabled = False
        LabelValues1.Visible = False
        LabelMod0.Enabled = False
        LabelMod1.Enabled = False
        LabelMod1.Visible = False
        ButtonSetConfig.Enabled = False
        ButtonClearConfig.Enabled = False
        TextBoxActiveChannelConfig = Nothing
        TextBoxAnalogRateConfig = Nothing
        HideAnalogConfig()
    End Sub

    Private Sub GetConfigParameterValues(ByVal device As J2534Device, ByVal channel As J2534Channel)
        If device Is Nothing OrElse channel Is Nothing Then Return
        For i As Integer = 0 To channel.ConfParameter.Length - 1
            If channel.ConfParameter(i).Id <> 0 Then
                Dim result As J2534Result = GetConfigParameter(device, channel, channel.ConfParameter(i).Id)
                ShowResult(device, result, "PassThruIoctl - GET_CONFIG " & channel.ConfParameter(i).Id.ToString)
            End If
        Next i
        Return
    End Sub

    Public Function ConfigChangesPresent() As ConfigChanges
        Return ConfigChangesPresent(-1)
    End Function
    Public Function ConfigChangesPresent(ByVal index As Integer) As ConfigChanges
        Dim result As ConfigChanges = ConfigChanges.None
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse index >= channel.ConfParameter.Length Then Return result
        If index >= 0 Then
            If TextBoxParamVal(index).Text.Length <> 0 Then
                result = result Or ConfigChanges.TextExists
                If channel.ConfParameter(index).Value <> GetConfigValue(channel, channel.ConfParameter(index).Id) Then
                    result = result Or ConfigChanges.ValueMismatch
                End If
            End If
            If Not String.IsNullOrWhiteSpace(channel.ConfParameter(index).NewText) Then
                result = result Or ConfigChanges.TextExists
            End If
        Else
            For i As Integer = 0 To channel.ConfParameter.Length - 1
                If LabelParamVal(i) IsNot Nothing AndAlso LabelParamVal(i).Visible Then
                    If TextBoxParamVal(i).Text.Length <> 0 Then
                        result = result Or ConfigChanges.TextExists
                        If channel.ConfParameter(i).Value <> GetConfigValue(channel, channel.ConfParameter(i).Id) Then
                            result = result Or ConfigChanges.ValueMismatch
                        End If
                    End If
                    If Not String.IsNullOrWhiteSpace(channel.ConfParameter(i).NewText) Then
                        result = result Or ConfigChanges.TextExists
                    End If
                End If
            Next i
        End If
        Return result
    End Function

    Private Sub SetEditConfig(ByVal index As Integer)
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse index < 0 OrElse index >= channel.ConfParameter.Length Then Return
        channel.ConfParameter(index).NewText = TextBoxParamVal(index).Text
    End Sub

    Private Sub UpdateConfigEdit()
        Dim changes As ConfigChanges = ConfigChangesPresent()
        ButtonSetConfig.Enabled = changes.HasFlag(ConfigChanges.ValueMismatch)
        ButtonClearConfig.Enabled = changes.HasFlag(ConfigChanges.TextExists)
    End Sub

    Private Sub ShowAnalogConfig()
        GroupBoxAnalogConfig.Visible = True
        GroupBoxAnalogConfig.Location = New Point(350, 25)
        UpdateChannelCheckBoxes()
    End Sub

    Private Sub HideAnalogConfig()
        GroupBoxAnalogConfig.Visible = False
    End Sub

    Private Sub SetChannelText()
        If TextBoxActiveChannelConfig Is Nothing Then Return
        Dim mask As UInteger
        For i As Integer = 0 To 31
            mask = 1UI << i
            If CheckBoxCH(i).Checked Then
                configChannels = configChannels Or mask
            Else
                configChannels = configChannels And Not mask
            End If
        Next i
        settingTextBoxActiveChannelConfig = True
        TextBoxActiveChannelConfig.Text = "0x" & configChannels.ToString("X8")
        settingTextBoxActiveChannelConfig = False
    End Sub

    Private Sub SetRateText()
        UInteger.TryParse(TextBoxAnalogRate.Text, configRate)
        Dim mask As UInteger = &H8000_0000UI
        If ComboBoxAnalogRateMode.SelectedIndex = 1 Then
            configRate = configRate Or mask
        Else
            configRate = configRate And Not mask
        End If
        TextBoxAnalogRateConfig.Text = "0x" & configRate.ToString("X8")
    End Sub

    Private Sub UpdateChannelCheckBoxes()
        Dim mask As UInteger
        Dim maxSelected As Integer = MSB(configChannels)
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return

        Dim maxActiveChannel As Integer = MSB(channel.AnalogSubsystem.ActiveChannels) + 1
        For i As Integer = 0 To MAX_ANALOG_CH
            mask = 1UI << i
            CheckBoxCH(i).Checked = (configChannels And mask) <> 0
            CheckBoxCH(i).Enabled = i < Math.Max(maxActiveChannel, channel.AnalogSubsystem.MaxChannel)
            CheckBoxCH(i).Visible = i < 16 OrElse i < Math.Max(maxActiveChannel, channel.AnalogSubsystem.MaxChannel) OrElse i <= maxSelected
        Next i
        LabelMaxSample.Text = "Max sample rate: " & channel.AnalogSubsystem.MaxSampleRate

    End Sub

    Private Sub SetRateControls()
        Dim mask As UInteger = &H8000_0000UI
        ComboBoxAnalogRateMode.SelectedIndex = If((configRate And mask) <> 0, 1, 0)
        settingTextBoxAnalogRate = True
        TextBoxAnalogRate.Text = (configRate And Not mask).ToString
        settingTextBoxAnalogRate = False
    End Sub

    Private Sub UpdateRateControls()
        Dim rateValue As UInteger = TextBoxAnalogRateConfig.Text.ToUint32
        Dim mask As UInteger = &H8000_0000UI
        ComboBoxAnalogRateMode.SelectedIndex = If((rateValue And mask) <> 0, 1, 0)
        settingTextBoxAnalogRate = True
        TextBoxAnalogRate.Text = (rateValue And Not mask).ToString
        settingTextBoxAnalogRate = False
    End Sub

    Public Sub ShowResult(ByVal device As J2534Device, ByVal errorCode As J2534Result, ByVal message As String)
        Dim errorBytes(79) As Byte
        Dim display, errorDesc As String
        If errorCode = J2534Result.ERR_BUFFER_EMPTY Then Return
        If errorCode <> J2534Result.STATUS_NOERROR Then
            If device?.API?.PassThruGetLastError IsNot Nothing Then
                device.API.PassThruGetLastError(errorBytes)
            End If
            errorDesc = Encoding.ASCII.GetString(errorBytes).TrimEnd(ControlChars.NullChar)
        Else
            errorDesc = "OK"
        End If
        If Not String.IsNullOrWhiteSpace(message) Then
            display = message & If(errorDesc.Length > 0, " - " & errorDesc, "")
        Else
            display = errorDesc
        End If
        If ToolStripStatusLabel2.Text <> display Then
            Dim item As ListViewItem = ListViewResults.Items.Add(device.Name)
            item.SubItems.Add(display)
            ToolStripStatusLabel2.Text = display
        End If
        If ListViewResults.Items.Count > 5000 Then
            ListViewResults.Items.RemoveAt(0)
        End If
    End Sub

    Private Function GetConfigValue(ByVal channel As J2534Channel, ByVal parameterId As UInteger) As UInteger
        Dim result As UInteger = 0
        If channel Is Nothing Then Return result
        Dim index As Integer = channel.ParameterIndex(parameterId)
        If index < 0 Then Return result
        Dim isHex As Boolean = False
        Dim text As String = StripSpaces(TextBoxParamVal(index).Text)
        If text.StartsWith("&h", IgnoreCase) OrElse text.StartsWith("0x", IgnoreCase) Then
            text = text.Substring(2)
            isHex = True
        ElseIf MustBeHex(text) OrElse parameterId = NODE_ADDRESS OrElse
                                      parameterId = J1962_PINS OrElse
                                      parameterId = J1939_PINS OrElse
                                      parameterId = J1708_PINS Then
            isHex = True
        End If
        If isHex Then
            UInteger.TryParse(text, NumberStyles.HexNumber, Nothing, result)
        Else
            UInteger.TryParse(text, result)
        End If
        Return result
    End Function

    Private Function OpenJ2534Device(ByVal device As J2534Device) As Boolean
        Dim result As Boolean = False
        If device?.API?.PassThruOpen Is Nothing OrElse device?.API?.PassThruReadVersion Is Nothing Then
            ShowResult(device, DirectCast(-1, J2534Result), "DLL Failed to Load")
            RemoveBox(device)
            Return result
        End If

        If device.Connected Then Return result

        Dim deviceId As UInteger
        Dim tempName As String = "J2534-2:"
        If ComboBoxDevice.SelectedIndex < 0 Then
            tempName &= ComboBoxDevice.Text
        End If
        Dim namePointer As IntPtr = Marshal.StringToHGlobalAnsi(tempName)
        Dim passThruOpenResult As J2534Result = device.API.PassThruOpen(namePointer.ToInt32, deviceId)
        Marshal.FreeHGlobal(namePointer)
        If passThruOpenResult <> J2534Result.STATUS_NOERROR Then
            ShowResult(device, passThruOpenResult, "PassThruOpen()")
            RemoveBox(device)
            Return result
        End If
        device.SetDeviceId(deviceId)

        If ComboBoxDevice.SelectedIndex < 0 Then
            device.Name = ComboBoxDevice.Text
            ComboBoxDevice.Items.Add(ComboBoxDevice.Text)
        End If

        Dim fwVersionBytes(79) As Byte
        Dim dllVersionBytes(79) As Byte
        Dim apiVersionBytes(79) As Byte
        Dim passThruReadVersionResult As J2534Result = device.API.PassThruReadVersion(device.DeviceId, fwVersionBytes, dllVersionBytes, apiVersionBytes)
        If passThruReadVersionResult = J2534Result.STATUS_NOERROR Then
            device.BuildChannels()
            device.Version = GetVersionNumericPart(Encoding.ASCII.GetString(fwVersionBytes).TrimEnd(ControlChars.NullChar))
            device.Description = "Device Name: " & device.Name & Environment.NewLine &
                                               "Serial Number: " & device.SerialNo & Environment.NewLine &
                                               "Device Firmware Version: " & device.Version & Environment.NewLine &
                                               "J2534 DLL Version: " & Encoding.ASCII.GetString(dllVersionBytes).TrimEnd(ControlChars.NullChar) & Environment.NewLine &
                                               "SAE J2534 API Version: " & Encoding.ASCII.GetString(apiVersionBytes).TrimEnd(ControlChars.NullChar)
            device.Connected = True
            device.Online = True
            AddHandler device.TimerMessageReceive.Tick, AddressOf TimerMessageReceive_Tick
            result = True
        Else
            device.Description = "Unable to connect"
            device.Connected = False
            device.Online = False
            ReDim device.ChannelSet(-1)
            result = False
        End If
        Return result
    End Function

    Private Function CloseJ2534Device(ByVal device As J2534Device) As Boolean
        Dim result As Boolean = False
        If device?.API?.PassThruClose Is Nothing Then Return result
        Dim passThruCloseResult As J2534Result = device.API.PassThruClose(device.DeviceId)
        ShowResult(device, passThruCloseResult, "PassThruClose(" & device.DeviceId & ")")
        If passThruCloseResult = J2534Result.STATUS_NOERROR OrElse passThruCloseResult = J2534Result.ERR_DEVICE_NOT_CONNECTED Then
            device.Connected = False
            result = True
        Else
            result = False
        End If
        RemoveHandler device.TimerMessageReceive.Tick, AddressOf TimerMessageReceive_Tick
        HandleOutgoingDevice(device)
        RemoveBox(device)
        Return result
    End Function

    Private Function ConnectChannel(ByVal device As J2534Device, ByVal options As J2534ConnectOptions) As Integer
        Dim result As J2534Result = DirectCast(-1, J2534Result)
        If device?.API?.PassThruConnect Is Nothing OrElse
           device?.API?.PassThruIoctl Is Nothing OrElse options?.Channel Is Nothing Then Return result

        Dim channelId As UInteger
        Dim config As SCONFIG
        Dim configList As SCONFIG_LIST
        If Not options.Channel.Connected Then
            result = device.API.PassThruConnect(device.DeviceId, options.Channel.ProtocolId, options.ConnectFlags, options.BaudRate, channelId)
            ShowResult(device, result, "PassThruConnect")
            If result = J2534Result.STATUS_NOERROR Then
                options.Channel.ChannelId = channelId
                options.Channel.MixedMode = False
                options.Channel.Connected = True
                options.Channel.BaudRate = options.BaudRate
                options.Channel.ConnectFlags = options.ConnectFlags
                If options.Channel.IsPinSwitched Then
                    config.Parameter = options.Connector
                    config.Value = options.Pins
                    configList.NumOfParams = 1
                    Dim configPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(config))
                    Marshal.StructureToPtr(config, configPointer, False)
                    configList.ConfigPtr = configPointer.ToInt32
                    Dim configListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(configList))
                    Marshal.StructureToPtr(configList, configListPointer, False)

                    result = device.API.PassThruIoctl(channelId, SET_CONFIG, configListPointer.ToInt32, 0)
                    If result = J2534Result.STATUS_NOERROR Then
                        options.Channel.Connector = config.Parameter
                        options.Channel.SelectedPins = config.Value
                    End If
                    Marshal.FreeHGlobal(configListPointer)
                    Marshal.FreeHGlobal(configPointer)
                End If
                If result = J2534Result.STATUS_NOERROR Then
                    'Sets Loopback by default
                    config.Parameter = LOOPBACK
                    config.Value = 1
                    configList.NumOfParams = 1
                    Dim configPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(config))
                    Marshal.StructureToPtr(config, configPointer, False)
                    configList.ConfigPtr = configPointer.ToInt32
                    Dim configListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(configList))
                    Marshal.StructureToPtr(configList, configListPointer, False)
                    result = device.API.PassThruIoctl(channelId, SET_CONFIG, configListPointer.ToInt32, 0)
                    Marshal.FreeHGlobal(configListPointer)
                    Marshal.FreeHGlobal(configPointer)
                End If
                If options.MixedMode Then
                    config.Parameter = CAN_MIXED_FORMAT
                    config.Value = J2534MixedMode.CAN_MIXED_FORMAT_ON
                    configList.NumOfParams = 1
                    Dim configPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(config))
                    Marshal.StructureToPtr(config, configPointer, False)
                    configList.ConfigPtr = configPointer.ToInt32
                    Dim configListPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(configList))
                    Marshal.StructureToPtr(configList, configListPointer, False)
                    result = device.API.PassThruIoctl(channelId, SET_CONFIG, configListPointer.ToInt32, 0)
                    If result <> J2534Result.STATUS_NOERROR Then
                        ShowResult(device, result, "PasThruIoctl CAN_MIXED_FORMAT")
                    Else
                        options.Channel.MixedMode = True
                    End If
                    Marshal.FreeHGlobal(configListPointer)
                    Marshal.FreeHGlobal(configPointer)
                End If
                If options.Channel.IsCAN OrElse options.Channel.IsISO15765 Then
                    If (options.ConnectFlags And J2534ConnectFlags.CAN_29BIT_ID) <> 0 Then
                        options.Channel.DefaultTxFlags = options.Channel.DefaultTxFlags Or J2534TxFlags.CAN_29BIT_ID
                    Else
                        options.Channel.DefaultTxFlags = options.Channel.DefaultTxFlags And Not J2534TxFlags.CAN_29BIT_ID
                    End If
                End If
                GetConfigParameterValues(device, options.Channel)
            End If
        Else
            ToolStripStatusLabel2.Text = "Protocol ID (" & options.Channel.ProtocolId & " - " & options.Channel.Name & ") is already open"
            result = J2534Result.ERR_CHANNEL_IN_USE
        End If
        UpdateConnectControls()
        UpdateComboBoxAvailableChannel()
        ComboBoxConnectChannel.Focus()
        Return result
    End Function

    Public Function DisconnectChannel(ByVal device As J2534Device, ByVal channel As J2534Channel) As Integer
        Dim result As J2534Result = DirectCast(-1, J2534Result)
        If device?.API?.PassThruDisconnect Is Nothing OrElse channel Is Nothing Then Return result
        If channel.Connected Then
            result = selectedDevice.API.PassThruDisconnect(channel.ChannelId)
            channel.ChannelId = 0
            channel.Connected = False
            ShowResult(selectedDevice, result, "PassThruDisconnect")
            If result = J2534Result.STATUS_NOERROR Then
                If channel.Filter IsNot Nothing Then
                    For i As Integer = 0 To channel.Filter.Length - 1
                        channel.Filter(i).FilterType = 0
                        channel.Filter(i).MaskMessage.Clear()
                        channel.Filter(i).PatternMessage.Clear()
                        channel.Filter(i).FlowMessage.Clear()
                        channel.Filter(i).MessageId = 0
                        channel.Filter(i).Enabled = False
                    Next i
                End If
                If channel.PeriodicMessage IsNot Nothing Then
                    For i As Integer = 0 To channel.PeriodicMessage.Length - 1
                        channel.PeriodicMessage(i).Message.Clear()
                        channel.PeriodicMessage(i).Interval = 100
                        channel.PeriodicMessage(i).MessageId = 0
                        channel.PeriodicMessage(i).Enabled = False
                    Next i
                End If
                If channel.FunctionalMessage IsNot Nothing Then
                    For i As Integer = 0 To channel.FunctionalMessage.Length - 1
                        channel.FunctionalMessage(i).Value = 0
                        channel.FunctionalMessage(i).NewText = String.Empty
                    Next i
                End If
                channel.Connector = 0
                channel.SelectedPins = 0
            End If
        Else
            ToolStripStatusLabel2.Text = "Protocol ID (" & channel.ProtocolId & " - " & channel.Name & ") is not open"
        End If

        UpdateConnectControls()
        UpdateComboBoxAvailableChannel()
        Return result
    End Function

    Private Sub SetInitControls()
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not (channel.IsISO9141 OrElse channel.IsISO14230) Then
            TextBox5BInitECU.Text = String.Empty
            TextBox5BInitECU.Enabled = False
            Label5BKeyWord0.Text = String.Empty
            Label5BKeyWord0.Enabled = False
            Label5BKeyWord1.Text = String.Empty
            Label5BKeyWord1.Enabled = False
            ButtonExecute5BInit.Enabled = False
            Label5BECU.Enabled = False
            LabelKWlabel0.Enabled = False
            LabelKWlabel1.Enabled = False
            GroupBox5BInit.Enabled = False

            TextBoxFIMessage.Text = String.Empty
            TextBoxFIMessage.Enabled = False
            TextBoxFIFlags.Text = String.Empty
            TextBoxFIFlags.Enabled = False
            LabelFIResponse.Text = String.Empty
            LabelFIResponse.Enabled = False
            LabelFIRxStatus.Text = String.Empty
            LabelFIRxStatus.Enabled = False
            ButtonExecuteFastInit.Enabled = False
            LabelFIMsgData.Enabled = False
            LabelFIRXTitle.Enabled = False
            LabelFIMsgResp.Enabled = False
            LabelFITXFlags.Enabled = False
            GroupBoxFastInit.Enabled = False
        Else
            TextBox5BInitECU.Text = "0x33"
            TextBox5BInitECU.Enabled = True
            Label5BKeyWord0.Text = String.Empty
            Label5BKeyWord0.Enabled = True
            Label5BKeyWord1.Text = String.Empty
            Label5BKeyWord1.Enabled = True
            ButtonExecute5BInit.Enabled = True
            Label5BECU.Enabled = True
            LabelKWlabel0.Enabled = True
            LabelKWlabel1.Enabled = True
            GroupBox5BInit.Enabled = True

            TextBoxFIMessage.Text = "c1 33 f1 81"
            TextBoxFIMessage.Enabled = True
            TextBoxFIFlags.Text = "0x00000000"
            TextBoxFIFlags.Enabled = True
            LabelFIResponse.Text = String.Empty
            LabelFIResponse.Enabled = True
            LabelFIRxStatus.Text = String.Empty
            LabelFIRxStatus.Enabled = False
            ButtonExecuteFastInit.Enabled = True
            LabelFIMsgData.Enabled = True
            LabelFIRXTitle.Enabled = True
            LabelFIMsgResp.Enabled = True
            LabelFITXFlags.Enabled = True
            GroupBoxFastInit.Enabled = True
        End If

    End Sub

    Private Sub SetMessageControls()
        Dim connected As Boolean = ComboBoxMessageChannel.Items.Count > 0
        ButtonReceive.Enabled = connected
        ButtonClearRx.Enabled = connected
        ButtonClearTx.Enabled = connected
        TextBoxOutFlags.Enabled = connected
        TextBoxMessageOut.Enabled = connected
        TextBoxReadRate.Enabled = connected
        TextBoxTimeOut.Enabled = connected
        LabelScratchPad.Enabled = TextBoxMessageOut.Enabled
        LabelMsgFlags.Enabled = TextBoxOutFlags.Enabled
        LabelTO.Enabled = TextBoxTimeOut.Enabled
        LabelReadRate.Enabled = TextBoxReadRate.Enabled
    End Sub

    Private Sub SetFunctionalMessageLookupTableControls()
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not channel.IsJ1850PWM Then
            For i As Integer = 0 To MAX_FUNC_MSG
                LabelFuncId(i).Enabled = False
                LabelFunctionalMessage(i).Text = String.Empty
                LabelFunctionalMessage(i).Enabled = False
                TextBoxFunctionalMessage(i).Text = String.Empty
                TextBoxFunctionalMessage(i).Enabled = False
                CheckBoxFunctionalMessageDelete(i).Checked = False
                CheckBoxFunctionalMessageDelete(i).Visible = False
                LabelFunctionalMessageValues.Enabled = False
                LabelFunctionalMessageModify.Enabled = False
                ButtonClearAllFunctionalMessages.Enabled = False
                ButtonCancelFunctionalMessages.Enabled = False
                ButtonApplyFunctionalMessages.Enabled = False
            Next i
        Else
            For i As Integer = 0 To MAX_FUNC_MSG
                LabelFuncId(i).Enabled = True
                If channel.FunctionalMessage(i).Value = 0 Then
                    LabelFunctionalMessage(i).Text = String.Empty
                    CheckBoxFunctionalMessageDelete(i).Visible = False
                Else
                    LabelFunctionalMessage(i).Text = "0x" & channel.FunctionalMessage(i).Value.ToString("X2")
                    CheckBoxFunctionalMessageDelete(i).Visible = True
                End If
                LabelFunctionalMessage(i).Enabled = True
                TextBoxFunctionalMessage(i).Text = channel.FunctionalMessage(i).NewText
                TextBoxFunctionalMessage(i).Enabled = True
                CheckBoxFunctionalMessageDelete(i).Checked = False
                LabelFunctionalMessageValues.Enabled = True
                LabelFunctionalMessageModify.Enabled = True
                ButtonClearAllFunctionalMessages.Enabled = True
                ButtonCancelFunctionalMessages.Enabled = True
                ButtonApplyFunctionalMessages.Enabled = True
            Next i
        End If
        UpdateFunctionalMessageLookupTableEdit()
    End Sub

    Private Sub DeleteFromFunctionalMessageLookupTable(ByVal funcAddress As Byte)
        Dim inputMsg As SBYTE_ARRAY
        inputMsg.NumOfBytes = 1
        Dim byteAddress() As Byte = {funcAddress}
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If selectedDevice?.API?.PassThruIoctl Is Nothing OrElse channel Is Nothing Then Return
        If String.IsNullOrWhiteSpace(ComboBoxAvailableChannel.Text) Then Return
        If Not channel.IsJ1850PWM Then
            SetFunctionalMessageLookupTableControls()
            Return
        End If
        Dim funcAddressPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(byteAddress(0)))
        Marshal.Copy(byteAddress, 0, funcAddressPointer, 1)
        inputMsg.BytePtr = funcAddressPointer.ToInt32
        Dim inputMsgPointer As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(inputMsg))
        Marshal.StructureToPtr(inputMsg, inputMsgPointer, True)
        Dim result As J2534Result = selectedDevice.API.PassThruIoctl(channel.ChannelId, DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE, inputMsgPointer.ToInt32, 0)
        Marshal.FreeHGlobal(funcAddressPointer)
        Marshal.FreeHGlobal(inputMsgPointer)
        ShowResult(selectedDevice, result, "PassThruIoctl - DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE")
    End Sub

    Private Function FunctionalMessageLookupTableChangesPresent() As Boolean
        Return FunctionalMessageLookupTableChangesPresent(-1)
    End Function
    Private Function FunctionalMessageLookupTableChangesPresent(ByVal index As Integer) As Boolean
        Dim result As Boolean = False
        Dim loFM, hiFM As Integer
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing OrElse Not channel.IsJ1850PWM Then Return result
        If index < 0 Then
            loFM = 0
            hiFM = MAX_FUNC_MSG
        Else
            loFM = index
            hiFM = index
        End If
        For i As Integer = loFM To hiFM
            If LabelFunctionalMessage(i).Text.ToByte <> TextBoxFunctionalMessage(i).Text.ToByte Then
                result = True
            End If
            Dim b As Byte = TextBoxFunctionalMessage(i).Text.ToByte
            If b <> channel.FunctionalMessage(i).NewText.ToByte Then
                result = True
            End If
            If b <> channel.FunctionalMessage(i).Value Then
                result = True
            End If
            If CheckBoxFunctionalMessageDelete(i).Checked Then result = True
        Next i
        Return result
    End Function

    Private Sub UpdateFunctionalMessageLookupTableEdit()
        ButtonApplyFunctionalMessages.Enabled = FunctionalMessageLookupTableChangesPresent()
        ButtonCancelFunctionalMessages.Enabled = ButtonApplyFunctionalMessages.Enabled
    End Sub

    Public Function SetProgrammingVoltage(ByVal device As J2534Device) As Integer
        Dim result As J2534Result = DirectCast(-1, J2534Result)
        If device?.API?.PassThruSetProgrammingVoltage Is Nothing Then Return result
        Dim voltSetting As UInteger
        Dim mode As PinMode
        If optSet.Checked Then
            If device.Analog.PinSetting < 5000 Then device.Analog.PinSetting = 5000
            If device.Analog.PinSetting > 20000 Then device.Analog.PinSetting = 20000
            TextBoxVoltSetting.Text = device.Analog.PinSetting.ToString
            voltSetting = device.Analog.PinSetting
            mode = PinMode.Volt
        ElseIf optVoltOff.Checked Then
            voltSetting = J2534VoltageValue.VOLTAGE_OFF
            mode = PinMode.Open
        ElseIf optShortToGround.Checked Then
            voltSetting = J2534VoltageValue.SHORT_TO_GROUND
            mode = PinMode.Shorted
        End If
        For i As Integer = 0 To RadioButtonPin.Length - 1
            If Not RadioButtonPin(i) Is Nothing Then
                If RadioButtonPin(i).Checked Then
                    result = device.API.PassThruSetProgrammingVoltage(device.DeviceId, CUInt(i), voltSetting)
                    If result = J2534Result.STATUS_NOERROR Then
                        RadioButtonPin(i).Tag = mode
                        If mode = PinMode.Volt Then LabelPin(i).Text = "Volt"
                        If mode = PinMode.Shorted Then LabelPin(i).Text = "Gnd"
                        If mode = PinMode.Open Then LabelPin(i).Text = String.Empty
                    End If
                    ShowResult(device, result, "PassThruSetProgrammingVoltage")
                End If
                device.Analog.Pin(i).Mode = DirectCast(RadioButtonPin(i).Tag, PinMode)
                device.Analog.Pin(i).Selected = RadioButtonPin(i).Checked
            End If
        Next i
        Return result
    End Function

    Public Sub SetVoltControls()
        Dim selectedAnalog As String = ComboBoxAnalogChannel.Text
        ComboBoxAnalogChannel.Items.Clear()
        If selectedDevice IsNot Nothing AndAlso selectedDevice.Connected Then
            For Each channel As J2534Channel In selectedDevice.ChannelSet
                If channel.IsAnalog AndAlso channel.Connected Then
                    ComboBoxAnalogChannel.Items.Add(channel)
                End If
            Next channel
            If ComboBoxAnalogChannel.Items.Count > 0 Then
                If selectedAnalog.Length > 0 Then
                    ComboBoxAnalogChannel.Text = selectedAnalog
                Else
                    ComboBoxAnalogChannel.SelectedIndex = 0
                End If
            End If
            ComboBoxAnalogChannel.Enabled = ComboBoxAnalogChannel.Items.Count > 1

            Dim selectedPin As Integer = -1
            For i As Integer = 0 To RadioButtonPin.Length - 1
                If Not RadioButtonPin(i) Is Nothing Then
                    RadioButtonPin(i).Enabled = True
                    If Not TypeOf RadioButtonPin(i).Tag Is PinMode Then RadioButtonPin(i).Tag = PinMode.Open
                    If RadioButtonPin(i).Checked Then selectedPin = i
                    If DirectCast(RadioButtonPin(i).Tag, PinMode) = PinMode.Volt Then
                        LabelPin(i).Text = "Volt"
                    ElseIf DirectCast(RadioButtonPin(i).Tag, PinMode) = PinMode.Shorted Then
                        LabelPin(i).Text = "Gnd"
                    Else
                        LabelPin(i).Text = String.Empty
                    End If

                End If
            Next i
            If selectedPin < 0 Then
                RadioButtonPin(0).Checked = True
                selectedPin = 0
            End If
            GroupBoxPin.Enabled = True
            optVoltOff.Enabled = True
            optShortToGround.Enabled = selectedDevice.Analog.Pin(selectedPin).CanBeShorted
            optSet.Enabled = selectedDevice.Analog.Pin(selectedPin).CanBeSet
            optVoltOff.Checked = Not (optShortToGround.Checked OrElse optSet.Checked)

            TextBoxVoltSetting.Text = selectedDevice.Analog.PinSetting.ToString
            TextBoxVoltSetting.Enabled = True
            LabelMV.Enabled = True
            LabelVoltRead.Enabled = True
            If selectedDevice.Analog.PinReading < 1000 Then
                LabelVoltRead.Text = selectedDevice.Analog.PinReading.ToString & " mVDC"
            Else
                LabelVoltRead.Text = (selectedDevice.Analog.PinReading / 1000).ToString & " VDC"
            End If
            ButtonSetVoltage.Enabled = True
            ButtonReadVolt.Enabled = True
            GroupBoxVolt.Enabled = True
            GroupBoxProgVoltage.Enabled = True

            LabelBattRead.Enabled = True
            If selectedDevice.Analog.BatteryReading < 1000 Then
                LabelBattRead.Text = selectedDevice.Analog.BatteryReading.ToString & " mVDC"
            Else
                LabelBattRead.Text = (selectedDevice.Analog.BatteryReading / 1000).ToString & " VDC"
            End If
            ButtonReadBatt.Enabled = True
            GroupBoxBattVoltage.Enabled = True

        Else 'box is not OK
            For i As Integer = 0 To RadioButtonPin.Length - 1
                If Not RadioButtonPin(i) Is Nothing Then
                    RadioButtonPin(i).Enabled = False
                    RadioButtonPin(i).Checked = False
                    LabelPin(i).Text = String.Empty
                    RadioButtonPin(i).Tag = PinMode.Open
                End If
            Next i
            GroupBoxPin.Enabled = False
            optVoltOff.Enabled = False
            optVoltOff.Checked = False
            optShortToGround.Enabled = False
            optShortToGround.Checked = False
            optSet.Enabled = False
            optSet.Checked = False
            TextBoxVoltSetting.Text = String.Empty
            TextBoxVoltSetting.Enabled = False
            LabelMV.Enabled = False
            LabelVoltRead.Enabled = False
            LabelVoltRead.Text = String.Empty
            ButtonSetVoltage.Enabled = False
            ButtonReadVolt.Enabled = False
            GroupBoxVolt.Enabled = False
            GroupBoxProgVoltage.Enabled = False
            LabelBattRead.Enabled = False
            LabelBattRead.Text = String.Empty
            ButtonReadBatt.Enabled = False
            GroupBoxBattVoltage.Enabled = False
            ComboBoxAnalogChannel.Enabled = False
        End If
        ShowAnalogInputs(selectedDevice)
        LabelAnalogChannel.Enabled = ComboBoxAnalogChannel.Enabled
    End Sub

    Public Sub UpdatePinControls(ByVal device As J2534Device)
        If device Is Nothing Then Return
        Dim index As Integer
        If Not GroupBoxProgVoltage.Enabled Then Return
        For i As Integer = 0 To RadioButtonPin.Length - 1
            If RadioButtonPin(i) IsNot Nothing AndAlso RadioButtonPin(i).Checked Then
                index = i
                Exit For
            End If
        Next i
        optShortToGround.Enabled = selectedDevice.Analog.Pin(index).CanBeShorted
        optSet.Enabled = selectedDevice.Analog.Pin(index).CanBeSet
        If optSet.Checked AndAlso Not optSet.Enabled Then optVoltOff.Checked = True
        If optShortToGround.Checked And Not optShortToGround.Enabled Then optVoltOff.Checked = True
    End Sub

    Private Sub ShowAnalogInputs(ByVal device As J2534Device)
        If device IsNot selectedDevice Then Return
        Dim channel As J2534Channel = TryCast(ComboBoxAnalogChannel.SelectedItem, J2534Channel)
        If device IsNot Nothing AndAlso channel IsNot Nothing AndAlso
           device.Connected AndAlso channel.Connected AndAlso
           channel.AnalogSubsystem.ActiveChannels <> 0 Then

            Dim maxActive As Integer = MSB(channel.AnalogSubsystem.ActiveChannels)
            For i As Integer = 0 To MAX_ANALOG_CH
                If (channel.AnalogSubsystem.ActiveChannels And (1 << i).ToUint32) <> 0 Then
                    LabelAnalogCH(i).Enabled = True
                    LabelAnalogRead(i).Enabled = True
                    If Math.Abs(channel.AnalogSubsystem.AnalogIn(i)) < 1000 Then
                        LabelAnalogRead(i).Text = channel.AnalogSubsystem.AnalogIn(i) & " mVDC"
                    Else
                        LabelAnalogRead(i).Text = channel.AnalogSubsystem.AnalogIn(i) / 1000 & " VDC"
                    End If
                    ProgressBarAnalog(i).Minimum = channel.AnalogSubsystem.RangeLow
                    ProgressBarAnalog(i).Maximum = channel.AnalogSubsystem.RangeHigh + 1
                    If channel.AnalogSubsystem.AnalogIn(i) < ProgressBarAnalog(i).Minimum Then
                        ProgressBarAnalog(i).Value = ProgressBarAnalog(i).Minimum
                    ElseIf channel.AnalogSubsystem.AnalogIn(i) >= channel.AnalogSubsystem.RangeHigh Then
                        ProgressBarAnalog(i).Value = ProgressBarAnalog(i).Maximum
                        ProgressBarAnalog(i).Value = channel.AnalogSubsystem.RangeHigh
                    Else
                        ProgressBarAnalog(i).Value = channel.AnalogSubsystem.AnalogIn(i) + 1
                        ProgressBarAnalog(i).Value = channel.AnalogSubsystem.AnalogIn(i)
                    End If
                Else
                    LabelAnalogCH(i).Enabled = False
                    LabelAnalogRead(i).Enabled = False
                    LabelAnalogRead(i).Text = "0 mVDC"
                    ProgressBarAnalog(i).Value = ProgressBarAnalog(i).Minimum
                End If

                Dim channelVisible As Boolean = i < 10 OrElse i <= Math.Max(maxActive, channel.AnalogSubsystem.MaxChannel)
                LabelAnalogCH(i).Visible = channelVisible
                LabelAnalogRead(i).Visible = channelVisible
                ProgressBarAnalog(i).Visible = channelVisible

            Next i
            LabelAnalogReading.Enabled = True
            LabelAnalogLow0.Enabled = True
            LabelAnalogLow1.Enabled = True
            LabelAnalogHigh0.Enabled = True
            LabelAnalogHigh1.Enabled = True
            LabelAnalogLow0.Text = channel.AnalogSubsystem.RangeLow / 1000 & " VDC"
            LabelAnalogLow1.Text = LabelAnalogLow0.Text
            LabelAnalogHigh0.Text = channel.AnalogSubsystem.RangeHigh / 1000 & " VDC"
            LabelAnalogHigh1.Text = LabelAnalogHigh0.Text
            GroupBoxAnalog.Enabled = True

        Else
            For i As Integer = 0 To MAX_ANALOG_CH
                LabelAnalogCH(i).Enabled = False
                LabelAnalogRead(i).Enabled = False
                LabelAnalogRead(i).Text = "0 mVDC"
                ProgressBarAnalog(i).Value = ProgressBarAnalog(i).Minimum
                If i > 9 Then
                    LabelAnalogCH(i).Visible = False
                    LabelAnalogRead(i).Visible = False
                    ProgressBarAnalog(i).Visible = False
                End If
            Next i
            LabelAnalogReading.Enabled = False
            LabelAnalogLow0.Enabled = False
            LabelAnalogLow1.Enabled = False
            LabelAnalogHigh0.Enabled = False
            LabelAnalogHigh1.Enabled = False
            LabelAnalogLow0.Text = String.Empty
            LabelAnalogLow1.Text = LabelAnalogLow0.Text
            LabelAnalogHigh0.Text = String.Empty
            LabelAnalogHigh1.Text = LabelAnalogHigh0.Text
            GroupBoxAnalog.Enabled = False
        End If
    End Sub

    Private Shared Sub AddMessageToOutgoingListView(ByVal thisListView As ListView, ByVal messageText As J2534MessageText)
        Dim item As ListViewItem = thisListView.Items.Add(messageText.ProtocolName)
        item.SubItems.Add(messageText.TxFlags)
        item.SubItems.Add(messageText.Data)
        item.SubItems.Add(messageText.Comment)
    End Sub

    Private Function GetListViewOutMessageText() As J2534MessageText
        Dim result As New J2534MessageText
        If selectedDevice IsNot Nothing AndAlso selectedDevice.ListViewMessageOut.SelectedItems.Count > 0 Then
            result.ProtocolName = selectedDevice.ListViewMessageOut.SelectedItems(0).Text
            result.TxFlags = selectedDevice.ListViewMessageOut.SelectedItems(0).SubItems(1).Text
            result.Data = selectedDevice.ListViewMessageOut.SelectedItems(0).SubItems(2).Text
            result.Comment = selectedDevice.ListViewMessageOut.SelectedItems(0).SubItems(3).Text
        End If
        Return result
    End Function

    Private Function GetDefaultFlags(ByVal device As J2534Device, ByVal thisTextBox As TextBox) As UInteger
        If device Is Nothing Then Return 0
        Dim flags As UInteger = 0
        Dim channel As J2534Channel = Nothing
        If thisTextBox Is TextBoxConnectFlags Then
            channel = TryCast(ComboBoxConnectChannel.SelectedItem, J2534Channel)
            If channel IsNot Nothing Then flags = channel.DefaultConnectFlags
        ElseIf thisTextBox Is TextBoxOutFlags Then
            channel = TryCast(ComboBoxMessageChannel.SelectedItem, J2534Channel)
            If channel IsNot Nothing Then flags = channel.DefaultTxFlags
        Else
            channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
            If channel Is Nothing Then Return 0
            If thisTextBox.Name.StartsWith("_TextBoxPeriodicMessageFlags", IgnoreCase) Then
                If channel.IsISO15765 AndAlso channel.MixedMode Then
                    Dim index As Integer = Array.IndexOf(TextBoxPeriodicMessageFlags, thisTextBox)
                    Dim testChannel As J2534Channel = TryCast(ComboBoxPeriodicMessageChannel(index).SelectedItem, J2534Channel)
                    If testChannel IsNot Nothing Then channel = testChannel
                End If
            ElseIf thisTextBox.Name.StartsWith("_TextBoxFilterFlags", IgnoreCase) Then
                If channel.IsISO15765 AndAlso channel.MixedMode Then
                    Dim index As Integer = Array.IndexOf(TextBoxFilterFlags, thisTextBox)
                    If ComboBoxFilterType(index).Text.Equals("Pass", IgnoreCase) OrElse ComboBoxFilterType(index).Text.Equals("Block", IgnoreCase) Then
                        channel = ISO_ChannelToCAN_Channel(device, channel)
                    End If
                End If
            End If
            If channel IsNot Nothing Then flags = channel.DefaultTxFlags
        End If
        Return flags
    End Function

    Private Sub InitComboBoxAPI()
        ComboBoxAPI.Items.Clear()
        Dim count As Integer = GetRegistryPassThruSupportInfo()
        If count = 0 Then
            ComboBoxAPI.Items.Add("No information found in the registry")
            ComboBoxAPI.SelectedIndex = 0
        Else
            ComboBoxAPI.Items.AddRange(J2534_APIs)
            ComboBoxAPI.SelectedIndex = 0
        End If
        ComboBoxAPI.Enabled = ComboBoxAPI.Items.Count > 1
    End Sub

    Private Sub UpdateAPILabels()
        If J2534_APIs.Length < 1 Then
            TextBoxDllPath.Text = String.Empty
            LabelVendor.Text = "Vendor: none"
            LabelDevice.Text = "Device: none"
            LabelDllName.Text = "J2534 dll: none"
            ButtonLoadDLL.Enabled = False
        Else
            Dim api As J2534_API = TryCast(ComboBoxAPI.SelectedItem, J2534_API)
            If api IsNot Nothing Then
                TextBoxDllPath.Text = api.LibraryName
                LabelVendor.Text = "Vendor: " & api.VendorName
                LabelDevice.Text = "Device: " & api.DeviceName
                LabelDllName.Text = "J2534 dll: " & api.LibraryName & If(api.DllIsLoaded, " - Loaded", "")
                ButtonLoadDLL.Enabled = Not api.DllIsLoaded
            Else
                ButtonLoadDLL.Enabled = False
            End If
        End If
    End Sub

    Private Sub UpdateComboBoxDevice()
        Dim initialText As String = ComboBoxDevice.Text
        ComboBoxDevice.Items.Clear()
        ComboBoxDevice.Text = ""
        Dim nextBox As Integer = GetAvailableDeviceIndex()
        Dim api As J2534_API = TryCast(ComboBoxAPI.SelectedItem, J2534_API)
        If api IsNot Nothing Then
            If api.DllIsLoaded Then
                For Each Box As J2534Device In J2534Devices.Values
                    Dim okToAdd As Boolean = Box.API IsNot Nothing AndAlso Box.API Is api
                    For Each deviceItem As String In ComboBoxDevice.Items
                        If Box.Name.Equals(deviceItem, IgnoreCase) Then okToAdd = False
                    Next deviceItem
                    If okToAdd Then
                        ComboBoxDevice.Items.Add(Box.Name)
                    End If
                Next Box
                Dim baseItem As String = api.DeviceName & " " & nextBox
                ComboBoxDevice.Items.Add(baseItem)
            End If
        End If
        Dim j As Integer = 0
        For i As Integer = 0 To ComboBoxDevice.Items.Count - 1
            If Not String.IsNullOrWhiteSpace(initialText) AndAlso String.Equals(initialText, ComboBoxDevice.Items(i).ToString, IgnoreCase) Then j = i
        Next i
        If ComboBoxDevice.Items.Count > 0 Then ComboBoxDevice.SelectedIndex = j
        ComboBoxDevice.Enabled = ComboBoxDevice.Items.Count > 0
        LabelComboDevice.Enabled = ComboBoxDevice.Enabled
    End Sub

    Private Sub UpdateDeviceLabels()
        Dim device As J2534Device = NameToDevice(ComboBoxDevice.Text)
        If device IsNot Nothing Then
            Dim infoString As String = "Name: " & device.Name & Environment.NewLine &
                                       "Version: " & device.Version & Environment.NewLine
            If device.Connected Then
                infoString &= "Status: Open" & Environment.NewLine
            Else
                infoString &= "Status: Available" & Environment.NewLine
            End If
            If device.Connected Then
                infoString &= "Device ID: " & device.DeviceId
            Else
                infoString &= "Device ID: none"
            End If
            LabelDeviceInfo.Text = infoString

            ButtonOpenBox.Enabled = device.API IsNot Nothing AndAlso
                                    device.API.DllIsLoaded AndAlso
                                    Not device.Connected
            ButtonCloseBox.Enabled = device.API IsNot Nothing AndAlso
                                    device.API.DllIsLoaded AndAlso
                                    ComboBoxAPI.SelectedItem IsNot Nothing AndAlso
                                    ComboBoxAPI.SelectedItem Is device.API AndAlso
                                    device.Connected
        Else
            Dim api As J2534_API = TryCast(ComboBoxAPI.SelectedItem, J2534_API)
            LabelDeviceInfo.Text = String.Empty
            ButtonOpenBox.Enabled = api IsNot Nothing AndAlso
                                    api.DllIsLoaded AndAlso
                                    Not String.IsNullOrWhiteSpace(ComboBoxDevice.Text)
            ButtonCloseBox.Enabled = False
        End If
    End Sub

    Private Sub UpdateDeviceInfo(ByVal device As J2534Device)
        If device IsNot Nothing AndAlso J2534Devices.Values.Contains(device) Then
            LabelJ2534Info.Text = device.Description
            If device.Connected Then
                LabelProtSupport.Text = "Supported Protocols: " & device.SupportedProtocols
            Else
                LabelProtSupport.Text = String.Empty
            End If
        Else
            LabelJ2534Info.Text = String.Empty
            LabelProtSupport.Text = String.Empty
        End If
    End Sub

    Private Sub UpdateComboBoxAvailableDevice()
        Dim initialText As String = ComboBoxAvailableDevice.Text
        ComboBoxAvailableDevice.Items.Clear()
        For Each device As J2534Device In J2534Devices.Values
            If device.Connected Then ComboBoxAvailableDevice.Items.Add(device)
        Next device
        Dim itemCount As Integer = ComboBoxAvailableDevice.Items.Count - 1
        If itemCount >= 0 Then
            Dim index As Integer = 0
            If Not String.IsNullOrEmpty(initialText) Then
                For i As Integer = 0 To itemCount
                    If String.Equals(ComboBoxAvailableDevice.Items(i).ToString, initialText, IgnoreCase) Then
                        index = i
                        Exit For
                    End If
                Next i
            End If
            ComboBoxAvailableDevice.SelectedIndex = index
        Else
            UpdateDeviceLabels()
        End If
        ComboBoxAvailableDevice.Enabled = ComboBoxAvailableDevice.Items.Count > 1
        For i As Integer = 0 To LabelDeviceCombo.Length - 1
            LabelDeviceCombo(i).Enabled = ComboBoxAvailableDevice.Enabled
        Next i
        UpdateTabs()
        If TabControl1.SelectedTab IsNot TabPageConnect Then ComboBoxAvailableDevice.Focus()
    End Sub

    Private Shared Sub MsgInCopyLine(ByVal thisListView As ListView)
        If thisListView.Items.Count < 1 Then Return
        Dim sb As New StringBuilder
        For Each item As ListViewItem In thisListView.SelectedItems
            sb.AppendLine(item.Text & "," &
                                item.SubItems(1).Text & "," &
                                item.SubItems(2).Text & "," &
                                item.SubItems(3).Text)
        Next item
        Clipboard.SetText(sb.ToString)
    End Sub

    Private Shared Sub ResultsCopyLine(ByVal thisListView As ListView)
        If thisListView.Items.Count < 1 Then Return
        Dim sb As New StringBuilder
        For Each item As ListViewItem In thisListView.SelectedItems
            sb.AppendLine(item.Text & "," & item.SubItems(1).Text)
        Next item
        Clipboard.SetText(sb.ToString)
    End Sub

    Private Sub MakeFilterFromIncomingMessage(ByVal thisListView As ListView)
        Dim device As J2534Device = GetListViewParentDevice(thisListView)
        If device IsNot Nothing AndAlso thisListView.SelectedItems.Count > 0 Then
            Dim messageText As New J2534MessageText With {.ProtocolName = thisListView.SelectedItems(0).SubItems(1).Text}
            Dim channel As J2534Channel = device.Channel(messageText.ProtocolName)
            If channel Is Nothing Then Return
            Dim channelISO As J2534Channel = Nothing
            If channel.IsCAN AndAlso Not channel.Connected Then
                channelISO = CAN_ChannelToISO_Channel(device, channel)
            End If
            If channelISO IsNot Nothing Then channel = channelISO
            Dim nextFilter As Integer = channel.NextFilter
            If nextFilter < 0 Then Return
            messageText.Data = thisListView.SelectedItems(0).SubItems(3).Text
            channel.Filter(nextFilter).PatternMessage = messageText.ToMessage
            channel.Filter(nextFilter).MaskMessage = GetMessageMask(channel.Filter(nextFilter).PatternMessage)
            channel.Filter(nextFilter).FlowMessage = PASSTHRU_MSG.CreateInstance
            channel.Filter(nextFilter).FlowMessage.TxFlags = channel.DefaultTxFlags
            channel.Filter(nextFilter).FilterType = J2534FilterType.PASS_FILTER
            SetFilterEditBuffer(nextFilter)
        End If
    End Sub

    Private Sub SaveListViewItemListToFile(ByVal itemList As ListViewItem())
        If itemList.Length < 1 Then Return
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileSave.InitialDirectory = fileFolder
        DialogFileSave.FileName = String.Empty
        DialogFileSave.DefaultExt = ".in"
        DialogFileSave.Filter = "Message In Files (*.in)|*.in"
        DialogFileSave.Title = "Save Messages As:"
        DialogFileSave.RestoreDirectory = True
        DialogFileSave.CheckPathExists = True
        DialogFileSave.OverwritePrompt = True
        If DialogFileSave.ShowDialog() = DialogResult.Cancel Then Return
        Dim fileName As String = DialogFileSave.FileName
        Dim sb As New StringBuilder
        For Each listItem As ListViewItem In itemList
            sb.AppendLine(listItem.Text & "," &
                          listItem.SubItems(1).Text & "," &
                          listItem.SubItems(2).Text & "," &
                          listItem.SubItems(3).Text)
        Next listItem
        If sb.Length = 0 Then Return
        Using sw As New StreamWriter(fileName, False, Encoding.ASCII)
            sw.Write(sb.ToString)
        End Using
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(fileName)
    End Sub

    Private Sub ModifySelectedOutgoingMessage(ByVal thisListView As ListView)
        If thisListView Is Nothing Then Return
        If thisListView.SelectedItems.Count <= 0 Then Return
        Dim messageText As J2534MessageText = GetListViewOutMessageText()
        Dim selectedItem As ListViewItem = thisListView.SelectedItems(0)
        Dim result As DialogResult = FormMessageEdit.ShowDialog(messageText, selectedDevice)
        If result = DialogResult.OK AndAlso thisListView.SelectedItems(0) Is selectedItem Then
            If thisListView.SelectedItems.Count > 0 Then
                selectedDevice.ListViewMessageOut.SelectedItems(0).Text = messageText.ProtocolName
                selectedDevice.ListViewMessageOut.SelectedItems(0).SubItems(1).Text = messageText.TxFlags
                selectedDevice.ListViewMessageOut.SelectedItems(0).SubItems(2).Text = messageText.Data
                selectedDevice.ListViewMessageOut.SelectedItems(0).SubItems(3).Text = messageText.Comment
            End If
        End If
    End Sub

    Private Sub LoadOutgoingMessageListFromFile(ByVal thisListView As ListView)
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileOpen.InitialDirectory = fileFolder
        DialogFileOpen.FileName = String.Empty
        DialogFileOpen.DefaultExt = ".out"
        DialogFileOpen.Filter = "Message Files (*.out;*.msg)|*.out;*.msg"
        DialogFileOpen.Title = "Load Message File:"
        DialogFileOpen.RestoreDirectory = True
        DialogFileOpen.CheckPathExists = False
        DialogFileOpen.Multiselect = True
        If DialogFileOpen.ShowDialog() = DialogResult.Cancel Then Return
        For Each fileName As String In DialogFileOpen.FileNames
            Using sr As New FileIO.TextFieldParser(fileName)
                sr.TextFieldType = FileIO.FieldType.Delimited
                sr.SetDelimiters(",")
                While Not sr.EndOfData
                    Try
                        Dim data As String() = sr.ReadFields
                        If data.Length > 0 Then
                            Dim c As Integer = data.Length
                            Dim listItem As ListViewItem = thisListView.Items.Add(data(0))
                            If c > 1 Then listItem.SubItems.Add(data(1))
                            If c > 2 Then listItem.SubItems.Add(data(2))
                            If c > 3 Then listItem.SubItems.Add(data(3).Replace(ControlChars.Quote & ControlChars.Quote, ControlChars.Quote))
                        End If
                    Catch
                    End Try
                End While
            End Using
        Next fileName
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(DialogFileOpen.FileName)
    End Sub

    Private Sub SaveOutgoingMessageListToFile(ByVal thisListView As ListView)
        If thisListView Is Nothing Then Return
        If thisListView.Items.Count < 1 Then Return
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileSave.InitialDirectory = fileFolder
        DialogFileSave.FileName = String.Empty
        DialogFileSave.DefaultExt = ".out"
        DialogFileSave.Filter = "Message In Files (*.out)|*.out"
        DialogFileSave.Title = "Save Messages As:"
        DialogFileSave.RestoreDirectory = True
        DialogFileSave.CheckPathExists = True
        DialogFileSave.OverwritePrompt = True
        If DialogFileSave.ShowDialog() = DialogResult.Cancel Then Return
        Dim fileName As String = DialogFileSave.FileName
        Dim sb As New StringBuilder
        For Each item As ListViewItem In thisListView.Items
            sb.AppendLine(item.Text & "," &
                          item.SubItems(1).Text & "," &
                          item.SubItems(2).Text & "," &
                          PrepareCSV(item.SubItems(3).Text))
        Next item
        Using sw As New StreamWriter(DialogFileSave.FileName, False, Encoding.ASCII)
            sw.Write(sb.ToString)
        End Using
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(fileName)
    End Sub

    Private Sub CopyOutgoingMessageToScratchPad(ByVal thisListView As ListView)
        If thisListView.SelectedItems.Count > 0 Then
            ComboBoxMessageChannel.Text = thisListView.SelectedItems(0).Text
            TextBoxOutFlags.Text = thisListView.SelectedItems(0).SubItems(1).Text
            TextBoxMessageOut.Text = thisListView.SelectedItems(0).SubItems(2).Text
        End If
    End Sub

    Private Function GetScratchPadMessageText() As J2534MessageText
        Dim result As New J2534MessageText
        Dim comment As String = FormInputBox.ShowDialog("Please enter a comment for the message:",
                                              "Add to Outgoing Message Set", Nothing,
                                              Me.Left + Me.Width \ 2, Me.Top + Me.Height \ 2 - 60).Response
        result.ProtocolName = ComboBoxMessageChannel.Text
        result.Data = TextBoxMessageOut.Text
        result.TxFlags = TextBoxOutFlags.Text
        result.Comment = comment
        Return result
    End Function

    Private Function GetNewMessageTextFromDialog() As J2534MessageText
        Dim result As New J2534MessageText With
            {
            .Comment = "Add a comment for this message",
            .ProtocolName = ComboBoxMessageChannel.Text,
            .TxFlags = FlagsToText(0UI)
            }
        If String.IsNullOrWhiteSpace(result.ProtocolName) Then result.ProtocolName = "CAN"
        If Not FormMessageEdit.ShowDialog(result, selectedDevice) = DialogResult.OK Then result.ProtocolName = String.Empty
        Return result
    End Function

    Private Sub MakePeriodicFromOutgoingMessage(ByVal thisListView As ListView)
        If thisListView.SelectedItems.Count > 0 Then
            Dim device As J2534Device = GetListViewParentDevice(thisListView)
            If device Is Nothing Then Return
            Dim messageText As New J2534MessageText With {.ProtocolName = thisListView.SelectedItems(0).Text}
            Dim channel As J2534Channel = device.Channel(messageText.ProtocolName)
            If channel Is Nothing Then Return
            Dim channelISO As J2534Channel = Nothing
            If channel.IsCAN AndAlso Not channel.Connected Then
                channelISO = CAN_ChannelToISO_Channel(device, channel)
            End If
            If channelISO IsNot Nothing Then channel = channelISO
            Dim nextPM As Integer = If(channel?.NextPeriodicMessage, -1)
            If nextPM < 0 Then Return
            messageText.Data = thisListView.SelectedItems(0).SubItems(2).Text
            messageText.TxFlags = thisListView.SelectedItems(0).SubItems(1).Text
            channel.PeriodicMessage(nextPM).Message = messageText.ToMessage
            SetPeriodicMessageEditBuffer(nextPM)
        End If
    End Sub

    Public Sub LoadFiltersFromFile()
        If Not TextBoxFilterMask(0).Enabled Then Return
        Dim filterTxt As J2534FilterText() = {}
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileOpen.InitialDirectory = fileFolder
        DialogFileOpen.FileName = String.Empty
        DialogFileOpen.DefaultExt = ".filter"
        DialogFileOpen.Filter = "Filter Files (*.filter)|*.filter"
        DialogFileOpen.Title = "Load Filters:"
        DialogFileOpen.ShowReadOnly = False
        DialogFileOpen.RestoreDirectory = True
        DialogFileOpen.CheckFileExists = True
        DialogFileOpen.CheckPathExists = False
        DialogFileOpen.Multiselect = False
        If DialogFileOpen.ShowDialog() = DialogResult.Cancel Then Return
        Dim fileName As String = DialogFileOpen.FileName
        Dim filterDocument As New XmlDocument()
        If Not File.Exists(fileName) Then Return
        Try
            filterDocument.Load(fileName)
        Catch
            MessageBox.Show(Me, "File may not have been a valid filter file", "Load Filter File", MessageBoxButtons.OK)
            Return
        End Try

        Try
            Dim nodeList As XmlNodeList = filterDocument.SelectSingleNode("Filters").ChildNodes
            For Each node As XmlNode In nodeList
                If node.Name.StartsWith("Filter", IgnoreCase) Then
                    Dim localFilter As New J2534FilterText
                    For Each item As XmlNode In node.ChildNodes
                        If item.Name.Equals("Type", IgnoreCase) Then
                            If Not String.IsNullOrWhiteSpace(item.InnerText) Then
                                localFilter.FilterType = item.InnerText
                            End If
                        ElseIf item.Name.Equals("MaskMsg", IgnoreCase) Then
                            For Each subItem As XmlNode In item.ChildNodes
                                If subItem.Name.Equals("Protocol", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.MaskMessageText.ProtocolName = subItem.InnerText
                                    End If
                                ElseIf subItem.Name.Equals("Data", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.MaskMessageText.Data = subItem.InnerText
                                    End If
                                ElseIf subItem.Name.Equals("TxFlags", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.MaskMessageText.TxFlags = subItem.InnerText
                                    End If
                                End If
                            Next subItem
                        ElseIf item.Name.Equals("PattMsg", IgnoreCase) Then
                            For Each subItem As XmlNode In item.ChildNodes
                                If subItem.Name.Equals("Protocol", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.PatternMessageText.ProtocolName = subItem.InnerText
                                    End If
                                ElseIf subItem.Name.Equals("Data", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.PatternMessageText.Data = subItem.InnerText
                                    End If
                                ElseIf subItem.Name.Equals("TxFlags", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.PatternMessageText.TxFlags = subItem.InnerText
                                    End If
                                End If
                            Next subItem
                        ElseIf item.Name.Equals("FlowMsg", IgnoreCase) Then
                            For Each subItem As XmlNode In item.ChildNodes
                                If subItem.Name.Equals("Protocol", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.FlowMessageText.ProtocolName = subItem.InnerText
                                    End If
                                ElseIf subItem.Name.Equals("Data", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.FlowMessageText.Data = subItem.InnerText
                                    End If
                                ElseIf subItem.Name.Equals("TxFlags", IgnoreCase) Then
                                    If Not String.IsNullOrWhiteSpace(subItem.InnerText) Then
                                        localFilter.FlowMessageText.TxFlags = subItem.InnerText
                                    End If
                                End If
                            Next subItem
                        ElseIf item.Name.Equals("Type", IgnoreCase) Then
                            If Not String.IsNullOrWhiteSpace(item.InnerText) Then
                                localFilter.FilterType = item.InnerText
                            End If
                        ElseIf item.Name.Equals("Enabled", IgnoreCase) Then
                            Boolean.TryParse(item.InnerText, localFilter.Enabled)
                        End If

                    Next item
                    Dim nextFilter As Integer = filterTxt.Length
                    ReDim Preserve filterTxt(nextFilter)
                    filterTxt(nextFilter) = localFilter
                End If
            Next node

        Catch
        End Try
        Dim c As Integer = filterTxt.Length - 1
        If c > MAX_FILTER Then c = MAX_FILTER
        settingFilters = True
        For i As Integer = 0 To c
            TextBoxFilterMask(i).Text = filterTxt(i).MaskMessageText.Data
            TextBoxFilterPatt(i).Text = filterTxt(i).PatternMessageText.Data
            TextBoxFilterFlow(i).Text = filterTxt(i).FlowMessageText.Data
            TextBoxFilterFlags(i).Text = filterTxt(i).FlowMessageText.TxFlags
            ComboBoxFilterType(i).Text = filterTxt(i).FilterType
            SetFilterEditBuffer(i)
        Next i
        settingFilters = False
        UpdateFilterEdit()
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(fileName)
    End Sub

    Private Sub SaveFiltersToFile()
        Dim protName As String = ComboBoxAvailableChannel.Text
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileSave.InitialDirectory = fileFolder
        DialogFileSave.FileName = String.Empty
        DialogFileSave.DefaultExt = ".filter"
        DialogFileSave.Filter = "Filter Files (*.filter)|*.filter"
        DialogFileSave.Title = "Save Filters As:"
        DialogFileSave.RestoreDirectory = True
        DialogFileSave.CheckPathExists = True
        DialogFileSave.OverwritePrompt = True
        If DialogFileSave.ShowDialog() = DialogResult.Cancel Then Return
        Dim fileName As String = DialogFileSave.FileName
        Dim document As New XmlDocument
        document.AppendChild(document.CreateXmlDeclaration("1.0", "utf-8", String.Empty))

        Dim filterType As String = String.Empty
        Dim filtersNode As XmlNode = document.CreateElement("Filters")
        document.AppendChild(filtersNode)
        Dim filterNode, itemNode, subItemNode As XmlNode
        Dim protocolNode As XmlNode = document.CreateElement("Protocol")
        protocolNode.InnerText = protName
        filtersNode.AppendChild(protocolNode)
        For filterIndex As Integer = 0 To MAX_FILTER
            If Not channel.Filter(filterIndex).IsEmpty Then
                Dim mtxtMask As J2534MessageText = channel.Filter(filterIndex).MaskMessage.ToMessageText
                Dim mtxtPatt As J2534MessageText = channel.Filter(filterIndex).PatternMessage.ToMessageText
                Dim mtxtFlow As J2534MessageText = channel.Filter(filterIndex).FlowMessage.ToMessageText
                filterNode = document.CreateElement("Filter-" & filterIndex.ToString("00"))
                filterType = FilterTypeToName(channel.Filter(filterIndex).FilterType)
                itemNode = document.CreateElement("Type")
                itemNode.InnerText = filterType
                filterNode.AppendChild(itemNode)
                If Not String.IsNullOrWhiteSpace(mtxtMask.Data) Then
                    itemNode = document.CreateElement("MaskMsg")
                    subItemNode = document.CreateElement("Protocol")
                    subItemNode.InnerText = mtxtMask.ProtocolName
                    itemNode.AppendChild(subItemNode)
                    subItemNode = document.CreateElement("Data")
                    subItemNode.InnerText = mtxtMask.Data
                    itemNode.AppendChild(subItemNode)
                    subItemNode = document.CreateElement("TxFlags")
                    subItemNode.InnerText = mtxtMask.TxFlags
                    itemNode.AppendChild(subItemNode)
                    filterNode.AppendChild(itemNode)
                End If
                If Not String.IsNullOrWhiteSpace(mtxtPatt.Data) Then
                    itemNode = document.CreateElement("PattMsg")
                    subItemNode = document.CreateElement("Protocol")
                    subItemNode.InnerText = mtxtPatt.ProtocolName
                    itemNode.AppendChild(subItemNode)
                    subItemNode = document.CreateElement("Data")
                    subItemNode.InnerText = mtxtPatt.Data
                    itemNode.AppendChild(subItemNode)
                    subItemNode = document.CreateElement("TxFlags")
                    subItemNode.InnerText = mtxtPatt.TxFlags
                    itemNode.AppendChild(subItemNode)
                    filterNode.AppendChild(itemNode)
                End If
                If Not String.IsNullOrWhiteSpace(mtxtFlow.Data) Then
                    itemNode = document.CreateElement("FlowMsg")
                    subItemNode = document.CreateElement("Protocol")
                    subItemNode.InnerText = mtxtFlow.ProtocolName
                    itemNode.AppendChild(subItemNode)
                    subItemNode = document.CreateElement("Data")
                    subItemNode.InnerText = mtxtFlow.Data
                    itemNode.AppendChild(subItemNode)
                    subItemNode = document.CreateElement("TxFlags")
                    subItemNode.InnerText = mtxtFlow.TxFlags
                    itemNode.AppendChild(subItemNode)
                    filterNode.AppendChild(itemNode)
                End If
                itemNode = document.CreateElement("ID")
                itemNode.InnerText = channel.Filter(filterIndex).MessageId.ToString
                filterNode.AppendChild(itemNode)
                itemNode = document.CreateElement("Enabled")
                itemNode.InnerText = channel.Filter(filterIndex).Enabled.ToString
                filterNode.AppendChild(itemNode)
                filtersNode.AppendChild(filterNode)
            End If 'filter is not empty
        Next filterIndex

        Dim xws As New XmlWriterSettings With
            {
            .Indent = True,
            .IndentChars = "    ",
            .CloseOutput = True,
            .NamespaceHandling = NamespaceHandling.OmitDuplicates
            }
        Try
            Using xm As XmlWriter = XmlWriter.Create(fileName, xws)
                document.Save(xm)
            End Using
        Catch ex As Exception
        End Try
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(fileName)
    End Sub

    Public Sub LoadPeriodicMessagesFromFile()
        If Not TextBoxPeriodicMessage(0).Enabled Then Return
        Dim PMText As J2534PeriodicMessageText() = {}
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileOpen.InitialDirectory = fileFolder
        DialogFileOpen.FileName = String.Empty
        DialogFileOpen.DefaultExt = ".periodic"
        DialogFileOpen.Filter = "Periodic Message Files (*.periodic)|*.periodic"
        DialogFileOpen.Title = "Load Periodic Messages:"
        DialogFileOpen.ShowReadOnly = False
        DialogFileOpen.RestoreDirectory = True
        DialogFileOpen.CheckFileExists = True
        DialogFileOpen.CheckPathExists = False
        DialogFileOpen.Multiselect = False
        If DialogFileOpen.ShowDialog() = DialogResult.Cancel Then Return
        Dim fileName As String = DialogFileOpen.FileName
        Dim pmDocument As New XmlDocument()
        If Not File.Exists(fileName) Then Return
        Try
            pmDocument.Load(fileName)
        Catch
            MessageBox.Show(Me, "File may not have been a valid filter file", "Load Filter File", MessageBoxButtons.OK)
            Return
        End Try

        Try
            Dim nodeList As XmlNodeList = pmDocument.SelectSingleNode("PeriodicMessages").ChildNodes
            For Each node As XmlNode In nodeList
                If node.Name.StartsWith("PeriodicMessage", IgnoreCase) Then
                    Dim localPMText As New J2534PeriodicMessageText
                    For Each item As XmlNode In node.ChildNodes
                        If item.Name.Equals("Protocol", IgnoreCase) Then
                            If Not String.IsNullOrWhiteSpace(item.InnerText) Then
                                localPMText.MessageText.ProtocolName = item.InnerText
                            End If
                        ElseIf item.Name.Equals("ID", IgnoreCase) Then
                            UInteger.TryParse(item.InnerText, localPMText.MessageId)
                        ElseIf item.Name.Equals("Message", IgnoreCase) Then
                            If Not String.IsNullOrWhiteSpace(item.InnerText) Then
                                localPMText.MessageText.Data = item.InnerText
                            End If
                        ElseIf item.Name.Equals("TxFlags", IgnoreCase) Then
                            If Not String.IsNullOrWhiteSpace(item.InnerText) Then
                                localPMText.MessageText.TxFlags = item.InnerText
                            End If
                        ElseIf item.Name.Equals("Enabled", IgnoreCase) Then
                            Boolean.TryParse(item.InnerText, localPMText.Enabled)
                        ElseIf item.Name.Equals("Interval", IgnoreCase) Then
                            UInteger.TryParse(item.InnerText, localPMText.Interval)
                        End If
                    Next item
                    Dim nextPM As Integer = PMText.Length
                    ReDim Preserve PMText(nextPM)
                    PMText(nextPM) = localPMText
                End If
            Next node
        Catch
        End Try
        Dim n As Integer = PMText.Length - 1
        If n > MAX_PM Then n = MAX_PM
        For i As Integer = 0 To n
            settingPeriodicMessages = True
            TextBoxPeriodicMessage(i).Text = PMText(i).MessageText.Data
            If ComboBoxPeriodicMessageChannel(i).Visible Then
                ComboBoxPeriodicMessageChannel(i).Text = PMText(i).MessageText.ProtocolName
            End If
            TextBoxPeriodicMessageFlags(i).Text = PMText(i).MessageText.TxFlags
            TextBoxPeriodicMessageInterval(i).Text = PMText(i).Interval.ToString
            settingPeriodicMessages = False
            SetPeriodicMessageEditBuffer(i)
            UpdatePeriodicMessageCheck(i)
        Next i
        UpdatePeriodicMessageEdit()
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(fileName)
    End Sub

    Private Sub SavePeriodicMessagesToFile()
        Dim protName As String = ComboBoxAvailableChannel.Text
        Dim channel As J2534Channel = TryCast(ComboBoxAvailableChannel.SelectedItem, J2534Channel)
        If channel Is Nothing Then Return
        Dim fileFolder As String = My.Settings.DataFilePath
        If String.IsNullOrWhiteSpace(fileFolder) OrElse Not IO.Directory.Exists(fileFolder) Then fileFolder = GetDataPath()
        DialogFileSave.InitialDirectory = fileFolder
        DialogFileSave.FileName = String.Empty
        DialogFileSave.DefaultExt = ".periodic"
        DialogFileSave.Filter = "Periodic Message Files (*.periodic)|*.periodic"
        DialogFileSave.Title = "Save Periodic Messages As:"
        DialogFileSave.RestoreDirectory = True
        DialogFileSave.CheckPathExists = True
        DialogFileSave.OverwritePrompt = True
        If DialogFileSave.ShowDialog() = DialogResult.Cancel Then Return
        Dim fileName As String = DialogFileSave.FileName
        Dim document As New XmlDocument
        document.AppendChild(document.CreateXmlDeclaration("1.0", "utf-8", String.Empty))

        Dim pmsNode As XmlNode = document.CreateElement("PeriodicMessages")
        document.AppendChild(pmsNode)
        Dim pmNode, itemNode As XmlNode
        Dim protocolNode As XmlNode = document.CreateElement("Protocol")
        protocolNode.InnerText = protName
        pmsNode.AppendChild(protocolNode)
        For pmIndex As Integer = 0 To MAX_PM
            If Not channel.PeriodicMessage(pmIndex).IsEmpty Then
                Dim messageText As J2534MessageText = channel.PeriodicMessage(pmIndex).Message.ToMessageText
                pmNode = document.CreateElement("PeriodicMessage-" & pmIndex.ToString("00"))
                itemNode = document.CreateElement("Protocol")
                itemNode.InnerText = protName
                pmNode.AppendChild(itemNode)
                itemNode = document.CreateElement("ID")
                itemNode.InnerText = channel.PeriodicMessage(pmIndex).MessageId.ToString
                pmNode.AppendChild(itemNode)
                If Not String.IsNullOrWhiteSpace(messageText.Data) Then
                    itemNode = document.CreateElement("Message")
                    itemNode.InnerText = messageText.Data
                    pmNode.AppendChild(itemNode)
                End If
                itemNode = document.CreateElement("TxFlags")
                itemNode.InnerText = messageText.TxFlags
                pmNode.AppendChild(itemNode)
                itemNode = document.CreateElement("Interval")
                itemNode.InnerText = channel.PeriodicMessage(pmIndex).Interval.ToString
                pmNode.AppendChild(itemNode)
                itemNode = document.CreateElement("Enabled")
                itemNode.InnerText = channel.PeriodicMessage(pmIndex).Enabled.ToString
                pmNode.AppendChild(itemNode)
                pmsNode.AppendChild(pmNode)
            End If 'pm is not empty
        Next pmIndex

        Dim xws As New XmlWriterSettings With
            {
            .Indent = True,
            .IndentChars = "    ",
            .CloseOutput = True,
            .NamespaceHandling = NamespaceHandling.OmitDuplicates
            }
        Try
            Using xm As XmlWriter = XmlWriter.Create(fileName, xws)
                document.Save(xm)
            End Using
        Catch ex As Exception
        End Try
        My.Settings.DataFilePath = IO.Path.GetDirectoryName(fileName)
    End Sub

    Private Shared Function GetDataPath() As String
        Dim appPath As String = New IO.FileInfo(Application.ExecutablePath).DirectoryName
        Dim tryPath As String = appPath & "\Data"
        If IO.Directory.Exists(tryPath) Then Return tryPath
        Return appPath
    End Function

    Private Sub SetControlArrays()
        InitializeComboAvailableChannelLocator()
        InitializeComboAvailableBoxLocator()
        InitializeTextBoxParamVal()
        For i As Integer = 0 To TextBoxParamVal.Length - 1
            AddHandler TextBoxParamVal(i).TextChanged, AddressOf TextBoxParamVal_TextChanged
            AddHandler TextBoxParamVal(i).KeyPress, AddressOf TextBoxParamVal_KeyPress
            AddHandler TextBoxParamVal(i).Leave, AddressOf TextBoxParamVal_Leave
        Next
        InitializeTextBoxPeriodicMessage()
        For i As Integer = 0 To TextBoxPeriodicMessage.Length - 1
            AddHandler TextBoxPeriodicMessage(i).TextChanged, AddressOf TextBoxPeriodicMessage_TextChanged
            AddHandler TextBoxPeriodicMessage(i).KeyPress, AddressOf TextBoxPeriodicMessage_KeyPress
            AddHandler TextBoxPeriodicMessage(i).Leave, AddressOf TextBoxPeriodicMessage_Leave
        Next
        InitializeTextBoxPeriodicMessageInterval()
        For i As Integer = 0 To TextBoxPeriodicMessageInterval.Length - 1
            AddHandler TextBoxPeriodicMessageInterval(i).TextChanged, AddressOf TextBoxPeriodicMessageInterval_TextChanged
            AddHandler TextBoxPeriodicMessageInterval(i).KeyPress, AddressOf TextBoxPeriodicMessageInterval_KeyPress
            AddHandler TextBoxPeriodicMessageInterval(i).Leave, AddressOf TextBoxPeriodicMessageInterval_Leave
        Next
        InitializeTextBoxPeriodicMessageFlags()
        For i As Integer = 0 To TextBoxPeriodicMessageFlags.Length - 1
            AddHandler TextBoxPeriodicMessageFlags(i).TextChanged, AddressOf TextBoxPeriodicMessageFlags_TextChanged
            AddHandler TextBoxPeriodicMessageFlags(i).KeyPress, AddressOf TextBoxPeriodicMessageFlags_KeyPress
            AddHandler TextBoxPeriodicMessageFlags(i).Leave, AddressOf TextBoxPeriodicMessageFlags_Leave
        Next
        InitializeTextBoxFunctionalMessage()
        For i As Integer = 0 To TextBoxFunctionalMessage.Length - 1
            AddHandler TextBoxFunctionalMessage(i).TextChanged, AddressOf TextBoxFunctionalMessage_TextChanged
            AddHandler TextBoxFunctionalMessage(i).KeyPress, AddressOf TextBoxFunctionalMessage_KeyPress
            AddHandler TextBoxFunctionalMessage(i).Leave, AddressOf TextBoxFunctionalMessage_Leave
        Next
        InitializeTextBoxFilterPatt()
        For i As Integer = 0 To TextBoxFilterPatt.Length - 1
            AddHandler TextBoxFilterPatt(i).TextChanged, AddressOf TextBoxFilterPatt_TextChanged
            AddHandler TextBoxFilterPatt(i).KeyPress, AddressOf TextBoxFilterPatt_KeyPress
            AddHandler TextBoxFilterPatt(i).Leave, AddressOf TextBoxFilterPatt_Leave
        Next
        InitializeTextBoxFilterMask()
        For i As Integer = 0 To TextBoxFilterMask.Length - 1
            AddHandler TextBoxFilterMask(i).TextChanged, AddressOf TextBoxFilterMask_TextChanged
            AddHandler TextBoxFilterMask(i).KeyPress, AddressOf TextBoxFilterMask_KeyPress
            AddHandler TextBoxFilterMask(i).Leave, AddressOf TextBoxFilterMask_Leave
        Next
        InitializeTextBoxFilterFlow()
        For i As Integer = 0 To TextBoxFilterFlow.Length - 1
            AddHandler TextBoxFilterFlow(i).TextChanged, AddressOf TextBoxFilterFlow_TextChanged
            AddHandler TextBoxFilterFlow(i).KeyPress, AddressOf TextBoxFilterFlow_KeyPress
            AddHandler TextBoxFilterFlow(i).Leave, AddressOf TextBoxFilterFlow_Leave
        Next
        InitializeTextBoxFilterFlags()
        For i As Integer = 0 To TextBoxFilterFlags.Length - 1
            AddHandler TextBoxFilterFlags(i).TextChanged, AddressOf TextBoxFilterFlags_TextChanged
            AddHandler TextBoxFilterFlags(i).KeyPress, AddressOf TextBoxFilterFlags_KeyPress
            AddHandler TextBoxFilterFlags(i).Leave, AddressOf TextBoxFilterFlags_Leave
        Next
        InitializeProgressBarAnalog()
        InitializeRadioButtonPin()
        For i As Integer = 0 To RadioButtonPin.Length - 1
            If RadioButtonPin(i) IsNot Nothing Then AddHandler RadioButtonPin(i).CheckedChanged, AddressOf RadioButtonPin_CheckedChanged
        Next
        InitializeLabelChannel()
        InitializeLabelPin()
        InitializeLabelParameterName()
        InitializeLabelParamVal()
        InitializeLabelPeriodicMessageId()
        InitializeLabelPeriodicMessage()
        InitializeLabelFunctionalMessage()
        InitializeLabelFuncId()
        InitializeLabelFilterId()
        InitializeLabelFilter()
        InitializeLabelDeviceCombo()
        InitializeLabelAnalogRead()
        InitializeLabelAnalogCH()
        InitializeCheckBoxPeriodicMessageEnable()
        For i As Integer = 0 To CheckBoxPeriodicMessageEnable.Length - 1
            AddHandler CheckBoxPeriodicMessageEnable(i).Click, AddressOf CheckBoxPeriodicMessageEnable_Click
        Next
        InitializeCheckBoxFunctionalMessageDelete()
        For i As Integer = 0 To CheckBoxFunctionalMessageDelete.Length - 1
            AddHandler CheckBoxFunctionalMessageDelete(i).Click, AddressOf CheckBoxFunctionalMessageDelete_Click
        Next
        InitializeComboBoxPeriodicMessageChannel()
        For i As Integer = 0 To ComboBoxPeriodicMessageChannel.Length - 1
            AddHandler ComboBoxPeriodicMessageChannel(i).SelectionChangeCommitted, AddressOf ComboBoxPeriodicMessageChannel_SelectionChangeCommitted
            AddHandler ComboBoxPeriodicMessageChannel(i).Leave, AddressOf ComboBoxPeriodicMessageChannel_Leave
        Next
        InitializeComboBoxFilterType()
        For i As Integer = 0 To ComboBoxFilterType.Length - 1
            AddHandler ComboBoxFilterType(i).SelectionChangeCommitted, AddressOf ComboBoxFilterType_SelectionChangeCommitted
        Next
        InitializeCheckBoxCH()
        For i As Integer = 0 To CheckBoxCH.Length - 1
            AddHandler CheckBoxCH(i).Click, AddressOf CheckBoxCH_Click
        Next i
    End Sub

End Class