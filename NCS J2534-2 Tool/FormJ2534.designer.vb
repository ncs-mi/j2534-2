' NCS J2534-2 Tool
' Copyright � 2017, 2018 National Control Systems, Inc.
'
'
' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.
'
' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.
'
' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
' National Control Systems, Inc.
' 10737 Hamburg Rd
' Hamburg, MI 48139
' 810-231-2901

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormJ2534
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormJ2534))
        Me.ComboAvailableBoxLocator0 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator0 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator1 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator1 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator2 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator2 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator3 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator3 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator4 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator4 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator5 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator5 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator6 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator6 = New System.Windows.Forms.Label()
        Me.ComboAvailableBoxLocator7 = New System.Windows.Forms.Label()
        Me.ComboAvailableChannelLocator7 = New System.Windows.Forms.Label()
        Me.MenuStripMain = New System.Windows.Forms.MenuStrip()
        Me.MenuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileMsgOutLoad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileFilterLoad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFilePeriodicMessageLoad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFileMsgInSaveSelected = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileMsgInSaveAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileMsgOutSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileFilterSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFilePeriodicMessageSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFileSpace2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFileExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuEditCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditCopyLine = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditMsgInCopyData = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditSpace2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuEditMsgInMakeFilter = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditFlagsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditRxStatus = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditFlagsClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditFlagsSetDefault = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditMsgOutEditMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditMsgOutAddMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditMsgOutCopyToScratchPad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditScratchAddToOutgoingMessageSet = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditMsgOutMakePeriodicMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEditClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuHelpContents = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInCopyLine = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInCopyData = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInMakeFilter = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInRxStatus = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuMsgInSaveSelected = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInSaveAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgInClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutEditMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutAddMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutCopyToScratchPad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutMakePeriodicMessage = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuMsgOutSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutLoad = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMsgOutClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSpace3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuFlagsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuFlagsSetDefault = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPageConnect = New System.Windows.Forms.TabPage()
        Me.GroupBoxDevices = New System.Windows.Forms.GroupBox()
        Me.ComboBoxDevice = New System.Windows.Forms.ComboBox()
        Me.ButtonDiscover = New System.Windows.Forms.Button()
        Me.ButtonCloseBox = New System.Windows.Forms.Button()
        Me.ButtonOpenBox = New System.Windows.Forms.Button()
        Me.LabelDeviceInfo = New System.Windows.Forms.Label()
        Me.LabelComboDevice = New System.Windows.Forms.Label()
        Me.GroupBoxAPIs = New System.Windows.Forms.GroupBox()
        Me.ComboBoxAPI = New System.Windows.Forms.ComboBox()
        Me.ButtonLoadDLL = New System.Windows.Forms.Button()
        Me.TextBoxDllPath = New System.Windows.Forms.TextBox()
        Me.ContextMenuStripTextBox = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MenuTextBoxUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuTextBoxCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuTextBoxSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuTextBoxSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.LabelAPI = New System.Windows.Forms.Label()
        Me.LabelVendor = New System.Windows.Forms.Label()
        Me.LabelDevice = New System.Windows.Forms.Label()
        Me.LabelDllPath = New System.Windows.Forms.Label()
        Me.LabelDllName = New System.Windows.Forms.Label()
        Me.GroupBoxConnect = New System.Windows.Forms.GroupBox()
        Me.ComboBoxPins = New System.Windows.Forms.ComboBox()
        Me.ComboBoxConnector = New System.Windows.Forms.ComboBox()
        Me.CheckBoxMixedMode = New System.Windows.Forms.CheckBox()
        Me.TextBoxConnectFlags = New System.Windows.Forms.TextBox()
        Me.ContextMenuStripFlags = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MenuFlagsDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComboBoxBaudRate = New System.Windows.Forms.ComboBox()
        Me.ButtonConnect = New System.Windows.Forms.Button()
        Me.ButtonDisconnect = New System.Windows.Forms.Button()
        Me.ComboBoxConnectChannel = New System.Windows.Forms.ComboBox()
        Me.LabelPins = New System.Windows.Forms.Label()
        Me.LabelConn = New System.Windows.Forms.Label()
        Me.LabelConnectFlags = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo0 = New System.Windows.Forms.Label()
        Me.LabelBaud = New System.Windows.Forms.Label()
        Me.LabelConnectChannel = New System.Windows.Forms.Label()
        Me.GroupBoxJ2534Info = New System.Windows.Forms.GroupBox()
        Me.LabelProtSupport = New System.Windows.Forms.Label()
        Me.LabelJ2534Info = New System.Windows.Forms.Label()
        Me.TabPageMessages = New System.Windows.Forms.TabPage()
        Me.ButtonClaimJ1939Address = New System.Windows.Forms.Button()
        Me.LabelMsgFlags = New System.Windows.Forms.Label()
        Me.LabelChannel1 = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo1 = New System.Windows.Forms.Label()
        Me.LvOutLocator = New System.Windows.Forms.Label()
        Me.LvInLocator = New System.Windows.Forms.Label()
        Me.ComboBoxMessageChannel = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPadMessage = New System.Windows.Forms.CheckBox()
        Me.TextBoxTimeOut = New System.Windows.Forms.TextBox()
        Me.TextBoxOutFlags = New System.Windows.Forms.TextBox()
        Me.ButtonClearRx = New System.Windows.Forms.Button()
        Me.ButtonClearTx = New System.Windows.Forms.Button()
        Me.TextBoxMessageOut = New System.Windows.Forms.TextBox()
        Me.ContextMenuStripScratchPad = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MenuScratchPadUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuScratchPadSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuScratchPadCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuScratchPadCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuScratchPadPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuScratchPadDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuScratchPadSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuScratchPadSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuScratchPadSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuScratchPadAddToOutgoingMessageSet = New System.Windows.Forms.ToolStripMenuItem()
        Me.ButtonSend = New System.Windows.Forms.Button()
        Me.ButtonReceive = New System.Windows.Forms.Button()
        Me.TextBoxReadRate = New System.Windows.Forms.TextBox()
        Me.ButtonClearList = New System.Windows.Forms.Button()
        Me.LabelScratchPad = New System.Windows.Forms.Label()
        Me.LabelTO = New System.Windows.Forms.Label()
        Me.LabelReadRate = New System.Windows.Forms.Label()
        Me.TabPagePeriodicMessages = New System.Windows.Forms.TabPage()
        Me.ComboBoxPeriodicMessageChannel0 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel1 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel2 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel3 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel4 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel5 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel6 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel7 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel8 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxPeriodicMessageChannel9 = New System.Windows.Forms.ComboBox()
        Me.ButtonClearAllPeriodicMessages = New System.Windows.Forms.Button()
        Me.ButtonCancelPeriodicMessages = New System.Windows.Forms.Button()
        Me.ButtonApplyPeriodicMessages = New System.Windows.Forms.Button()
        Me.TextBoxPeriodicMessageFlags0 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags1 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags2 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags3 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags4 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags5 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags6 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags7 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags8 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageFlags9 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage0 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage1 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage2 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage3 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage4 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage5 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage6 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage7 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage8 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessage9 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval0 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval1 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval2 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval3 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval4 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval5 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval6 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval7 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval8 = New System.Windows.Forms.TextBox()
        Me.TextBoxPeriodicMessageInterval9 = New System.Windows.Forms.TextBox()
        Me.CheckBoxPeriodicMessageEnable0 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable1 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable2 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable3 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable4 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable5 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable6 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable7 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable8 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeriodicMessageEnable9 = New System.Windows.Forms.CheckBox()
        Me.LabelPeriodicMessageChannel = New System.Windows.Forms.Label()
        Me.LabelChannel2 = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo2 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageIds = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId0 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId1 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId2 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId3 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId4 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId5 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId6 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId7 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId8 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageId9 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageFlags = New System.Windows.Forms.Label()
        Me.LabelPMMessage = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage0 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage1 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage2 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage3 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage4 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage5 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage6 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage7 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage8 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessage9 = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageInterval = New System.Windows.Forms.Label()
        Me.LabelPeriodicMessageDelete = New System.Windows.Forms.Label()
        Me.TabPageFilters = New System.Windows.Forms.TabPage()
        Me.ButtonCreatePassFilter = New System.Windows.Forms.Button()
        Me.ButtonClearAllFilter = New System.Windows.Forms.Button()
        Me.ButtonCancelFilter = New System.Windows.Forms.Button()
        Me.TextBoxFilterFlow0 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow1 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow2 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow3 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow4 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow5 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow6 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow7 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow8 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlow9 = New System.Windows.Forms.TextBox()
        Me.ButtonApplyFilter = New System.Windows.Forms.Button()
        Me.TextBoxFilterFlags0 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags1 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags2 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags3 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags4 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags5 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags6 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags7 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags8 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterFlags9 = New System.Windows.Forms.TextBox()
        Me.ComboBoxFilterType0 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType1 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType2 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType3 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType4 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType5 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType6 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType7 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType8 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilterType9 = New System.Windows.Forms.ComboBox()
        Me.TextBoxFilterPatt0 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt1 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt2 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt3 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt4 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt5 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt6 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt7 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt8 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterPatt9 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask0 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask1 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask2 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask3 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask4 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask5 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask6 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask7 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask8 = New System.Windows.Forms.TextBox()
        Me.TextBoxFilterMask9 = New System.Windows.Forms.TextBox()
        Me.LabelChannel3 = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo3 = New System.Windows.Forms.Label()
        Me.LabelFilterIds = New System.Windows.Forms.Label()
        Me.LabelFilterId0 = New System.Windows.Forms.Label()
        Me.LabelFilterId1 = New System.Windows.Forms.Label()
        Me.LabelFilterId2 = New System.Windows.Forms.Label()
        Me.LabelFilterId3 = New System.Windows.Forms.Label()
        Me.LabelFilterId4 = New System.Windows.Forms.Label()
        Me.LabelFilterId5 = New System.Windows.Forms.Label()
        Me.LabelFilterId6 = New System.Windows.Forms.Label()
        Me.LabelFilterId7 = New System.Windows.Forms.Label()
        Me.LabelFilterId8 = New System.Windows.Forms.Label()
        Me.LabelFilterId9 = New System.Windows.Forms.Label()
        Me.LabelFilterType = New System.Windows.Forms.Label()
        Me.LabelFilterFlags = New System.Windows.Forms.Label()
        Me.LabelFilterFlow = New System.Windows.Forms.Label()
        Me.LabelFilterPatt = New System.Windows.Forms.Label()
        Me.LabelFilterMask = New System.Windows.Forms.Label()
        Me.LabelFilter0 = New System.Windows.Forms.Label()
        Me.LabelFilter1 = New System.Windows.Forms.Label()
        Me.LabelFilter2 = New System.Windows.Forms.Label()
        Me.LabelFilter3 = New System.Windows.Forms.Label()
        Me.LabelFilter4 = New System.Windows.Forms.Label()
        Me.LabelFilter5 = New System.Windows.Forms.Label()
        Me.LabelFilter6 = New System.Windows.Forms.Label()
        Me.LabelFilter7 = New System.Windows.Forms.Label()
        Me.LabelFilter8 = New System.Windows.Forms.Label()
        Me.LabelFilter9 = New System.Windows.Forms.Label()
        Me.TabPageConfig = New System.Windows.Forms.TabPage()
        Me.GroupBoxAnalogConfig = New System.Windows.Forms.GroupBox()
        Me.LabelMaxSample = New System.Windows.Forms.Label()
        Me.LabelAnalogRate = New System.Windows.Forms.Label()
        Me.TextBoxAnalogRate = New System.Windows.Forms.TextBox()
        Me.PanelAudioChannel = New System.Windows.Forms.Panel()
        Me.CheckBoxCH0 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH1 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH2 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH3 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH4 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH5 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH6 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH7 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH8 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH9 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH10 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH11 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH12 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH13 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH14 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH15 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH16 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH17 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH18 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH19 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH20 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH21 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH22 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH23 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH24 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH25 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH26 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH27 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH28 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH29 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH30 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCH31 = New System.Windows.Forms.CheckBox()
        Me.ComboBoxAnalogRateMode = New System.Windows.Forms.ComboBox()
        Me.ComboBoxAvailableDevice = New System.Windows.Forms.ComboBox()
        Me.ComboBoxAvailableChannel = New System.Windows.Forms.ComboBox()
        Me.ButtonSetConfig = New System.Windows.Forms.Button()
        Me.ButtonClearConfig = New System.Windows.Forms.Button()
        Me.LabelParamVal0 = New System.Windows.Forms.Label()
        Me.LabelParamVal1 = New System.Windows.Forms.Label()
        Me.LabelParamVal2 = New System.Windows.Forms.Label()
        Me.LabelParamVal3 = New System.Windows.Forms.Label()
        Me.LabelParamVal4 = New System.Windows.Forms.Label()
        Me.LabelParamVal5 = New System.Windows.Forms.Label()
        Me.LabelParamVal6 = New System.Windows.Forms.Label()
        Me.LabelParamVal7 = New System.Windows.Forms.Label()
        Me.LabelParamVal8 = New System.Windows.Forms.Label()
        Me.LabelParamVal9 = New System.Windows.Forms.Label()
        Me.LabelParamVal10 = New System.Windows.Forms.Label()
        Me.LabelParamVal11 = New System.Windows.Forms.Label()
        Me.LabelParamVal12 = New System.Windows.Forms.Label()
        Me.LabelParamVal13 = New System.Windows.Forms.Label()
        Me.LabelParamVal14 = New System.Windows.Forms.Label()
        Me.LabelParamVal15 = New System.Windows.Forms.Label()
        Me.LabelParamVal16 = New System.Windows.Forms.Label()
        Me.LabelParamVal17 = New System.Windows.Forms.Label()
        Me.LabelParamVal18 = New System.Windows.Forms.Label()
        Me.LabelParamVal19 = New System.Windows.Forms.Label()
        Me.LabelParamVal20 = New System.Windows.Forms.Label()
        Me.LabelParamVal21 = New System.Windows.Forms.Label()
        Me.LabelParameterName0 = New System.Windows.Forms.Label()
        Me.LabelParameterName1 = New System.Windows.Forms.Label()
        Me.LabelParameterName2 = New System.Windows.Forms.Label()
        Me.LabelParameterName3 = New System.Windows.Forms.Label()
        Me.LabelParameterName4 = New System.Windows.Forms.Label()
        Me.LabelParameterName5 = New System.Windows.Forms.Label()
        Me.LabelParameterName6 = New System.Windows.Forms.Label()
        Me.LabelParameterName7 = New System.Windows.Forms.Label()
        Me.LabelParameterName8 = New System.Windows.Forms.Label()
        Me.LabelParameterName9 = New System.Windows.Forms.Label()
        Me.LabelParameterName10 = New System.Windows.Forms.Label()
        Me.LabelParameterName11 = New System.Windows.Forms.Label()
        Me.LabelParameterName12 = New System.Windows.Forms.Label()
        Me.LabelParameterName13 = New System.Windows.Forms.Label()
        Me.LabelParameterName14 = New System.Windows.Forms.Label()
        Me.LabelParameterName15 = New System.Windows.Forms.Label()
        Me.LabelParameterName16 = New System.Windows.Forms.Label()
        Me.LabelParameterName17 = New System.Windows.Forms.Label()
        Me.LabelParameterName18 = New System.Windows.Forms.Label()
        Me.LabelParameterName19 = New System.Windows.Forms.Label()
        Me.LabelParameterName20 = New System.Windows.Forms.Label()
        Me.LabelParameterName21 = New System.Windows.Forms.Label()
        Me.TextBoxParamVal0 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal1 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal2 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal3 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal4 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal5 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal6 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal7 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal8 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal9 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal10 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal11 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal12 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal13 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal14 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal15 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal16 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal17 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal18 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal19 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal20 = New System.Windows.Forms.TextBox()
        Me.TextBoxParamVal21 = New System.Windows.Forms.TextBox()
        Me.LabelChannel4 = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo4 = New System.Windows.Forms.Label()
        Me.LabelParameters0 = New System.Windows.Forms.Label()
        Me.LabelValues0 = New System.Windows.Forms.Label()
        Me.LabelMod0 = New System.Windows.Forms.Label()
        Me.LabelParameters1 = New System.Windows.Forms.Label()
        Me.LabelValues1 = New System.Windows.Forms.Label()
        Me.LabelMod1 = New System.Windows.Forms.Label()
        Me.TabPageInit = New System.Windows.Forms.TabPage()
        Me.GroupBoxFastInit = New System.Windows.Forms.GroupBox()
        Me.ButtonExecuteFastInit = New System.Windows.Forms.Button()
        Me.TextBoxFIMessage = New System.Windows.Forms.TextBox()
        Me.TextBoxFIFlags = New System.Windows.Forms.TextBox()
        Me.LabelFIRXTitle = New System.Windows.Forms.Label()
        Me.LabelFITXFlags = New System.Windows.Forms.Label()
        Me.LabelFIRxStatus = New System.Windows.Forms.Label()
        Me.LabelFIResponse = New System.Windows.Forms.Label()
        Me.LabelFIMsgResp = New System.Windows.Forms.Label()
        Me.LabelFIMsgData = New System.Windows.Forms.Label()
        Me.GroupBox5BInit = New System.Windows.Forms.GroupBox()
        Me.TextBox5BInitECU = New System.Windows.Forms.TextBox()
        Me.ButtonExecute5BInit = New System.Windows.Forms.Button()
        Me.LabelKWlabel1 = New System.Windows.Forms.Label()
        Me.LabelKWlabel0 = New System.Windows.Forms.Label()
        Me.Label5BECU = New System.Windows.Forms.Label()
        Me.Label5BKeyWord0 = New System.Windows.Forms.Label()
        Me.Label5BKeyWord1 = New System.Windows.Forms.Label()
        Me.LabelChannel5 = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo5 = New System.Windows.Forms.Label()
        Me.TabPageFunctionalMessages = New System.Windows.Forms.TabPage()
        Me.ButtonClearAllFunctionalMessages = New System.Windows.Forms.Button()
        Me.ButtonApplyFunctionalMessages = New System.Windows.Forms.Button()
        Me.ButtonCancelFunctionalMessages = New System.Windows.Forms.Button()
        Me.CheckBoxFunctionalMessageDelete0 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete1 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete2 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete3 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete4 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete5 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete6 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete7 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete8 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete9 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete10 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete11 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete12 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete13 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete14 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete15 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete16 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete17 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete18 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete19 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete20 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete21 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete22 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete23 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete24 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete25 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete26 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete27 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete28 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete29 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete30 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFunctionalMessageDelete31 = New System.Windows.Forms.CheckBox()
        Me.TextBoxFunctionalMessage0 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage1 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage2 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage3 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage4 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage5 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage6 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage7 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage8 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage9 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage10 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage11 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage12 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage13 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage14 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage15 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage16 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage17 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage18 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage19 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage20 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage21 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage22 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage23 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage24 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage25 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage26 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage27 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage28 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage29 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage30 = New System.Windows.Forms.TextBox()
        Me.TextBoxFunctionalMessage31 = New System.Windows.Forms.TextBox()
        Me.LabelChannel6 = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo6 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessageModify = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessageValues = New System.Windows.Forms.Label()
        Me.LabelFuncId31 = New System.Windows.Forms.Label()
        Me.LabelFuncId30 = New System.Windows.Forms.Label()
        Me.LabelFuncId29 = New System.Windows.Forms.Label()
        Me.LabelFuncId28 = New System.Windows.Forms.Label()
        Me.LabelFuncId27 = New System.Windows.Forms.Label()
        Me.LabelFuncId26 = New System.Windows.Forms.Label()
        Me.LabelFuncId25 = New System.Windows.Forms.Label()
        Me.LabelFuncId24 = New System.Windows.Forms.Label()
        Me.LabelFuncId23 = New System.Windows.Forms.Label()
        Me.LabelFuncId22 = New System.Windows.Forms.Label()
        Me.LabelFuncId21 = New System.Windows.Forms.Label()
        Me.LabelFuncId20 = New System.Windows.Forms.Label()
        Me.LabelFuncId19 = New System.Windows.Forms.Label()
        Me.LabelFuncId18 = New System.Windows.Forms.Label()
        Me.LabelFuncId17 = New System.Windows.Forms.Label()
        Me.LabelFuncId16 = New System.Windows.Forms.Label()
        Me.LabelFuncId15 = New System.Windows.Forms.Label()
        Me.LabelFuncId14 = New System.Windows.Forms.Label()
        Me.LabelFuncId13 = New System.Windows.Forms.Label()
        Me.LabelFuncId12 = New System.Windows.Forms.Label()
        Me.LabelFuncId11 = New System.Windows.Forms.Label()
        Me.LabelFuncId10 = New System.Windows.Forms.Label()
        Me.LabelFuncId9 = New System.Windows.Forms.Label()
        Me.LabelFuncId8 = New System.Windows.Forms.Label()
        Me.LabelFuncId7 = New System.Windows.Forms.Label()
        Me.LabelFuncId6 = New System.Windows.Forms.Label()
        Me.LabelFuncId5 = New System.Windows.Forms.Label()
        Me.LabelFuncId4 = New System.Windows.Forms.Label()
        Me.LabelFuncId3 = New System.Windows.Forms.Label()
        Me.LabelFuncId2 = New System.Windows.Forms.Label()
        Me.LabelFuncId1 = New System.Windows.Forms.Label()
        Me.LabelFuncId0 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage0 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage1 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage2 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage3 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage4 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage5 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage6 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage7 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage8 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage9 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage10 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage11 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage12 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage13 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage14 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage15 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage16 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage17 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage18 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage19 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage20 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage21 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage22 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage23 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage24 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage25 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage26 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage27 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage28 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage29 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage30 = New System.Windows.Forms.Label()
        Me.LabelFunctionalMessage31 = New System.Windows.Forms.Label()
        Me.TabPageAnalog = New System.Windows.Forms.TabPage()
        Me.ComboBoxAnalogChannel = New System.Windows.Forms.ComboBox()
        Me.GroupBoxAnalog = New System.Windows.Forms.GroupBox()
        Me.PanelAnalog = New System.Windows.Forms.Panel()
        Me.LabelAnalogCH0 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH1 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH2 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH3 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH4 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH5 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH6 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH7 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH8 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH9 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH10 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH11 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH12 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH13 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH14 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH15 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH16 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH17 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH18 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH19 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH20 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH21 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH22 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH23 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH24 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH25 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH26 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH27 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH28 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH29 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH30 = New System.Windows.Forms.Label()
        Me.LabelAnalogCH31 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead0 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead1 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead2 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead3 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead4 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead5 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead6 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead7 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead8 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead9 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead10 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead11 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead12 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead13 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead14 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead15 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead16 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead17 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead18 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead19 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead20 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead21 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead22 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead23 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead24 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead25 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead26 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead27 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead28 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead29 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead30 = New System.Windows.Forms.Label()
        Me.LabelAnalogRead31 = New System.Windows.Forms.Label()
        Me.ProgressBarAnalog0 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog1 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog2 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog3 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog4 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog5 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog6 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog7 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog8 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog9 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog10 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog11 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog12 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog13 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog14 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog15 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog16 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog17 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog18 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog19 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog20 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog21 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog22 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog23 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog24 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog25 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog26 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog27 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog28 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog29 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog30 = New System.Windows.Forms.ProgressBar()
        Me.ProgressBarAnalog31 = New System.Windows.Forms.ProgressBar()
        Me.LabelAnalogLow1 = New System.Windows.Forms.Label()
        Me.LabelAnalogHigh1 = New System.Windows.Forms.Label()
        Me.LabelAnalogHigh0 = New System.Windows.Forms.Label()
        Me.LabelAnalogLow0 = New System.Windows.Forms.Label()
        Me.LabelAnalogReading = New System.Windows.Forms.Label()
        Me.GroupBoxBattVoltage = New System.Windows.Forms.GroupBox()
        Me.ButtonReadBatt = New System.Windows.Forms.Button()
        Me.LabelBattRead = New System.Windows.Forms.Label()
        Me.GroupBoxProgVoltage = New System.Windows.Forms.GroupBox()
        Me.GroupBoxVolt = New System.Windows.Forms.GroupBox()
        Me.LabelVoltWarning = New System.Windows.Forms.Label()
        Me.ButtonSetVoltage = New System.Windows.Forms.Button()
        Me.ButtonReadVolt = New System.Windows.Forms.Button()
        Me.TextBoxVoltSetting = New System.Windows.Forms.TextBox()
        Me.optSet = New System.Windows.Forms.RadioButton()
        Me.optShortToGround = New System.Windows.Forms.RadioButton()
        Me.optVoltOff = New System.Windows.Forms.RadioButton()
        Me.LabelVoltRead = New System.Windows.Forms.Label()
        Me.LabelMV = New System.Windows.Forms.Label()
        Me.GroupBoxPin = New System.Windows.Forms.GroupBox()
        Me.RadioButtonPin15 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin14 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin13 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin12 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin11 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin9 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin6 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonPin0 = New System.Windows.Forms.RadioButton()
        Me.LabelPin15 = New System.Windows.Forms.Label()
        Me.LabelPin14 = New System.Windows.Forms.Label()
        Me.LabelPin13 = New System.Windows.Forms.Label()
        Me.LabelPin12 = New System.Windows.Forms.Label()
        Me.LabelPin11 = New System.Windows.Forms.Label()
        Me.LabelPin9 = New System.Windows.Forms.Label()
        Me.LabelPin6 = New System.Windows.Forms.Label()
        Me.LabelPin0 = New System.Windows.Forms.Label()
        Me.LabelAnalogChannel = New System.Windows.Forms.Label()
        Me.LabelDeviceCombo7 = New System.Windows.Forms.Label()
        Me.TabPageResults = New System.Windows.Forms.TabPage()
        Me.ListViewResults = New System.Windows.Forms.ListView()
        Me.ListViewResultsColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ListViewResultsColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuStripResults = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MenuResultsCopyLine = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuResultsDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuResultsSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuResultsSpace1 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuResultsClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.DialogFileOpen = New System.Windows.Forms.OpenFileDialog()
        Me.DialogFileSave = New System.Windows.Forms.SaveFileDialog()
        Me.ContextMenuStripMessageIn = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ContextMenuStripMessageOut = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolTipMessageIn = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStripMain.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPageConnect.SuspendLayout()
        Me.GroupBoxDevices.SuspendLayout()
        Me.GroupBoxAPIs.SuspendLayout()
        Me.ContextMenuStripTextBox.SuspendLayout()
        Me.GroupBoxConnect.SuspendLayout()
        Me.ContextMenuStripFlags.SuspendLayout()
        Me.GroupBoxJ2534Info.SuspendLayout()
        Me.TabPageMessages.SuspendLayout()
        Me.ContextMenuStripScratchPad.SuspendLayout()
        Me.TabPagePeriodicMessages.SuspendLayout()
        Me.TabPageFilters.SuspendLayout()
        Me.TabPageConfig.SuspendLayout()
        Me.GroupBoxAnalogConfig.SuspendLayout()
        Me.PanelAudioChannel.SuspendLayout()
        Me.TabPageInit.SuspendLayout()
        Me.GroupBoxFastInit.SuspendLayout()
        Me.GroupBox5BInit.SuspendLayout()
        Me.TabPageFunctionalMessages.SuspendLayout()
        Me.TabPageAnalog.SuspendLayout()
        Me.GroupBoxAnalog.SuspendLayout()
        Me.PanelAnalog.SuspendLayout()
        Me.GroupBoxBattVoltage.SuspendLayout()
        Me.GroupBoxProgVoltage.SuspendLayout()
        Me.GroupBoxVolt.SuspendLayout()
        Me.GroupBoxPin.SuspendLayout()
        Me.TabPageResults.SuspendLayout()
        Me.ContextMenuStripResults.SuspendLayout()
        Me.ContextMenuStripMessageIn.SuspendLayout()
        Me.ContextMenuStripMessageOut.SuspendLayout()
        Me.SuspendLayout()
        '
        'ComboAvailableBoxLocator0
        '
        Me.ComboAvailableBoxLocator0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator0.Enabled = False
        Me.ComboAvailableBoxLocator0.Location = New System.Drawing.Point(16, 169)
        Me.ComboAvailableBoxLocator0.Name = "ComboAvailableBoxLocator0"
        Me.ComboAvailableBoxLocator0.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator0.TabIndex = 13
        Me.ComboAvailableBoxLocator0.Visible = False
        '
        'ComboAvailableChannelLocator0
        '
        Me.ComboAvailableChannelLocator0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator0.Enabled = False
        Me.ComboAvailableChannelLocator0.Location = New System.Drawing.Point(136, 169)
        Me.ComboAvailableChannelLocator0.Name = "ComboAvailableChannelLocator0"
        Me.ComboAvailableChannelLocator0.Size = New System.Drawing.Size(105, 21)
        Me.ComboAvailableChannelLocator0.TabIndex = 14
        Me.ComboAvailableChannelLocator0.Visible = False
        '
        'ComboAvailableBoxLocator1
        '
        Me.ComboAvailableBoxLocator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator1.Enabled = False
        Me.ComboAvailableBoxLocator1.Location = New System.Drawing.Point(8, 237)
        Me.ComboAvailableBoxLocator1.Name = "ComboAvailableBoxLocator1"
        Me.ComboAvailableBoxLocator1.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator1.TabIndex = 20
        Me.ComboAvailableBoxLocator1.Visible = False
        '
        'ComboAvailableChannelLocator1
        '
        Me.ComboAvailableChannelLocator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator1.Enabled = False
        Me.ComboAvailableChannelLocator1.Location = New System.Drawing.Point(134, 237)
        Me.ComboAvailableChannelLocator1.Name = "ComboAvailableChannelLocator1"
        Me.ComboAvailableChannelLocator1.Size = New System.Drawing.Size(105, 21)
        Me.ComboAvailableChannelLocator1.TabIndex = 2
        Me.ComboAvailableChannelLocator1.Visible = False
        '
        'ComboAvailableBoxLocator2
        '
        Me.ComboAvailableBoxLocator2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator2.Enabled = False
        Me.ComboAvailableBoxLocator2.Location = New System.Drawing.Point(398, 353)
        Me.ComboAvailableBoxLocator2.Name = "ComboAvailableBoxLocator2"
        Me.ComboAvailableBoxLocator2.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator2.TabIndex = 73
        Me.ComboAvailableBoxLocator2.Visible = False
        '
        'ComboAvailableChannelLocator2
        '
        Me.ComboAvailableChannelLocator2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator2.Enabled = False
        Me.ComboAvailableChannelLocator2.Location = New System.Drawing.Point(527, 353)
        Me.ComboAvailableChannelLocator2.Name = "ComboAvailableChannelLocator2"
        Me.ComboAvailableChannelLocator2.Size = New System.Drawing.Size(161, 21)
        Me.ComboAvailableChannelLocator2.TabIndex = 74
        Me.ComboAvailableChannelLocator2.Visible = False
        '
        'ComboAvailableBoxLocator3
        '
        Me.ComboAvailableBoxLocator3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator3.Enabled = False
        Me.ComboAvailableBoxLocator3.Location = New System.Drawing.Point(398, 353)
        Me.ComboAvailableBoxLocator3.Name = "ComboAvailableBoxLocator3"
        Me.ComboAvailableBoxLocator3.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator3.TabIndex = 54
        Me.ComboAvailableBoxLocator3.Visible = False
        '
        'ComboAvailableChannelLocator3
        '
        Me.ComboAvailableChannelLocator3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator3.Enabled = False
        Me.ComboAvailableChannelLocator3.Location = New System.Drawing.Point(527, 353)
        Me.ComboAvailableChannelLocator3.Name = "ComboAvailableChannelLocator3"
        Me.ComboAvailableChannelLocator3.Size = New System.Drawing.Size(161, 21)
        Me.ComboAvailableChannelLocator3.TabIndex = 55
        Me.ComboAvailableChannelLocator3.Visible = False
        '
        'ComboAvailableBoxLocator4
        '
        Me.ComboAvailableBoxLocator4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator4.Enabled = False
        Me.ComboAvailableBoxLocator4.Location = New System.Drawing.Point(398, 353)
        Me.ComboAvailableBoxLocator4.Name = "ComboAvailableBoxLocator4"
        Me.ComboAvailableBoxLocator4.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator4.TabIndex = 514
        Me.ComboAvailableBoxLocator4.Visible = False
        '
        'ComboAvailableChannelLocator4
        '
        Me.ComboAvailableChannelLocator4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator4.Enabled = False
        Me.ComboAvailableChannelLocator4.Location = New System.Drawing.Point(527, 353)
        Me.ComboAvailableChannelLocator4.Name = "ComboAvailableChannelLocator4"
        Me.ComboAvailableChannelLocator4.Size = New System.Drawing.Size(161, 21)
        Me.ComboAvailableChannelLocator4.TabIndex = 515
        Me.ComboAvailableChannelLocator4.Visible = False
        '
        'ComboAvailableBoxLocator5
        '
        Me.ComboAvailableBoxLocator5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator5.Enabled = False
        Me.ComboAvailableBoxLocator5.Location = New System.Drawing.Point(398, 353)
        Me.ComboAvailableBoxLocator5.Name = "ComboAvailableBoxLocator5"
        Me.ComboAvailableBoxLocator5.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator5.TabIndex = 2
        Me.ComboAvailableBoxLocator5.Visible = False
        '
        'ComboAvailableChannelLocator5
        '
        Me.ComboAvailableChannelLocator5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator5.Enabled = False
        Me.ComboAvailableChannelLocator5.Location = New System.Drawing.Point(527, 353)
        Me.ComboAvailableChannelLocator5.Name = "ComboAvailableChannelLocator5"
        Me.ComboAvailableChannelLocator5.Size = New System.Drawing.Size(161, 21)
        Me.ComboAvailableChannelLocator5.TabIndex = 3
        Me.ComboAvailableChannelLocator5.Visible = False
        '
        'ComboAvailableBoxLocator6
        '
        Me.ComboAvailableBoxLocator6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator6.Enabled = False
        Me.ComboAvailableBoxLocator6.Location = New System.Drawing.Point(398, 353)
        Me.ComboAvailableBoxLocator6.Name = "ComboAvailableBoxLocator6"
        Me.ComboAvailableBoxLocator6.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator6.TabIndex = 37
        Me.ComboAvailableBoxLocator6.Visible = False
        '
        'ComboAvailableChannelLocator6
        '
        Me.ComboAvailableChannelLocator6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator6.Enabled = False
        Me.ComboAvailableChannelLocator6.Location = New System.Drawing.Point(527, 353)
        Me.ComboAvailableChannelLocator6.Name = "ComboAvailableChannelLocator6"
        Me.ComboAvailableChannelLocator6.Size = New System.Drawing.Size(161, 21)
        Me.ComboAvailableChannelLocator6.TabIndex = 38
        Me.ComboAvailableChannelLocator6.Visible = False
        '
        'ComboAvailableBoxLocator7
        '
        Me.ComboAvailableBoxLocator7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableBoxLocator7.Enabled = False
        Me.ComboAvailableBoxLocator7.Location = New System.Drawing.Point(398, 353)
        Me.ComboAvailableBoxLocator7.Name = "ComboAvailableBoxLocator7"
        Me.ComboAvailableBoxLocator7.Size = New System.Drawing.Size(120, 21)
        Me.ComboAvailableBoxLocator7.TabIndex = 5
        Me.ComboAvailableBoxLocator7.Visible = False
        '
        'ComboAvailableChannelLocator7
        '
        Me.ComboAvailableChannelLocator7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ComboAvailableChannelLocator7.Enabled = False
        Me.ComboAvailableChannelLocator7.Location = New System.Drawing.Point(527, 353)
        Me.ComboAvailableChannelLocator7.Name = "ComboAvailableChannelLocator7"
        Me.ComboAvailableChannelLocator7.Size = New System.Drawing.Size(105, 21)
        Me.ComboAvailableChannelLocator7.TabIndex = 438
        Me.ComboAvailableChannelLocator7.Visible = False
        '
        'MenuStripMain
        '
        Me.MenuStripMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuFile, Me.MenuEdit, Me.MenuHelp})
        Me.MenuStripMain.Location = New System.Drawing.Point(0, 0)
        Me.MenuStripMain.Name = "MenuStripMain"
        Me.MenuStripMain.Size = New System.Drawing.Size(720, 24)
        Me.MenuStripMain.TabIndex = 3
        '
        'MenuFile
        '
        Me.MenuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuFileMsgOutLoad, Me.MenuFileFilterLoad, Me.MenuFilePeriodicMessageLoad, Me.MenuFileSpace1, Me.MenuFileMsgInSaveSelected, Me.MenuFileMsgInSaveAll, Me.MenuFileMsgOutSave, Me.MenuFileFilterSave, Me.MenuFilePeriodicMessageSave, Me.MenuFileSpace2, Me.MenuFileExit})
        Me.MenuFile.Name = "MenuFile"
        Me.MenuFile.Size = New System.Drawing.Size(37, 20)
        Me.MenuFile.Text = "&File"
        '
        'MenuFileMsgOutLoad
        '
        Me.MenuFileMsgOutLoad.Name = "MenuFileMsgOutLoad"
        Me.MenuFileMsgOutLoad.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileMsgOutLoad.Text = "Load Outgoing Message Set..."
        '
        'MenuFileFilterLoad
        '
        Me.MenuFileFilterLoad.Name = "MenuFileFilterLoad"
        Me.MenuFileFilterLoad.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileFilterLoad.Text = "Load Filters..."
        '
        'MenuFilePeriodicMessageLoad
        '
        Me.MenuFilePeriodicMessageLoad.Name = "MenuFilePeriodicMessageLoad"
        Me.MenuFilePeriodicMessageLoad.Size = New System.Drawing.Size(257, 22)
        Me.MenuFilePeriodicMessageLoad.Text = "Load Periodic Messages..."
        '
        'MenuFileSpace1
        '
        Me.MenuFileSpace1.Name = "MenuFileSpace1"
        Me.MenuFileSpace1.Size = New System.Drawing.Size(254, 6)
        '
        'MenuFileMsgInSaveSelected
        '
        Me.MenuFileMsgInSaveSelected.Name = "MenuFileMsgInSaveSelected"
        Me.MenuFileMsgInSaveSelected.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileMsgInSaveSelected.Text = "Save Selected Incoming Message..."
        '
        'MenuFileMsgInSaveAll
        '
        Me.MenuFileMsgInSaveAll.Name = "MenuFileMsgInSaveAll"
        Me.MenuFileMsgInSaveAll.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileMsgInSaveAll.Text = "Save All Incoming Messages..."
        '
        'MenuFileMsgOutSave
        '
        Me.MenuFileMsgOutSave.Name = "MenuFileMsgOutSave"
        Me.MenuFileMsgOutSave.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileMsgOutSave.Text = "Save Outgoing Message Set..."
        '
        'MenuFileFilterSave
        '
        Me.MenuFileFilterSave.Name = "MenuFileFilterSave"
        Me.MenuFileFilterSave.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileFilterSave.Text = "Save Filters..."
        '
        'MenuFilePeriodicMessageSave
        '
        Me.MenuFilePeriodicMessageSave.Name = "MenuFilePeriodicMessageSave"
        Me.MenuFilePeriodicMessageSave.Size = New System.Drawing.Size(257, 22)
        Me.MenuFilePeriodicMessageSave.Text = "Save Periodic Messages..."
        '
        'MenuFileSpace2
        '
        Me.MenuFileSpace2.Name = "MenuFileSpace2"
        Me.MenuFileSpace2.Size = New System.Drawing.Size(254, 6)
        '
        'MenuFileExit
        '
        Me.MenuFileExit.Name = "MenuFileExit"
        Me.MenuFileExit.Size = New System.Drawing.Size(257, 22)
        Me.MenuFileExit.Text = "E&xit"
        '
        'MenuEdit
        '
        Me.MenuEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuEditUndo, Me.MenuEditSpace1, Me.MenuEditCut, Me.MenuEditCopy, Me.MenuEditCopyLine, Me.MenuEditMsgInCopyData, Me.MenuEditPaste, Me.MenuEditDelete, Me.MenuEditSpace2, Me.MenuEditMsgInMakeFilter, Me.MenuEditFlagsEdit, Me.MenuEditRxStatus, Me.MenuEditFlagsClear, Me.MenuEditFlagsSetDefault, Me.MenuEditMsgOutEditMessage, Me.MenuEditMsgOutAddMessage, Me.MenuEditMsgOutCopyToScratchPad, Me.MenuEditScratchAddToOutgoingMessageSet, Me.MenuEditMsgOutMakePeriodicMessage, Me.MenuEditSelectAll, Me.MenuEditClear})
        Me.MenuEdit.Name = "MenuEdit"
        Me.MenuEdit.Size = New System.Drawing.Size(39, 20)
        Me.MenuEdit.Text = "&Edit"
        '
        'MenuEditUndo
        '
        Me.MenuEditUndo.Name = "MenuEditUndo"
        Me.MenuEditUndo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.MenuEditUndo.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditUndo.Text = "&Undo"
        '
        'MenuEditSpace1
        '
        Me.MenuEditSpace1.Name = "MenuEditSpace1"
        Me.MenuEditSpace1.Size = New System.Drawing.Size(224, 6)
        '
        'MenuEditCut
        '
        Me.MenuEditCut.Name = "MenuEditCut"
        Me.MenuEditCut.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.MenuEditCut.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditCut.Text = "Cu&t"
        '
        'MenuEditCopy
        '
        Me.MenuEditCopy.Name = "MenuEditCopy"
        Me.MenuEditCopy.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.MenuEditCopy.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditCopy.Text = "&Copy"
        '
        'MenuEditCopyLine
        '
        Me.MenuEditCopyLine.Name = "MenuEditCopyLine"
        Me.MenuEditCopyLine.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditCopyLine.Text = "Copy Line"
        '
        'MenuEditMsgInCopyData
        '
        Me.MenuEditMsgInCopyData.Name = "MenuEditMsgInCopyData"
        Me.MenuEditMsgInCopyData.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditMsgInCopyData.Text = "Copy Data"
        '
        'MenuEditPaste
        '
        Me.MenuEditPaste.Name = "MenuEditPaste"
        Me.MenuEditPaste.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.MenuEditPaste.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditPaste.Text = "&Paste"
        '
        'MenuEditDelete
        '
        Me.MenuEditDelete.Name = "MenuEditDelete"
        Me.MenuEditDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.MenuEditDelete.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditDelete.Text = "&Delete"
        '
        'MenuEditSpace2
        '
        Me.MenuEditSpace2.Name = "MenuEditSpace2"
        Me.MenuEditSpace2.Size = New System.Drawing.Size(224, 6)
        '
        'MenuEditMsgInMakeFilter
        '
        Me.MenuEditMsgInMakeFilter.Name = "MenuEditMsgInMakeFilter"
        Me.MenuEditMsgInMakeFilter.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditMsgInMakeFilter.Text = "Make Filter"
        '
        'MenuEditFlagsEdit
        '
        Me.MenuEditFlagsEdit.Name = "MenuEditFlagsEdit"
        Me.MenuEditFlagsEdit.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditFlagsEdit.Text = "Edit Flags"
        '
        'MenuEditRxStatus
        '
        Me.MenuEditRxStatus.Name = "MenuEditRxStatus"
        Me.MenuEditRxStatus.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditRxStatus.Text = "Rx Status"
        '
        'MenuEditFlagsClear
        '
        Me.MenuEditFlagsClear.Name = "MenuEditFlagsClear"
        Me.MenuEditFlagsClear.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditFlagsClear.Text = "Clear Flags"
        '
        'MenuEditFlagsSetDefault
        '
        Me.MenuEditFlagsSetDefault.Name = "MenuEditFlagsSetDefault"
        Me.MenuEditFlagsSetDefault.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditFlagsSetDefault.Text = "Set Default Flags"
        '
        'MenuEditMsgOutEditMessage
        '
        Me.MenuEditMsgOutEditMessage.Name = "MenuEditMsgOutEditMessage"
        Me.MenuEditMsgOutEditMessage.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditMsgOutEditMessage.Text = "Edit Message"
        '
        'MenuEditMsgOutAddMessage
        '
        Me.MenuEditMsgOutAddMessage.Name = "MenuEditMsgOutAddMessage"
        Me.MenuEditMsgOutAddMessage.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditMsgOutAddMessage.Text = "Add Message"
        '
        'MenuEditMsgOutCopyToScratchPad
        '
        Me.MenuEditMsgOutCopyToScratchPad.Name = "MenuEditMsgOutCopyToScratchPad"
        Me.MenuEditMsgOutCopyToScratchPad.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditMsgOutCopyToScratchPad.Text = "Copy to Scratch Pad"
        '
        'MenuEditScratchAddToOutgoingMessageSet
        '
        Me.MenuEditScratchAddToOutgoingMessageSet.Name = "MenuEditScratchAddToOutgoingMessageSet"
        Me.MenuEditScratchAddToOutgoingMessageSet.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditScratchAddToOutgoingMessageSet.Text = "Add to Outgoing Mesage Set"
        '
        'MenuEditMsgOutMakePeriodicMessage
        '
        Me.MenuEditMsgOutMakePeriodicMessage.Name = "MenuEditMsgOutMakePeriodicMessage"
        Me.MenuEditMsgOutMakePeriodicMessage.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditMsgOutMakePeriodicMessage.Text = "Make Periodic Message"
        '
        'MenuEditSelectAll
        '
        Me.MenuEditSelectAll.Name = "MenuEditSelectAll"
        Me.MenuEditSelectAll.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.MenuEditSelectAll.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditSelectAll.Text = "Select &All"
        '
        'MenuEditClear
        '
        Me.MenuEditClear.Name = "MenuEditClear"
        Me.MenuEditClear.Size = New System.Drawing.Size(227, 22)
        Me.MenuEditClear.Text = "Clear"
        '
        'MenuHelp
        '
        Me.MenuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuHelpContents, Me.MenuHelpAbout})
        Me.MenuHelp.Name = "MenuHelp"
        Me.MenuHelp.Size = New System.Drawing.Size(44, 20)
        Me.MenuHelp.Text = "Help"
        '
        'MenuHelpContents
        '
        Me.MenuHelpContents.Enabled = False
        Me.MenuHelpContents.Name = "MenuHelpContents"
        Me.MenuHelpContents.Size = New System.Drawing.Size(131, 22)
        Me.MenuHelpContents.Text = "Contents..."
        '
        'MenuHelpAbout
        '
        Me.MenuHelpAbout.Name = "MenuHelpAbout"
        Me.MenuHelpAbout.Size = New System.Drawing.Size(131, 22)
        Me.MenuHelpAbout.Text = "About..."
        '
        'MenuMsgInCopyLine
        '
        Me.MenuMsgInCopyLine.Enabled = False
        Me.MenuMsgInCopyLine.Name = "MenuMsgInCopyLine"
        Me.MenuMsgInCopyLine.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInCopyLine.Text = "Copy Line"
        '
        'MenuMsgInCopyData
        '
        Me.MenuMsgInCopyData.Enabled = False
        Me.MenuMsgInCopyData.Name = "MenuMsgInCopyData"
        Me.MenuMsgInCopyData.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInCopyData.Text = "Copy Data"
        '
        'MenuMsgInMakeFilter
        '
        Me.MenuMsgInMakeFilter.Enabled = False
        Me.MenuMsgInMakeFilter.Name = "MenuMsgInMakeFilter"
        Me.MenuMsgInMakeFilter.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInMakeFilter.Text = "Make Filter"
        '
        'MenuMsgInRxStatus
        '
        Me.MenuMsgInRxStatus.Name = "MenuMsgInRxStatus"
        Me.MenuMsgInRxStatus.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInRxStatus.Text = "Rx Status"
        '
        'MenuMsgInSelectAll
        '
        Me.MenuMsgInSelectAll.Enabled = False
        Me.MenuMsgInSelectAll.Name = "MenuMsgInSelectAll"
        Me.MenuMsgInSelectAll.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInSelectAll.Text = "Select All"
        '
        'MenuMsgInSpace1
        '
        Me.MenuMsgInSpace1.Name = "MenuMsgInSpace1"
        Me.MenuMsgInSpace1.Size = New System.Drawing.Size(142, 6)
        '
        'MenuMsgInSaveSelected
        '
        Me.MenuMsgInSaveSelected.Enabled = False
        Me.MenuMsgInSaveSelected.Name = "MenuMsgInSaveSelected"
        Me.MenuMsgInSaveSelected.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInSaveSelected.Text = "Save Selected"
        '
        'MenuMsgInSaveAll
        '
        Me.MenuMsgInSaveAll.Enabled = False
        Me.MenuMsgInSaveAll.Name = "MenuMsgInSaveAll"
        Me.MenuMsgInSaveAll.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInSaveAll.Text = "Save All"
        '
        'MenuMsgInClear
        '
        Me.MenuMsgInClear.Enabled = False
        Me.MenuMsgInClear.Name = "MenuMsgInClear"
        Me.MenuMsgInClear.Size = New System.Drawing.Size(145, 22)
        Me.MenuMsgInClear.Text = "Clear"
        '
        'MenuMsgOutDelete
        '
        Me.MenuMsgOutDelete.Enabled = False
        Me.MenuMsgOutDelete.Name = "MenuMsgOutDelete"
        Me.MenuMsgOutDelete.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutDelete.Text = "Delete"
        '
        'MenuMsgOutEditMessage
        '
        Me.MenuMsgOutEditMessage.Enabled = False
        Me.MenuMsgOutEditMessage.Name = "MenuMsgOutEditMessage"
        Me.MenuMsgOutEditMessage.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutEditMessage.Text = "Edit Message"
        '
        'MenuMsgOutAddMessage
        '
        Me.MenuMsgOutAddMessage.Enabled = False
        Me.MenuMsgOutAddMessage.Name = "MenuMsgOutAddMessage"
        Me.MenuMsgOutAddMessage.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutAddMessage.Text = "Add Message"
        '
        'MenuMsgOutCopyToScratchPad
        '
        Me.MenuMsgOutCopyToScratchPad.Name = "MenuMsgOutCopyToScratchPad"
        Me.MenuMsgOutCopyToScratchPad.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutCopyToScratchPad.Text = "Copy to Scratch Pad"
        '
        'MenuMsgOutMakePeriodicMessage
        '
        Me.MenuMsgOutMakePeriodicMessage.Name = "MenuMsgOutMakePeriodicMessage"
        Me.MenuMsgOutMakePeriodicMessage.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutMakePeriodicMessage.Text = "Make Periodic Message"
        '
        'MenuMsgOutSpace1
        '
        Me.MenuMsgOutSpace1.Name = "MenuMsgOutSpace1"
        Me.MenuMsgOutSpace1.Size = New System.Drawing.Size(195, 6)
        '
        'MenuMsgOutSave
        '
        Me.MenuMsgOutSave.Enabled = False
        Me.MenuMsgOutSave.Name = "MenuMsgOutSave"
        Me.MenuMsgOutSave.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutSave.Text = "Save"
        '
        'MenuMsgOutLoad
        '
        Me.MenuMsgOutLoad.Enabled = False
        Me.MenuMsgOutLoad.Name = "MenuMsgOutLoad"
        Me.MenuMsgOutLoad.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutLoad.Text = "Load"
        '
        'MenuMsgOutClear
        '
        Me.MenuMsgOutClear.Enabled = False
        Me.MenuMsgOutClear.Name = "MenuMsgOutClear"
        Me.MenuMsgOutClear.Size = New System.Drawing.Size(198, 22)
        Me.MenuMsgOutClear.Text = "Clear"
        '
        'MenuFlagsUndo
        '
        Me.MenuFlagsUndo.Enabled = False
        Me.MenuFlagsUndo.Name = "MenuFlagsUndo"
        Me.MenuFlagsUndo.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsUndo.Text = "Undo"
        '
        'MenuFlagsSpace1
        '
        Me.MenuFlagsSpace1.Name = "MenuFlagsSpace1"
        Me.MenuFlagsSpace1.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsCut
        '
        Me.MenuFlagsCut.Enabled = False
        Me.MenuFlagsCut.Name = "MenuFlagsCut"
        Me.MenuFlagsCut.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsCut.Text = "Cut"
        '
        'MenuFlagsCopy
        '
        Me.MenuFlagsCopy.Enabled = False
        Me.MenuFlagsCopy.Name = "MenuFlagsCopy"
        Me.MenuFlagsCopy.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsCopy.Text = "Copy"
        '
        'MenuFlagsPaste
        '
        Me.MenuFlagsPaste.Enabled = False
        Me.MenuFlagsPaste.Name = "MenuFlagsPaste"
        Me.MenuFlagsPaste.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsPaste.Text = "Paste"
        '
        'MenuFlagsSpace2
        '
        Me.MenuFlagsSpace2.Name = "MenuFlagsSpace2"
        Me.MenuFlagsSpace2.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsSelectAll
        '
        Me.MenuFlagsSelectAll.Name = "MenuFlagsSelectAll"
        Me.MenuFlagsSelectAll.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsSelectAll.Text = "Select All"
        '
        'MenuFlagsSpace3
        '
        Me.MenuFlagsSpace3.Name = "MenuFlagsSpace3"
        Me.MenuFlagsSpace3.Size = New System.Drawing.Size(158, 6)
        '
        'MenuFlagsEdit
        '
        Me.MenuFlagsEdit.Name = "MenuFlagsEdit"
        Me.MenuFlagsEdit.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsEdit.Text = "Edit Flags"
        '
        'MenuFlagsClear
        '
        Me.MenuFlagsClear.Name = "MenuFlagsClear"
        Me.MenuFlagsClear.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsClear.Text = "Clear Flags"
        '
        'MenuFlagsSetDefault
        '
        Me.MenuFlagsSetDefault.Name = "MenuFlagsSetDefault"
        Me.MenuFlagsSetDefault.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsSetDefault.Text = "Set Default Flags"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 441)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.ShowItemToolTips = True
        Me.StatusStrip1.Size = New System.Drawing.Size(720, 25)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 1
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.AutoSize = False
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me.ToolStripStatusLabel1.Margin = New System.Windows.Forms.Padding(0)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(200, 25)
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.AutoSize = False
        Me.ToolStripStatusLabel2.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me.ToolStripStatusLabel2.Margin = New System.Windows.Forms.Padding(0)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(500, 25)
        Me.ToolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPageConnect)
        Me.TabControl1.Controls.Add(Me.TabPageMessages)
        Me.TabControl1.Controls.Add(Me.TabPagePeriodicMessages)
        Me.TabControl1.Controls.Add(Me.TabPageFilters)
        Me.TabControl1.Controls.Add(Me.TabPageConfig)
        Me.TabControl1.Controls.Add(Me.TabPageInit)
        Me.TabControl1.Controls.Add(Me.TabPageFunctionalMessages)
        Me.TabControl1.Controls.Add(Me.TabPageAnalog)
        Me.TabControl1.Controls.Add(Me.TabPageResults)
        Me.TabControl1.ItemSize = New System.Drawing.Size(47, 18)
        Me.TabControl1.Location = New System.Drawing.Point(0, 24)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(722, 421)
        Me.TabControl1.TabIndex = 0
        '
        'TabPageConnect
        '
        Me.TabPageConnect.Controls.Add(Me.GroupBoxDevices)
        Me.TabPageConnect.Controls.Add(Me.GroupBoxAPIs)
        Me.TabPageConnect.Controls.Add(Me.GroupBoxConnect)
        Me.TabPageConnect.Controls.Add(Me.GroupBoxJ2534Info)
        Me.TabPageConnect.Location = New System.Drawing.Point(4, 22)
        Me.TabPageConnect.Margin = New System.Windows.Forms.Padding(0)
        Me.TabPageConnect.Name = "TabPageConnect"
        Me.TabPageConnect.Size = New System.Drawing.Size(714, 395)
        Me.TabPageConnect.TabIndex = 0
        Me.TabPageConnect.Text = "Connect"
        '
        'GroupBoxDevices
        '
        Me.GroupBoxDevices.Controls.Add(Me.ComboBoxDevice)
        Me.GroupBoxDevices.Controls.Add(Me.ButtonDiscover)
        Me.GroupBoxDevices.Controls.Add(Me.ButtonCloseBox)
        Me.GroupBoxDevices.Controls.Add(Me.ButtonOpenBox)
        Me.GroupBoxDevices.Controls.Add(Me.LabelDeviceInfo)
        Me.GroupBoxDevices.Controls.Add(Me.LabelComboDevice)
        Me.GroupBoxDevices.Location = New System.Drawing.Point(428, 4)
        Me.GroupBoxDevices.Name = "GroupBoxDevices"
        Me.GroupBoxDevices.Size = New System.Drawing.Size(281, 177)
        Me.GroupBoxDevices.TabIndex = 1
        Me.GroupBoxDevices.TabStop = False
        Me.GroupBoxDevices.Text = "J2534 Devices:"
        '
        'ComboBoxDevice
        '
        Me.ComboBoxDevice.Enabled = False
        Me.ComboBoxDevice.Location = New System.Drawing.Point(16, 32)
        Me.ComboBoxDevice.Name = "ComboBoxDevice"
        Me.ComboBoxDevice.Size = New System.Drawing.Size(209, 21)
        Me.ComboBoxDevice.TabIndex = 3
        '
        'ButtonDiscover
        '
        Me.ButtonDiscover.Enabled = False
        Me.ButtonDiscover.Location = New System.Drawing.Point(16, 64)
        Me.ButtonDiscover.Name = "ButtonDiscover"
        Me.ButtonDiscover.Size = New System.Drawing.Size(73, 25)
        Me.ButtonDiscover.TabIndex = 0
        Me.ButtonDiscover.Text = "Discover"
        Me.ButtonDiscover.UseVisualStyleBackColor = True
        Me.ButtonDiscover.Visible = False
        '
        'ButtonCloseBox
        '
        Me.ButtonCloseBox.Location = New System.Drawing.Point(168, 64)
        Me.ButtonCloseBox.Name = "ButtonCloseBox"
        Me.ButtonCloseBox.Size = New System.Drawing.Size(57, 25)
        Me.ButtonCloseBox.TabIndex = 2
        Me.ButtonCloseBox.Text = "Close"
        Me.ButtonCloseBox.UseVisualStyleBackColor = True
        '
        'ButtonOpenBox
        '
        Me.ButtonOpenBox.Location = New System.Drawing.Point(104, 64)
        Me.ButtonOpenBox.Name = "ButtonOpenBox"
        Me.ButtonOpenBox.Size = New System.Drawing.Size(57, 25)
        Me.ButtonOpenBox.TabIndex = 1
        Me.ButtonOpenBox.Text = "Open"
        Me.ButtonOpenBox.UseVisualStyleBackColor = True
        '
        'LabelDeviceInfo
        '
        Me.LabelDeviceInfo.Location = New System.Drawing.Point(16, 104)
        Me.LabelDeviceInfo.Name = "LabelDeviceInfo"
        Me.LabelDeviceInfo.Size = New System.Drawing.Size(225, 65)
        Me.LabelDeviceInfo.TabIndex = 5
        Me.LabelDeviceInfo.Text = "LabelDeviceInfo"
        '
        'LabelComboDevice
        '
        Me.LabelComboDevice.AutoSize = True
        Me.LabelComboDevice.Location = New System.Drawing.Point(16, 16)
        Me.LabelComboDevice.Name = "LabelComboDevice"
        Me.LabelComboDevice.Size = New System.Drawing.Size(44, 13)
        Me.LabelComboDevice.TabIndex = 4
        Me.LabelComboDevice.Text = "Device:"
        '
        'GroupBoxAPIs
        '
        Me.GroupBoxAPIs.Controls.Add(Me.ComboBoxAPI)
        Me.GroupBoxAPIs.Controls.Add(Me.ButtonLoadDLL)
        Me.GroupBoxAPIs.Controls.Add(Me.TextBoxDllPath)
        Me.GroupBoxAPIs.Controls.Add(Me.LabelAPI)
        Me.GroupBoxAPIs.Controls.Add(Me.LabelVendor)
        Me.GroupBoxAPIs.Controls.Add(Me.LabelDevice)
        Me.GroupBoxAPIs.Controls.Add(Me.LabelDllPath)
        Me.GroupBoxAPIs.Controls.Add(Me.LabelDllName)
        Me.GroupBoxAPIs.Location = New System.Drawing.Point(4, 4)
        Me.GroupBoxAPIs.Name = "GroupBoxAPIs"
        Me.GroupBoxAPIs.Size = New System.Drawing.Size(409, 177)
        Me.GroupBoxAPIs.TabIndex = 0
        Me.GroupBoxAPIs.TabStop = False
        Me.GroupBoxAPIs.Text = "J2534 APIs:"
        '
        'ComboBoxAPI
        '
        Me.ComboBoxAPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxAPI.Location = New System.Drawing.Point(10, 32)
        Me.ComboBoxAPI.Name = "ComboBoxAPI"
        Me.ComboBoxAPI.Size = New System.Drawing.Size(321, 21)
        Me.ComboBoxAPI.TabIndex = 0
        '
        'ButtonLoadDLL
        '
        Me.ButtonLoadDLL.Location = New System.Drawing.Point(336, 69)
        Me.ButtonLoadDLL.Name = "ButtonLoadDLL"
        Me.ButtonLoadDLL.Size = New System.Drawing.Size(65, 25)
        Me.ButtonLoadDLL.TabIndex = 2
        Me.ButtonLoadDLL.Text = "Load DLL"
        Me.ButtonLoadDLL.UseVisualStyleBackColor = True
        '
        'TextBoxDllPath
        '
        Me.TextBoxDllPath.AcceptsReturn = True
        Me.TextBoxDllPath.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxDllPath.Enabled = False
        Me.TextBoxDllPath.Location = New System.Drawing.Point(10, 72)
        Me.TextBoxDllPath.MaxLength = 0
        Me.TextBoxDllPath.Name = "TextBoxDllPath"
        Me.TextBoxDllPath.Size = New System.Drawing.Size(321, 20)
        Me.TextBoxDllPath.TabIndex = 1
        '
        'ContextMenuStripTextBox
        '
        Me.ContextMenuStripTextBox.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuTextBoxUndo, Me.MenuTextBoxSeparator1, Me.MenuTextBoxCut, Me.MenuTextBoxCopy, Me.MenuTextBoxPaste, Me.MenuTextBoxDelete, Me.MenuTextBoxSeparator2, Me.MenuTextBoxSelectAll})
        Me.ContextMenuStripTextBox.Name = "ContextMenuStripTextBox"
        Me.ContextMenuStripTextBox.Size = New System.Drawing.Size(123, 148)
        '
        'MenuTextBoxUndo
        '
        Me.MenuTextBoxUndo.Name = "MenuTextBoxUndo"
        Me.MenuTextBoxUndo.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxUndo.Text = "Undo"
        '
        'MenuTextBoxSeparator1
        '
        Me.MenuTextBoxSeparator1.Name = "MenuTextBoxSeparator1"
        Me.MenuTextBoxSeparator1.Size = New System.Drawing.Size(119, 6)
        '
        'MenuTextBoxCut
        '
        Me.MenuTextBoxCut.Name = "MenuTextBoxCut"
        Me.MenuTextBoxCut.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxCut.Text = "Cut"
        '
        'MenuTextBoxCopy
        '
        Me.MenuTextBoxCopy.Name = "MenuTextBoxCopy"
        Me.MenuTextBoxCopy.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxCopy.Text = "Copy"
        '
        'MenuTextBoxPaste
        '
        Me.MenuTextBoxPaste.Name = "MenuTextBoxPaste"
        Me.MenuTextBoxPaste.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxPaste.Text = "Paste"
        '
        'MenuTextBoxDelete
        '
        Me.MenuTextBoxDelete.Name = "MenuTextBoxDelete"
        Me.MenuTextBoxDelete.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxDelete.Text = "Delete"
        '
        'MenuTextBoxSeparator2
        '
        Me.MenuTextBoxSeparator2.Name = "MenuTextBoxSeparator2"
        Me.MenuTextBoxSeparator2.Size = New System.Drawing.Size(119, 6)
        '
        'MenuTextBoxSelectAll
        '
        Me.MenuTextBoxSelectAll.Name = "MenuTextBoxSelectAll"
        Me.MenuTextBoxSelectAll.Size = New System.Drawing.Size(122, 22)
        Me.MenuTextBoxSelectAll.Text = "Select All"
        '
        'LabelAPI
        '
        Me.LabelAPI.AutoSize = True
        Me.LabelAPI.Location = New System.Drawing.Point(10, 16)
        Me.LabelAPI.Name = "LabelAPI"
        Me.LabelAPI.Size = New System.Drawing.Size(34, 13)
        Me.LabelAPI.TabIndex = 3
        Me.LabelAPI.Text = "Type:"
        '
        'LabelVendor
        '
        Me.LabelVendor.Location = New System.Drawing.Point(10, 96)
        Me.LabelVendor.Name = "LabelVendor"
        Me.LabelVendor.Size = New System.Drawing.Size(393, 17)
        Me.LabelVendor.TabIndex = 5
        Me.LabelVendor.Text = "LabelVendor"
        '
        'LabelDevice
        '
        Me.LabelDevice.Location = New System.Drawing.Point(10, 112)
        Me.LabelDevice.Name = "LabelDevice"
        Me.LabelDevice.Size = New System.Drawing.Size(393, 17)
        Me.LabelDevice.TabIndex = 6
        Me.LabelDevice.Text = "LabelDevice"
        '
        'LabelDllPath
        '
        Me.LabelDllPath.AutoSize = True
        Me.LabelDllPath.Location = New System.Drawing.Point(10, 56)
        Me.LabelDllPath.Name = "LabelDllPath"
        Me.LabelDllPath.Size = New System.Drawing.Size(67, 13)
        Me.LabelDllPath.TabIndex = 4
        Me.LabelDllPath.Text = "Path to DLL:"
        '
        'LabelDllName
        '
        Me.LabelDllName.Location = New System.Drawing.Point(10, 128)
        Me.LabelDllName.Name = "LabelDllName"
        Me.LabelDllName.Size = New System.Drawing.Size(393, 17)
        Me.LabelDllName.TabIndex = 7
        Me.LabelDllName.Text = "LabelDllName"
        '
        'GroupBoxConnect
        '
        Me.GroupBoxConnect.Controls.Add(Me.ComboBoxPins)
        Me.GroupBoxConnect.Controls.Add(Me.ComboBoxConnector)
        Me.GroupBoxConnect.Controls.Add(Me.CheckBoxMixedMode)
        Me.GroupBoxConnect.Controls.Add(Me.TextBoxConnectFlags)
        Me.GroupBoxConnect.Controls.Add(Me.ComboBoxBaudRate)
        Me.GroupBoxConnect.Controls.Add(Me.ButtonConnect)
        Me.GroupBoxConnect.Controls.Add(Me.ButtonDisconnect)
        Me.GroupBoxConnect.Controls.Add(Me.ComboBoxConnectChannel)
        Me.GroupBoxConnect.Controls.Add(Me.LabelPins)
        Me.GroupBoxConnect.Controls.Add(Me.LabelConn)
        Me.GroupBoxConnect.Controls.Add(Me.LabelConnectFlags)
        Me.GroupBoxConnect.Controls.Add(Me.LabelDeviceCombo0)
        Me.GroupBoxConnect.Controls.Add(Me.LabelBaud)
        Me.GroupBoxConnect.Controls.Add(Me.LabelConnectChannel)
        Me.GroupBoxConnect.Controls.Add(Me.ComboAvailableBoxLocator0)
        Me.GroupBoxConnect.Controls.Add(Me.ComboAvailableChannelLocator0)
        Me.GroupBoxConnect.Location = New System.Drawing.Point(428, 188)
        Me.GroupBoxConnect.Name = "GroupBoxConnect"
        Me.GroupBoxConnect.Size = New System.Drawing.Size(281, 201)
        Me.GroupBoxConnect.TabIndex = 3
        Me.GroupBoxConnect.TabStop = False
        Me.GroupBoxConnect.Text = "Connect:"
        '
        'ComboBoxPins
        '
        Me.ComboBoxPins.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPins.Location = New System.Drawing.Point(96, 88)
        Me.ComboBoxPins.Name = "ComboBoxPins"
        Me.ComboBoxPins.Size = New System.Drawing.Size(81, 21)
        Me.ComboBoxPins.TabIndex = 4
        '
        'ComboBoxConnector
        '
        Me.ComboBoxConnector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxConnector.Location = New System.Drawing.Point(16, 88)
        Me.ComboBoxConnector.Name = "ComboBoxConnector"
        Me.ComboBoxConnector.Size = New System.Drawing.Size(73, 21)
        Me.ComboBoxConnector.TabIndex = 3
        '
        'CheckBoxMixedMode
        '
        Me.CheckBoxMixedMode.Location = New System.Drawing.Point(24, 56)
        Me.CheckBoxMixedMode.Name = "CheckBoxMixedMode"
        Me.CheckBoxMixedMode.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxMixedMode.TabIndex = 8
        Me.CheckBoxMixedMode.Text = "Mixed Mode"
        Me.CheckBoxMixedMode.UseVisualStyleBackColor = True
        '
        'TextBoxConnectFlags
        '
        Me.TextBoxConnectFlags.AcceptsReturn = True
        Me.TextBoxConnectFlags.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxConnectFlags.Location = New System.Drawing.Point(184, 128)
        Me.TextBoxConnectFlags.MaxLength = 0
        Me.TextBoxConnectFlags.Name = "TextBoxConnectFlags"
        Me.TextBoxConnectFlags.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxConnectFlags.TabIndex = 6
        '
        'ContextMenuStripFlags
        '
        Me.ContextMenuStripFlags.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuFlagsUndo, Me.MenuFlagsSpace1, Me.MenuFlagsCut, Me.MenuFlagsCopy, Me.MenuFlagsPaste, Me.MenuFlagsDelete, Me.MenuFlagsSpace2, Me.MenuFlagsSelectAll, Me.MenuFlagsSpace3, Me.MenuFlagsEdit, Me.MenuFlagsClear, Me.MenuFlagsSetDefault})
        Me.ContextMenuStripFlags.Name = "ContextMenuStripFlags"
        Me.ContextMenuStripFlags.Size = New System.Drawing.Size(162, 220)
        '
        'MenuFlagsDelete
        '
        Me.MenuFlagsDelete.Name = "MenuFlagsDelete"
        Me.MenuFlagsDelete.Size = New System.Drawing.Size(161, 22)
        Me.MenuFlagsDelete.Text = "Delete"
        '
        'ComboBoxBaudRate
        '
        Me.ComboBoxBaudRate.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.ComboBoxBaudRate.Location = New System.Drawing.Point(16, 128)
        Me.ComboBoxBaudRate.Name = "ComboBoxBaudRate"
        Me.ComboBoxBaudRate.Size = New System.Drawing.Size(161, 21)
        Me.ComboBoxBaudRate.TabIndex = 5
        '
        'ButtonConnect
        '
        Me.ButtonConnect.Location = New System.Drawing.Point(184, 32)
        Me.ButtonConnect.Name = "ButtonConnect"
        Me.ButtonConnect.Size = New System.Drawing.Size(81, 25)
        Me.ButtonConnect.TabIndex = 1
        Me.ButtonConnect.Text = "Connect"
        Me.ButtonConnect.UseVisualStyleBackColor = True
        '
        'ButtonDisconnect
        '
        Me.ButtonDisconnect.Location = New System.Drawing.Point(184, 64)
        Me.ButtonDisconnect.Name = "ButtonDisconnect"
        Me.ButtonDisconnect.Size = New System.Drawing.Size(81, 25)
        Me.ButtonDisconnect.TabIndex = 2
        Me.ButtonDisconnect.Text = "Disconnect"
        Me.ButtonDisconnect.UseVisualStyleBackColor = True
        '
        'ComboBoxConnectChannel
        '
        Me.ComboBoxConnectChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxConnectChannel.Location = New System.Drawing.Point(16, 32)
        Me.ComboBoxConnectChannel.Name = "ComboBoxConnectChannel"
        Me.ComboBoxConnectChannel.Size = New System.Drawing.Size(161, 21)
        Me.ComboBoxConnectChannel.TabIndex = 0
        '
        'LabelPins
        '
        Me.LabelPins.AutoSize = True
        Me.LabelPins.Location = New System.Drawing.Point(96, 72)
        Me.LabelPins.Name = "LabelPins"
        Me.LabelPins.Size = New System.Drawing.Size(30, 13)
        Me.LabelPins.TabIndex = 10
        Me.LabelPins.Text = "Pins:"
        '
        'LabelConn
        '
        Me.LabelConn.AutoSize = True
        Me.LabelConn.Location = New System.Drawing.Point(16, 72)
        Me.LabelConn.Name = "LabelConn"
        Me.LabelConn.Size = New System.Drawing.Size(59, 13)
        Me.LabelConn.TabIndex = 9
        Me.LabelConn.Text = "Connector:"
        '
        'LabelConnectFlags
        '
        Me.LabelConnectFlags.AutoSize = True
        Me.LabelConnectFlags.Location = New System.Drawing.Point(184, 112)
        Me.LabelConnectFlags.Name = "LabelConnectFlags"
        Me.LabelConnectFlags.Size = New System.Drawing.Size(78, 13)
        Me.LabelConnectFlags.TabIndex = 15
        Me.LabelConnectFlags.Text = "Connect Flags:"
        '
        'LabelDeviceCombo0
        '
        Me.LabelDeviceCombo0.Location = New System.Drawing.Point(16, 152)
        Me.LabelDeviceCombo0.Name = "LabelDeviceCombo0"
        Me.LabelDeviceCombo0.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo0.TabIndex = 12
        Me.LabelDeviceCombo0.Text = "Device:"
        '
        'LabelBaud
        '
        Me.LabelBaud.AutoSize = True
        Me.LabelBaud.Location = New System.Drawing.Point(16, 112)
        Me.LabelBaud.Name = "LabelBaud"
        Me.LabelBaud.Size = New System.Drawing.Size(61, 13)
        Me.LabelBaud.TabIndex = 11
        Me.LabelBaud.Text = "Baud Rate:"
        '
        'LabelConnectChannel
        '
        Me.LabelConnectChannel.Location = New System.Drawing.Point(16, 16)
        Me.LabelConnectChannel.Name = "LabelConnectChannel"
        Me.LabelConnectChannel.Size = New System.Drawing.Size(49, 17)
        Me.LabelConnectChannel.TabIndex = 7
        Me.LabelConnectChannel.Text = "Channel:"
        '
        'GroupBoxJ2534Info
        '
        Me.GroupBoxJ2534Info.Controls.Add(Me.LabelProtSupport)
        Me.GroupBoxJ2534Info.Controls.Add(Me.LabelJ2534Info)
        Me.GroupBoxJ2534Info.Location = New System.Drawing.Point(4, 188)
        Me.GroupBoxJ2534Info.Name = "GroupBoxJ2534Info"
        Me.GroupBoxJ2534Info.Size = New System.Drawing.Size(409, 201)
        Me.GroupBoxJ2534Info.TabIndex = 2
        Me.GroupBoxJ2534Info.TabStop = False
        Me.GroupBoxJ2534Info.Text = "J2534 Device Information:"
        '
        'LabelProtSupport
        '
        Me.LabelProtSupport.Location = New System.Drawing.Point(10, 101)
        Me.LabelProtSupport.Name = "LabelProtSupport"
        Me.LabelProtSupport.Size = New System.Drawing.Size(385, 97)
        Me.LabelProtSupport.TabIndex = 1
        Me.LabelProtSupport.Text = "LabelProtSupport"
        '
        'LabelJ2534Info
        '
        Me.LabelJ2534Info.AutoSize = True
        Me.LabelJ2534Info.Location = New System.Drawing.Point(10, 16)
        Me.LabelJ2534Info.Name = "LabelJ2534Info"
        Me.LabelJ2534Info.Size = New System.Drawing.Size(80, 13)
        Me.LabelJ2534Info.TabIndex = 0
        Me.LabelJ2534Info.Text = "LabelJ2534Info"
        '
        'TabPageMessages
        '
        Me.TabPageMessages.Controls.Add(Me.ButtonClaimJ1939Address)
        Me.TabPageMessages.Controls.Add(Me.LabelMsgFlags)
        Me.TabPageMessages.Controls.Add(Me.LabelChannel1)
        Me.TabPageMessages.Controls.Add(Me.LabelDeviceCombo1)
        Me.TabPageMessages.Controls.Add(Me.LvOutLocator)
        Me.TabPageMessages.Controls.Add(Me.LvInLocator)
        Me.TabPageMessages.Controls.Add(Me.ComboBoxMessageChannel)
        Me.TabPageMessages.Controls.Add(Me.CheckBoxPadMessage)
        Me.TabPageMessages.Controls.Add(Me.TextBoxTimeOut)
        Me.TabPageMessages.Controls.Add(Me.TextBoxOutFlags)
        Me.TabPageMessages.Controls.Add(Me.ButtonClearRx)
        Me.TabPageMessages.Controls.Add(Me.ButtonClearTx)
        Me.TabPageMessages.Controls.Add(Me.TextBoxMessageOut)
        Me.TabPageMessages.Controls.Add(Me.ButtonSend)
        Me.TabPageMessages.Controls.Add(Me.ButtonReceive)
        Me.TabPageMessages.Controls.Add(Me.TextBoxReadRate)
        Me.TabPageMessages.Controls.Add(Me.ButtonClearList)
        Me.TabPageMessages.Controls.Add(Me.LabelScratchPad)
        Me.TabPageMessages.Controls.Add(Me.LabelTO)
        Me.TabPageMessages.Controls.Add(Me.LabelReadRate)
        Me.TabPageMessages.Controls.Add(Me.ComboAvailableBoxLocator1)
        Me.TabPageMessages.Controls.Add(Me.ComboAvailableChannelLocator1)
        Me.TabPageMessages.Location = New System.Drawing.Point(4, 22)
        Me.TabPageMessages.Name = "TabPageMessages"
        Me.TabPageMessages.Size = New System.Drawing.Size(714, 395)
        Me.TabPageMessages.TabIndex = 1
        Me.TabPageMessages.Text = "Messages"
        '
        'ButtonClaimJ1939Address
        '
        Me.ButtonClaimJ1939Address.Enabled = False
        Me.ButtonClaimJ1939Address.Location = New System.Drawing.Point(633, 351)
        Me.ButtonClaimJ1939Address.Name = "ButtonClaimJ1939Address"
        Me.ButtonClaimJ1939Address.Size = New System.Drawing.Size(72, 39)
        Me.ButtonClaimJ1939Address.TabIndex = 5
        Me.ButtonClaimJ1939Address.Text = "Claim J1939" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address..."
        Me.ButtonClaimJ1939Address.UseVisualStyleBackColor = True
        Me.ButtonClaimJ1939Address.Visible = False
        '
        'LabelMsgFlags
        '
        Me.LabelMsgFlags.AutoSize = True
        Me.LabelMsgFlags.Location = New System.Drawing.Point(242, 224)
        Me.LabelMsgFlags.Name = "LabelMsgFlags"
        Me.LabelMsgFlags.Size = New System.Drawing.Size(35, 13)
        Me.LabelMsgFlags.TabIndex = 18
        Me.LabelMsgFlags.Text = "Flags:"
        '
        'LabelChannel1
        '
        Me.LabelChannel1.AutoSize = True
        Me.LabelChannel1.Location = New System.Drawing.Point(134, 224)
        Me.LabelChannel1.Name = "LabelChannel1"
        Me.LabelChannel1.Size = New System.Drawing.Size(49, 13)
        Me.LabelChannel1.TabIndex = 17
        Me.LabelChannel1.Text = "Channel:"
        '
        'LabelDeviceCombo1
        '
        Me.LabelDeviceCombo1.AutoSize = True
        Me.LabelDeviceCombo1.Location = New System.Drawing.Point(5, 224)
        Me.LabelDeviceCombo1.Name = "LabelDeviceCombo1"
        Me.LabelDeviceCombo1.Size = New System.Drawing.Size(44, 13)
        Me.LabelDeviceCombo1.TabIndex = 16
        Me.LabelDeviceCombo1.Text = "Device:"
        '
        'LvOutLocator
        '
        Me.LvOutLocator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LvOutLocator.Enabled = False
        Me.LvOutLocator.Location = New System.Drawing.Point(8, 261)
        Me.LvOutLocator.Name = "LvOutLocator"
        Me.LvOutLocator.Size = New System.Drawing.Size(617, 129)
        Me.LvOutLocator.TabIndex = 21
        '
        'LvInLocator
        '
        Me.LvInLocator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LvInLocator.Enabled = False
        Me.LvInLocator.Location = New System.Drawing.Point(8, 13)
        Me.LvInLocator.Name = "LvInLocator"
        Me.LvInLocator.Size = New System.Drawing.Size(617, 201)
        Me.LvInLocator.TabIndex = 15
        '
        'ComboBoxMessageChannel
        '
        Me.ComboBoxMessageChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMessageChannel.Location = New System.Drawing.Point(134, 237)
        Me.ComboBoxMessageChannel.Name = "ComboBoxMessageChannel"
        Me.ComboBoxMessageChannel.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxMessageChannel.TabIndex = 1
        '
        'CheckBoxPadMessage
        '
        Me.CheckBoxPadMessage.Location = New System.Drawing.Point(633, 277)
        Me.CheckBoxPadMessage.Name = "CheckBoxPadMessage"
        Me.CheckBoxPadMessage.Size = New System.Drawing.Size(49, 17)
        Me.CheckBoxPadMessage.TabIndex = 13
        Me.CheckBoxPadMessage.Text = "Pad"
        Me.CheckBoxPadMessage.UseVisualStyleBackColor = True
        '
        'TextBoxTimeOut
        '
        Me.TextBoxTimeOut.AcceptsReturn = True
        Me.TextBoxTimeOut.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxTimeOut.Location = New System.Drawing.Point(632, 325)
        Me.TextBoxTimeOut.MaxLength = 0
        Me.TextBoxTimeOut.Name = "TextBoxTimeOut"
        Me.TextBoxTimeOut.Size = New System.Drawing.Size(73, 20)
        Me.TextBoxTimeOut.TabIndex = 6
        '
        'TextBoxOutFlags
        '
        Me.TextBoxOutFlags.AcceptsReturn = True
        Me.TextBoxOutFlags.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxOutFlags.Location = New System.Drawing.Point(245, 237)
        Me.TextBoxOutFlags.MaxLength = 0
        Me.TextBoxOutFlags.Name = "TextBoxOutFlags"
        Me.TextBoxOutFlags.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxOutFlags.TabIndex = 3
        '
        'ButtonClearRx
        '
        Me.ButtonClearRx.Location = New System.Drawing.Point(632, 13)
        Me.ButtonClearRx.Name = "ButtonClearRx"
        Me.ButtonClearRx.Size = New System.Drawing.Size(72, 34)
        Me.ButtonClearRx.TabIndex = 7
        Me.ButtonClearRx.Text = "Clear" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Rx Buffer"
        Me.ButtonClearRx.UseVisualStyleBackColor = True
        '
        'ButtonClearTx
        '
        Me.ButtonClearTx.Location = New System.Drawing.Point(632, 53)
        Me.ButtonClearTx.Name = "ButtonClearTx"
        Me.ButtonClearTx.Size = New System.Drawing.Size(72, 34)
        Me.ButtonClearTx.TabIndex = 8
        Me.ButtonClearTx.Text = "Clear" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Tx Buffer"
        Me.ButtonClearTx.UseVisualStyleBackColor = True
        '
        'TextBoxMessageOut
        '
        Me.TextBoxMessageOut.AcceptsReturn = True
        Me.TextBoxMessageOut.ContextMenuStrip = Me.ContextMenuStripScratchPad
        Me.TextBoxMessageOut.Location = New System.Drawing.Point(332, 237)
        Me.TextBoxMessageOut.MaxLength = 0
        Me.TextBoxMessageOut.Name = "TextBoxMessageOut"
        Me.TextBoxMessageOut.Size = New System.Drawing.Size(293, 20)
        Me.TextBoxMessageOut.TabIndex = 4
        '
        'ContextMenuStripScratchPad
        '
        Me.ContextMenuStripScratchPad.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuScratchPadUndo, Me.MenuScratchPadSeparator1, Me.MenuScratchPadCut, Me.MenuScratchPadCopy, Me.MenuScratchPadPaste, Me.MenuScratchPadDelete, Me.MenuScratchPadSeparator2, Me.MenuScratchPadSelectAll, Me.MenuScratchPadSeparator3, Me.MenuScratchPadAddToOutgoingMessageSet})
        Me.ContextMenuStripScratchPad.Name = "ContextMenuStripScratchPad"
        Me.ContextMenuStripScratchPad.Size = New System.Drawing.Size(224, 176)
        '
        'MenuScratchPadUndo
        '
        Me.MenuScratchPadUndo.Name = "MenuScratchPadUndo"
        Me.MenuScratchPadUndo.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadUndo.Text = "Undo"
        '
        'MenuScratchPadSeparator1
        '
        Me.MenuScratchPadSeparator1.Name = "MenuScratchPadSeparator1"
        Me.MenuScratchPadSeparator1.Size = New System.Drawing.Size(220, 6)
        '
        'MenuScratchPadCut
        '
        Me.MenuScratchPadCut.Name = "MenuScratchPadCut"
        Me.MenuScratchPadCut.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadCut.Text = "Cut"
        '
        'MenuScratchPadCopy
        '
        Me.MenuScratchPadCopy.Name = "MenuScratchPadCopy"
        Me.MenuScratchPadCopy.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadCopy.Text = "Copy"
        '
        'MenuScratchPadPaste
        '
        Me.MenuScratchPadPaste.Name = "MenuScratchPadPaste"
        Me.MenuScratchPadPaste.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadPaste.Text = "Paste"
        '
        'MenuScratchPadDelete
        '
        Me.MenuScratchPadDelete.Name = "MenuScratchPadDelete"
        Me.MenuScratchPadDelete.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadDelete.Text = "Delete"
        '
        'MenuScratchPadSeparator2
        '
        Me.MenuScratchPadSeparator2.Name = "MenuScratchPadSeparator2"
        Me.MenuScratchPadSeparator2.Size = New System.Drawing.Size(220, 6)
        '
        'MenuScratchPadSelectAll
        '
        Me.MenuScratchPadSelectAll.Name = "MenuScratchPadSelectAll"
        Me.MenuScratchPadSelectAll.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadSelectAll.Text = "Select All"
        '
        'MenuScratchPadSeparator3
        '
        Me.MenuScratchPadSeparator3.Name = "MenuScratchPadSeparator3"
        Me.MenuScratchPadSeparator3.Size = New System.Drawing.Size(220, 6)
        '
        'MenuScratchPadAddToOutgoingMessageSet
        '
        Me.MenuScratchPadAddToOutgoingMessageSet.Name = "MenuScratchPadAddToOutgoingMessageSet"
        Me.MenuScratchPadAddToOutgoingMessageSet.Size = New System.Drawing.Size(223, 22)
        Me.MenuScratchPadAddToOutgoingMessageSet.Text = "Add to Ougoing Mesage Set"
        '
        'ButtonSend
        '
        Me.ButtonSend.Location = New System.Drawing.Point(632, 232)
        Me.ButtonSend.Name = "ButtonSend"
        Me.ButtonSend.Size = New System.Drawing.Size(72, 39)
        Me.ButtonSend.TabIndex = 0
        Me.ButtonSend.Text = "Send"
        Me.ButtonSend.UseVisualStyleBackColor = True
        '
        'ButtonReceive
        '
        Me.ButtonReceive.Location = New System.Drawing.Point(632, 125)
        Me.ButtonReceive.Name = "ButtonReceive"
        Me.ButtonReceive.Size = New System.Drawing.Size(72, 25)
        Me.ButtonReceive.TabIndex = 10
        Me.ButtonReceive.Text = "Start"
        Me.ButtonReceive.UseVisualStyleBackColor = True
        '
        'TextBoxReadRate
        '
        Me.TextBoxReadRate.AcceptsReturn = True
        Me.TextBoxReadRate.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxReadRate.Location = New System.Drawing.Point(632, 173)
        Me.TextBoxReadRate.MaxLength = 0
        Me.TextBoxReadRate.Name = "TextBoxReadRate"
        Me.TextBoxReadRate.Size = New System.Drawing.Size(73, 20)
        Me.TextBoxReadRate.TabIndex = 11
        '
        'ButtonClearList
        '
        Me.ButtonClearList.Location = New System.Drawing.Point(632, 93)
        Me.ButtonClearList.Name = "ButtonClearList"
        Me.ButtonClearList.Size = New System.Drawing.Size(72, 25)
        Me.ButtonClearList.TabIndex = 9
        Me.ButtonClearList.Text = "Clear"
        Me.ButtonClearList.UseVisualStyleBackColor = True
        '
        'LabelScratchPad
        '
        Me.LabelScratchPad.AutoSize = True
        Me.LabelScratchPad.Location = New System.Drawing.Point(332, 224)
        Me.LabelScratchPad.Name = "LabelScratchPad"
        Me.LabelScratchPad.Size = New System.Drawing.Size(69, 13)
        Me.LabelScratchPad.TabIndex = 19
        Me.LabelScratchPad.Text = "Scratch Pad:"
        '
        'LabelTO
        '
        Me.LabelTO.Location = New System.Drawing.Point(631, 309)
        Me.LabelTO.Name = "LabelTO"
        Me.LabelTO.Size = New System.Drawing.Size(72, 13)
        Me.LabelTO.TabIndex = 14
        Me.LabelTO.Text = "Timeout:"
        '
        'LabelReadRate
        '
        Me.LabelReadRate.Location = New System.Drawing.Point(632, 157)
        Me.LabelReadRate.Name = "LabelReadRate"
        Me.LabelReadRate.Size = New System.Drawing.Size(41, 17)
        Me.LabelReadRate.TabIndex = 12
        Me.LabelReadRate.Text = "Rate:"
        '
        'TabPagePeriodicMessages
        '
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboBoxPeriodicMessageChannel1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ButtonClearAllPeriodicMessages)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ButtonCancelPeriodicMessages)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ButtonApplyPeriodicMessages)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageFlags9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessage9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.TextBoxPeriodicMessageInterval9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.CheckBoxPeriodicMessageEnable9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageChannel)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelChannel2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelDeviceCombo2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageIds)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageId9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageFlags)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPMMessage)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage0)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage1)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage3)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage4)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage5)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage6)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage7)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage8)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessage9)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageInterval)
        Me.TabPagePeriodicMessages.Controls.Add(Me.LabelPeriodicMessageDelete)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboAvailableBoxLocator2)
        Me.TabPagePeriodicMessages.Controls.Add(Me.ComboAvailableChannelLocator2)
        Me.TabPagePeriodicMessages.Location = New System.Drawing.Point(4, 22)
        Me.TabPagePeriodicMessages.Name = "TabPagePeriodicMessages"
        Me.TabPagePeriodicMessages.Size = New System.Drawing.Size(714, 395)
        Me.TabPagePeriodicMessages.TabIndex = 2
        Me.TabPagePeriodicMessages.Text = "Periodic Messages"
        '
        'ComboBoxPeriodicMessageChannel0
        '
        Me.ComboBoxPeriodicMessageChannel0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel0.Location = New System.Drawing.Point(297, 23)
        Me.ComboBoxPeriodicMessageChannel0.Name = "ComboBoxPeriodicMessageChannel0"
        Me.ComboBoxPeriodicMessageChannel0.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel0.TabIndex = 1
        '
        'ComboBoxPeriodicMessageChannel1
        '
        Me.ComboBoxPeriodicMessageChannel1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel1.Location = New System.Drawing.Point(297, 48)
        Me.ComboBoxPeriodicMessageChannel1.Name = "ComboBoxPeriodicMessageChannel1"
        Me.ComboBoxPeriodicMessageChannel1.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel1.TabIndex = 6
        '
        'ComboBoxPeriodicMessageChannel2
        '
        Me.ComboBoxPeriodicMessageChannel2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel2.Location = New System.Drawing.Point(297, 72)
        Me.ComboBoxPeriodicMessageChannel2.Name = "ComboBoxPeriodicMessageChannel2"
        Me.ComboBoxPeriodicMessageChannel2.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel2.TabIndex = 11
        '
        'ComboBoxPeriodicMessageChannel3
        '
        Me.ComboBoxPeriodicMessageChannel3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel3.Location = New System.Drawing.Point(297, 96)
        Me.ComboBoxPeriodicMessageChannel3.Name = "ComboBoxPeriodicMessageChannel3"
        Me.ComboBoxPeriodicMessageChannel3.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel3.TabIndex = 16
        '
        'ComboBoxPeriodicMessageChannel4
        '
        Me.ComboBoxPeriodicMessageChannel4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel4.Location = New System.Drawing.Point(297, 120)
        Me.ComboBoxPeriodicMessageChannel4.Name = "ComboBoxPeriodicMessageChannel4"
        Me.ComboBoxPeriodicMessageChannel4.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel4.TabIndex = 21
        '
        'ComboBoxPeriodicMessageChannel5
        '
        Me.ComboBoxPeriodicMessageChannel5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel5.Location = New System.Drawing.Point(297, 144)
        Me.ComboBoxPeriodicMessageChannel5.Name = "ComboBoxPeriodicMessageChannel5"
        Me.ComboBoxPeriodicMessageChannel5.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel5.TabIndex = 26
        '
        'ComboBoxPeriodicMessageChannel6
        '
        Me.ComboBoxPeriodicMessageChannel6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel6.Location = New System.Drawing.Point(297, 168)
        Me.ComboBoxPeriodicMessageChannel6.Name = "ComboBoxPeriodicMessageChannel6"
        Me.ComboBoxPeriodicMessageChannel6.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel6.TabIndex = 31
        '
        'ComboBoxPeriodicMessageChannel7
        '
        Me.ComboBoxPeriodicMessageChannel7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel7.Location = New System.Drawing.Point(297, 192)
        Me.ComboBoxPeriodicMessageChannel7.Name = "ComboBoxPeriodicMessageChannel7"
        Me.ComboBoxPeriodicMessageChannel7.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel7.TabIndex = 36
        '
        'ComboBoxPeriodicMessageChannel8
        '
        Me.ComboBoxPeriodicMessageChannel8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel8.Location = New System.Drawing.Point(297, 216)
        Me.ComboBoxPeriodicMessageChannel8.Name = "ComboBoxPeriodicMessageChannel8"
        Me.ComboBoxPeriodicMessageChannel8.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel8.TabIndex = 41
        '
        'ComboBoxPeriodicMessageChannel9
        '
        Me.ComboBoxPeriodicMessageChannel9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxPeriodicMessageChannel9.Location = New System.Drawing.Point(297, 240)
        Me.ComboBoxPeriodicMessageChannel9.Name = "ComboBoxPeriodicMessageChannel9"
        Me.ComboBoxPeriodicMessageChannel9.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxPeriodicMessageChannel9.TabIndex = 46
        '
        'ButtonClearAllPeriodicMessages
        '
        Me.ButtonClearAllPeriodicMessages.Location = New System.Drawing.Point(480, 288)
        Me.ButtonClearAllPeriodicMessages.Name = "ButtonClearAllPeriodicMessages"
        Me.ButtonClearAllPeriodicMessages.Size = New System.Drawing.Size(73, 25)
        Me.ButtonClearAllPeriodicMessages.TabIndex = 50
        Me.ButtonClearAllPeriodicMessages.Text = "Clear All"
        Me.ButtonClearAllPeriodicMessages.UseVisualStyleBackColor = True
        '
        'ButtonCancelPeriodicMessages
        '
        Me.ButtonCancelPeriodicMessages.Location = New System.Drawing.Point(560, 288)
        Me.ButtonCancelPeriodicMessages.Name = "ButtonCancelPeriodicMessages"
        Me.ButtonCancelPeriodicMessages.Size = New System.Drawing.Size(49, 25)
        Me.ButtonCancelPeriodicMessages.TabIndex = 51
        Me.ButtonCancelPeriodicMessages.Text = "Cancel"
        Me.ButtonCancelPeriodicMessages.UseVisualStyleBackColor = True
        '
        'ButtonApplyPeriodicMessage
        '
        Me.ButtonApplyPeriodicMessages.Location = New System.Drawing.Point(616, 288)
        Me.ButtonApplyPeriodicMessages.Name = "ButtonApplyPeriodicMessages"
        Me.ButtonApplyPeriodicMessages.Size = New System.Drawing.Size(49, 25)
        Me.ButtonApplyPeriodicMessages.TabIndex = 52
        Me.ButtonApplyPeriodicMessages.Text = "Apply"
        Me.ButtonApplyPeriodicMessages.UseVisualStyleBackColor = True
        '
        'TextBoxPeriodicMessageFlags0
        '
        Me.TextBoxPeriodicMessageFlags0.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags0.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags0.Location = New System.Drawing.Point(417, 24)
        Me.TextBoxPeriodicMessageFlags0.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags0.Name = "TextBoxPeriodicMessageFlags0"
        Me.TextBoxPeriodicMessageFlags0.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags0.TabIndex = 2
        '
        'TextBoxPeriodicMessageFlags1
        '
        Me.TextBoxPeriodicMessageFlags1.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags1.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags1.Location = New System.Drawing.Point(417, 48)
        Me.TextBoxPeriodicMessageFlags1.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags1.Name = "TextBoxPeriodicMessageFlags1"
        Me.TextBoxPeriodicMessageFlags1.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags1.TabIndex = 7
        '
        'TextBoxPeriodicMessageFlags2
        '
        Me.TextBoxPeriodicMessageFlags2.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags2.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags2.Location = New System.Drawing.Point(417, 72)
        Me.TextBoxPeriodicMessageFlags2.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags2.Name = "TextBoxPeriodicMessageFlags2"
        Me.TextBoxPeriodicMessageFlags2.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags2.TabIndex = 12
        '
        'TextBoxPeriodicMessageFlags3
        '
        Me.TextBoxPeriodicMessageFlags3.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags3.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags3.Location = New System.Drawing.Point(417, 96)
        Me.TextBoxPeriodicMessageFlags3.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags3.Name = "TextBoxPeriodicMessageFlags3"
        Me.TextBoxPeriodicMessageFlags3.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags3.TabIndex = 17
        '
        'TextBoxPeriodicMessageFlags4
        '
        Me.TextBoxPeriodicMessageFlags4.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags4.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags4.Location = New System.Drawing.Point(417, 120)
        Me.TextBoxPeriodicMessageFlags4.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags4.Name = "TextBoxPeriodicMessageFlags4"
        Me.TextBoxPeriodicMessageFlags4.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags4.TabIndex = 22
        '
        'TextBoxPeriodicMessageFlags5
        '
        Me.TextBoxPeriodicMessageFlags5.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags5.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags5.Location = New System.Drawing.Point(417, 144)
        Me.TextBoxPeriodicMessageFlags5.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags5.Name = "TextBoxPeriodicMessageFlags5"
        Me.TextBoxPeriodicMessageFlags5.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags5.TabIndex = 27
        '
        'TextBoxPeriodicMessageFlags6
        '
        Me.TextBoxPeriodicMessageFlags6.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags6.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags6.Location = New System.Drawing.Point(417, 168)
        Me.TextBoxPeriodicMessageFlags6.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags6.Name = "TextBoxPeriodicMessageFlags6"
        Me.TextBoxPeriodicMessageFlags6.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags6.TabIndex = 32
        '
        'TextBoxPeriodicMessageFlags7
        '
        Me.TextBoxPeriodicMessageFlags7.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags7.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags7.Location = New System.Drawing.Point(417, 192)
        Me.TextBoxPeriodicMessageFlags7.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags7.Name = "TextBoxPeriodicMessageFlags7"
        Me.TextBoxPeriodicMessageFlags7.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags7.TabIndex = 37
        '
        'TextBoxPeriodicMessageFlags8
        '
        Me.TextBoxPeriodicMessageFlags8.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags8.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags8.Location = New System.Drawing.Point(417, 216)
        Me.TextBoxPeriodicMessageFlags8.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags8.Name = "TextBoxPeriodicMessageFlags8"
        Me.TextBoxPeriodicMessageFlags8.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags8.TabIndex = 42
        '
        'TextBoxPeriodicMessageFlags9
        '
        Me.TextBoxPeriodicMessageFlags9.AcceptsReturn = True
        Me.TextBoxPeriodicMessageFlags9.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxPeriodicMessageFlags9.Location = New System.Drawing.Point(417, 240)
        Me.TextBoxPeriodicMessageFlags9.MaxLength = 0
        Me.TextBoxPeriodicMessageFlags9.Name = "TextBoxPeriodicMessageFlags9"
        Me.TextBoxPeriodicMessageFlags9.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageFlags9.TabIndex = 47
        '
        'TextBoxPeriodicMessage0
        '
        Me.TextBoxPeriodicMessage0.AcceptsReturn = True
        Me.TextBoxPeriodicMessage0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage0.Location = New System.Drawing.Point(25, 24)
        Me.TextBoxPeriodicMessage0.MaxLength = 0
        Me.TextBoxPeriodicMessage0.Name = "TextBoxPeriodicMessage0"
        Me.TextBoxPeriodicMessage0.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage0.TabIndex = 0
        '
        'TextBoxPeriodicMessage1
        '
        Me.TextBoxPeriodicMessage1.AcceptsReturn = True
        Me.TextBoxPeriodicMessage1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage1.Location = New System.Drawing.Point(25, 48)
        Me.TextBoxPeriodicMessage1.MaxLength = 0
        Me.TextBoxPeriodicMessage1.Name = "TextBoxPeriodicMessage1"
        Me.TextBoxPeriodicMessage1.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage1.TabIndex = 5
        '
        'TextBoxPeriodicMessage2
        '
        Me.TextBoxPeriodicMessage2.AcceptsReturn = True
        Me.TextBoxPeriodicMessage2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage2.Location = New System.Drawing.Point(25, 72)
        Me.TextBoxPeriodicMessage2.MaxLength = 0
        Me.TextBoxPeriodicMessage2.Name = "TextBoxPeriodicMessage2"
        Me.TextBoxPeriodicMessage2.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage2.TabIndex = 10
        '
        'TextBoxPeriodicMessage3
        '
        Me.TextBoxPeriodicMessage3.AcceptsReturn = True
        Me.TextBoxPeriodicMessage3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage3.Location = New System.Drawing.Point(25, 96)
        Me.TextBoxPeriodicMessage3.MaxLength = 0
        Me.TextBoxPeriodicMessage3.Name = "TextBoxPeriodicMessage3"
        Me.TextBoxPeriodicMessage3.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage3.TabIndex = 15
        '
        'TextBoxPeriodicMessage4
        '
        Me.TextBoxPeriodicMessage4.AcceptsReturn = True
        Me.TextBoxPeriodicMessage4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage4.Location = New System.Drawing.Point(25, 120)
        Me.TextBoxPeriodicMessage4.MaxLength = 0
        Me.TextBoxPeriodicMessage4.Name = "TextBoxPeriodicMessage4"
        Me.TextBoxPeriodicMessage4.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage4.TabIndex = 20
        '
        'TextBoxPeriodicMessage5
        '
        Me.TextBoxPeriodicMessage5.AcceptsReturn = True
        Me.TextBoxPeriodicMessage5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage5.Location = New System.Drawing.Point(25, 144)
        Me.TextBoxPeriodicMessage5.MaxLength = 0
        Me.TextBoxPeriodicMessage5.Name = "TextBoxPeriodicMessage5"
        Me.TextBoxPeriodicMessage5.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage5.TabIndex = 25
        '
        'TextBoxPeriodicMessage6
        '
        Me.TextBoxPeriodicMessage6.AcceptsReturn = True
        Me.TextBoxPeriodicMessage6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage6.Location = New System.Drawing.Point(25, 168)
        Me.TextBoxPeriodicMessage6.MaxLength = 0
        Me.TextBoxPeriodicMessage6.Name = "TextBoxPeriodicMessage6"
        Me.TextBoxPeriodicMessage6.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage6.TabIndex = 30
        '
        'TextBoxPeriodicMessage7
        '
        Me.TextBoxPeriodicMessage7.AcceptsReturn = True
        Me.TextBoxPeriodicMessage7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage7.Location = New System.Drawing.Point(25, 192)
        Me.TextBoxPeriodicMessage7.MaxLength = 0
        Me.TextBoxPeriodicMessage7.Name = "TextBoxPeriodicMessage7"
        Me.TextBoxPeriodicMessage7.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage7.TabIndex = 35
        '
        'TextBoxPeriodicMessage8
        '
        Me.TextBoxPeriodicMessage8.AcceptsReturn = True
        Me.TextBoxPeriodicMessage8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage8.Location = New System.Drawing.Point(25, 216)
        Me.TextBoxPeriodicMessage8.MaxLength = 0
        Me.TextBoxPeriodicMessage8.Name = "TextBoxPeriodicMessage8"
        Me.TextBoxPeriodicMessage8.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage8.TabIndex = 40
        '
        'TextBoxPeriodicMessage9
        '
        Me.TextBoxPeriodicMessage9.AcceptsReturn = True
        Me.TextBoxPeriodicMessage9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessage9.Location = New System.Drawing.Point(25, 240)
        Me.TextBoxPeriodicMessage9.MaxLength = 0
        Me.TextBoxPeriodicMessage9.Name = "TextBoxPeriodicMessage9"
        Me.TextBoxPeriodicMessage9.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxPeriodicMessage9.TabIndex = 45
        '
        'TextBoxPeriodicMessageInterval0
        '
        Me.TextBoxPeriodicMessageInterval0.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval0.Location = New System.Drawing.Point(513, 24)
        Me.TextBoxPeriodicMessageInterval0.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval0.Name = "TextBoxPeriodicMessageInterval0"
        Me.TextBoxPeriodicMessageInterval0.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval0.TabIndex = 3
        '
        'TextBoxPeriodicMessageInterval1
        '
        Me.TextBoxPeriodicMessageInterval1.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval1.Location = New System.Drawing.Point(513, 48)
        Me.TextBoxPeriodicMessageInterval1.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval1.Name = "TextBoxPeriodicMessageInterval1"
        Me.TextBoxPeriodicMessageInterval1.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval1.TabIndex = 8
        '
        'TextBoxPeriodicMessageInterval2
        '
        Me.TextBoxPeriodicMessageInterval2.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval2.Location = New System.Drawing.Point(513, 72)
        Me.TextBoxPeriodicMessageInterval2.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval2.Name = "TextBoxPeriodicMessageInterval2"
        Me.TextBoxPeriodicMessageInterval2.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval2.TabIndex = 13
        '
        'TextBoxPeriodicMessageInterval3
        '
        Me.TextBoxPeriodicMessageInterval3.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval3.Location = New System.Drawing.Point(513, 96)
        Me.TextBoxPeriodicMessageInterval3.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval3.Name = "TextBoxPeriodicMessageInterval3"
        Me.TextBoxPeriodicMessageInterval3.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval3.TabIndex = 18
        '
        'TextBoxPeriodicMessageInterval4
        '
        Me.TextBoxPeriodicMessageInterval4.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval4.Location = New System.Drawing.Point(513, 120)
        Me.TextBoxPeriodicMessageInterval4.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval4.Name = "TextBoxPeriodicMessageInterval4"
        Me.TextBoxPeriodicMessageInterval4.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval4.TabIndex = 23
        '
        'TextBoxPeriodicMessageInterval5
        '
        Me.TextBoxPeriodicMessageInterval5.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval5.Location = New System.Drawing.Point(513, 144)
        Me.TextBoxPeriodicMessageInterval5.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval5.Name = "TextBoxPeriodicMessageInterval5"
        Me.TextBoxPeriodicMessageInterval5.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval5.TabIndex = 28
        '
        'TextBoxPeriodicMessageInterval6
        '
        Me.TextBoxPeriodicMessageInterval6.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval6.Location = New System.Drawing.Point(513, 168)
        Me.TextBoxPeriodicMessageInterval6.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval6.Name = "TextBoxPeriodicMessageInterval6"
        Me.TextBoxPeriodicMessageInterval6.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval6.TabIndex = 33
        '
        'TextBoxPeriodicMessageInterval7
        '
        Me.TextBoxPeriodicMessageInterval7.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval7.Location = New System.Drawing.Point(513, 192)
        Me.TextBoxPeriodicMessageInterval7.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval7.Name = "TextBoxPeriodicMessageInterval7"
        Me.TextBoxPeriodicMessageInterval7.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval7.TabIndex = 38
        '
        'TextBoxPeriodicMessageInterval8
        '
        Me.TextBoxPeriodicMessageInterval8.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval8.Location = New System.Drawing.Point(513, 216)
        Me.TextBoxPeriodicMessageInterval8.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval8.Name = "TextBoxPeriodicMessageInterval8"
        Me.TextBoxPeriodicMessageInterval8.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval8.TabIndex = 43
        '
        'TextBoxPeriodicMessageInterval9
        '
        Me.TextBoxPeriodicMessageInterval9.AcceptsReturn = True
        Me.TextBoxPeriodicMessageInterval9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxPeriodicMessageInterval9.Location = New System.Drawing.Point(513, 240)
        Me.TextBoxPeriodicMessageInterval9.MaxLength = 0
        Me.TextBoxPeriodicMessageInterval9.Name = "TextBoxPeriodicMessageInterval9"
        Me.TextBoxPeriodicMessageInterval9.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxPeriodicMessageInterval9.TabIndex = 48
        '
        'CheckBoxPeriodicMessageEnable0
        '
        Me.CheckBoxPeriodicMessageEnable0.Location = New System.Drawing.Point(625, 24)
        Me.CheckBoxPeriodicMessageEnable0.Name = "CheckBoxPeriodicMessageEnable0"
        Me.CheckBoxPeriodicMessageEnable0.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable0.TabIndex = 63
        Me.CheckBoxPeriodicMessageEnable0.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable0.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable1
        '
        Me.CheckBoxPeriodicMessageEnable1.Location = New System.Drawing.Point(625, 48)
        Me.CheckBoxPeriodicMessageEnable1.Name = "CheckBoxPeriodicMessageEnable1"
        Me.CheckBoxPeriodicMessageEnable1.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable1.TabIndex = 64
        Me.CheckBoxPeriodicMessageEnable1.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable1.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable2
        '
        Me.CheckBoxPeriodicMessageEnable2.Location = New System.Drawing.Point(625, 72)
        Me.CheckBoxPeriodicMessageEnable2.Name = "CheckBoxPeriodicMessageEnable2"
        Me.CheckBoxPeriodicMessageEnable2.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable2.TabIndex = 65
        Me.CheckBoxPeriodicMessageEnable2.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable2.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable3
        '
        Me.CheckBoxPeriodicMessageEnable3.Location = New System.Drawing.Point(625, 96)
        Me.CheckBoxPeriodicMessageEnable3.Name = "CheckBoxPeriodicMessageEnable3"
        Me.CheckBoxPeriodicMessageEnable3.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable3.TabIndex = 66
        Me.CheckBoxPeriodicMessageEnable3.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable3.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable4
        '
        Me.CheckBoxPeriodicMessageEnable4.Location = New System.Drawing.Point(625, 120)
        Me.CheckBoxPeriodicMessageEnable4.Name = "CheckBoxPeriodicMessageEnable4"
        Me.CheckBoxPeriodicMessageEnable4.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable4.TabIndex = 67
        Me.CheckBoxPeriodicMessageEnable4.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable4.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable5
        '
        Me.CheckBoxPeriodicMessageEnable5.Location = New System.Drawing.Point(625, 144)
        Me.CheckBoxPeriodicMessageEnable5.Name = "CheckBoxPeriodicMessageEnable5"
        Me.CheckBoxPeriodicMessageEnable5.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable5.TabIndex = 68
        Me.CheckBoxPeriodicMessageEnable5.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable5.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable6
        '
        Me.CheckBoxPeriodicMessageEnable6.Location = New System.Drawing.Point(625, 168)
        Me.CheckBoxPeriodicMessageEnable6.Name = "CheckBoxPeriodicMessageEnable6"
        Me.CheckBoxPeriodicMessageEnable6.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable6.TabIndex = 69
        Me.CheckBoxPeriodicMessageEnable6.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable6.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable7
        '
        Me.CheckBoxPeriodicMessageEnable7.Location = New System.Drawing.Point(625, 192)
        Me.CheckBoxPeriodicMessageEnable7.Name = "CheckBoxPeriodicMessageEnable7"
        Me.CheckBoxPeriodicMessageEnable7.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable7.TabIndex = 70
        Me.CheckBoxPeriodicMessageEnable7.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable7.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable8
        '
        Me.CheckBoxPeriodicMessageEnable8.Location = New System.Drawing.Point(625, 216)
        Me.CheckBoxPeriodicMessageEnable8.Name = "CheckBoxPeriodicMessageEnable8"
        Me.CheckBoxPeriodicMessageEnable8.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable8.TabIndex = 71
        Me.CheckBoxPeriodicMessageEnable8.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable8.UseVisualStyleBackColor = True
        '
        'CheckBoxPeriodicMessageEnable9
        '
        Me.CheckBoxPeriodicMessageEnable9.Location = New System.Drawing.Point(625, 240)
        Me.CheckBoxPeriodicMessageEnable9.Name = "CheckBoxPeriodicMessageEnable9"
        Me.CheckBoxPeriodicMessageEnable9.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxPeriodicMessageEnable9.TabIndex = 72
        Me.CheckBoxPeriodicMessageEnable9.Text = "Enabled"
        Me.CheckBoxPeriodicMessageEnable9.UseVisualStyleBackColor = True
        '
        'LabelPeriodicMessageChannel
        '
        Me.LabelPeriodicMessageChannel.Location = New System.Drawing.Point(294, 8)
        Me.LabelPeriodicMessageChannel.Name = "LabelPeriodicMessageChannel"
        Me.LabelPeriodicMessageChannel.Size = New System.Drawing.Size(103, 13)
        Me.LabelPeriodicMessageChannel.TabIndex = 78
        Me.LabelPeriodicMessageChannel.Text = "Channel:"
        '
        'LabelChannel2
        '
        Me.LabelChannel2.Location = New System.Drawing.Point(524, 336)
        Me.LabelChannel2.Name = "LabelChannel2"
        Me.LabelChannel2.Size = New System.Drawing.Size(49, 17)
        Me.LabelChannel2.TabIndex = 76
        Me.LabelChannel2.Text = "Channel:"
        '
        'LabelDeviceCombo2
        '
        Me.LabelDeviceCombo2.Location = New System.Drawing.Point(395, 336)
        Me.LabelDeviceCombo2.Name = "LabelDeviceCombo2"
        Me.LabelDeviceCombo2.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo2.TabIndex = 75
        Me.LabelDeviceCombo2.Text = "Device:"
        '
        'LabelPeriodicMessageIds
        '
        Me.LabelPeriodicMessageIds.AutoSize = True
        Me.LabelPeriodicMessageIds.Location = New System.Drawing.Point(601, 8)
        Me.LabelPeriodicMessageIds.Name = "LabelPeriodicMessageIds"
        Me.LabelPeriodicMessageIds.Size = New System.Drawing.Size(21, 13)
        Me.LabelPeriodicMessageIds.TabIndex = 81
        Me.LabelPeriodicMessageIds.Text = "ID:"
        Me.LabelPeriodicMessageIds.Visible = False
        '
        'LabelPeriodicMessageId0
        '
        Me.LabelPeriodicMessageId0.Location = New System.Drawing.Point(601, 24)
        Me.LabelPeriodicMessageId0.Name = "LabelPeriodicMessageId0"
        Me.LabelPeriodicMessageId0.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId0.TabIndex = 4
        Me.LabelPeriodicMessageId0.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId0.Visible = False
        '
        'LabelPeriodicMessageId1
        '
        Me.LabelPeriodicMessageId1.Location = New System.Drawing.Point(601, 48)
        Me.LabelPeriodicMessageId1.Name = "LabelPeriodicMessageId1"
        Me.LabelPeriodicMessageId1.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId1.TabIndex = 9
        Me.LabelPeriodicMessageId1.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId1.Visible = False
        '
        'LabelPeriodicMessageId2
        '
        Me.LabelPeriodicMessageId2.Location = New System.Drawing.Point(601, 72)
        Me.LabelPeriodicMessageId2.Name = "LabelPeriodicMessageId2"
        Me.LabelPeriodicMessageId2.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId2.TabIndex = 14
        Me.LabelPeriodicMessageId2.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId2.Visible = False
        '
        'LabelPeriodicMessageId3
        '
        Me.LabelPeriodicMessageId3.Location = New System.Drawing.Point(601, 96)
        Me.LabelPeriodicMessageId3.Name = "LabelPeriodicMessageId3"
        Me.LabelPeriodicMessageId3.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId3.TabIndex = 19
        Me.LabelPeriodicMessageId3.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId3.Visible = False
        '
        'LabelPeriodicMessageId4
        '
        Me.LabelPeriodicMessageId4.Location = New System.Drawing.Point(601, 120)
        Me.LabelPeriodicMessageId4.Name = "LabelPeriodicMessageId4"
        Me.LabelPeriodicMessageId4.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId4.TabIndex = 24
        Me.LabelPeriodicMessageId4.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId4.Visible = False
        '
        'LabelPeriodicMessageId5
        '
        Me.LabelPeriodicMessageId5.Location = New System.Drawing.Point(601, 144)
        Me.LabelPeriodicMessageId5.Name = "LabelPeriodicMessageId5"
        Me.LabelPeriodicMessageId5.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId5.TabIndex = 29
        Me.LabelPeriodicMessageId5.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId5.Visible = False
        '
        'LabelPeriodicMessageId6
        '
        Me.LabelPeriodicMessageId6.Location = New System.Drawing.Point(601, 168)
        Me.LabelPeriodicMessageId6.Name = "LabelPeriodicMessageId6"
        Me.LabelPeriodicMessageId6.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId6.TabIndex = 34
        Me.LabelPeriodicMessageId6.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId6.Visible = False
        '
        'LabelPeriodicMessageId7
        '
        Me.LabelPeriodicMessageId7.Location = New System.Drawing.Point(601, 192)
        Me.LabelPeriodicMessageId7.Name = "LabelPeriodicMessageId7"
        Me.LabelPeriodicMessageId7.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId7.TabIndex = 39
        Me.LabelPeriodicMessageId7.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId7.Visible = False
        '
        'LabelPeriodicMessageId8
        '
        Me.LabelPeriodicMessageId8.Location = New System.Drawing.Point(601, 216)
        Me.LabelPeriodicMessageId8.Name = "LabelPeriodicMessageId8"
        Me.LabelPeriodicMessageId8.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId8.TabIndex = 44
        Me.LabelPeriodicMessageId8.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId8.Visible = False
        '
        'LabelPeriodicMessageId9
        '
        Me.LabelPeriodicMessageId9.Location = New System.Drawing.Point(601, 240)
        Me.LabelPeriodicMessageId9.Name = "LabelPeriodicMessageId9"
        Me.LabelPeriodicMessageId9.Size = New System.Drawing.Size(17, 17)
        Me.LabelPeriodicMessageId9.TabIndex = 49
        Me.LabelPeriodicMessageId9.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelPeriodicMessageId9.Visible = False
        '
        'LabelPeriodicMessageFlags
        '
        Me.LabelPeriodicMessageFlags.Location = New System.Drawing.Point(417, 8)
        Me.LabelPeriodicMessageFlags.Name = "LabelPeriodicMessageFlags"
        Me.LabelPeriodicMessageFlags.Size = New System.Drawing.Size(81, 17)
        Me.LabelPeriodicMessageFlags.TabIndex = 79
        Me.LabelPeriodicMessageFlags.Text = "Tx Flags:"
        '
        'LabelPMMessage
        '
        Me.LabelPMMessage.Location = New System.Drawing.Point(25, 8)
        Me.LabelPMMessage.Name = "LabelPMMessage"
        Me.LabelPMMessage.Size = New System.Drawing.Size(122, 17)
        Me.LabelPMMessage.TabIndex = 77
        Me.LabelPMMessage.Text = "Message:"
        '
        'LabelPeriodicMessage0
        '
        Me.LabelPeriodicMessage0.Location = New System.Drawing.Point(9, 24)
        Me.LabelPeriodicMessage0.Name = "LabelPeriodicMessage0"
        Me.LabelPeriodicMessage0.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage0.TabIndex = 53
        Me.LabelPeriodicMessage0.Text = "0:"
        '
        'LabelPeriodicMessage1
        '
        Me.LabelPeriodicMessage1.Location = New System.Drawing.Point(9, 48)
        Me.LabelPeriodicMessage1.Name = "LabelPeriodicMessage1"
        Me.LabelPeriodicMessage1.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage1.TabIndex = 54
        Me.LabelPeriodicMessage1.Text = "1:"
        '
        'LabelPeriodicMessage2
        '
        Me.LabelPeriodicMessage2.Location = New System.Drawing.Point(9, 72)
        Me.LabelPeriodicMessage2.Name = "LabelPeriodicMessage2"
        Me.LabelPeriodicMessage2.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage2.TabIndex = 55
        Me.LabelPeriodicMessage2.Text = "2:"
        '
        'LabelPeriodicMessage3
        '
        Me.LabelPeriodicMessage3.Location = New System.Drawing.Point(9, 96)
        Me.LabelPeriodicMessage3.Name = "LabelPeriodicMessage3"
        Me.LabelPeriodicMessage3.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage3.TabIndex = 56
        Me.LabelPeriodicMessage3.Text = "3:"
        '
        'LabelPeriodicMessage4
        '
        Me.LabelPeriodicMessage4.Location = New System.Drawing.Point(9, 120)
        Me.LabelPeriodicMessage4.Name = "LabelPeriodicMessage4"
        Me.LabelPeriodicMessage4.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage4.TabIndex = 57
        Me.LabelPeriodicMessage4.Text = "4:"
        '
        'LabelPeriodicMessage5
        '
        Me.LabelPeriodicMessage5.Location = New System.Drawing.Point(9, 144)
        Me.LabelPeriodicMessage5.Name = "LabelPeriodicMessage5"
        Me.LabelPeriodicMessage5.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage5.TabIndex = 58
        Me.LabelPeriodicMessage5.Text = "5:"
        '
        'LabelPeriodicMessage6
        '
        Me.LabelPeriodicMessage6.Location = New System.Drawing.Point(9, 168)
        Me.LabelPeriodicMessage6.Name = "LabelPeriodicMessage6"
        Me.LabelPeriodicMessage6.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage6.TabIndex = 59
        Me.LabelPeriodicMessage6.Text = "6:"
        '
        'LabelPeriodicMessage7
        '
        Me.LabelPeriodicMessage7.Location = New System.Drawing.Point(9, 192)
        Me.LabelPeriodicMessage7.Name = "LabelPeriodicMessage7"
        Me.LabelPeriodicMessage7.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage7.TabIndex = 60
        Me.LabelPeriodicMessage7.Text = "7:"
        '
        'LabelPeriodicMessage8
        '
        Me.LabelPeriodicMessage8.Location = New System.Drawing.Point(9, 216)
        Me.LabelPeriodicMessage8.Name = "LabelPeriodicMessage8"
        Me.LabelPeriodicMessage8.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage8.TabIndex = 61
        Me.LabelPeriodicMessage8.Text = "8:"
        '
        'LabelPeriodicMessage9
        '
        Me.LabelPeriodicMessage9.Location = New System.Drawing.Point(9, 240)
        Me.LabelPeriodicMessage9.Name = "LabelPeriodicMessage9"
        Me.LabelPeriodicMessage9.Size = New System.Drawing.Size(9, 17)
        Me.LabelPeriodicMessage9.TabIndex = 62
        Me.LabelPeriodicMessage9.Text = "9:"
        '
        'LabelPeriodicMessageInterval
        '
        Me.LabelPeriodicMessageInterval.Location = New System.Drawing.Point(513, 8)
        Me.LabelPeriodicMessageInterval.Name = "LabelPeriodicMessageInterval"
        Me.LabelPeriodicMessageInterval.Size = New System.Drawing.Size(70, 17)
        Me.LabelPeriodicMessageInterval.TabIndex = 80
        Me.LabelPeriodicMessageInterval.Text = "Interval:"
        '
        'LabelPeriodicMessageDelete
        '
        Me.LabelPeriodicMessageDelete.Location = New System.Drawing.Point(625, 8)
        Me.LabelPeriodicMessageDelete.Name = "LabelPeriodicMessageDelete"
        Me.LabelPeriodicMessageDelete.Size = New System.Drawing.Size(65, 17)
        Me.LabelPeriodicMessageDelete.TabIndex = 82
        Me.LabelPeriodicMessageDelete.Text = "Enable:"
        '
        'TabPageFilters
        '
        Me.TabPageFilters.Controls.Add(Me.ButtonCreatePassFilter)
        Me.TabPageFilters.Controls.Add(Me.ButtonClearAllFilter)
        Me.TabPageFilters.Controls.Add(Me.ButtonCancelFilter)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow0)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow1)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow2)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow3)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow4)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow5)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow6)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow7)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow8)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlow9)
        Me.TabPageFilters.Controls.Add(Me.ButtonApplyFilter)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags0)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags1)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags2)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags3)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags4)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags5)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags6)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags7)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags8)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterFlags9)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType0)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType1)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType2)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType3)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType4)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType5)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType6)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType7)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType8)
        Me.TabPageFilters.Controls.Add(Me.ComboBoxFilterType9)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt0)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt1)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt2)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt3)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt4)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt5)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt6)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt7)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt8)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterPatt9)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask0)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask1)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask2)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask3)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask4)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask5)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask6)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask7)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask8)
        Me.TabPageFilters.Controls.Add(Me.TextBoxFilterMask9)
        Me.TabPageFilters.Controls.Add(Me.LabelChannel3)
        Me.TabPageFilters.Controls.Add(Me.LabelDeviceCombo3)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterIds)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId0)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId1)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId2)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId3)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId4)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId5)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId6)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId7)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId8)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterId9)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterType)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterFlags)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterFlow)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterPatt)
        Me.TabPageFilters.Controls.Add(Me.LabelFilterMask)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter0)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter1)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter2)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter3)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter4)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter5)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter6)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter7)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter8)
        Me.TabPageFilters.Controls.Add(Me.LabelFilter9)
        Me.TabPageFilters.Controls.Add(Me.ComboAvailableBoxLocator3)
        Me.TabPageFilters.Controls.Add(Me.ComboAvailableChannelLocator3)
        Me.TabPageFilters.Location = New System.Drawing.Point(4, 22)
        Me.TabPageFilters.Name = "TabPageFilters"
        Me.TabPageFilters.Size = New System.Drawing.Size(714, 395)
        Me.TabPageFilters.TabIndex = 3
        Me.TabPageFilters.Text = "Filters"
        '
        'ButtonCreatePassFilter
        '
        Me.ButtonCreatePassFilter.Location = New System.Drawing.Point(344, 288)
        Me.ButtonCreatePassFilter.Name = "ButtonCreatePassFilter"
        Me.ButtonCreatePassFilter.Size = New System.Drawing.Size(129, 25)
        Me.ButtonCreatePassFilter.TabIndex = 50
        Me.ButtonCreatePassFilter.Text = "Create ""Pass All"" Filter"
        Me.ButtonCreatePassFilter.UseVisualStyleBackColor = True
        '
        'ButtonClearAllFilter
        '
        Me.ButtonClearAllFilter.Location = New System.Drawing.Point(480, 288)
        Me.ButtonClearAllFilter.Name = "ButtonClearAllFilter"
        Me.ButtonClearAllFilter.Size = New System.Drawing.Size(73, 25)
        Me.ButtonClearAllFilter.TabIndex = 51
        Me.ButtonClearAllFilter.Text = "Clear All"
        Me.ButtonClearAllFilter.UseVisualStyleBackColor = True
        '
        'ButtonCancelFilter
        '
        Me.ButtonCancelFilter.Location = New System.Drawing.Point(560, 288)
        Me.ButtonCancelFilter.Name = "ButtonCancelFilter"
        Me.ButtonCancelFilter.Size = New System.Drawing.Size(49, 25)
        Me.ButtonCancelFilter.TabIndex = 52
        Me.ButtonCancelFilter.Text = "Cancel"
        Me.ButtonCancelFilter.UseVisualStyleBackColor = True
        '
        'TextBoxFilterFlow0
        '
        Me.TextBoxFilterFlow0.AcceptsReturn = True
        Me.TextBoxFilterFlow0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow0.Location = New System.Drawing.Point(359, 23)
        Me.TextBoxFilterFlow0.MaxLength = 0
        Me.TextBoxFilterFlow0.Name = "TextBoxFilterFlow0"
        Me.TextBoxFilterFlow0.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow0.TabIndex = 2
        '
        'TextBoxFilterFlow1
        '
        Me.TextBoxFilterFlow1.AcceptsReturn = True
        Me.TextBoxFilterFlow1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow1.Location = New System.Drawing.Point(359, 47)
        Me.TextBoxFilterFlow1.MaxLength = 0
        Me.TextBoxFilterFlow1.Name = "TextBoxFilterFlow1"
        Me.TextBoxFilterFlow1.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow1.TabIndex = 7
        '
        'TextBoxFilterFlow2
        '
        Me.TextBoxFilterFlow2.AcceptsReturn = True
        Me.TextBoxFilterFlow2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow2.Location = New System.Drawing.Point(359, 71)
        Me.TextBoxFilterFlow2.MaxLength = 0
        Me.TextBoxFilterFlow2.Name = "TextBoxFilterFlow2"
        Me.TextBoxFilterFlow2.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow2.TabIndex = 12
        '
        'TextBoxFilterFlow3
        '
        Me.TextBoxFilterFlow3.AcceptsReturn = True
        Me.TextBoxFilterFlow3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow3.Location = New System.Drawing.Point(359, 95)
        Me.TextBoxFilterFlow3.MaxLength = 0
        Me.TextBoxFilterFlow3.Name = "TextBoxFilterFlow3"
        Me.TextBoxFilterFlow3.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow3.TabIndex = 17
        '
        'TextBoxFilterFlow4
        '
        Me.TextBoxFilterFlow4.AcceptsReturn = True
        Me.TextBoxFilterFlow4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow4.Location = New System.Drawing.Point(359, 119)
        Me.TextBoxFilterFlow4.MaxLength = 0
        Me.TextBoxFilterFlow4.Name = "TextBoxFilterFlow4"
        Me.TextBoxFilterFlow4.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow4.TabIndex = 22
        '
        'TextBoxFilterFlow5
        '
        Me.TextBoxFilterFlow5.AcceptsReturn = True
        Me.TextBoxFilterFlow5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow5.Location = New System.Drawing.Point(359, 143)
        Me.TextBoxFilterFlow5.MaxLength = 0
        Me.TextBoxFilterFlow5.Name = "TextBoxFilterFlow5"
        Me.TextBoxFilterFlow5.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow5.TabIndex = 27
        '
        'TextBoxFilterFlow6
        '
        Me.TextBoxFilterFlow6.AcceptsReturn = True
        Me.TextBoxFilterFlow6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow6.Location = New System.Drawing.Point(359, 167)
        Me.TextBoxFilterFlow6.MaxLength = 0
        Me.TextBoxFilterFlow6.Name = "TextBoxFilterFlow6"
        Me.TextBoxFilterFlow6.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow6.TabIndex = 32
        '
        'TextBoxFilterFlow7
        '
        Me.TextBoxFilterFlow7.AcceptsReturn = True
        Me.TextBoxFilterFlow7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow7.Location = New System.Drawing.Point(359, 191)
        Me.TextBoxFilterFlow7.MaxLength = 0
        Me.TextBoxFilterFlow7.Name = "TextBoxFilterFlow7"
        Me.TextBoxFilterFlow7.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow7.TabIndex = 37
        '
        'TextBoxFilterFlow8
        '
        Me.TextBoxFilterFlow8.AcceptsReturn = True
        Me.TextBoxFilterFlow8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow8.Location = New System.Drawing.Point(359, 215)
        Me.TextBoxFilterFlow8.MaxLength = 0
        Me.TextBoxFilterFlow8.Name = "TextBoxFilterFlow8"
        Me.TextBoxFilterFlow8.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow8.TabIndex = 42
        '
        'TextBoxFilterFlow9
        '
        Me.TextBoxFilterFlow9.AcceptsReturn = True
        Me.TextBoxFilterFlow9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterFlow9.Location = New System.Drawing.Point(359, 239)
        Me.TextBoxFilterFlow9.MaxLength = 0
        Me.TextBoxFilterFlow9.Name = "TextBoxFilterFlow9"
        Me.TextBoxFilterFlow9.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterFlow9.TabIndex = 47
        '
        'ButtonApplyFilter
        '
        Me.ButtonApplyFilter.Location = New System.Drawing.Point(616, 288)
        Me.ButtonApplyFilter.Name = "ButtonApplyFilter"
        Me.ButtonApplyFilter.Size = New System.Drawing.Size(49, 25)
        Me.ButtonApplyFilter.TabIndex = 53
        Me.ButtonApplyFilter.Text = "Apply"
        Me.ButtonApplyFilter.UseVisualStyleBackColor = True
        '
        'TextBoxFilterFlags0
        '
        Me.TextBoxFilterFlags0.AcceptsReturn = True
        Me.TextBoxFilterFlags0.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags0.Location = New System.Drawing.Point(527, 23)
        Me.TextBoxFilterFlags0.MaxLength = 0
        Me.TextBoxFilterFlags0.Name = "TextBoxFilterFlags0"
        Me.TextBoxFilterFlags0.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags0.TabIndex = 3
        '
        'TextBoxFilterFlags1
        '
        Me.TextBoxFilterFlags1.AcceptsReturn = True
        Me.TextBoxFilterFlags1.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags1.Location = New System.Drawing.Point(527, 47)
        Me.TextBoxFilterFlags1.MaxLength = 0
        Me.TextBoxFilterFlags1.Name = "TextBoxFilterFlags1"
        Me.TextBoxFilterFlags1.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags1.TabIndex = 8
        '
        'TextBoxFilterFlags2
        '
        Me.TextBoxFilterFlags2.AcceptsReturn = True
        Me.TextBoxFilterFlags2.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags2.Location = New System.Drawing.Point(527, 71)
        Me.TextBoxFilterFlags2.MaxLength = 0
        Me.TextBoxFilterFlags2.Name = "TextBoxFilterFlags2"
        Me.TextBoxFilterFlags2.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags2.TabIndex = 13
        '
        'TextBoxFilterFlags3
        '
        Me.TextBoxFilterFlags3.AcceptsReturn = True
        Me.TextBoxFilterFlags3.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags3.Location = New System.Drawing.Point(527, 95)
        Me.TextBoxFilterFlags3.MaxLength = 0
        Me.TextBoxFilterFlags3.Name = "TextBoxFilterFlags3"
        Me.TextBoxFilterFlags3.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags3.TabIndex = 18
        '
        'TextBoxFilterFlags4
        '
        Me.TextBoxFilterFlags4.AcceptsReturn = True
        Me.TextBoxFilterFlags4.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags4.Location = New System.Drawing.Point(527, 119)
        Me.TextBoxFilterFlags4.MaxLength = 0
        Me.TextBoxFilterFlags4.Name = "TextBoxFilterFlags4"
        Me.TextBoxFilterFlags4.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags4.TabIndex = 23
        '
        'TextBoxFilterFlags5
        '
        Me.TextBoxFilterFlags5.AcceptsReturn = True
        Me.TextBoxFilterFlags5.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags5.Location = New System.Drawing.Point(527, 143)
        Me.TextBoxFilterFlags5.MaxLength = 0
        Me.TextBoxFilterFlags5.Name = "TextBoxFilterFlags5"
        Me.TextBoxFilterFlags5.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags5.TabIndex = 28
        '
        'TextBoxFilterFlags6
        '
        Me.TextBoxFilterFlags6.AcceptsReturn = True
        Me.TextBoxFilterFlags6.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags6.Location = New System.Drawing.Point(527, 167)
        Me.TextBoxFilterFlags6.MaxLength = 0
        Me.TextBoxFilterFlags6.Name = "TextBoxFilterFlags6"
        Me.TextBoxFilterFlags6.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags6.TabIndex = 33
        '
        'TextBoxFilterFlags7
        '
        Me.TextBoxFilterFlags7.AcceptsReturn = True
        Me.TextBoxFilterFlags7.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags7.Location = New System.Drawing.Point(527, 191)
        Me.TextBoxFilterFlags7.MaxLength = 0
        Me.TextBoxFilterFlags7.Name = "TextBoxFilterFlags7"
        Me.TextBoxFilterFlags7.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags7.TabIndex = 38
        '
        'TextBoxFilterFlags8
        '
        Me.TextBoxFilterFlags8.AcceptsReturn = True
        Me.TextBoxFilterFlags8.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags8.Location = New System.Drawing.Point(527, 215)
        Me.TextBoxFilterFlags8.MaxLength = 0
        Me.TextBoxFilterFlags8.Name = "TextBoxFilterFlags8"
        Me.TextBoxFilterFlags8.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags8.TabIndex = 43
        '
        'TextBoxFilterFlags9
        '
        Me.TextBoxFilterFlags9.AcceptsReturn = True
        Me.TextBoxFilterFlags9.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFilterFlags9.Location = New System.Drawing.Point(527, 239)
        Me.TextBoxFilterFlags9.MaxLength = 0
        Me.TextBoxFilterFlags9.Name = "TextBoxFilterFlags9"
        Me.TextBoxFilterFlags9.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFilterFlags9.TabIndex = 48
        '
        'ComboBoxFilterType0
        '
        Me.ComboBoxFilterType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType0.Location = New System.Drawing.Point(615, 23)
        Me.ComboBoxFilterType0.Name = "ComboBoxFilterType0"
        Me.ComboBoxFilterType0.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType0.TabIndex = 4
        '
        'ComboBoxFilterType1
        '
        Me.ComboBoxFilterType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType1.Location = New System.Drawing.Point(615, 47)
        Me.ComboBoxFilterType1.Name = "ComboBoxFilterType1"
        Me.ComboBoxFilterType1.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType1.TabIndex = 9
        '
        'ComboBoxFilterType2
        '
        Me.ComboBoxFilterType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType2.Location = New System.Drawing.Point(615, 71)
        Me.ComboBoxFilterType2.Name = "ComboBoxFilterType2"
        Me.ComboBoxFilterType2.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType2.TabIndex = 14
        '
        'ComboBoxFilterType3
        '
        Me.ComboBoxFilterType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType3.Location = New System.Drawing.Point(615, 95)
        Me.ComboBoxFilterType3.Name = "ComboBoxFilterType3"
        Me.ComboBoxFilterType3.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType3.TabIndex = 19
        '
        'ComboBoxFilterType4
        '
        Me.ComboBoxFilterType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType4.Location = New System.Drawing.Point(615, 119)
        Me.ComboBoxFilterType4.Name = "ComboBoxFilterType4"
        Me.ComboBoxFilterType4.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType4.TabIndex = 24
        '
        'ComboBoxFilterType5
        '
        Me.ComboBoxFilterType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType5.Location = New System.Drawing.Point(615, 143)
        Me.ComboBoxFilterType5.Name = "ComboBoxFilterType5"
        Me.ComboBoxFilterType5.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType5.TabIndex = 29
        '
        'ComboBoxFilterType6
        '
        Me.ComboBoxFilterType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType6.Location = New System.Drawing.Point(615, 167)
        Me.ComboBoxFilterType6.Name = "ComboBoxFilterType6"
        Me.ComboBoxFilterType6.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType6.TabIndex = 34
        '
        'ComboBoxFilterType7
        '
        Me.ComboBoxFilterType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType7.Location = New System.Drawing.Point(615, 191)
        Me.ComboBoxFilterType7.Name = "ComboBoxFilterType7"
        Me.ComboBoxFilterType7.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType7.TabIndex = 39
        '
        'ComboBoxFilterType8
        '
        Me.ComboBoxFilterType8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType8.Location = New System.Drawing.Point(615, 215)
        Me.ComboBoxFilterType8.Name = "ComboBoxFilterType8"
        Me.ComboBoxFilterType8.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType8.TabIndex = 44
        '
        'ComboBoxFilterType9
        '
        Me.ComboBoxFilterType9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType9.Location = New System.Drawing.Point(615, 239)
        Me.ComboBoxFilterType9.Name = "ComboBoxFilterType9"
        Me.ComboBoxFilterType9.Size = New System.Drawing.Size(57, 21)
        Me.ComboBoxFilterType9.TabIndex = 49
        '
        'TextBoxFilterPatt0
        '
        Me.TextBoxFilterPatt0.AcceptsReturn = True
        Me.TextBoxFilterPatt0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt0.Location = New System.Drawing.Point(191, 23)
        Me.TextBoxFilterPatt0.MaxLength = 0
        Me.TextBoxFilterPatt0.Name = "TextBoxFilterPatt0"
        Me.TextBoxFilterPatt0.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt0.TabIndex = 1
        '
        'TextBoxFilterPatt1
        '
        Me.TextBoxFilterPatt1.AcceptsReturn = True
        Me.TextBoxFilterPatt1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt1.Location = New System.Drawing.Point(191, 47)
        Me.TextBoxFilterPatt1.MaxLength = 0
        Me.TextBoxFilterPatt1.Name = "TextBoxFilterPatt1"
        Me.TextBoxFilterPatt1.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt1.TabIndex = 6
        '
        'TextBoxFilterPatt2
        '
        Me.TextBoxFilterPatt2.AcceptsReturn = True
        Me.TextBoxFilterPatt2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt2.Location = New System.Drawing.Point(191, 71)
        Me.TextBoxFilterPatt2.MaxLength = 0
        Me.TextBoxFilterPatt2.Name = "TextBoxFilterPatt2"
        Me.TextBoxFilterPatt2.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt2.TabIndex = 11
        '
        'TextBoxFilterPatt3
        '
        Me.TextBoxFilterPatt3.AcceptsReturn = True
        Me.TextBoxFilterPatt3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt3.Location = New System.Drawing.Point(191, 95)
        Me.TextBoxFilterPatt3.MaxLength = 0
        Me.TextBoxFilterPatt3.Name = "TextBoxFilterPatt3"
        Me.TextBoxFilterPatt3.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt3.TabIndex = 16
        '
        'TextBoxFilterPatt4
        '
        Me.TextBoxFilterPatt4.AcceptsReturn = True
        Me.TextBoxFilterPatt4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt4.Location = New System.Drawing.Point(191, 119)
        Me.TextBoxFilterPatt4.MaxLength = 0
        Me.TextBoxFilterPatt4.Name = "TextBoxFilterPatt4"
        Me.TextBoxFilterPatt4.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt4.TabIndex = 21
        '
        'TextBoxFilterPatt5
        '
        Me.TextBoxFilterPatt5.AcceptsReturn = True
        Me.TextBoxFilterPatt5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt5.Location = New System.Drawing.Point(191, 143)
        Me.TextBoxFilterPatt5.MaxLength = 0
        Me.TextBoxFilterPatt5.Name = "TextBoxFilterPatt5"
        Me.TextBoxFilterPatt5.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt5.TabIndex = 26
        '
        'TextBoxFilterPatt6
        '
        Me.TextBoxFilterPatt6.AcceptsReturn = True
        Me.TextBoxFilterPatt6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt6.Location = New System.Drawing.Point(191, 167)
        Me.TextBoxFilterPatt6.MaxLength = 0
        Me.TextBoxFilterPatt6.Name = "TextBoxFilterPatt6"
        Me.TextBoxFilterPatt6.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt6.TabIndex = 31
        '
        'TextBoxFilterPatt7
        '
        Me.TextBoxFilterPatt7.AcceptsReturn = True
        Me.TextBoxFilterPatt7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt7.Location = New System.Drawing.Point(191, 191)
        Me.TextBoxFilterPatt7.MaxLength = 0
        Me.TextBoxFilterPatt7.Name = "TextBoxFilterPatt7"
        Me.TextBoxFilterPatt7.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt7.TabIndex = 36
        '
        'TextBoxFilterPatt8
        '
        Me.TextBoxFilterPatt8.AcceptsReturn = True
        Me.TextBoxFilterPatt8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt8.Location = New System.Drawing.Point(191, 215)
        Me.TextBoxFilterPatt8.MaxLength = 0
        Me.TextBoxFilterPatt8.Name = "TextBoxFilterPatt8"
        Me.TextBoxFilterPatt8.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt8.TabIndex = 41
        '
        'TextBoxFilterPatt9
        '
        Me.TextBoxFilterPatt9.AcceptsReturn = True
        Me.TextBoxFilterPatt9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterPatt9.Location = New System.Drawing.Point(191, 239)
        Me.TextBoxFilterPatt9.MaxLength = 0
        Me.TextBoxFilterPatt9.Name = "TextBoxFilterPatt9"
        Me.TextBoxFilterPatt9.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterPatt9.TabIndex = 46
        '
        'TextBoxFilterMask0
        '
        Me.TextBoxFilterMask0.AcceptsReturn = True
        Me.TextBoxFilterMask0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask0.Location = New System.Drawing.Point(23, 23)
        Me.TextBoxFilterMask0.MaxLength = 0
        Me.TextBoxFilterMask0.Name = "TextBoxFilterMask0"
        Me.TextBoxFilterMask0.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask0.TabIndex = 0
        '
        'TextBoxFilterMask1
        '
        Me.TextBoxFilterMask1.AcceptsReturn = True
        Me.TextBoxFilterMask1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask1.Location = New System.Drawing.Point(23, 47)
        Me.TextBoxFilterMask1.MaxLength = 0
        Me.TextBoxFilterMask1.Name = "TextBoxFilterMask1"
        Me.TextBoxFilterMask1.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask1.TabIndex = 5
        '
        'TextBoxFilterMask2
        '
        Me.TextBoxFilterMask2.AcceptsReturn = True
        Me.TextBoxFilterMask2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask2.Location = New System.Drawing.Point(23, 71)
        Me.TextBoxFilterMask2.MaxLength = 0
        Me.TextBoxFilterMask2.Name = "TextBoxFilterMask2"
        Me.TextBoxFilterMask2.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask2.TabIndex = 10
        '
        'TextBoxFilterMask3
        '
        Me.TextBoxFilterMask3.AcceptsReturn = True
        Me.TextBoxFilterMask3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask3.Location = New System.Drawing.Point(23, 95)
        Me.TextBoxFilterMask3.MaxLength = 0
        Me.TextBoxFilterMask3.Name = "TextBoxFilterMask3"
        Me.TextBoxFilterMask3.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask3.TabIndex = 15
        '
        'TextBoxFilterMask4
        '
        Me.TextBoxFilterMask4.AcceptsReturn = True
        Me.TextBoxFilterMask4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask4.Location = New System.Drawing.Point(23, 119)
        Me.TextBoxFilterMask4.MaxLength = 0
        Me.TextBoxFilterMask4.Name = "TextBoxFilterMask4"
        Me.TextBoxFilterMask4.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask4.TabIndex = 20
        '
        'TextBoxFilterMask5
        '
        Me.TextBoxFilterMask5.AcceptsReturn = True
        Me.TextBoxFilterMask5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask5.Location = New System.Drawing.Point(23, 143)
        Me.TextBoxFilterMask5.MaxLength = 0
        Me.TextBoxFilterMask5.Name = "TextBoxFilterMask5"
        Me.TextBoxFilterMask5.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask5.TabIndex = 25
        '
        'TextBoxFilterMask6
        '
        Me.TextBoxFilterMask6.AcceptsReturn = True
        Me.TextBoxFilterMask6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask6.Location = New System.Drawing.Point(23, 167)
        Me.TextBoxFilterMask6.MaxLength = 0
        Me.TextBoxFilterMask6.Name = "TextBoxFilterMask6"
        Me.TextBoxFilterMask6.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask6.TabIndex = 30
        '
        'TextBoxFilterMask7
        '
        Me.TextBoxFilterMask7.AcceptsReturn = True
        Me.TextBoxFilterMask7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask7.Location = New System.Drawing.Point(23, 191)
        Me.TextBoxFilterMask7.MaxLength = 0
        Me.TextBoxFilterMask7.Name = "TextBoxFilterMask7"
        Me.TextBoxFilterMask7.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask7.TabIndex = 35
        '
        'TextBoxFilterMask8
        '
        Me.TextBoxFilterMask8.AcceptsReturn = True
        Me.TextBoxFilterMask8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask8.Location = New System.Drawing.Point(23, 215)
        Me.TextBoxFilterMask8.MaxLength = 0
        Me.TextBoxFilterMask8.Name = "TextBoxFilterMask8"
        Me.TextBoxFilterMask8.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask8.TabIndex = 40
        '
        'TextBoxFilterMask9
        '
        Me.TextBoxFilterMask9.AcceptsReturn = True
        Me.TextBoxFilterMask9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFilterMask9.Location = New System.Drawing.Point(23, 239)
        Me.TextBoxFilterMask9.MaxLength = 0
        Me.TextBoxFilterMask9.Name = "TextBoxFilterMask9"
        Me.TextBoxFilterMask9.Size = New System.Drawing.Size(161, 20)
        Me.TextBoxFilterMask9.TabIndex = 45
        '
        'LabelChannel3
        '
        Me.LabelChannel3.Location = New System.Drawing.Point(524, 336)
        Me.LabelChannel3.Name = "LabelChannel3"
        Me.LabelChannel3.Size = New System.Drawing.Size(49, 17)
        Me.LabelChannel3.TabIndex = 57
        Me.LabelChannel3.Text = "Channel:"
        '
        'LabelDeviceCombo3
        '
        Me.LabelDeviceCombo3.Location = New System.Drawing.Point(395, 336)
        Me.LabelDeviceCombo3.Name = "LabelDeviceCombo3"
        Me.LabelDeviceCombo3.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo3.TabIndex = 56
        Me.LabelDeviceCombo3.Text = "Device:"
        '
        'LabelFilterIds
        '
        Me.LabelFilterIds.Location = New System.Drawing.Point(671, 7)
        Me.LabelFilterIds.Name = "LabelFilterIds"
        Me.LabelFilterIds.Size = New System.Drawing.Size(43, 17)
        Me.LabelFilterIds.TabIndex = 83
        Me.LabelFilterIds.Text = "En:"
        '
        'LabelFilterId0
        '
        Me.LabelFilterId0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId0.Location = New System.Drawing.Point(671, 23)
        Me.LabelFilterId0.Name = "LabelFilterId0"
        Me.LabelFilterId0.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId0.TabIndex = 68
        Me.LabelFilterId0.Text = "*"
        Me.LabelFilterId0.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId1
        '
        Me.LabelFilterId1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId1.Location = New System.Drawing.Point(671, 47)
        Me.LabelFilterId1.Name = "LabelFilterId1"
        Me.LabelFilterId1.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId1.TabIndex = 69
        Me.LabelFilterId1.Text = "*"
        Me.LabelFilterId1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId2
        '
        Me.LabelFilterId2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId2.Location = New System.Drawing.Point(671, 71)
        Me.LabelFilterId2.Name = "LabelFilterId2"
        Me.LabelFilterId2.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId2.TabIndex = 70
        Me.LabelFilterId2.Text = "*"
        Me.LabelFilterId2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId3
        '
        Me.LabelFilterId3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId3.Location = New System.Drawing.Point(671, 95)
        Me.LabelFilterId3.Name = "LabelFilterId3"
        Me.LabelFilterId3.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId3.TabIndex = 71
        Me.LabelFilterId3.Text = "*"
        Me.LabelFilterId3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId4
        '
        Me.LabelFilterId4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId4.Location = New System.Drawing.Point(671, 119)
        Me.LabelFilterId4.Name = "LabelFilterId4"
        Me.LabelFilterId4.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId4.TabIndex = 72
        Me.LabelFilterId4.Text = "*"
        Me.LabelFilterId4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId5
        '
        Me.LabelFilterId5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId5.Location = New System.Drawing.Point(671, 143)
        Me.LabelFilterId5.Name = "LabelFilterId5"
        Me.LabelFilterId5.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId5.TabIndex = 73
        Me.LabelFilterId5.Text = "*"
        Me.LabelFilterId5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId6
        '
        Me.LabelFilterId6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId6.Location = New System.Drawing.Point(671, 167)
        Me.LabelFilterId6.Name = "LabelFilterId6"
        Me.LabelFilterId6.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId6.TabIndex = 74
        Me.LabelFilterId6.Text = "*"
        Me.LabelFilterId6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId7
        '
        Me.LabelFilterId7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId7.Location = New System.Drawing.Point(671, 191)
        Me.LabelFilterId7.Name = "LabelFilterId7"
        Me.LabelFilterId7.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId7.TabIndex = 75
        Me.LabelFilterId7.Text = "*"
        Me.LabelFilterId7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId8
        '
        Me.LabelFilterId8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId8.Location = New System.Drawing.Point(671, 215)
        Me.LabelFilterId8.Name = "LabelFilterId8"
        Me.LabelFilterId8.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId8.TabIndex = 76
        Me.LabelFilterId8.Text = "*"
        Me.LabelFilterId8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterId9
        '
        Me.LabelFilterId9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFilterId9.Location = New System.Drawing.Point(671, 239)
        Me.LabelFilterId9.Name = "LabelFilterId9"
        Me.LabelFilterId9.Size = New System.Drawing.Size(22, 17)
        Me.LabelFilterId9.TabIndex = 77
        Me.LabelFilterId9.Text = "*"
        Me.LabelFilterId9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFilterType
        '
        Me.LabelFilterType.Location = New System.Drawing.Point(615, 7)
        Me.LabelFilterType.Name = "LabelFilterType"
        Me.LabelFilterType.Size = New System.Drawing.Size(57, 17)
        Me.LabelFilterType.TabIndex = 82
        Me.LabelFilterType.Text = "Filter:"
        '
        'LabelFilterFlags
        '
        Me.LabelFilterFlags.Location = New System.Drawing.Point(527, 7)
        Me.LabelFilterFlags.Name = "LabelFilterFlags"
        Me.LabelFilterFlags.Size = New System.Drawing.Size(63, 17)
        Me.LabelFilterFlags.TabIndex = 81
        Me.LabelFilterFlags.Text = "Tx Flags:"
        '
        'LabelFilterFlow
        '
        Me.LabelFilterFlow.Location = New System.Drawing.Point(359, 7)
        Me.LabelFilterFlow.Name = "LabelFilterFlow"
        Me.LabelFilterFlow.Size = New System.Drawing.Size(137, 17)
        Me.LabelFilterFlow.TabIndex = 80
        Me.LabelFilterFlow.Text = "Flow Control:"
        '
        'LabelFilterPatt
        '
        Me.LabelFilterPatt.Location = New System.Drawing.Point(191, 7)
        Me.LabelFilterPatt.Name = "LabelFilterPatt"
        Me.LabelFilterPatt.Size = New System.Drawing.Size(140, 17)
        Me.LabelFilterPatt.TabIndex = 79
        Me.LabelFilterPatt.Text = "Pattern:"
        '
        'LabelFilterMask
        '
        Me.LabelFilterMask.Location = New System.Drawing.Point(23, 7)
        Me.LabelFilterMask.Name = "LabelFilterMask"
        Me.LabelFilterMask.Size = New System.Drawing.Size(161, 17)
        Me.LabelFilterMask.TabIndex = 78
        Me.LabelFilterMask.Text = "Mask:"
        '
        'LabelFilter0
        '
        Me.LabelFilter0.Location = New System.Drawing.Point(7, 23)
        Me.LabelFilter0.Name = "LabelFilter0"
        Me.LabelFilter0.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter0.TabIndex = 58
        Me.LabelFilter0.Text = "0:"
        '
        'LabelFilter1
        '
        Me.LabelFilter1.Location = New System.Drawing.Point(7, 47)
        Me.LabelFilter1.Name = "LabelFilter1"
        Me.LabelFilter1.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter1.TabIndex = 59
        Me.LabelFilter1.Text = "1:"
        '
        'LabelFilter2
        '
        Me.LabelFilter2.Location = New System.Drawing.Point(7, 71)
        Me.LabelFilter2.Name = "LabelFilter2"
        Me.LabelFilter2.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter2.TabIndex = 60
        Me.LabelFilter2.Text = "2:"
        '
        'LabelFilter3
        '
        Me.LabelFilter3.Location = New System.Drawing.Point(7, 95)
        Me.LabelFilter3.Name = "LabelFilter3"
        Me.LabelFilter3.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter3.TabIndex = 61
        Me.LabelFilter3.Text = "3:"
        '
        'LabelFilter4
        '
        Me.LabelFilter4.Location = New System.Drawing.Point(7, 119)
        Me.LabelFilter4.Name = "LabelFilter4"
        Me.LabelFilter4.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter4.TabIndex = 62
        Me.LabelFilter4.Text = "4:"
        '
        'LabelFilter5
        '
        Me.LabelFilter5.Location = New System.Drawing.Point(7, 143)
        Me.LabelFilter5.Name = "LabelFilter5"
        Me.LabelFilter5.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter5.TabIndex = 63
        Me.LabelFilter5.Text = "5:"
        '
        'LabelFilter6
        '
        Me.LabelFilter6.Location = New System.Drawing.Point(7, 167)
        Me.LabelFilter6.Name = "LabelFilter6"
        Me.LabelFilter6.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter6.TabIndex = 64
        Me.LabelFilter6.Text = "6:"
        '
        'LabelFilter7
        '
        Me.LabelFilter7.Location = New System.Drawing.Point(7, 191)
        Me.LabelFilter7.Name = "LabelFilter7"
        Me.LabelFilter7.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter7.TabIndex = 65
        Me.LabelFilter7.Text = "7:"
        '
        'LabelFilter8
        '
        Me.LabelFilter8.Location = New System.Drawing.Point(7, 215)
        Me.LabelFilter8.Name = "LabelFilter8"
        Me.LabelFilter8.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter8.TabIndex = 66
        Me.LabelFilter8.Text = "8:"
        '
        'LabelFilter9
        '
        Me.LabelFilter9.Location = New System.Drawing.Point(7, 239)
        Me.LabelFilter9.Name = "LabelFilter9"
        Me.LabelFilter9.Size = New System.Drawing.Size(9, 17)
        Me.LabelFilter9.TabIndex = 67
        Me.LabelFilter9.Text = "9:"
        '
        'TabPageConfig
        '
        Me.TabPageConfig.Controls.Add(Me.GroupBoxAnalogConfig)
        Me.TabPageConfig.Controls.Add(Me.ComboBoxAvailableDevice)
        Me.TabPageConfig.Controls.Add(Me.ComboBoxAvailableChannel)
        Me.TabPageConfig.Controls.Add(Me.ButtonSetConfig)
        Me.TabPageConfig.Controls.Add(Me.ButtonClearConfig)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal0)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal1)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal2)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal3)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal4)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal5)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal6)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal7)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal8)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal9)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal10)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal11)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal12)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal13)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal14)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal15)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal16)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal17)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal18)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal19)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal20)
        Me.TabPageConfig.Controls.Add(Me.LabelParamVal21)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName0)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName1)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName2)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName3)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName4)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName5)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName6)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName7)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName8)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName9)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName10)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName11)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName12)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName13)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName14)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName15)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName16)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName17)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName18)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName19)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName20)
        Me.TabPageConfig.Controls.Add(Me.LabelParameterName21)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal0)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal1)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal2)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal3)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal4)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal5)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal6)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal7)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal8)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal9)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal10)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal11)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal12)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal13)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal14)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal15)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal16)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal17)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal18)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal19)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal20)
        Me.TabPageConfig.Controls.Add(Me.TextBoxParamVal21)
        Me.TabPageConfig.Controls.Add(Me.LabelChannel4)
        Me.TabPageConfig.Controls.Add(Me.LabelDeviceCombo4)
        Me.TabPageConfig.Controls.Add(Me.LabelParameters0)
        Me.TabPageConfig.Controls.Add(Me.LabelValues0)
        Me.TabPageConfig.Controls.Add(Me.LabelMod0)
        Me.TabPageConfig.Controls.Add(Me.LabelParameters1)
        Me.TabPageConfig.Controls.Add(Me.LabelValues1)
        Me.TabPageConfig.Controls.Add(Me.LabelMod1)
        Me.TabPageConfig.Controls.Add(Me.ComboAvailableBoxLocator4)
        Me.TabPageConfig.Controls.Add(Me.ComboAvailableChannelLocator4)
        Me.TabPageConfig.Location = New System.Drawing.Point(4, 22)
        Me.TabPageConfig.Name = "TabPageConfig"
        Me.TabPageConfig.Size = New System.Drawing.Size(714, 395)
        Me.TabPageConfig.TabIndex = 4
        Me.TabPageConfig.Text = "Config"
        '
        'GroupBoxAnalogConfig
        '
        Me.GroupBoxAnalogConfig.Controls.Add(Me.LabelMaxSample)
        Me.GroupBoxAnalogConfig.Controls.Add(Me.LabelAnalogRate)
        Me.GroupBoxAnalogConfig.Controls.Add(Me.TextBoxAnalogRate)
        Me.GroupBoxAnalogConfig.Controls.Add(Me.PanelAudioChannel)
        Me.GroupBoxAnalogConfig.Controls.Add(Me.ComboBoxAnalogRateMode)
        Me.GroupBoxAnalogConfig.Location = New System.Drawing.Point(33, 290)
        Me.GroupBoxAnalogConfig.Name = "GroupBoxAnalogConfig"
        Me.GroupBoxAnalogConfig.Size = New System.Drawing.Size(293, 193)
        Me.GroupBoxAnalogConfig.TabIndex = 24
        Me.GroupBoxAnalogConfig.TabStop = False
        Me.GroupBoxAnalogConfig.Text = "Analog Channels and Rate:"
        Me.GroupBoxAnalogConfig.Visible = False
        '
        'LabelMaxSample
        '
        Me.LabelMaxSample.AutoSize = True
        Me.LabelMaxSample.Location = New System.Drawing.Point(23, 169)
        Me.LabelMaxSample.Name = "LabelMaxSample"
        Me.LabelMaxSample.Size = New System.Drawing.Size(87, 13)
        Me.LabelMaxSample.TabIndex = 16
        Me.LabelMaxSample.Text = "Max sample rate:"
        '
        'LabelAnalogRate
        '
        Me.LabelAnalogRate.AutoSize = True
        Me.LabelAnalogRate.Location = New System.Drawing.Point(20, 123)
        Me.LabelAnalogRate.Name = "LabelAnalogRate"
        Me.LabelAnalogRate.Size = New System.Drawing.Size(33, 13)
        Me.LabelAnalogRate.TabIndex = 15
        Me.LabelAnalogRate.Text = "Rate:"
        '
        'TextBoxAnalogRate
        '
        Me.TextBoxAnalogRate.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxAnalogRate.Location = New System.Drawing.Point(23, 139)
        Me.TextBoxAnalogRate.Name = "TextBoxAnalogRate"
        Me.TextBoxAnalogRate.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAnalogRate.TabIndex = 13
        '
        'PanelAudioChannel
        '
        Me.PanelAudioChannel.AutoScroll = True
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH0)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH1)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH2)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH3)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH4)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH5)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH6)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH7)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH8)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH9)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH10)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH11)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH12)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH13)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH14)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH15)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH16)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH17)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH18)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH19)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH20)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH21)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH22)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH23)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH24)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH25)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH26)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH27)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH28)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH29)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH30)
        Me.PanelAudioChannel.Controls.Add(Me.CheckBoxCH31)
        Me.PanelAudioChannel.Location = New System.Drawing.Point(6, 19)
        Me.PanelAudioChannel.Name = "PanelAudioChannel"
        Me.PanelAudioChannel.Size = New System.Drawing.Size(269, 95)
        Me.PanelAudioChannel.TabIndex = 0
        '
        'CheckBoxCH0
        '
        Me.CheckBoxCH0.AutoSize = True
        Me.CheckBoxCH0.Location = New System.Drawing.Point(4, 4)
        Me.CheckBoxCH0.Name = "CheckBoxCH0"
        Me.CheckBoxCH0.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH0.TabIndex = 0
        Me.CheckBoxCH0.Text = "Ch 1"
        Me.CheckBoxCH0.UseVisualStyleBackColor = True
        '
        'CheckBoxCH1
        '
        Me.CheckBoxCH1.AutoSize = True
        Me.CheckBoxCH1.Location = New System.Drawing.Point(63, 4)
        Me.CheckBoxCH1.Name = "CheckBoxCH1"
        Me.CheckBoxCH1.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH1.TabIndex = 1
        Me.CheckBoxCH1.Text = "Ch 2"
        Me.CheckBoxCH1.UseVisualStyleBackColor = True
        '
        'CheckBoxCH2
        '
        Me.CheckBoxCH2.AutoSize = True
        Me.CheckBoxCH2.Location = New System.Drawing.Point(122, 4)
        Me.CheckBoxCH2.Name = "CheckBoxCH2"
        Me.CheckBoxCH2.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH2.TabIndex = 2
        Me.CheckBoxCH2.Text = "Ch 3"
        Me.CheckBoxCH2.UseVisualStyleBackColor = True
        '
        'CheckBoxCH3
        '
        Me.CheckBoxCH3.AutoSize = True
        Me.CheckBoxCH3.Location = New System.Drawing.Point(181, 4)
        Me.CheckBoxCH3.Name = "CheckBoxCH3"
        Me.CheckBoxCH3.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH3.TabIndex = 3
        Me.CheckBoxCH3.Text = "Ch 4"
        Me.CheckBoxCH3.UseVisualStyleBackColor = True
        '
        'CheckBoxCH4
        '
        Me.CheckBoxCH4.AutoSize = True
        Me.CheckBoxCH4.Location = New System.Drawing.Point(4, 27)
        Me.CheckBoxCH4.Name = "CheckBoxCH4"
        Me.CheckBoxCH4.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH4.TabIndex = 4
        Me.CheckBoxCH4.Text = "Ch 5"
        Me.CheckBoxCH4.UseVisualStyleBackColor = True
        '
        'CheckBoxCH5
        '
        Me.CheckBoxCH5.AutoSize = True
        Me.CheckBoxCH5.Location = New System.Drawing.Point(63, 27)
        Me.CheckBoxCH5.Name = "CheckBoxCH5"
        Me.CheckBoxCH5.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH5.TabIndex = 5
        Me.CheckBoxCH5.Text = "Ch 6"
        Me.CheckBoxCH5.UseVisualStyleBackColor = True
        '
        'CheckBoxCH6
        '
        Me.CheckBoxCH6.AutoSize = True
        Me.CheckBoxCH6.Location = New System.Drawing.Point(122, 27)
        Me.CheckBoxCH6.Name = "CheckBoxCH6"
        Me.CheckBoxCH6.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH6.TabIndex = 6
        Me.CheckBoxCH6.Text = "Ch 7"
        Me.CheckBoxCH6.UseVisualStyleBackColor = True
        '
        'CheckBoxCH7
        '
        Me.CheckBoxCH7.AutoSize = True
        Me.CheckBoxCH7.Location = New System.Drawing.Point(181, 27)
        Me.CheckBoxCH7.Name = "CheckBoxCH7"
        Me.CheckBoxCH7.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH7.TabIndex = 7
        Me.CheckBoxCH7.Text = "Ch 8"
        Me.CheckBoxCH7.UseVisualStyleBackColor = True
        '
        'CheckBoxCH8
        '
        Me.CheckBoxCH8.AutoSize = True
        Me.CheckBoxCH8.Location = New System.Drawing.Point(4, 50)
        Me.CheckBoxCH8.Name = "CheckBoxCH8"
        Me.CheckBoxCH8.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxCH8.TabIndex = 8
        Me.CheckBoxCH8.Text = "Ch 9"
        Me.CheckBoxCH8.UseVisualStyleBackColor = True
        '
        'CheckBoxCH9
        '
        Me.CheckBoxCH9.AutoSize = True
        Me.CheckBoxCH9.Location = New System.Drawing.Point(63, 50)
        Me.CheckBoxCH9.Name = "CheckBoxCH9"
        Me.CheckBoxCH9.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH9.TabIndex = 9
        Me.CheckBoxCH9.Text = "Ch 10"
        Me.CheckBoxCH9.UseVisualStyleBackColor = True
        '
        'CheckBoxCH10
        '
        Me.CheckBoxCH10.AutoSize = True
        Me.CheckBoxCH10.Location = New System.Drawing.Point(122, 50)
        Me.CheckBoxCH10.Name = "CheckBoxCH10"
        Me.CheckBoxCH10.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH10.TabIndex = 10
        Me.CheckBoxCH10.Text = "Ch 11"
        Me.CheckBoxCH10.UseVisualStyleBackColor = True
        '
        'CheckBoxCH11
        '
        Me.CheckBoxCH11.AutoSize = True
        Me.CheckBoxCH11.Location = New System.Drawing.Point(181, 50)
        Me.CheckBoxCH11.Name = "CheckBoxCH11"
        Me.CheckBoxCH11.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH11.TabIndex = 11
        Me.CheckBoxCH11.Text = "Ch 12"
        Me.CheckBoxCH11.UseVisualStyleBackColor = True
        '
        'CheckBoxCH12
        '
        Me.CheckBoxCH12.AutoSize = True
        Me.CheckBoxCH12.Location = New System.Drawing.Point(4, 73)
        Me.CheckBoxCH12.Name = "CheckBoxCH12"
        Me.CheckBoxCH12.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH12.TabIndex = 12
        Me.CheckBoxCH12.Text = "Ch 13"
        Me.CheckBoxCH12.UseVisualStyleBackColor = True
        '
        'CheckBoxCH13
        '
        Me.CheckBoxCH13.AutoSize = True
        Me.CheckBoxCH13.Location = New System.Drawing.Point(63, 73)
        Me.CheckBoxCH13.Name = "CheckBoxCH13"
        Me.CheckBoxCH13.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH13.TabIndex = 13
        Me.CheckBoxCH13.Text = "Ch 14"
        Me.CheckBoxCH13.UseVisualStyleBackColor = True
        '
        'CheckBoxCH14
        '
        Me.CheckBoxCH14.AutoSize = True
        Me.CheckBoxCH14.Location = New System.Drawing.Point(122, 73)
        Me.CheckBoxCH14.Name = "CheckBoxCH14"
        Me.CheckBoxCH14.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH14.TabIndex = 14
        Me.CheckBoxCH14.Text = "Ch 15"
        Me.CheckBoxCH14.UseVisualStyleBackColor = True
        '
        'CheckBoxCH15
        '
        Me.CheckBoxCH15.AutoSize = True
        Me.CheckBoxCH15.Location = New System.Drawing.Point(181, 73)
        Me.CheckBoxCH15.Name = "CheckBoxCH15"
        Me.CheckBoxCH15.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH15.TabIndex = 15
        Me.CheckBoxCH15.Text = "Ch 16"
        Me.CheckBoxCH15.UseVisualStyleBackColor = True
        '
        'CheckBoxCH16
        '
        Me.CheckBoxCH16.AutoSize = True
        Me.CheckBoxCH16.Location = New System.Drawing.Point(4, 96)
        Me.CheckBoxCH16.Name = "CheckBoxCH16"
        Me.CheckBoxCH16.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH16.TabIndex = 16
        Me.CheckBoxCH16.Text = "Ch 17"
        Me.CheckBoxCH16.UseVisualStyleBackColor = True
        '
        'CheckBoxCH17
        '
        Me.CheckBoxCH17.AutoSize = True
        Me.CheckBoxCH17.Location = New System.Drawing.Point(63, 96)
        Me.CheckBoxCH17.Name = "CheckBoxCH17"
        Me.CheckBoxCH17.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH17.TabIndex = 17
        Me.CheckBoxCH17.Text = "Ch 18"
        Me.CheckBoxCH17.UseVisualStyleBackColor = True
        '
        'CheckBoxCH18
        '
        Me.CheckBoxCH18.AutoSize = True
        Me.CheckBoxCH18.Location = New System.Drawing.Point(122, 96)
        Me.CheckBoxCH18.Name = "CheckBoxCH18"
        Me.CheckBoxCH18.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH18.TabIndex = 18
        Me.CheckBoxCH18.Text = "Ch 19"
        Me.CheckBoxCH18.UseVisualStyleBackColor = True
        '
        'CheckBoxCH19
        '
        Me.CheckBoxCH19.AutoSize = True
        Me.CheckBoxCH19.Location = New System.Drawing.Point(181, 96)
        Me.CheckBoxCH19.Name = "CheckBoxCH19"
        Me.CheckBoxCH19.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH19.TabIndex = 19
        Me.CheckBoxCH19.Text = "Ch 20"
        Me.CheckBoxCH19.UseVisualStyleBackColor = True
        '
        'CheckBoxCH20
        '
        Me.CheckBoxCH20.AutoSize = True
        Me.CheckBoxCH20.Location = New System.Drawing.Point(4, 119)
        Me.CheckBoxCH20.Name = "CheckBoxCH20"
        Me.CheckBoxCH20.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH20.TabIndex = 20
        Me.CheckBoxCH20.Text = "Ch 21"
        Me.CheckBoxCH20.UseVisualStyleBackColor = True
        '
        'CheckBoxCH21
        '
        Me.CheckBoxCH21.AutoSize = True
        Me.CheckBoxCH21.Location = New System.Drawing.Point(63, 119)
        Me.CheckBoxCH21.Name = "CheckBoxCH21"
        Me.CheckBoxCH21.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH21.TabIndex = 21
        Me.CheckBoxCH21.Text = "Ch 22"
        Me.CheckBoxCH21.UseVisualStyleBackColor = True
        '
        'CheckBoxCH22
        '
        Me.CheckBoxCH22.AutoSize = True
        Me.CheckBoxCH22.Location = New System.Drawing.Point(122, 119)
        Me.CheckBoxCH22.Name = "CheckBoxCH22"
        Me.CheckBoxCH22.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH22.TabIndex = 22
        Me.CheckBoxCH22.Text = "Ch 23"
        Me.CheckBoxCH22.UseVisualStyleBackColor = True
        '
        'CheckBoxCH23
        '
        Me.CheckBoxCH23.AutoSize = True
        Me.CheckBoxCH23.Location = New System.Drawing.Point(181, 119)
        Me.CheckBoxCH23.Name = "CheckBoxCH23"
        Me.CheckBoxCH23.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH23.TabIndex = 23
        Me.CheckBoxCH23.Text = "Ch 24"
        Me.CheckBoxCH23.UseVisualStyleBackColor = True
        '
        'CheckBoxCH24
        '
        Me.CheckBoxCH24.AutoSize = True
        Me.CheckBoxCH24.Location = New System.Drawing.Point(4, 142)
        Me.CheckBoxCH24.Name = "CheckBoxCH24"
        Me.CheckBoxCH24.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH24.TabIndex = 24
        Me.CheckBoxCH24.Text = "Ch 25"
        Me.CheckBoxCH24.UseVisualStyleBackColor = True
        '
        'CheckBoxCH25
        '
        Me.CheckBoxCH25.AutoSize = True
        Me.CheckBoxCH25.Location = New System.Drawing.Point(63, 142)
        Me.CheckBoxCH25.Name = "CheckBoxCH25"
        Me.CheckBoxCH25.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH25.TabIndex = 25
        Me.CheckBoxCH25.Text = "Ch 26"
        Me.CheckBoxCH25.UseVisualStyleBackColor = True
        '
        'CheckBoxCH26
        '
        Me.CheckBoxCH26.AutoSize = True
        Me.CheckBoxCH26.Location = New System.Drawing.Point(122, 142)
        Me.CheckBoxCH26.Name = "CheckBoxCH26"
        Me.CheckBoxCH26.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH26.TabIndex = 26
        Me.CheckBoxCH26.Text = "Ch 27"
        Me.CheckBoxCH26.UseVisualStyleBackColor = True
        '
        'CheckBoxCH27
        '
        Me.CheckBoxCH27.AutoSize = True
        Me.CheckBoxCH27.Location = New System.Drawing.Point(181, 142)
        Me.CheckBoxCH27.Name = "CheckBoxCH27"
        Me.CheckBoxCH27.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH27.TabIndex = 27
        Me.CheckBoxCH27.Text = "Ch 28"
        Me.CheckBoxCH27.UseVisualStyleBackColor = True
        '
        'CheckBoxCH28
        '
        Me.CheckBoxCH28.AutoSize = True
        Me.CheckBoxCH28.Location = New System.Drawing.Point(4, 165)
        Me.CheckBoxCH28.Name = "CheckBoxCH28"
        Me.CheckBoxCH28.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH28.TabIndex = 28
        Me.CheckBoxCH28.Text = "Ch 29"
        Me.CheckBoxCH28.UseVisualStyleBackColor = True
        '
        'CheckBoxCH29
        '
        Me.CheckBoxCH29.AutoSize = True
        Me.CheckBoxCH29.Location = New System.Drawing.Point(63, 165)
        Me.CheckBoxCH29.Name = "CheckBoxCH29"
        Me.CheckBoxCH29.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH29.TabIndex = 29
        Me.CheckBoxCH29.Text = "Ch 30"
        Me.CheckBoxCH29.UseVisualStyleBackColor = True
        '
        'CheckBoxCH30
        '
        Me.CheckBoxCH30.AutoSize = True
        Me.CheckBoxCH30.Location = New System.Drawing.Point(122, 165)
        Me.CheckBoxCH30.Name = "CheckBoxCH30"
        Me.CheckBoxCH30.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH30.TabIndex = 30
        Me.CheckBoxCH30.Text = "Ch 31"
        Me.CheckBoxCH30.UseVisualStyleBackColor = True
        '
        'CheckBoxCH31
        '
        Me.CheckBoxCH31.AutoSize = True
        Me.CheckBoxCH31.Location = New System.Drawing.Point(181, 165)
        Me.CheckBoxCH31.Name = "CheckBoxCH31"
        Me.CheckBoxCH31.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxCH31.TabIndex = 31
        Me.CheckBoxCH31.Text = "Ch 32"
        Me.CheckBoxCH31.UseVisualStyleBackColor = True
        '
        'ComboBoxAnalogRateMode
        '
        Me.ComboBoxAnalogRateMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxAnalogRateMode.FormattingEnabled = True
        Me.ComboBoxAnalogRateMode.Items.AddRange(New Object() {"Readings/Sec", "Seconds"})
        Me.ComboBoxAnalogRateMode.Location = New System.Drawing.Point(129, 138)
        Me.ComboBoxAnalogRateMode.Name = "ComboBoxAnalogRateMode"
        Me.ComboBoxAnalogRateMode.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxAnalogRateMode.TabIndex = 14
        '
        'ComboBoxAvailableDevice
        '
        Me.ComboBoxAvailableDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxAvailableDevice.Location = New System.Drawing.Point(398, 351)
        Me.ComboBoxAvailableDevice.Name = "ComboBoxAvailableDevice"
        Me.ComboBoxAvailableDevice.Size = New System.Drawing.Size(120, 21)
        Me.ComboBoxAvailableDevice.TabIndex = 500
        '
        'ComboBoxAvailableChannel
        '
        Me.ComboBoxAvailableChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxAvailableChannel.Location = New System.Drawing.Point(527, 351)
        Me.ComboBoxAvailableChannel.Name = "ComboBoxAvailableChannel"
        Me.ComboBoxAvailableChannel.Size = New System.Drawing.Size(161, 21)
        Me.ComboBoxAvailableChannel.TabIndex = 501
        '
        'ButtonSetConfig
        '
        Me.ButtonSetConfig.Location = New System.Drawing.Point(631, 290)
        Me.ButtonSetConfig.Name = "ButtonSetConfig"
        Me.ButtonSetConfig.Size = New System.Drawing.Size(57, 25)
        Me.ButtonSetConfig.TabIndex = 22
        Me.ButtonSetConfig.Text = "Set"
        Me.ButtonSetConfig.UseVisualStyleBackColor = True
        '
        'ButtonClearConfig
        '
        Me.ButtonClearConfig.Location = New System.Drawing.Point(631, 322)
        Me.ButtonClearConfig.Name = "ButtonClearConfig"
        Me.ButtonClearConfig.Size = New System.Drawing.Size(57, 25)
        Me.ButtonClearConfig.TabIndex = 23
        Me.ButtonClearConfig.Text = "Clear"
        Me.ButtonClearConfig.UseVisualStyleBackColor = True
        '
        'LabelParamVal0
        '
        Me.LabelParamVal0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal0.Location = New System.Drawing.Point(168, 24)
        Me.LabelParamVal0.Name = "LabelParamVal0"
        Me.LabelParamVal0.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal0.TabIndex = 36
        Me.LabelParamVal0.Visible = False
        '
        'LabelParamVal1
        '
        Me.LabelParamVal1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal1.Location = New System.Drawing.Point(168, 48)
        Me.LabelParamVal1.Name = "LabelParamVal1"
        Me.LabelParamVal1.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal1.TabIndex = 37
        Me.LabelParamVal1.Visible = False
        '
        'LabelParamVal2
        '
        Me.LabelParamVal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal2.Location = New System.Drawing.Point(168, 72)
        Me.LabelParamVal2.Name = "LabelParamVal2"
        Me.LabelParamVal2.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal2.TabIndex = 38
        Me.LabelParamVal2.Visible = False
        '
        'LabelParamVal3
        '
        Me.LabelParamVal3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal3.Location = New System.Drawing.Point(168, 96)
        Me.LabelParamVal3.Name = "LabelParamVal3"
        Me.LabelParamVal3.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal3.TabIndex = 39
        Me.LabelParamVal3.Visible = False
        '
        'LabelParamVal4
        '
        Me.LabelParamVal4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal4.Location = New System.Drawing.Point(168, 120)
        Me.LabelParamVal4.Name = "LabelParamVal4"
        Me.LabelParamVal4.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal4.TabIndex = 40
        Me.LabelParamVal4.Visible = False
        '
        'LabelParamVal5
        '
        Me.LabelParamVal5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal5.Location = New System.Drawing.Point(168, 144)
        Me.LabelParamVal5.Name = "LabelParamVal5"
        Me.LabelParamVal5.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal5.TabIndex = 41
        Me.LabelParamVal5.Visible = False
        '
        'LabelParamVal6
        '
        Me.LabelParamVal6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal6.Location = New System.Drawing.Point(168, 168)
        Me.LabelParamVal6.Name = "LabelParamVal6"
        Me.LabelParamVal6.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal6.TabIndex = 42
        Me.LabelParamVal6.Visible = False
        '
        'LabelParamVal7
        '
        Me.LabelParamVal7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal7.Location = New System.Drawing.Point(168, 192)
        Me.LabelParamVal7.Name = "LabelParamVal7"
        Me.LabelParamVal7.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal7.TabIndex = 43
        Me.LabelParamVal7.Visible = False
        '
        'LabelParamVal8
        '
        Me.LabelParamVal8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal8.Location = New System.Drawing.Point(168, 216)
        Me.LabelParamVal8.Name = "LabelParamVal8"
        Me.LabelParamVal8.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal8.TabIndex = 44
        Me.LabelParamVal8.Visible = False
        '
        'LabelParamVal9
        '
        Me.LabelParamVal9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal9.Location = New System.Drawing.Point(168, 240)
        Me.LabelParamVal9.Name = "LabelParamVal9"
        Me.LabelParamVal9.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal9.TabIndex = 45
        Me.LabelParamVal9.Visible = False
        '
        'LabelParamVal10
        '
        Me.LabelParamVal10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal10.Location = New System.Drawing.Point(168, 264)
        Me.LabelParamVal10.Name = "LabelParamVal10"
        Me.LabelParamVal10.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal10.TabIndex = 46
        Me.LabelParamVal10.Visible = False
        '
        'LabelParamVal11
        '
        Me.LabelParamVal11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal11.Location = New System.Drawing.Point(488, 24)
        Me.LabelParamVal11.Name = "LabelParamVal11"
        Me.LabelParamVal11.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal11.TabIndex = 58
        Me.LabelParamVal11.Visible = False
        '
        'LabelParamVal12
        '
        Me.LabelParamVal12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal12.Location = New System.Drawing.Point(488, 48)
        Me.LabelParamVal12.Name = "LabelParamVal12"
        Me.LabelParamVal12.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal12.TabIndex = 59
        Me.LabelParamVal12.Visible = False
        '
        'LabelParamVal13
        '
        Me.LabelParamVal13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal13.Location = New System.Drawing.Point(488, 72)
        Me.LabelParamVal13.Name = "LabelParamVal13"
        Me.LabelParamVal13.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal13.TabIndex = 60
        Me.LabelParamVal13.Visible = False
        '
        'LabelParamVal14
        '
        Me.LabelParamVal14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal14.Location = New System.Drawing.Point(488, 96)
        Me.LabelParamVal14.Name = "LabelParamVal14"
        Me.LabelParamVal14.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal14.TabIndex = 61
        Me.LabelParamVal14.Visible = False
        '
        'LabelParamVal15
        '
        Me.LabelParamVal15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal15.Location = New System.Drawing.Point(488, 120)
        Me.LabelParamVal15.Name = "LabelParamVal15"
        Me.LabelParamVal15.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal15.TabIndex = 62
        Me.LabelParamVal15.Visible = False
        '
        'LabelParamVal16
        '
        Me.LabelParamVal16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal16.Location = New System.Drawing.Point(488, 144)
        Me.LabelParamVal16.Name = "LabelParamVal16"
        Me.LabelParamVal16.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal16.TabIndex = 63
        Me.LabelParamVal16.Visible = False
        '
        'LabelParamVal17
        '
        Me.LabelParamVal17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal17.Location = New System.Drawing.Point(488, 168)
        Me.LabelParamVal17.Name = "LabelParamVal17"
        Me.LabelParamVal17.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal17.TabIndex = 64
        Me.LabelParamVal17.Visible = False
        '
        'LabelParamVal18
        '
        Me.LabelParamVal18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal18.Location = New System.Drawing.Point(488, 192)
        Me.LabelParamVal18.Name = "LabelParamVal18"
        Me.LabelParamVal18.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal18.TabIndex = 65
        Me.LabelParamVal18.Visible = False
        '
        'LabelParamVal19
        '
        Me.LabelParamVal19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal19.Location = New System.Drawing.Point(488, 216)
        Me.LabelParamVal19.Name = "LabelParamVal19"
        Me.LabelParamVal19.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal19.TabIndex = 66
        Me.LabelParamVal19.Visible = False
        '
        'LabelParamVal20
        '
        Me.LabelParamVal20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal20.Location = New System.Drawing.Point(488, 240)
        Me.LabelParamVal20.Name = "LabelParamVal20"
        Me.LabelParamVal20.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal20.TabIndex = 67
        Me.LabelParamVal20.Visible = False
        '
        'LabelParamVal21
        '
        Me.LabelParamVal21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelParamVal21.Location = New System.Drawing.Point(488, 264)
        Me.LabelParamVal21.Name = "LabelParamVal21"
        Me.LabelParamVal21.Size = New System.Drawing.Size(70, 19)
        Me.LabelParamVal21.TabIndex = 68
        Me.LabelParamVal21.Visible = False
        '
        'LabelParameterName0
        '
        Me.LabelParameterName0.Location = New System.Drawing.Point(0, 24)
        Me.LabelParameterName0.Name = "LabelParameterName0"
        Me.LabelParameterName0.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName0.TabIndex = 25
        Me.LabelParameterName0.Text = "LabelParameterName(0)"
        Me.LabelParameterName0.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName0.Visible = False
        '
        'LabelParameterName1
        '
        Me.LabelParameterName1.Location = New System.Drawing.Point(0, 48)
        Me.LabelParameterName1.Name = "LabelParameterName1"
        Me.LabelParameterName1.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName1.TabIndex = 26
        Me.LabelParameterName1.Text = "LabelParameterName(1)"
        Me.LabelParameterName1.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName1.Visible = False
        '
        'LabelParameterName2
        '
        Me.LabelParameterName2.Location = New System.Drawing.Point(0, 72)
        Me.LabelParameterName2.Name = "LabelParameterName2"
        Me.LabelParameterName2.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName2.TabIndex = 27
        Me.LabelParameterName2.Text = "LabelParameterName(2)"
        Me.LabelParameterName2.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName2.Visible = False
        '
        'LabelParameterName3
        '
        Me.LabelParameterName3.Location = New System.Drawing.Point(0, 96)
        Me.LabelParameterName3.Name = "LabelParameterName3"
        Me.LabelParameterName3.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName3.TabIndex = 28
        Me.LabelParameterName3.Text = "LabelParameterName(3)"
        Me.LabelParameterName3.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName3.Visible = False
        '
        'LabelParameterName4
        '
        Me.LabelParameterName4.Location = New System.Drawing.Point(0, 120)
        Me.LabelParameterName4.Name = "LabelParameterName4"
        Me.LabelParameterName4.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName4.TabIndex = 29
        Me.LabelParameterName4.Text = "LabelParameterName(4)"
        Me.LabelParameterName4.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName4.Visible = False
        '
        'LabelParameterName5
        '
        Me.LabelParameterName5.Location = New System.Drawing.Point(0, 144)
        Me.LabelParameterName5.Name = "LabelParameterName5"
        Me.LabelParameterName5.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName5.TabIndex = 30
        Me.LabelParameterName5.Text = "LabelParameterName(5)"
        Me.LabelParameterName5.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName5.Visible = False
        '
        'LabelParameterName6
        '
        Me.LabelParameterName6.Location = New System.Drawing.Point(0, 168)
        Me.LabelParameterName6.Name = "LabelParameterName6"
        Me.LabelParameterName6.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName6.TabIndex = 31
        Me.LabelParameterName6.Text = "LabelParameterName(6)"
        Me.LabelParameterName6.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName6.Visible = False
        '
        'LabelParameterName7
        '
        Me.LabelParameterName7.Location = New System.Drawing.Point(0, 192)
        Me.LabelParameterName7.Name = "LabelParameterName7"
        Me.LabelParameterName7.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName7.TabIndex = 32
        Me.LabelParameterName7.Text = "LabelParameterName(7)"
        Me.LabelParameterName7.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName7.Visible = False
        '
        'LabelParameterName8
        '
        Me.LabelParameterName8.Location = New System.Drawing.Point(0, 216)
        Me.LabelParameterName8.Name = "LabelParameterName8"
        Me.LabelParameterName8.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName8.TabIndex = 33
        Me.LabelParameterName8.Text = "LabelParameterName(8)"
        Me.LabelParameterName8.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName8.Visible = False
        '
        'LabelParameterName9
        '
        Me.LabelParameterName9.Location = New System.Drawing.Point(0, 240)
        Me.LabelParameterName9.Name = "LabelParameterName9"
        Me.LabelParameterName9.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName9.TabIndex = 34
        Me.LabelParameterName9.Text = "LabelParameterName(9)"
        Me.LabelParameterName9.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName9.Visible = False
        '
        'LabelParameterName10
        '
        Me.LabelParameterName10.Location = New System.Drawing.Point(0, 264)
        Me.LabelParameterName10.Name = "LabelParameterName10"
        Me.LabelParameterName10.Size = New System.Drawing.Size(161, 17)
        Me.LabelParameterName10.TabIndex = 35
        Me.LabelParameterName10.Text = "LabelParameterName(10)"
        Me.LabelParameterName10.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName10.Visible = False
        '
        'LabelParameterName11
        '
        Me.LabelParameterName11.Location = New System.Drawing.Point(326, 24)
        Me.LabelParameterName11.Name = "LabelParameterName11"
        Me.LabelParameterName11.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName11.TabIndex = 47
        Me.LabelParameterName11.Text = "LabelParameterName(11)"
        Me.LabelParameterName11.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName11.Visible = False
        '
        'LabelParameterName12
        '
        Me.LabelParameterName12.Location = New System.Drawing.Point(326, 48)
        Me.LabelParameterName12.Name = "LabelParameterName12"
        Me.LabelParameterName12.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName12.TabIndex = 48
        Me.LabelParameterName12.Text = "LabelParameterName(12)"
        Me.LabelParameterName12.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName12.Visible = False
        '
        'LabelParameterName13
        '
        Me.LabelParameterName13.Location = New System.Drawing.Point(326, 72)
        Me.LabelParameterName13.Name = "LabelParameterName13"
        Me.LabelParameterName13.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName13.TabIndex = 49
        Me.LabelParameterName13.Text = "LabelParameterName(13)"
        Me.LabelParameterName13.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName13.Visible = False
        '
        'LabelParameterName14
        '
        Me.LabelParameterName14.Location = New System.Drawing.Point(326, 96)
        Me.LabelParameterName14.Name = "LabelParameterName14"
        Me.LabelParameterName14.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName14.TabIndex = 50
        Me.LabelParameterName14.Text = "LabelParameterName(14)"
        Me.LabelParameterName14.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName14.Visible = False
        '
        'LabelParameterName15
        '
        Me.LabelParameterName15.Location = New System.Drawing.Point(326, 120)
        Me.LabelParameterName15.Name = "LabelParameterName15"
        Me.LabelParameterName15.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName15.TabIndex = 51
        Me.LabelParameterName15.Text = "LabelParameterName(15)"
        Me.LabelParameterName15.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName15.Visible = False
        '
        'LabelParameterName16
        '
        Me.LabelParameterName16.Location = New System.Drawing.Point(326, 144)
        Me.LabelParameterName16.Name = "LabelParameterName16"
        Me.LabelParameterName16.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName16.TabIndex = 52
        Me.LabelParameterName16.Text = "LabelParameterName(16)"
        Me.LabelParameterName16.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName16.Visible = False
        '
        'LabelParameterName17
        '
        Me.LabelParameterName17.Location = New System.Drawing.Point(326, 168)
        Me.LabelParameterName17.Name = "LabelParameterName17"
        Me.LabelParameterName17.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName17.TabIndex = 53
        Me.LabelParameterName17.Text = "LabelParameterName(17)"
        Me.LabelParameterName17.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName17.Visible = False
        '
        'LabelParameterName18
        '
        Me.LabelParameterName18.Location = New System.Drawing.Point(326, 192)
        Me.LabelParameterName18.Name = "LabelParameterName18"
        Me.LabelParameterName18.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName18.TabIndex = 54
        Me.LabelParameterName18.Text = "LabelParameterName(18)"
        Me.LabelParameterName18.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName18.Visible = False
        '
        'LabelParameterName19
        '
        Me.LabelParameterName19.Location = New System.Drawing.Point(326, 216)
        Me.LabelParameterName19.Name = "LabelParameterName19"
        Me.LabelParameterName19.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName19.TabIndex = 55
        Me.LabelParameterName19.Text = "LabelParameterName(19)"
        Me.LabelParameterName19.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName19.Visible = False
        '
        'LabelParameterName20
        '
        Me.LabelParameterName20.Location = New System.Drawing.Point(326, 240)
        Me.LabelParameterName20.Name = "LabelParameterName20"
        Me.LabelParameterName20.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName20.TabIndex = 56
        Me.LabelParameterName20.Text = "LabelParameterName(20)"
        Me.LabelParameterName20.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName20.Visible = False
        '
        'LabelParameterName21
        '
        Me.LabelParameterName21.Location = New System.Drawing.Point(326, 264)
        Me.LabelParameterName21.Name = "LabelParameterName21"
        Me.LabelParameterName21.Size = New System.Drawing.Size(155, 17)
        Me.LabelParameterName21.TabIndex = 57
        Me.LabelParameterName21.Text = "LabelParameterName(21)"
        Me.LabelParameterName21.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelParameterName21.Visible = False
        '
        'TextBoxParamVal0
        '
        Me.TextBoxParamVal0.AcceptsReturn = True
        Me.TextBoxParamVal0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal0.Location = New System.Drawing.Point(244, 24)
        Me.TextBoxParamVal0.MaxLength = 0
        Me.TextBoxParamVal0.Name = "TextBoxParamVal0"
        Me.TextBoxParamVal0.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal0.TabIndex = 0
        Me.TextBoxParamVal0.Visible = False
        '
        'TextBoxParamVal1
        '
        Me.TextBoxParamVal1.AcceptsReturn = True
        Me.TextBoxParamVal1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal1.Location = New System.Drawing.Point(244, 48)
        Me.TextBoxParamVal1.MaxLength = 0
        Me.TextBoxParamVal1.Name = "TextBoxParamVal1"
        Me.TextBoxParamVal1.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal1.TabIndex = 1
        Me.TextBoxParamVal1.Visible = False
        '
        'TextBoxParamVal2
        '
        Me.TextBoxParamVal2.AcceptsReturn = True
        Me.TextBoxParamVal2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal2.Location = New System.Drawing.Point(244, 72)
        Me.TextBoxParamVal2.MaxLength = 0
        Me.TextBoxParamVal2.Name = "TextBoxParamVal2"
        Me.TextBoxParamVal2.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal2.TabIndex = 2
        Me.TextBoxParamVal2.Visible = False
        '
        'TextBoxParamVal3
        '
        Me.TextBoxParamVal3.AcceptsReturn = True
        Me.TextBoxParamVal3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal3.Location = New System.Drawing.Point(244, 96)
        Me.TextBoxParamVal3.MaxLength = 0
        Me.TextBoxParamVal3.Name = "TextBoxParamVal3"
        Me.TextBoxParamVal3.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal3.TabIndex = 3
        Me.TextBoxParamVal3.Visible = False
        '
        'TextBoxParamVal4
        '
        Me.TextBoxParamVal4.AcceptsReturn = True
        Me.TextBoxParamVal4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal4.Location = New System.Drawing.Point(244, 120)
        Me.TextBoxParamVal4.MaxLength = 0
        Me.TextBoxParamVal4.Name = "TextBoxParamVal4"
        Me.TextBoxParamVal4.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal4.TabIndex = 4
        Me.TextBoxParamVal4.Visible = False
        '
        'TextBoxParamVal5
        '
        Me.TextBoxParamVal5.AcceptsReturn = True
        Me.TextBoxParamVal5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal5.Location = New System.Drawing.Point(244, 144)
        Me.TextBoxParamVal5.MaxLength = 0
        Me.TextBoxParamVal5.Name = "TextBoxParamVal5"
        Me.TextBoxParamVal5.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal5.TabIndex = 5
        Me.TextBoxParamVal5.Visible = False
        '
        'TextBoxParamVal6
        '
        Me.TextBoxParamVal6.AcceptsReturn = True
        Me.TextBoxParamVal6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal6.Location = New System.Drawing.Point(244, 168)
        Me.TextBoxParamVal6.MaxLength = 0
        Me.TextBoxParamVal6.Name = "TextBoxParamVal6"
        Me.TextBoxParamVal6.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal6.TabIndex = 6
        Me.TextBoxParamVal6.Visible = False
        '
        'TextBoxParamVal7
        '
        Me.TextBoxParamVal7.AcceptsReturn = True
        Me.TextBoxParamVal7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal7.Location = New System.Drawing.Point(244, 192)
        Me.TextBoxParamVal7.MaxLength = 0
        Me.TextBoxParamVal7.Name = "TextBoxParamVal7"
        Me.TextBoxParamVal7.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal7.TabIndex = 7
        Me.TextBoxParamVal7.Visible = False
        '
        'TextBoxParamVal8
        '
        Me.TextBoxParamVal8.AcceptsReturn = True
        Me.TextBoxParamVal8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal8.Location = New System.Drawing.Point(244, 216)
        Me.TextBoxParamVal8.MaxLength = 0
        Me.TextBoxParamVal8.Name = "TextBoxParamVal8"
        Me.TextBoxParamVal8.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal8.TabIndex = 8
        Me.TextBoxParamVal8.Visible = False
        '
        'TextBoxParamVal9
        '
        Me.TextBoxParamVal9.AcceptsReturn = True
        Me.TextBoxParamVal9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal9.Location = New System.Drawing.Point(244, 240)
        Me.TextBoxParamVal9.MaxLength = 0
        Me.TextBoxParamVal9.Name = "TextBoxParamVal9"
        Me.TextBoxParamVal9.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal9.TabIndex = 9
        Me.TextBoxParamVal9.Visible = False
        '
        'TextBoxParamVal10
        '
        Me.TextBoxParamVal10.AcceptsReturn = True
        Me.TextBoxParamVal10.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal10.Location = New System.Drawing.Point(244, 264)
        Me.TextBoxParamVal10.MaxLength = 0
        Me.TextBoxParamVal10.Name = "TextBoxParamVal10"
        Me.TextBoxParamVal10.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal10.TabIndex = 10
        Me.TextBoxParamVal10.Visible = False
        '
        'TextBoxParamVal11
        '
        Me.TextBoxParamVal11.AcceptsReturn = True
        Me.TextBoxParamVal11.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal11.Location = New System.Drawing.Point(564, 24)
        Me.TextBoxParamVal11.MaxLength = 0
        Me.TextBoxParamVal11.Name = "TextBoxParamVal11"
        Me.TextBoxParamVal11.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal11.TabIndex = 11
        Me.TextBoxParamVal11.Visible = False
        '
        'TextBoxParamVal12
        '
        Me.TextBoxParamVal12.AcceptsReturn = True
        Me.TextBoxParamVal12.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal12.Location = New System.Drawing.Point(564, 48)
        Me.TextBoxParamVal12.MaxLength = 0
        Me.TextBoxParamVal12.Name = "TextBoxParamVal12"
        Me.TextBoxParamVal12.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal12.TabIndex = 12
        Me.TextBoxParamVal12.Visible = False
        '
        'TextBoxParamVal13
        '
        Me.TextBoxParamVal13.AcceptsReturn = True
        Me.TextBoxParamVal13.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal13.Location = New System.Drawing.Point(564, 72)
        Me.TextBoxParamVal13.MaxLength = 0
        Me.TextBoxParamVal13.Name = "TextBoxParamVal13"
        Me.TextBoxParamVal13.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal13.TabIndex = 13
        Me.TextBoxParamVal13.Visible = False
        '
        'TextBoxParamVal14
        '
        Me.TextBoxParamVal14.AcceptsReturn = True
        Me.TextBoxParamVal14.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal14.Location = New System.Drawing.Point(564, 96)
        Me.TextBoxParamVal14.MaxLength = 0
        Me.TextBoxParamVal14.Name = "TextBoxParamVal14"
        Me.TextBoxParamVal14.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal14.TabIndex = 14
        Me.TextBoxParamVal14.Visible = False
        '
        'TextBoxParamVal15
        '
        Me.TextBoxParamVal15.AcceptsReturn = True
        Me.TextBoxParamVal15.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal15.Location = New System.Drawing.Point(564, 120)
        Me.TextBoxParamVal15.MaxLength = 0
        Me.TextBoxParamVal15.Name = "TextBoxParamVal15"
        Me.TextBoxParamVal15.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal15.TabIndex = 15
        Me.TextBoxParamVal15.Visible = False
        '
        'TextBoxParamVal16
        '
        Me.TextBoxParamVal16.AcceptsReturn = True
        Me.TextBoxParamVal16.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal16.Location = New System.Drawing.Point(564, 144)
        Me.TextBoxParamVal16.MaxLength = 0
        Me.TextBoxParamVal16.Name = "TextBoxParamVal16"
        Me.TextBoxParamVal16.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal16.TabIndex = 16
        Me.TextBoxParamVal16.Visible = False
        '
        'TextBoxParamVal17
        '
        Me.TextBoxParamVal17.AcceptsReturn = True
        Me.TextBoxParamVal17.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal17.Location = New System.Drawing.Point(564, 168)
        Me.TextBoxParamVal17.MaxLength = 0
        Me.TextBoxParamVal17.Name = "TextBoxParamVal17"
        Me.TextBoxParamVal17.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal17.TabIndex = 17
        Me.TextBoxParamVal17.Visible = False
        '
        'TextBoxParamVal18
        '
        Me.TextBoxParamVal18.AcceptsReturn = True
        Me.TextBoxParamVal18.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal18.Location = New System.Drawing.Point(564, 192)
        Me.TextBoxParamVal18.MaxLength = 0
        Me.TextBoxParamVal18.Name = "TextBoxParamVal18"
        Me.TextBoxParamVal18.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal18.TabIndex = 18
        Me.TextBoxParamVal18.Visible = False
        '
        'TextBoxParamVal19
        '
        Me.TextBoxParamVal19.AcceptsReturn = True
        Me.TextBoxParamVal19.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal19.Location = New System.Drawing.Point(564, 216)
        Me.TextBoxParamVal19.MaxLength = 0
        Me.TextBoxParamVal19.Name = "TextBoxParamVal19"
        Me.TextBoxParamVal19.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal19.TabIndex = 19
        Me.TextBoxParamVal19.Visible = False
        '
        'TextBoxParamVal20
        '
        Me.TextBoxParamVal20.AcceptsReturn = True
        Me.TextBoxParamVal20.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal20.Location = New System.Drawing.Point(564, 240)
        Me.TextBoxParamVal20.MaxLength = 0
        Me.TextBoxParamVal20.Name = "TextBoxParamVal20"
        Me.TextBoxParamVal20.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal20.TabIndex = 20
        Me.TextBoxParamVal20.Visible = False
        '
        'TextBoxParamVal21
        '
        Me.TextBoxParamVal21.AcceptsReturn = True
        Me.TextBoxParamVal21.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxParamVal21.Location = New System.Drawing.Point(564, 264)
        Me.TextBoxParamVal21.MaxLength = 0
        Me.TextBoxParamVal21.Name = "TextBoxParamVal21"
        Me.TextBoxParamVal21.Size = New System.Drawing.Size(82, 20)
        Me.TextBoxParamVal21.TabIndex = 21
        Me.TextBoxParamVal21.Visible = False
        '
        'LabelChannel4
        '
        Me.LabelChannel4.Location = New System.Drawing.Point(524, 336)
        Me.LabelChannel4.Name = "LabelChannel4"
        Me.LabelChannel4.Size = New System.Drawing.Size(49, 17)
        Me.LabelChannel4.TabIndex = 433
        Me.LabelChannel4.Text = "Channel:"
        '
        'LabelDeviceCombo4
        '
        Me.LabelDeviceCombo4.Location = New System.Drawing.Point(395, 336)
        Me.LabelDeviceCombo4.Name = "LabelDeviceCombo4"
        Me.LabelDeviceCombo4.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo4.TabIndex = 427
        Me.LabelDeviceCombo4.Text = "Device:"
        '
        'LabelParameters0
        '
        Me.LabelParameters0.Location = New System.Drawing.Point(8, 8)
        Me.LabelParameters0.Name = "LabelParameters0"
        Me.LabelParameters0.Size = New System.Drawing.Size(154, 13)
        Me.LabelParameters0.TabIndex = 69
        Me.LabelParameters0.Text = "Parameter Name:"
        Me.LabelParameters0.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelValues0
        '
        Me.LabelValues0.AutoSize = True
        Me.LabelValues0.Location = New System.Drawing.Point(168, 8)
        Me.LabelValues0.Name = "LabelValues0"
        Me.LabelValues0.Size = New System.Drawing.Size(37, 13)
        Me.LabelValues0.TabIndex = 70
        Me.LabelValues0.Text = "Value:"
        '
        'LabelMod0
        '
        Me.LabelMod0.AutoSize = True
        Me.LabelMod0.Location = New System.Drawing.Point(244, 8)
        Me.LabelMod0.Name = "LabelMod0"
        Me.LabelMod0.Size = New System.Drawing.Size(41, 13)
        Me.LabelMod0.TabIndex = 71
        Me.LabelMod0.Text = "Modify:"
        '
        'LabelParameters1
        '
        Me.LabelParameters1.Location = New System.Drawing.Point(327, 8)
        Me.LabelParameters1.Name = "LabelParameters1"
        Me.LabelParameters1.Size = New System.Drawing.Size(155, 16)
        Me.LabelParameters1.TabIndex = 72
        Me.LabelParameters1.Text = "Parameter Name:"
        Me.LabelParameters1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelValues1
        '
        Me.LabelValues1.AutoSize = True
        Me.LabelValues1.Location = New System.Drawing.Point(488, 8)
        Me.LabelValues1.Name = "LabelValues1"
        Me.LabelValues1.Size = New System.Drawing.Size(37, 13)
        Me.LabelValues1.TabIndex = 73
        Me.LabelValues1.Text = "Value:"
        '
        'LabelMod1
        '
        Me.LabelMod1.AutoSize = True
        Me.LabelMod1.Location = New System.Drawing.Point(564, 8)
        Me.LabelMod1.Name = "LabelMod1"
        Me.LabelMod1.Size = New System.Drawing.Size(41, 13)
        Me.LabelMod1.TabIndex = 74
        Me.LabelMod1.Text = "Modify:"
        '
        'TabPageInit
        '
        Me.TabPageInit.Controls.Add(Me.GroupBoxFastInit)
        Me.TabPageInit.Controls.Add(Me.GroupBox5BInit)
        Me.TabPageInit.Controls.Add(Me.LabelChannel5)
        Me.TabPageInit.Controls.Add(Me.LabelDeviceCombo5)
        Me.TabPageInit.Controls.Add(Me.ComboAvailableBoxLocator5)
        Me.TabPageInit.Controls.Add(Me.ComboAvailableChannelLocator5)
        Me.TabPageInit.Location = New System.Drawing.Point(4, 22)
        Me.TabPageInit.Name = "TabPageInit"
        Me.TabPageInit.Size = New System.Drawing.Size(714, 395)
        Me.TabPageInit.TabIndex = 5
        Me.TabPageInit.Text = "Init"
        '
        'GroupBoxFastInit
        '
        Me.GroupBoxFastInit.Controls.Add(Me.ButtonExecuteFastInit)
        Me.GroupBoxFastInit.Controls.Add(Me.TextBoxFIMessage)
        Me.GroupBoxFastInit.Controls.Add(Me.TextBoxFIFlags)
        Me.GroupBoxFastInit.Controls.Add(Me.LabelFIRXTitle)
        Me.GroupBoxFastInit.Controls.Add(Me.LabelFITXFlags)
        Me.GroupBoxFastInit.Controls.Add(Me.LabelFIRxStatus)
        Me.GroupBoxFastInit.Controls.Add(Me.LabelFIResponse)
        Me.GroupBoxFastInit.Controls.Add(Me.LabelFIMsgResp)
        Me.GroupBoxFastInit.Controls.Add(Me.LabelFIMsgData)
        Me.GroupBoxFastInit.Location = New System.Drawing.Point(328, 24)
        Me.GroupBoxFastInit.Name = "GroupBoxFastInit"
        Me.GroupBoxFastInit.Size = New System.Drawing.Size(369, 297)
        Me.GroupBoxFastInit.TabIndex = 1
        Me.GroupBoxFastInit.TabStop = False
        Me.GroupBoxFastInit.Text = "Fast Init"
        '
        'ButtonExecuteFastInit
        '
        Me.ButtonExecuteFastInit.Location = New System.Drawing.Point(272, 104)
        Me.ButtonExecuteFastInit.Name = "ButtonExecuteFastInit"
        Me.ButtonExecuteFastInit.Size = New System.Drawing.Size(57, 25)
        Me.ButtonExecuteFastInit.TabIndex = 0
        Me.ButtonExecuteFastInit.Text = "Execute"
        Me.ButtonExecuteFastInit.UseVisualStyleBackColor = True
        '
        'TextBoxFIMessage
        '
        Me.TextBoxFIMessage.AcceptsReturn = True
        Me.TextBoxFIMessage.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFIMessage.Location = New System.Drawing.Point(16, 32)
        Me.TextBoxFIMessage.MaxLength = 0
        Me.TextBoxFIMessage.Name = "TextBoxFIMessage"
        Me.TextBoxFIMessage.Size = New System.Drawing.Size(225, 20)
        Me.TextBoxFIMessage.TabIndex = 1
        '
        'TextBoxFIFlags
        '
        Me.TextBoxFIFlags.AcceptsReturn = True
        Me.TextBoxFIFlags.ContextMenuStrip = Me.ContextMenuStripFlags
        Me.TextBoxFIFlags.Location = New System.Drawing.Point(248, 32)
        Me.TextBoxFIFlags.MaxLength = 0
        Me.TextBoxFIFlags.Name = "TextBoxFIFlags"
        Me.TextBoxFIFlags.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxFIFlags.TabIndex = 2
        '
        'LabelFIRXTitle
        '
        Me.LabelFIRXTitle.Location = New System.Drawing.Point(248, 136)
        Me.LabelFIRXTitle.Name = "LabelFIRXTitle"
        Me.LabelFIRXTitle.Size = New System.Drawing.Size(81, 17)
        Me.LabelFIRXTitle.TabIndex = 6
        Me.LabelFIRXTitle.Text = "Rx Status:"
        '
        'LabelFITXFlags
        '
        Me.LabelFITXFlags.Location = New System.Drawing.Point(248, 16)
        Me.LabelFITXFlags.Name = "LabelFITXFlags"
        Me.LabelFITXFlags.Size = New System.Drawing.Size(81, 17)
        Me.LabelFITXFlags.TabIndex = 4
        Me.LabelFITXFlags.Text = "Tx Flags:"
        '
        'LabelFIRxStatus
        '
        Me.LabelFIRxStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFIRxStatus.Location = New System.Drawing.Point(248, 153)
        Me.LabelFIRxStatus.Name = "LabelFIRxStatus"
        Me.LabelFIRxStatus.Size = New System.Drawing.Size(81, 19)
        Me.LabelFIRxStatus.TabIndex = 8
        '
        'LabelFIResponse
        '
        Me.LabelFIResponse.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFIResponse.Location = New System.Drawing.Point(16, 153)
        Me.LabelFIResponse.Name = "LabelFIResponse"
        Me.LabelFIResponse.Size = New System.Drawing.Size(225, 91)
        Me.LabelFIResponse.TabIndex = 7
        '
        'LabelFIMsgResp
        '
        Me.LabelFIMsgResp.Location = New System.Drawing.Point(16, 136)
        Me.LabelFIMsgResp.Name = "LabelFIMsgResp"
        Me.LabelFIMsgResp.Size = New System.Drawing.Size(186, 17)
        Me.LabelFIMsgResp.TabIndex = 5
        Me.LabelFIMsgResp.Text = "Message Response:"
        '
        'LabelFIMsgData
        '
        Me.LabelFIMsgData.Location = New System.Drawing.Point(16, 16)
        Me.LabelFIMsgData.Name = "LabelFIMsgData"
        Me.LabelFIMsgData.Size = New System.Drawing.Size(210, 17)
        Me.LabelFIMsgData.TabIndex = 3
        Me.LabelFIMsgData.Text = "Message Data:"
        '
        'GroupBox5BInit
        '
        Me.GroupBox5BInit.Controls.Add(Me.TextBox5BInitECU)
        Me.GroupBox5BInit.Controls.Add(Me.ButtonExecute5BInit)
        Me.GroupBox5BInit.Controls.Add(Me.LabelKWlabel1)
        Me.GroupBox5BInit.Controls.Add(Me.LabelKWlabel0)
        Me.GroupBox5BInit.Controls.Add(Me.Label5BECU)
        Me.GroupBox5BInit.Controls.Add(Me.Label5BKeyWord0)
        Me.GroupBox5BInit.Controls.Add(Me.Label5BKeyWord1)
        Me.GroupBox5BInit.Location = New System.Drawing.Point(8, 24)
        Me.GroupBox5BInit.Name = "GroupBox5BInit"
        Me.GroupBox5BInit.Size = New System.Drawing.Size(313, 297)
        Me.GroupBox5BInit.TabIndex = 0
        Me.GroupBox5BInit.TabStop = False
        Me.GroupBox5BInit.Text = "Five Baud Init"
        '
        'TextBox5BInitECU
        '
        Me.TextBox5BInitECU.AcceptsReturn = True
        Me.TextBox5BInitECU.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBox5BInitECU.Location = New System.Drawing.Point(32, 51)
        Me.TextBox5BInitECU.MaxLength = 0
        Me.TextBox5BInitECU.Name = "TextBox5BInitECU"
        Me.TextBox5BInitECU.Size = New System.Drawing.Size(57, 20)
        Me.TextBox5BInitECU.TabIndex = 3
        '
        'ButtonExecute5BInit
        '
        Me.ButtonExecute5BInit.Location = New System.Drawing.Point(200, 75)
        Me.ButtonExecute5BInit.Name = "ButtonExecute5BInit"
        Me.ButtonExecute5BInit.Size = New System.Drawing.Size(57, 25)
        Me.ButtonExecute5BInit.TabIndex = 0
        Me.ButtonExecute5BInit.Text = "Execute"
        Me.ButtonExecute5BInit.UseVisualStyleBackColor = True
        '
        'LabelKWlabel1
        '
        Me.LabelKWlabel1.Location = New System.Drawing.Point(184, 32)
        Me.LabelKWlabel1.Name = "LabelKWlabel1"
        Me.LabelKWlabel1.Size = New System.Drawing.Size(73, 15)
        Me.LabelKWlabel1.TabIndex = 6
        Me.LabelKWlabel1.Text = "Keyword 2:"
        '
        'LabelKWlabel0
        '
        Me.LabelKWlabel0.Location = New System.Drawing.Point(104, 32)
        Me.LabelKWlabel0.Name = "LabelKWlabel0"
        Me.LabelKWlabel0.Size = New System.Drawing.Size(73, 15)
        Me.LabelKWlabel0.TabIndex = 5
        Me.LabelKWlabel0.Text = "Keyword 1:"
        '
        'Label5BECU
        '
        Me.Label5BECU.Location = New System.Drawing.Point(24, 32)
        Me.Label5BECU.Name = "Label5BECU"
        Me.Label5BECU.Size = New System.Drawing.Size(85, 15)
        Me.Label5BECU.TabIndex = 4
        Me.Label5BECU.Text = "ECU Address:"
        '
        'Label5BKeyWord0
        '
        Me.Label5BKeyWord0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label5BKeyWord0.Location = New System.Drawing.Point(104, 51)
        Me.Label5BKeyWord0.Name = "Label5BKeyWord0"
        Me.Label5BKeyWord0.Size = New System.Drawing.Size(73, 19)
        Me.Label5BKeyWord0.TabIndex = 1
        '
        'Label5BKeyWord1
        '
        Me.Label5BKeyWord1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label5BKeyWord1.Location = New System.Drawing.Point(184, 51)
        Me.Label5BKeyWord1.Name = "Label5BKeyWord1"
        Me.Label5BKeyWord1.Size = New System.Drawing.Size(73, 19)
        Me.Label5BKeyWord1.TabIndex = 2
        '
        'LabelChannel5
        '
        Me.LabelChannel5.Location = New System.Drawing.Point(524, 336)
        Me.LabelChannel5.Name = "LabelChannel5"
        Me.LabelChannel5.Size = New System.Drawing.Size(49, 17)
        Me.LabelChannel5.TabIndex = 5
        Me.LabelChannel5.Text = "Channel:"
        '
        'LabelDeviceCombo5
        '
        Me.LabelDeviceCombo5.Location = New System.Drawing.Point(395, 336)
        Me.LabelDeviceCombo5.Name = "LabelDeviceCombo5"
        Me.LabelDeviceCombo5.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo5.TabIndex = 4
        Me.LabelDeviceCombo5.Text = "Device:"
        '
        'TabPageFunctionalMessages
        '
        Me.TabPageFunctionalMessages.Controls.Add(Me.ButtonClearAllFunctionalMessages)
        Me.TabPageFunctionalMessages.Controls.Add(Me.ButtonApplyFunctionalMessages)
        Me.TabPageFunctionalMessages.Controls.Add(Me.ButtonCancelFunctionalMessages)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete0)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete1)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete2)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete3)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete4)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete5)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete7)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete8)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete9)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete10)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete11)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete12)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete13)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete14)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete15)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete16)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete17)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete18)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete19)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete20)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete21)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete22)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete23)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete24)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete25)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete26)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete27)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete28)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete29)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete30)
        Me.TabPageFunctionalMessages.Controls.Add(Me.CheckBoxFunctionalMessageDelete31)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage0)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage1)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage2)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage3)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage4)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage5)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage7)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage8)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage9)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage10)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage11)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage12)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage13)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage14)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage15)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage16)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage17)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage18)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage19)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage20)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage21)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage22)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage23)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage24)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage25)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage26)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage27)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage28)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage29)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage30)
        Me.TabPageFunctionalMessages.Controls.Add(Me.TextBoxFunctionalMessage31)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelChannel6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelDeviceCombo6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessageModify)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessageValues)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId31)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId30)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId29)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId28)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId27)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId26)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId25)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId24)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId23)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId22)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId21)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId20)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId19)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId18)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId17)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId16)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId15)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId14)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId13)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId12)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId11)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId10)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId9)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId8)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId7)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId5)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId4)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId3)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId2)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId1)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFuncId0)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage0)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage1)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage2)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage3)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage4)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage5)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage7)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage8)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage9)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage10)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage11)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage12)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage13)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage14)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage15)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage16)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage17)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage18)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage19)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage20)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage21)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage22)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage23)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage24)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage25)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage26)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage27)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage28)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage29)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage30)
        Me.TabPageFunctionalMessages.Controls.Add(Me.LabelFunctionalMessage31)
        Me.TabPageFunctionalMessages.Controls.Add(Me.ComboAvailableBoxLocator6)
        Me.TabPageFunctionalMessages.Controls.Add(Me.ComboAvailableChannelLocator6)
        Me.TabPageFunctionalMessages.Location = New System.Drawing.Point(4, 22)
        Me.TabPageFunctionalMessages.Name = "TabPageFunctionalMessages"
        Me.TabPageFunctionalMessages.Size = New System.Drawing.Size(714, 395)
        Me.TabPageFunctionalMessages.TabIndex = 6
        Me.TabPageFunctionalMessages.Text = "Functional Messages"
        '
        'ButtonClearAllFunctionalMessages
        '
        Me.ButtonClearAllFunctionalMessages.Location = New System.Drawing.Point(504, 304)
        Me.ButtonClearAllFunctionalMessages.Name = "ButtonClearAllFunctionalMessages"
        Me.ButtonClearAllFunctionalMessages.Size = New System.Drawing.Size(73, 25)
        Me.ButtonClearAllFunctionalMessages.TabIndex = 32
        Me.ButtonClearAllFunctionalMessages.Text = "Clear All"
        Me.ButtonClearAllFunctionalMessages.UseVisualStyleBackColor = True
        '
        'ButtonApplyFunctionalMessages
        '
        Me.ButtonApplyFunctionalMessages.Location = New System.Drawing.Point(639, 304)
        Me.ButtonApplyFunctionalMessages.Name = "ButtonApplyFunctionalMessages"
        Me.ButtonApplyFunctionalMessages.Size = New System.Drawing.Size(49, 25)
        Me.ButtonApplyFunctionalMessages.TabIndex = 34
        Me.ButtonApplyFunctionalMessages.Text = "Apply"
        Me.ButtonApplyFunctionalMessages.UseVisualStyleBackColor = True
        '
        'ButtonCancelFunctionalMessages
        '
        Me.ButtonCancelFunctionalMessages.Location = New System.Drawing.Point(583, 304)
        Me.ButtonCancelFunctionalMessages.Name = "ButtonCancelFunctionalMessages"
        Me.ButtonCancelFunctionalMessages.Size = New System.Drawing.Size(49, 25)
        Me.ButtonCancelFunctionalMessages.TabIndex = 33
        Me.ButtonCancelFunctionalMessages.Text = "Cancel"
        Me.ButtonCancelFunctionalMessages.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete0
        '
        Me.CheckBoxFunctionalMessageDelete0.Location = New System.Drawing.Point(177, 48)
        Me.CheckBoxFunctionalMessageDelete0.Name = "CheckBoxFunctionalMessageDelete0"
        Me.CheckBoxFunctionalMessageDelete0.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete0.TabIndex = 63
        Me.CheckBoxFunctionalMessageDelete0.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete0.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete1
        '
        Me.CheckBoxFunctionalMessageDelete1.Location = New System.Drawing.Point(177, 72)
        Me.CheckBoxFunctionalMessageDelete1.Name = "CheckBoxFunctionalMessageDelete1"
        Me.CheckBoxFunctionalMessageDelete1.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete1.TabIndex = 64
        Me.CheckBoxFunctionalMessageDelete1.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete1.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete2
        '
        Me.CheckBoxFunctionalMessageDelete2.Location = New System.Drawing.Point(177, 96)
        Me.CheckBoxFunctionalMessageDelete2.Name = "CheckBoxFunctionalMessageDelete2"
        Me.CheckBoxFunctionalMessageDelete2.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete2.TabIndex = 65
        Me.CheckBoxFunctionalMessageDelete2.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete2.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete3
        '
        Me.CheckBoxFunctionalMessageDelete3.Location = New System.Drawing.Point(177, 120)
        Me.CheckBoxFunctionalMessageDelete3.Name = "CheckBoxFunctionalMessageDelete3"
        Me.CheckBoxFunctionalMessageDelete3.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete3.TabIndex = 66
        Me.CheckBoxFunctionalMessageDelete3.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete3.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete4
        '
        Me.CheckBoxFunctionalMessageDelete4.Location = New System.Drawing.Point(177, 144)
        Me.CheckBoxFunctionalMessageDelete4.Name = "CheckBoxFunctionalMessageDelete4"
        Me.CheckBoxFunctionalMessageDelete4.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete4.TabIndex = 67
        Me.CheckBoxFunctionalMessageDelete4.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete4.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete5
        '
        Me.CheckBoxFunctionalMessageDelete5.Location = New System.Drawing.Point(177, 168)
        Me.CheckBoxFunctionalMessageDelete5.Name = "CheckBoxFunctionalMessageDelete5"
        Me.CheckBoxFunctionalMessageDelete5.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete5.TabIndex = 68
        Me.CheckBoxFunctionalMessageDelete5.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete5.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete6
        '
        Me.CheckBoxFunctionalMessageDelete6.Location = New System.Drawing.Point(177, 192)
        Me.CheckBoxFunctionalMessageDelete6.Name = "CheckBoxFunctionalMessageDelete6"
        Me.CheckBoxFunctionalMessageDelete6.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete6.TabIndex = 69
        Me.CheckBoxFunctionalMessageDelete6.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete6.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete7
        '
        Me.CheckBoxFunctionalMessageDelete7.Location = New System.Drawing.Point(177, 216)
        Me.CheckBoxFunctionalMessageDelete7.Name = "CheckBoxFunctionalMessageDelete7"
        Me.CheckBoxFunctionalMessageDelete7.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete7.TabIndex = 70
        Me.CheckBoxFunctionalMessageDelete7.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete7.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete8
        '
        Me.CheckBoxFunctionalMessageDelete8.Location = New System.Drawing.Point(177, 240)
        Me.CheckBoxFunctionalMessageDelete8.Name = "CheckBoxFunctionalMessageDelete8"
        Me.CheckBoxFunctionalMessageDelete8.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete8.TabIndex = 71
        Me.CheckBoxFunctionalMessageDelete8.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete8.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete9
        '
        Me.CheckBoxFunctionalMessageDelete9.Location = New System.Drawing.Point(177, 264)
        Me.CheckBoxFunctionalMessageDelete9.Name = "CheckBoxFunctionalMessageDelete9"
        Me.CheckBoxFunctionalMessageDelete9.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete9.TabIndex = 72
        Me.CheckBoxFunctionalMessageDelete9.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete9.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete10
        '
        Me.CheckBoxFunctionalMessageDelete10.Location = New System.Drawing.Point(177, 288)
        Me.CheckBoxFunctionalMessageDelete10.Name = "CheckBoxFunctionalMessageDelete10"
        Me.CheckBoxFunctionalMessageDelete10.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete10.TabIndex = 73
        Me.CheckBoxFunctionalMessageDelete10.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete10.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete11
        '
        Me.CheckBoxFunctionalMessageDelete11.Location = New System.Drawing.Point(385, 48)
        Me.CheckBoxFunctionalMessageDelete11.Name = "CheckBoxFunctionalMessageDelete11"
        Me.CheckBoxFunctionalMessageDelete11.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete11.TabIndex = 96
        Me.CheckBoxFunctionalMessageDelete11.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete11.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete12
        '
        Me.CheckBoxFunctionalMessageDelete12.Location = New System.Drawing.Point(385, 72)
        Me.CheckBoxFunctionalMessageDelete12.Name = "CheckBoxFunctionalMessageDelete12"
        Me.CheckBoxFunctionalMessageDelete12.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete12.TabIndex = 97
        Me.CheckBoxFunctionalMessageDelete12.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete12.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete13
        '
        Me.CheckBoxFunctionalMessageDelete13.Location = New System.Drawing.Point(385, 96)
        Me.CheckBoxFunctionalMessageDelete13.Name = "CheckBoxFunctionalMessageDelete13"
        Me.CheckBoxFunctionalMessageDelete13.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete13.TabIndex = 98
        Me.CheckBoxFunctionalMessageDelete13.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete13.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete14
        '
        Me.CheckBoxFunctionalMessageDelete14.Location = New System.Drawing.Point(385, 120)
        Me.CheckBoxFunctionalMessageDelete14.Name = "CheckBoxFunctionalMessageDelete14"
        Me.CheckBoxFunctionalMessageDelete14.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete14.TabIndex = 99
        Me.CheckBoxFunctionalMessageDelete14.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete14.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete15
        '
        Me.CheckBoxFunctionalMessageDelete15.Location = New System.Drawing.Point(385, 144)
        Me.CheckBoxFunctionalMessageDelete15.Name = "CheckBoxFunctionalMessageDelete15"
        Me.CheckBoxFunctionalMessageDelete15.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete15.TabIndex = 100
        Me.CheckBoxFunctionalMessageDelete15.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete15.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete16
        '
        Me.CheckBoxFunctionalMessageDelete16.Location = New System.Drawing.Point(385, 168)
        Me.CheckBoxFunctionalMessageDelete16.Name = "CheckBoxFunctionalMessageDelete16"
        Me.CheckBoxFunctionalMessageDelete16.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete16.TabIndex = 101
        Me.CheckBoxFunctionalMessageDelete16.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete16.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete17
        '
        Me.CheckBoxFunctionalMessageDelete17.Location = New System.Drawing.Point(385, 192)
        Me.CheckBoxFunctionalMessageDelete17.Name = "CheckBoxFunctionalMessageDelete17"
        Me.CheckBoxFunctionalMessageDelete17.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete17.TabIndex = 102
        Me.CheckBoxFunctionalMessageDelete17.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete17.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete18
        '
        Me.CheckBoxFunctionalMessageDelete18.Location = New System.Drawing.Point(385, 216)
        Me.CheckBoxFunctionalMessageDelete18.Name = "CheckBoxFunctionalMessageDelete18"
        Me.CheckBoxFunctionalMessageDelete18.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete18.TabIndex = 103
        Me.CheckBoxFunctionalMessageDelete18.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete18.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete19
        '
        Me.CheckBoxFunctionalMessageDelete19.Location = New System.Drawing.Point(385, 240)
        Me.CheckBoxFunctionalMessageDelete19.Name = "CheckBoxFunctionalMessageDelete19"
        Me.CheckBoxFunctionalMessageDelete19.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete19.TabIndex = 104
        Me.CheckBoxFunctionalMessageDelete19.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete19.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete20
        '
        Me.CheckBoxFunctionalMessageDelete20.Location = New System.Drawing.Point(385, 264)
        Me.CheckBoxFunctionalMessageDelete20.Name = "CheckBoxFunctionalMessageDelete20"
        Me.CheckBoxFunctionalMessageDelete20.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete20.TabIndex = 105
        Me.CheckBoxFunctionalMessageDelete20.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete20.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete21
        '
        Me.CheckBoxFunctionalMessageDelete21.Location = New System.Drawing.Point(385, 288)
        Me.CheckBoxFunctionalMessageDelete21.Name = "CheckBoxFunctionalMessageDelete21"
        Me.CheckBoxFunctionalMessageDelete21.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete21.TabIndex = 106
        Me.CheckBoxFunctionalMessageDelete21.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete21.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete22
        '
        Me.CheckBoxFunctionalMessageDelete22.Location = New System.Drawing.Point(593, 48)
        Me.CheckBoxFunctionalMessageDelete22.Name = "CheckBoxFunctionalMessageDelete22"
        Me.CheckBoxFunctionalMessageDelete22.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete22.TabIndex = 127
        Me.CheckBoxFunctionalMessageDelete22.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete22.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete23
        '
        Me.CheckBoxFunctionalMessageDelete23.Location = New System.Drawing.Point(593, 72)
        Me.CheckBoxFunctionalMessageDelete23.Name = "CheckBoxFunctionalMessageDelete23"
        Me.CheckBoxFunctionalMessageDelete23.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete23.TabIndex = 128
        Me.CheckBoxFunctionalMessageDelete23.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete23.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete24
        '
        Me.CheckBoxFunctionalMessageDelete24.Location = New System.Drawing.Point(593, 96)
        Me.CheckBoxFunctionalMessageDelete24.Name = "CheckBoxFunctionalMessageDelete24"
        Me.CheckBoxFunctionalMessageDelete24.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete24.TabIndex = 129
        Me.CheckBoxFunctionalMessageDelete24.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete24.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete25
        '
        Me.CheckBoxFunctionalMessageDelete25.Location = New System.Drawing.Point(593, 120)
        Me.CheckBoxFunctionalMessageDelete25.Name = "CheckBoxFunctionalMessageDelete25"
        Me.CheckBoxFunctionalMessageDelete25.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete25.TabIndex = 130
        Me.CheckBoxFunctionalMessageDelete25.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete25.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete26
        '
        Me.CheckBoxFunctionalMessageDelete26.Location = New System.Drawing.Point(593, 144)
        Me.CheckBoxFunctionalMessageDelete26.Name = "CheckBoxFunctionalMessageDelete26"
        Me.CheckBoxFunctionalMessageDelete26.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete26.TabIndex = 131
        Me.CheckBoxFunctionalMessageDelete26.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete26.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete27
        '
        Me.CheckBoxFunctionalMessageDelete27.Location = New System.Drawing.Point(593, 168)
        Me.CheckBoxFunctionalMessageDelete27.Name = "CheckBoxFunctionalMessageDelete27"
        Me.CheckBoxFunctionalMessageDelete27.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete27.TabIndex = 132
        Me.CheckBoxFunctionalMessageDelete27.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete27.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete28
        '
        Me.CheckBoxFunctionalMessageDelete28.Location = New System.Drawing.Point(593, 192)
        Me.CheckBoxFunctionalMessageDelete28.Name = "CheckBoxFunctionalMessageDelete28"
        Me.CheckBoxFunctionalMessageDelete28.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete28.TabIndex = 133
        Me.CheckBoxFunctionalMessageDelete28.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete28.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete29
        '
        Me.CheckBoxFunctionalMessageDelete29.Location = New System.Drawing.Point(593, 216)
        Me.CheckBoxFunctionalMessageDelete29.Name = "CheckBoxFunctionalMessageDelete29"
        Me.CheckBoxFunctionalMessageDelete29.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete29.TabIndex = 134
        Me.CheckBoxFunctionalMessageDelete29.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete29.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete30
        '
        Me.CheckBoxFunctionalMessageDelete30.Location = New System.Drawing.Point(593, 240)
        Me.CheckBoxFunctionalMessageDelete30.Name = "CheckBoxFunctionalMessageDelete30"
        Me.CheckBoxFunctionalMessageDelete30.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete30.TabIndex = 135
        Me.CheckBoxFunctionalMessageDelete30.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete30.UseVisualStyleBackColor = True
        '
        'CheckBoxFunctionalMessageDelete31
        '
        Me.CheckBoxFunctionalMessageDelete31.Location = New System.Drawing.Point(593, 264)
        Me.CheckBoxFunctionalMessageDelete31.Name = "CheckBoxFunctionalMessageDelete31"
        Me.CheckBoxFunctionalMessageDelete31.Size = New System.Drawing.Size(57, 17)
        Me.CheckBoxFunctionalMessageDelete31.TabIndex = 136
        Me.CheckBoxFunctionalMessageDelete31.Text = "Delete"
        Me.CheckBoxFunctionalMessageDelete31.UseVisualStyleBackColor = True
        '
        'TextBoxFunctionalMessage0
        '
        Me.TextBoxFunctionalMessage0.AcceptsReturn = True
        Me.TextBoxFunctionalMessage0.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage0.Location = New System.Drawing.Point(113, 48)
        Me.TextBoxFunctionalMessage0.MaxLength = 0
        Me.TextBoxFunctionalMessage0.Name = "TextBoxFunctionalMessage0"
        Me.TextBoxFunctionalMessage0.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage0.TabIndex = 0
        '
        'TextBoxFunctionalMessage1
        '
        Me.TextBoxFunctionalMessage1.AcceptsReturn = True
        Me.TextBoxFunctionalMessage1.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage1.Location = New System.Drawing.Point(113, 72)
        Me.TextBoxFunctionalMessage1.MaxLength = 0
        Me.TextBoxFunctionalMessage1.Name = "TextBoxFunctionalMessage1"
        Me.TextBoxFunctionalMessage1.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage1.TabIndex = 1
        '
        'TextBoxFunctionalMessage2
        '
        Me.TextBoxFunctionalMessage2.AcceptsReturn = True
        Me.TextBoxFunctionalMessage2.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage2.Location = New System.Drawing.Point(113, 96)
        Me.TextBoxFunctionalMessage2.MaxLength = 0
        Me.TextBoxFunctionalMessage2.Name = "TextBoxFunctionalMessage2"
        Me.TextBoxFunctionalMessage2.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage2.TabIndex = 2
        '
        'TextBoxFunctionalMessage3
        '
        Me.TextBoxFunctionalMessage3.AcceptsReturn = True
        Me.TextBoxFunctionalMessage3.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage3.Location = New System.Drawing.Point(113, 120)
        Me.TextBoxFunctionalMessage3.MaxLength = 0
        Me.TextBoxFunctionalMessage3.Name = "TextBoxFunctionalMessage3"
        Me.TextBoxFunctionalMessage3.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage3.TabIndex = 3
        '
        'TextBoxFunctionalMessage4
        '
        Me.TextBoxFunctionalMessage4.AcceptsReturn = True
        Me.TextBoxFunctionalMessage4.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage4.Location = New System.Drawing.Point(113, 144)
        Me.TextBoxFunctionalMessage4.MaxLength = 0
        Me.TextBoxFunctionalMessage4.Name = "TextBoxFunctionalMessage4"
        Me.TextBoxFunctionalMessage4.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage4.TabIndex = 4
        '
        'TextBoxFunctionalMessage5
        '
        Me.TextBoxFunctionalMessage5.AcceptsReturn = True
        Me.TextBoxFunctionalMessage5.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage5.Location = New System.Drawing.Point(113, 168)
        Me.TextBoxFunctionalMessage5.MaxLength = 0
        Me.TextBoxFunctionalMessage5.Name = "TextBoxFunctionalMessage5"
        Me.TextBoxFunctionalMessage5.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage5.TabIndex = 5
        '
        'TextBoxFunctionalMessage6
        '
        Me.TextBoxFunctionalMessage6.AcceptsReturn = True
        Me.TextBoxFunctionalMessage6.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage6.Location = New System.Drawing.Point(113, 192)
        Me.TextBoxFunctionalMessage6.MaxLength = 0
        Me.TextBoxFunctionalMessage6.Name = "TextBoxFunctionalMessage6"
        Me.TextBoxFunctionalMessage6.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage6.TabIndex = 6
        '
        'TextBoxFunctionalMessage7
        '
        Me.TextBoxFunctionalMessage7.AcceptsReturn = True
        Me.TextBoxFunctionalMessage7.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage7.Location = New System.Drawing.Point(113, 216)
        Me.TextBoxFunctionalMessage7.MaxLength = 0
        Me.TextBoxFunctionalMessage7.Name = "TextBoxFunctionalMessage7"
        Me.TextBoxFunctionalMessage7.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage7.TabIndex = 7
        '
        'TextBoxFunctionalMessage8
        '
        Me.TextBoxFunctionalMessage8.AcceptsReturn = True
        Me.TextBoxFunctionalMessage8.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage8.Location = New System.Drawing.Point(113, 240)
        Me.TextBoxFunctionalMessage8.MaxLength = 0
        Me.TextBoxFunctionalMessage8.Name = "TextBoxFunctionalMessage8"
        Me.TextBoxFunctionalMessage8.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage8.TabIndex = 8
        '
        'TextBoxFunctionalMessage9
        '
        Me.TextBoxFunctionalMessage9.AcceptsReturn = True
        Me.TextBoxFunctionalMessage9.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage9.Location = New System.Drawing.Point(113, 264)
        Me.TextBoxFunctionalMessage9.MaxLength = 0
        Me.TextBoxFunctionalMessage9.Name = "TextBoxFunctionalMessage9"
        Me.TextBoxFunctionalMessage9.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage9.TabIndex = 9
        '
        'TextBoxFunctionalMessage10
        '
        Me.TextBoxFunctionalMessage10.AcceptsReturn = True
        Me.TextBoxFunctionalMessage10.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage10.Location = New System.Drawing.Point(113, 288)
        Me.TextBoxFunctionalMessage10.MaxLength = 0
        Me.TextBoxFunctionalMessage10.Name = "TextBoxFunctionalMessage10"
        Me.TextBoxFunctionalMessage10.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage10.TabIndex = 10
        '
        'TextBoxFunctionalMessage11
        '
        Me.TextBoxFunctionalMessage11.AcceptsReturn = True
        Me.TextBoxFunctionalMessage11.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage11.Location = New System.Drawing.Point(321, 48)
        Me.TextBoxFunctionalMessage11.MaxLength = 0
        Me.TextBoxFunctionalMessage11.Name = "TextBoxFunctionalMessage11"
        Me.TextBoxFunctionalMessage11.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage11.TabIndex = 11
        '
        'TextBoxFunctionalMessage12
        '
        Me.TextBoxFunctionalMessage12.AcceptsReturn = True
        Me.TextBoxFunctionalMessage12.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage12.Location = New System.Drawing.Point(321, 72)
        Me.TextBoxFunctionalMessage12.MaxLength = 0
        Me.TextBoxFunctionalMessage12.Name = "TextBoxFunctionalMessage12"
        Me.TextBoxFunctionalMessage12.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage12.TabIndex = 12
        '
        'TextBoxFunctionalMessage13
        '
        Me.TextBoxFunctionalMessage13.AcceptsReturn = True
        Me.TextBoxFunctionalMessage13.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage13.Location = New System.Drawing.Point(321, 96)
        Me.TextBoxFunctionalMessage13.MaxLength = 0
        Me.TextBoxFunctionalMessage13.Name = "TextBoxFunctionalMessage13"
        Me.TextBoxFunctionalMessage13.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage13.TabIndex = 13
        '
        'TextBoxFunctionalMessage14
        '
        Me.TextBoxFunctionalMessage14.AcceptsReturn = True
        Me.TextBoxFunctionalMessage14.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage14.Location = New System.Drawing.Point(321, 120)
        Me.TextBoxFunctionalMessage14.MaxLength = 0
        Me.TextBoxFunctionalMessage14.Name = "TextBoxFunctionalMessage14"
        Me.TextBoxFunctionalMessage14.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage14.TabIndex = 14
        '
        'TextBoxFunctionalMessage15
        '
        Me.TextBoxFunctionalMessage15.AcceptsReturn = True
        Me.TextBoxFunctionalMessage15.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage15.Location = New System.Drawing.Point(321, 144)
        Me.TextBoxFunctionalMessage15.MaxLength = 0
        Me.TextBoxFunctionalMessage15.Name = "TextBoxFunctionalMessage15"
        Me.TextBoxFunctionalMessage15.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage15.TabIndex = 15
        '
        'TextBoxFunctionalMessage16
        '
        Me.TextBoxFunctionalMessage16.AcceptsReturn = True
        Me.TextBoxFunctionalMessage16.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage16.Location = New System.Drawing.Point(321, 168)
        Me.TextBoxFunctionalMessage16.MaxLength = 0
        Me.TextBoxFunctionalMessage16.Name = "TextBoxFunctionalMessage16"
        Me.TextBoxFunctionalMessage16.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage16.TabIndex = 16
        '
        'TextBoxFunctionalMessage17
        '
        Me.TextBoxFunctionalMessage17.AcceptsReturn = True
        Me.TextBoxFunctionalMessage17.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage17.Location = New System.Drawing.Point(321, 192)
        Me.TextBoxFunctionalMessage17.MaxLength = 0
        Me.TextBoxFunctionalMessage17.Name = "TextBoxFunctionalMessage17"
        Me.TextBoxFunctionalMessage17.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage17.TabIndex = 17
        '
        'TextBoxFunctionalMessage18
        '
        Me.TextBoxFunctionalMessage18.AcceptsReturn = True
        Me.TextBoxFunctionalMessage18.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage18.Location = New System.Drawing.Point(321, 216)
        Me.TextBoxFunctionalMessage18.MaxLength = 0
        Me.TextBoxFunctionalMessage18.Name = "TextBoxFunctionalMessage18"
        Me.TextBoxFunctionalMessage18.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage18.TabIndex = 18
        '
        'TextBoxFunctionalMessage19
        '
        Me.TextBoxFunctionalMessage19.AcceptsReturn = True
        Me.TextBoxFunctionalMessage19.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage19.Location = New System.Drawing.Point(321, 240)
        Me.TextBoxFunctionalMessage19.MaxLength = 0
        Me.TextBoxFunctionalMessage19.Name = "TextBoxFunctionalMessage19"
        Me.TextBoxFunctionalMessage19.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage19.TabIndex = 19
        '
        'TextBoxFunctionalMessage20
        '
        Me.TextBoxFunctionalMessage20.AcceptsReturn = True
        Me.TextBoxFunctionalMessage20.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage20.Location = New System.Drawing.Point(321, 264)
        Me.TextBoxFunctionalMessage20.MaxLength = 0
        Me.TextBoxFunctionalMessage20.Name = "TextBoxFunctionalMessage20"
        Me.TextBoxFunctionalMessage20.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage20.TabIndex = 20
        '
        'TextBoxFunctionalMessage21
        '
        Me.TextBoxFunctionalMessage21.AcceptsReturn = True
        Me.TextBoxFunctionalMessage21.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage21.Location = New System.Drawing.Point(321, 288)
        Me.TextBoxFunctionalMessage21.MaxLength = 0
        Me.TextBoxFunctionalMessage21.Name = "TextBoxFunctionalMessage21"
        Me.TextBoxFunctionalMessage21.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage21.TabIndex = 21
        '
        'TextBoxFunctionalMessage22
        '
        Me.TextBoxFunctionalMessage22.AcceptsReturn = True
        Me.TextBoxFunctionalMessage22.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage22.Location = New System.Drawing.Point(529, 48)
        Me.TextBoxFunctionalMessage22.MaxLength = 0
        Me.TextBoxFunctionalMessage22.Name = "TextBoxFunctionalMessage22"
        Me.TextBoxFunctionalMessage22.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage22.TabIndex = 22
        '
        'TextBoxFunctionalMessage23
        '
        Me.TextBoxFunctionalMessage23.AcceptsReturn = True
        Me.TextBoxFunctionalMessage23.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage23.Location = New System.Drawing.Point(529, 72)
        Me.TextBoxFunctionalMessage23.MaxLength = 0
        Me.TextBoxFunctionalMessage23.Name = "TextBoxFunctionalMessage23"
        Me.TextBoxFunctionalMessage23.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage23.TabIndex = 23
        '
        'TextBoxFunctionalMessage24
        '
        Me.TextBoxFunctionalMessage24.AcceptsReturn = True
        Me.TextBoxFunctionalMessage24.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage24.Location = New System.Drawing.Point(529, 96)
        Me.TextBoxFunctionalMessage24.MaxLength = 0
        Me.TextBoxFunctionalMessage24.Name = "TextBoxFunctionalMessage24"
        Me.TextBoxFunctionalMessage24.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage24.TabIndex = 24
        '
        'TextBoxFunctionalMessage25
        '
        Me.TextBoxFunctionalMessage25.AcceptsReturn = True
        Me.TextBoxFunctionalMessage25.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage25.Location = New System.Drawing.Point(529, 120)
        Me.TextBoxFunctionalMessage25.MaxLength = 0
        Me.TextBoxFunctionalMessage25.Name = "TextBoxFunctionalMessage25"
        Me.TextBoxFunctionalMessage25.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage25.TabIndex = 25
        '
        'TextBoxFunctionalMessage26
        '
        Me.TextBoxFunctionalMessage26.AcceptsReturn = True
        Me.TextBoxFunctionalMessage26.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage26.Location = New System.Drawing.Point(529, 144)
        Me.TextBoxFunctionalMessage26.MaxLength = 0
        Me.TextBoxFunctionalMessage26.Name = "TextBoxFunctionalMessage26"
        Me.TextBoxFunctionalMessage26.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage26.TabIndex = 26
        '
        'TextBoxFunctionalMessage27
        '
        Me.TextBoxFunctionalMessage27.AcceptsReturn = True
        Me.TextBoxFunctionalMessage27.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage27.Location = New System.Drawing.Point(529, 168)
        Me.TextBoxFunctionalMessage27.MaxLength = 0
        Me.TextBoxFunctionalMessage27.Name = "TextBoxFunctionalMessage27"
        Me.TextBoxFunctionalMessage27.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage27.TabIndex = 27
        '
        'TextBoxFunctionalMessage28
        '
        Me.TextBoxFunctionalMessage28.AcceptsReturn = True
        Me.TextBoxFunctionalMessage28.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage28.Location = New System.Drawing.Point(529, 192)
        Me.TextBoxFunctionalMessage28.MaxLength = 0
        Me.TextBoxFunctionalMessage28.Name = "TextBoxFunctionalMessage28"
        Me.TextBoxFunctionalMessage28.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage28.TabIndex = 28
        '
        'TextBoxFunctionalMessage29
        '
        Me.TextBoxFunctionalMessage29.AcceptsReturn = True
        Me.TextBoxFunctionalMessage29.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage29.Location = New System.Drawing.Point(529, 216)
        Me.TextBoxFunctionalMessage29.MaxLength = 0
        Me.TextBoxFunctionalMessage29.Name = "TextBoxFunctionalMessage29"
        Me.TextBoxFunctionalMessage29.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage29.TabIndex = 29
        '
        'TextBoxFunctionalMessage30
        '
        Me.TextBoxFunctionalMessage30.AcceptsReturn = True
        Me.TextBoxFunctionalMessage30.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage30.Location = New System.Drawing.Point(529, 240)
        Me.TextBoxFunctionalMessage30.MaxLength = 0
        Me.TextBoxFunctionalMessage30.Name = "TextBoxFunctionalMessage30"
        Me.TextBoxFunctionalMessage30.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage30.TabIndex = 30
        '
        'TextBoxFunctionalMessage31
        '
        Me.TextBoxFunctionalMessage31.AcceptsReturn = True
        Me.TextBoxFunctionalMessage31.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxFunctionalMessage31.Location = New System.Drawing.Point(529, 264)
        Me.TextBoxFunctionalMessage31.MaxLength = 0
        Me.TextBoxFunctionalMessage31.Name = "TextBoxFunctionalMessage31"
        Me.TextBoxFunctionalMessage31.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxFunctionalMessage31.TabIndex = 31
        '
        'LabelChannel6
        '
        Me.LabelChannel6.Location = New System.Drawing.Point(524, 336)
        Me.LabelChannel6.Name = "LabelChannel6"
        Me.LabelChannel6.Size = New System.Drawing.Size(49, 17)
        Me.LabelChannel6.TabIndex = 36
        Me.LabelChannel6.Text = "Channel:"
        '
        'LabelDeviceCombo6
        '
        Me.LabelDeviceCombo6.Location = New System.Drawing.Point(395, 336)
        Me.LabelDeviceCombo6.Name = "LabelDeviceCombo6"
        Me.LabelDeviceCombo6.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo6.TabIndex = 35
        Me.LabelDeviceCombo6.Text = "Device:"
        '
        'LabelFunctionalMessageModify
        '
        Me.LabelFunctionalMessageModify.Location = New System.Drawing.Point(113, 32)
        Me.LabelFunctionalMessageModify.Name = "LabelFunctionalMessageModify"
        Me.LabelFunctionalMessageModify.Size = New System.Drawing.Size(57, 13)
        Me.LabelFunctionalMessageModify.TabIndex = 40
        Me.LabelFunctionalMessageModify.Text = "Modify:"
        '
        'LabelFunctionalMessageValues
        '
        Me.LabelFunctionalMessageValues.Location = New System.Drawing.Point(65, 32)
        Me.LabelFunctionalMessageValues.Name = "LabelFunctionalMessageValues"
        Me.LabelFunctionalMessageValues.Size = New System.Drawing.Size(42, 16)
        Me.LabelFunctionalMessageValues.TabIndex = 39
        Me.LabelFunctionalMessageValues.Text = "Addr:"
        '
        'LabelFuncId31
        '
        Me.LabelFuncId31.Location = New System.Drawing.Point(456, 265)
        Me.LabelFuncId31.Name = "LabelFuncId31"
        Me.LabelFuncId31.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId31.TabIndex = 116
        Me.LabelFuncId31.Text = "31:"
        Me.LabelFuncId31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId30
        '
        Me.LabelFuncId30.Location = New System.Drawing.Point(456, 241)
        Me.LabelFuncId30.Name = "LabelFuncId30"
        Me.LabelFuncId30.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId30.TabIndex = 115
        Me.LabelFuncId30.Text = "30:"
        Me.LabelFuncId30.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId29
        '
        Me.LabelFuncId29.Location = New System.Drawing.Point(456, 217)
        Me.LabelFuncId29.Name = "LabelFuncId29"
        Me.LabelFuncId29.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId29.TabIndex = 114
        Me.LabelFuncId29.Text = "29:"
        Me.LabelFuncId29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId28
        '
        Me.LabelFuncId28.Location = New System.Drawing.Point(456, 193)
        Me.LabelFuncId28.Name = "LabelFuncId28"
        Me.LabelFuncId28.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId28.TabIndex = 113
        Me.LabelFuncId28.Text = "28:"
        Me.LabelFuncId28.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId27
        '
        Me.LabelFuncId27.Location = New System.Drawing.Point(456, 169)
        Me.LabelFuncId27.Name = "LabelFuncId27"
        Me.LabelFuncId27.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId27.TabIndex = 112
        Me.LabelFuncId27.Text = "27:"
        Me.LabelFuncId27.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId26
        '
        Me.LabelFuncId26.Location = New System.Drawing.Point(456, 145)
        Me.LabelFuncId26.Name = "LabelFuncId26"
        Me.LabelFuncId26.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId26.TabIndex = 111
        Me.LabelFuncId26.Text = "26:"
        Me.LabelFuncId26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId25
        '
        Me.LabelFuncId25.Location = New System.Drawing.Point(456, 121)
        Me.LabelFuncId25.Name = "LabelFuncId25"
        Me.LabelFuncId25.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId25.TabIndex = 110
        Me.LabelFuncId25.Text = "25:"
        Me.LabelFuncId25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId24
        '
        Me.LabelFuncId24.Location = New System.Drawing.Point(456, 97)
        Me.LabelFuncId24.Name = "LabelFuncId24"
        Me.LabelFuncId24.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId24.TabIndex = 109
        Me.LabelFuncId24.Text = "24:"
        Me.LabelFuncId24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId23
        '
        Me.LabelFuncId23.Location = New System.Drawing.Point(456, 73)
        Me.LabelFuncId23.Name = "LabelFuncId23"
        Me.LabelFuncId23.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId23.TabIndex = 108
        Me.LabelFuncId23.Text = "23:"
        Me.LabelFuncId23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId22
        '
        Me.LabelFuncId22.Location = New System.Drawing.Point(456, 49)
        Me.LabelFuncId22.Name = "LabelFuncId22"
        Me.LabelFuncId22.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId22.TabIndex = 107
        Me.LabelFuncId22.Text = "22:"
        Me.LabelFuncId22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId21
        '
        Me.LabelFuncId21.Location = New System.Drawing.Point(248, 289)
        Me.LabelFuncId21.Name = "LabelFuncId21"
        Me.LabelFuncId21.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId21.TabIndex = 84
        Me.LabelFuncId21.Text = "21:"
        Me.LabelFuncId21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId20
        '
        Me.LabelFuncId20.Location = New System.Drawing.Point(248, 265)
        Me.LabelFuncId20.Name = "LabelFuncId20"
        Me.LabelFuncId20.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId20.TabIndex = 83
        Me.LabelFuncId20.Text = "20:"
        Me.LabelFuncId20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId19
        '
        Me.LabelFuncId19.Location = New System.Drawing.Point(248, 241)
        Me.LabelFuncId19.Name = "LabelFuncId19"
        Me.LabelFuncId19.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId19.TabIndex = 82
        Me.LabelFuncId19.Text = "19:"
        Me.LabelFuncId19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId18
        '
        Me.LabelFuncId18.Location = New System.Drawing.Point(248, 217)
        Me.LabelFuncId18.Name = "LabelFuncId18"
        Me.LabelFuncId18.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId18.TabIndex = 81
        Me.LabelFuncId18.Text = "18:"
        Me.LabelFuncId18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId17
        '
        Me.LabelFuncId17.Location = New System.Drawing.Point(248, 193)
        Me.LabelFuncId17.Name = "LabelFuncId17"
        Me.LabelFuncId17.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId17.TabIndex = 80
        Me.LabelFuncId17.Text = "17:"
        Me.LabelFuncId17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId16
        '
        Me.LabelFuncId16.Location = New System.Drawing.Point(248, 169)
        Me.LabelFuncId16.Name = "LabelFuncId16"
        Me.LabelFuncId16.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId16.TabIndex = 79
        Me.LabelFuncId16.Text = "16:"
        Me.LabelFuncId16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId15
        '
        Me.LabelFuncId15.Location = New System.Drawing.Point(248, 145)
        Me.LabelFuncId15.Name = "LabelFuncId15"
        Me.LabelFuncId15.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId15.TabIndex = 78
        Me.LabelFuncId15.Text = "15:"
        Me.LabelFuncId15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId14
        '
        Me.LabelFuncId14.Location = New System.Drawing.Point(248, 121)
        Me.LabelFuncId14.Name = "LabelFuncId14"
        Me.LabelFuncId14.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId14.TabIndex = 77
        Me.LabelFuncId14.Text = "14:"
        Me.LabelFuncId14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId13
        '
        Me.LabelFuncId13.Location = New System.Drawing.Point(248, 97)
        Me.LabelFuncId13.Name = "LabelFuncId13"
        Me.LabelFuncId13.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId13.TabIndex = 76
        Me.LabelFuncId13.Text = "13:"
        Me.LabelFuncId13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId12
        '
        Me.LabelFuncId12.Location = New System.Drawing.Point(248, 73)
        Me.LabelFuncId12.Name = "LabelFuncId12"
        Me.LabelFuncId12.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId12.TabIndex = 75
        Me.LabelFuncId12.Text = "12:"
        Me.LabelFuncId12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId11
        '
        Me.LabelFuncId11.Location = New System.Drawing.Point(248, 49)
        Me.LabelFuncId11.Name = "LabelFuncId11"
        Me.LabelFuncId11.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId11.TabIndex = 74
        Me.LabelFuncId11.Text = "11:"
        Me.LabelFuncId11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId10
        '
        Me.LabelFuncId10.Location = New System.Drawing.Point(40, 289)
        Me.LabelFuncId10.Name = "LabelFuncId10"
        Me.LabelFuncId10.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId10.TabIndex = 51
        Me.LabelFuncId10.Text = "10:"
        Me.LabelFuncId10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId9
        '
        Me.LabelFuncId9.Location = New System.Drawing.Point(40, 265)
        Me.LabelFuncId9.Name = "LabelFuncId9"
        Me.LabelFuncId9.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId9.TabIndex = 50
        Me.LabelFuncId9.Text = "9:"
        Me.LabelFuncId9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId8
        '
        Me.LabelFuncId8.Location = New System.Drawing.Point(40, 241)
        Me.LabelFuncId8.Name = "LabelFuncId8"
        Me.LabelFuncId8.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId8.TabIndex = 49
        Me.LabelFuncId8.Text = "8:"
        Me.LabelFuncId8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId7
        '
        Me.LabelFuncId7.Location = New System.Drawing.Point(40, 217)
        Me.LabelFuncId7.Name = "LabelFuncId7"
        Me.LabelFuncId7.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId7.TabIndex = 48
        Me.LabelFuncId7.Text = "7:"
        Me.LabelFuncId7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId6
        '
        Me.LabelFuncId6.Location = New System.Drawing.Point(40, 193)
        Me.LabelFuncId6.Name = "LabelFuncId6"
        Me.LabelFuncId6.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId6.TabIndex = 47
        Me.LabelFuncId6.Text = "6:"
        Me.LabelFuncId6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId5
        '
        Me.LabelFuncId5.Location = New System.Drawing.Point(40, 169)
        Me.LabelFuncId5.Name = "LabelFuncId5"
        Me.LabelFuncId5.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId5.TabIndex = 46
        Me.LabelFuncId5.Text = "5:"
        Me.LabelFuncId5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId4
        '
        Me.LabelFuncId4.Location = New System.Drawing.Point(40, 145)
        Me.LabelFuncId4.Name = "LabelFuncId4"
        Me.LabelFuncId4.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId4.TabIndex = 45
        Me.LabelFuncId4.Text = "4:"
        Me.LabelFuncId4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId3
        '
        Me.LabelFuncId3.Location = New System.Drawing.Point(40, 121)
        Me.LabelFuncId3.Name = "LabelFuncId3"
        Me.LabelFuncId3.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId3.TabIndex = 44
        Me.LabelFuncId3.Text = "3:"
        Me.LabelFuncId3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId2
        '
        Me.LabelFuncId2.Location = New System.Drawing.Point(40, 97)
        Me.LabelFuncId2.Name = "LabelFuncId2"
        Me.LabelFuncId2.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId2.TabIndex = 43
        Me.LabelFuncId2.Text = "2:"
        Me.LabelFuncId2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId1
        '
        Me.LabelFuncId1.Location = New System.Drawing.Point(40, 73)
        Me.LabelFuncId1.Name = "LabelFuncId1"
        Me.LabelFuncId1.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId1.TabIndex = 42
        Me.LabelFuncId1.Text = "1:"
        Me.LabelFuncId1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFuncId0
        '
        Me.LabelFuncId0.Location = New System.Drawing.Point(40, 49)
        Me.LabelFuncId0.Name = "LabelFuncId0"
        Me.LabelFuncId0.Size = New System.Drawing.Size(22, 17)
        Me.LabelFuncId0.TabIndex = 41
        Me.LabelFuncId0.Text = "0:"
        Me.LabelFuncId0.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelFunctionalMessage0
        '
        Me.LabelFunctionalMessage0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage0.Location = New System.Drawing.Point(65, 48)
        Me.LabelFunctionalMessage0.Name = "LabelFunctionalMessage0"
        Me.LabelFunctionalMessage0.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage0.TabIndex = 52
        '
        'LabelFunctionalMessage1
        '
        Me.LabelFunctionalMessage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage1.Location = New System.Drawing.Point(65, 72)
        Me.LabelFunctionalMessage1.Name = "LabelFunctionalMessage1"
        Me.LabelFunctionalMessage1.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage1.TabIndex = 53
        '
        'LabelFunctionalMessage2
        '
        Me.LabelFunctionalMessage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage2.Location = New System.Drawing.Point(65, 96)
        Me.LabelFunctionalMessage2.Name = "LabelFunctionalMessage2"
        Me.LabelFunctionalMessage2.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage2.TabIndex = 54
        '
        'LabelFunctionalMessage3
        '
        Me.LabelFunctionalMessage3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage3.Location = New System.Drawing.Point(65, 120)
        Me.LabelFunctionalMessage3.Name = "LabelFunctionalMessage3"
        Me.LabelFunctionalMessage3.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage3.TabIndex = 55
        '
        'LabelFunctionalMessage4
        '
        Me.LabelFunctionalMessage4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage4.Location = New System.Drawing.Point(65, 144)
        Me.LabelFunctionalMessage4.Name = "LabelFunctionalMessage4"
        Me.LabelFunctionalMessage4.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage4.TabIndex = 56
        '
        'LabelFunctionalMessage5
        '
        Me.LabelFunctionalMessage5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage5.Location = New System.Drawing.Point(65, 168)
        Me.LabelFunctionalMessage5.Name = "LabelFunctionalMessage5"
        Me.LabelFunctionalMessage5.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage5.TabIndex = 57
        '
        'LabelFunctionalMessage6
        '
        Me.LabelFunctionalMessage6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage6.Location = New System.Drawing.Point(65, 192)
        Me.LabelFunctionalMessage6.Name = "LabelFunctionalMessage6"
        Me.LabelFunctionalMessage6.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage6.TabIndex = 58
        '
        'LabelFunctionalMessage7
        '
        Me.LabelFunctionalMessage7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage7.Location = New System.Drawing.Point(65, 216)
        Me.LabelFunctionalMessage7.Name = "LabelFunctionalMessage7"
        Me.LabelFunctionalMessage7.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage7.TabIndex = 59
        '
        'LabelFunctionalMessage8
        '
        Me.LabelFunctionalMessage8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage8.Location = New System.Drawing.Point(65, 240)
        Me.LabelFunctionalMessage8.Name = "LabelFunctionalMessage8"
        Me.LabelFunctionalMessage8.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage8.TabIndex = 60
        '
        'LabelFunctionalMessage9
        '
        Me.LabelFunctionalMessage9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage9.Location = New System.Drawing.Point(65, 264)
        Me.LabelFunctionalMessage9.Name = "LabelFunctionalMessage9"
        Me.LabelFunctionalMessage9.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage9.TabIndex = 61
        '
        'LabelFunctionalMessage10
        '
        Me.LabelFunctionalMessage10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage10.Location = New System.Drawing.Point(65, 288)
        Me.LabelFunctionalMessage10.Name = "LabelFunctionalMessage10"
        Me.LabelFunctionalMessage10.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage10.TabIndex = 62
        '
        'LabelFunctionalMessage11
        '
        Me.LabelFunctionalMessage11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage11.Location = New System.Drawing.Point(273, 48)
        Me.LabelFunctionalMessage11.Name = "LabelFunctionalMessage11"
        Me.LabelFunctionalMessage11.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage11.TabIndex = 85
        '
        'LabelFunctionalMessage12
        '
        Me.LabelFunctionalMessage12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage12.Location = New System.Drawing.Point(273, 72)
        Me.LabelFunctionalMessage12.Name = "LabelFunctionalMessage12"
        Me.LabelFunctionalMessage12.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage12.TabIndex = 86
        '
        'LabelFunctionalMessage13
        '
        Me.LabelFunctionalMessage13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage13.Location = New System.Drawing.Point(273, 96)
        Me.LabelFunctionalMessage13.Name = "LabelFunctionalMessage13"
        Me.LabelFunctionalMessage13.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage13.TabIndex = 87
        '
        'LabelFunctionalMessage14
        '
        Me.LabelFunctionalMessage14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage14.Location = New System.Drawing.Point(273, 120)
        Me.LabelFunctionalMessage14.Name = "LabelFunctionalMessage14"
        Me.LabelFunctionalMessage14.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage14.TabIndex = 88
        '
        'LabelFunctionalMessage15
        '
        Me.LabelFunctionalMessage15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage15.Location = New System.Drawing.Point(273, 144)
        Me.LabelFunctionalMessage15.Name = "LabelFunctionalMessage15"
        Me.LabelFunctionalMessage15.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage15.TabIndex = 89
        '
        'LabelFunctionalMessage16
        '
        Me.LabelFunctionalMessage16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage16.Location = New System.Drawing.Point(273, 168)
        Me.LabelFunctionalMessage16.Name = "LabelFunctionalMessage16"
        Me.LabelFunctionalMessage16.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage16.TabIndex = 90
        '
        'LabelFunctionalMessage17
        '
        Me.LabelFunctionalMessage17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage17.Location = New System.Drawing.Point(273, 192)
        Me.LabelFunctionalMessage17.Name = "LabelFunctionalMessage17"
        Me.LabelFunctionalMessage17.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage17.TabIndex = 91
        '
        'LabelFunctionalMessage18
        '
        Me.LabelFunctionalMessage18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage18.Location = New System.Drawing.Point(273, 216)
        Me.LabelFunctionalMessage18.Name = "LabelFunctionalMessage18"
        Me.LabelFunctionalMessage18.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage18.TabIndex = 92
        '
        'LabelFunctionalMessage19
        '
        Me.LabelFunctionalMessage19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage19.Location = New System.Drawing.Point(273, 240)
        Me.LabelFunctionalMessage19.Name = "LabelFunctionalMessage19"
        Me.LabelFunctionalMessage19.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage19.TabIndex = 93
        '
        'LabelFunctionalMessage20
        '
        Me.LabelFunctionalMessage20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage20.Location = New System.Drawing.Point(273, 264)
        Me.LabelFunctionalMessage20.Name = "LabelFunctionalMessage20"
        Me.LabelFunctionalMessage20.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage20.TabIndex = 94
        '
        'LabelFunctionalMessage21
        '
        Me.LabelFunctionalMessage21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage21.Location = New System.Drawing.Point(273, 288)
        Me.LabelFunctionalMessage21.Name = "LabelFunctionalMessage21"
        Me.LabelFunctionalMessage21.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage21.TabIndex = 95
        '
        'LabelFunctionalMessage22
        '
        Me.LabelFunctionalMessage22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage22.Location = New System.Drawing.Point(481, 48)
        Me.LabelFunctionalMessage22.Name = "LabelFunctionalMessage22"
        Me.LabelFunctionalMessage22.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage22.TabIndex = 117
        '
        'LabelFunctionalMessage23
        '
        Me.LabelFunctionalMessage23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage23.Location = New System.Drawing.Point(481, 72)
        Me.LabelFunctionalMessage23.Name = "LabelFunctionalMessage23"
        Me.LabelFunctionalMessage23.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage23.TabIndex = 118
        '
        'LabelFunctionalMessage24
        '
        Me.LabelFunctionalMessage24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage24.Location = New System.Drawing.Point(481, 96)
        Me.LabelFunctionalMessage24.Name = "LabelFunctionalMessage24"
        Me.LabelFunctionalMessage24.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage24.TabIndex = 119
        '
        'LabelFunctionalMessage25
        '
        Me.LabelFunctionalMessage25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage25.Location = New System.Drawing.Point(481, 120)
        Me.LabelFunctionalMessage25.Name = "LabelFunctionalMessage25"
        Me.LabelFunctionalMessage25.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage25.TabIndex = 120
        '
        'LabelFunctionalMessage26
        '
        Me.LabelFunctionalMessage26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage26.Location = New System.Drawing.Point(481, 144)
        Me.LabelFunctionalMessage26.Name = "LabelFunctionalMessage26"
        Me.LabelFunctionalMessage26.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage26.TabIndex = 121
        '
        'LabelFunctionalMessage27
        '
        Me.LabelFunctionalMessage27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage27.Location = New System.Drawing.Point(481, 168)
        Me.LabelFunctionalMessage27.Name = "LabelFunctionalMessage27"
        Me.LabelFunctionalMessage27.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage27.TabIndex = 122
        '
        'LabelFunctionalMessage28
        '
        Me.LabelFunctionalMessage28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage28.Location = New System.Drawing.Point(481, 192)
        Me.LabelFunctionalMessage28.Name = "LabelFunctionalMessage28"
        Me.LabelFunctionalMessage28.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage28.TabIndex = 123
        '
        'LabelFunctionalMessage29
        '
        Me.LabelFunctionalMessage29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage29.Location = New System.Drawing.Point(481, 216)
        Me.LabelFunctionalMessage29.Name = "LabelFunctionalMessage29"
        Me.LabelFunctionalMessage29.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage29.TabIndex = 124
        '
        'LabelFunctionalMessage30
        '
        Me.LabelFunctionalMessage30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage30.Location = New System.Drawing.Point(481, 240)
        Me.LabelFunctionalMessage30.Name = "LabelFunctionalMessage30"
        Me.LabelFunctionalMessage30.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage30.TabIndex = 125
        '
        'LabelFunctionalMessage31
        '
        Me.LabelFunctionalMessage31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelFunctionalMessage31.Location = New System.Drawing.Point(481, 264)
        Me.LabelFunctionalMessage31.Name = "LabelFunctionalMessage31"
        Me.LabelFunctionalMessage31.Size = New System.Drawing.Size(41, 19)
        Me.LabelFunctionalMessage31.TabIndex = 126
        '
        'TabPageAnalog
        '
        Me.TabPageAnalog.Controls.Add(Me.ComboBoxAnalogChannel)
        Me.TabPageAnalog.Controls.Add(Me.GroupBoxAnalog)
        Me.TabPageAnalog.Controls.Add(Me.GroupBoxBattVoltage)
        Me.TabPageAnalog.Controls.Add(Me.GroupBoxProgVoltage)
        Me.TabPageAnalog.Controls.Add(Me.LabelAnalogChannel)
        Me.TabPageAnalog.Controls.Add(Me.LabelDeviceCombo7)
        Me.TabPageAnalog.Controls.Add(Me.ComboAvailableBoxLocator7)
        Me.TabPageAnalog.Controls.Add(Me.ComboAvailableChannelLocator7)
        Me.TabPageAnalog.Location = New System.Drawing.Point(4, 22)
        Me.TabPageAnalog.Name = "TabPageAnalog"
        Me.TabPageAnalog.Size = New System.Drawing.Size(714, 395)
        Me.TabPageAnalog.TabIndex = 7
        Me.TabPageAnalog.Text = "IO && Programming Voltages"
        '
        'ComboBoxAnalogChannel
        '
        Me.ComboBoxAnalogChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxAnalogChannel.Location = New System.Drawing.Point(527, 353)
        Me.ComboBoxAnalogChannel.Name = "ComboBoxAnalogChannel"
        Me.ComboBoxAnalogChannel.Size = New System.Drawing.Size(161, 21)
        Me.ComboBoxAnalogChannel.TabIndex = 399
        '
        'GroupBoxAnalog
        '
        Me.GroupBoxAnalog.Controls.Add(Me.PanelAnalog)
        Me.GroupBoxAnalog.Controls.Add(Me.LabelAnalogLow1)
        Me.GroupBoxAnalog.Controls.Add(Me.LabelAnalogHigh1)
        Me.GroupBoxAnalog.Controls.Add(Me.LabelAnalogHigh0)
        Me.GroupBoxAnalog.Controls.Add(Me.LabelAnalogLow0)
        Me.GroupBoxAnalog.Controls.Add(Me.LabelAnalogReading)
        Me.GroupBoxAnalog.Location = New System.Drawing.Point(8, 3)
        Me.GroupBoxAnalog.Name = "GroupBoxAnalog"
        Me.GroupBoxAnalog.Size = New System.Drawing.Size(357, 322)
        Me.GroupBoxAnalog.TabIndex = 0
        Me.GroupBoxAnalog.TabStop = False
        Me.GroupBoxAnalog.Text = "Analog IO"
        '
        'PanelAnalog
        '
        Me.PanelAnalog.AutoScroll = True
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH0)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH1)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH2)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH3)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH4)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH5)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH6)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH7)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH8)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH9)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH10)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH11)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH12)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH13)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH14)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH15)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH16)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH17)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH18)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH19)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH20)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH21)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH22)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH23)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH24)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH25)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH26)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH27)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH28)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH29)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH30)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogCH31)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead0)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead1)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead2)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead3)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead4)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead5)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead6)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead7)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead8)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead9)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead10)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead11)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead12)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead13)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead14)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead15)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead16)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead17)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead18)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead19)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead20)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead21)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead22)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead23)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead24)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead25)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead26)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead27)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead28)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead29)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead30)
        Me.PanelAnalog.Controls.Add(Me.LabelAnalogRead31)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog0)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog1)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog2)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog3)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog4)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog5)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog6)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog7)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog8)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog9)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog10)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog11)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog12)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog13)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog14)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog15)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog16)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog17)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog18)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog19)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog20)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog21)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog22)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog23)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog24)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog25)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog26)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog27)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog28)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog29)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog30)
        Me.PanelAnalog.Controls.Add(Me.ProgressBarAnalog31)
        Me.PanelAnalog.Location = New System.Drawing.Point(6, 37)
        Me.PanelAnalog.Name = "PanelAnalog"
        Me.PanelAnalog.Size = New System.Drawing.Size(345, 247)
        Me.PanelAnalog.TabIndex = 0
        '
        'LabelAnalogCH0
        '
        Me.LabelAnalogCH0.Location = New System.Drawing.Point(2, 10)
        Me.LabelAnalogCH0.Name = "LabelAnalogCH0"
        Me.LabelAnalogCH0.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH0.TabIndex = 0
        Me.LabelAnalogCH0.Text = "Channel 1:"
        Me.LabelAnalogCH0.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH1
        '
        Me.LabelAnalogCH1.Location = New System.Drawing.Point(2, 34)
        Me.LabelAnalogCH1.Name = "LabelAnalogCH1"
        Me.LabelAnalogCH1.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH1.TabIndex = 1
        Me.LabelAnalogCH1.Text = "Channel 2:"
        Me.LabelAnalogCH1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH2
        '
        Me.LabelAnalogCH2.Location = New System.Drawing.Point(2, 58)
        Me.LabelAnalogCH2.Name = "LabelAnalogCH2"
        Me.LabelAnalogCH2.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH2.TabIndex = 2
        Me.LabelAnalogCH2.Text = "Channel 3:"
        Me.LabelAnalogCH2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH3
        '
        Me.LabelAnalogCH3.Location = New System.Drawing.Point(2, 82)
        Me.LabelAnalogCH3.Name = "LabelAnalogCH3"
        Me.LabelAnalogCH3.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH3.TabIndex = 3
        Me.LabelAnalogCH3.Text = "Channel 4:"
        Me.LabelAnalogCH3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH4
        '
        Me.LabelAnalogCH4.Location = New System.Drawing.Point(2, 106)
        Me.LabelAnalogCH4.Name = "LabelAnalogCH4"
        Me.LabelAnalogCH4.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH4.TabIndex = 4
        Me.LabelAnalogCH4.Text = "Channel 5:"
        Me.LabelAnalogCH4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH5
        '
        Me.LabelAnalogCH5.Location = New System.Drawing.Point(2, 130)
        Me.LabelAnalogCH5.Name = "LabelAnalogCH5"
        Me.LabelAnalogCH5.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH5.TabIndex = 5
        Me.LabelAnalogCH5.Text = "Channel 6:"
        Me.LabelAnalogCH5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH6
        '
        Me.LabelAnalogCH6.Location = New System.Drawing.Point(2, 154)
        Me.LabelAnalogCH6.Name = "LabelAnalogCH6"
        Me.LabelAnalogCH6.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH6.TabIndex = 6
        Me.LabelAnalogCH6.Text = "Channel 7:"
        Me.LabelAnalogCH6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH7
        '
        Me.LabelAnalogCH7.Location = New System.Drawing.Point(2, 178)
        Me.LabelAnalogCH7.Name = "LabelAnalogCH7"
        Me.LabelAnalogCH7.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH7.TabIndex = 7
        Me.LabelAnalogCH7.Text = "Channel 8:"
        Me.LabelAnalogCH7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH8
        '
        Me.LabelAnalogCH8.Location = New System.Drawing.Point(2, 202)
        Me.LabelAnalogCH8.Name = "LabelAnalogCH8"
        Me.LabelAnalogCH8.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH8.TabIndex = 8
        Me.LabelAnalogCH8.Text = "Channel 9:"
        Me.LabelAnalogCH8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH9
        '
        Me.LabelAnalogCH9.Location = New System.Drawing.Point(2, 226)
        Me.LabelAnalogCH9.Name = "LabelAnalogCH9"
        Me.LabelAnalogCH9.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH9.TabIndex = 9
        Me.LabelAnalogCH9.Text = "Channel 10:"
        Me.LabelAnalogCH9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH10
        '
        Me.LabelAnalogCH10.Location = New System.Drawing.Point(2, 250)
        Me.LabelAnalogCH10.Name = "LabelAnalogCH10"
        Me.LabelAnalogCH10.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH10.TabIndex = 10
        Me.LabelAnalogCH10.Text = "Channel 11:"
        Me.LabelAnalogCH10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH11
        '
        Me.LabelAnalogCH11.Location = New System.Drawing.Point(2, 274)
        Me.LabelAnalogCH11.Name = "LabelAnalogCH11"
        Me.LabelAnalogCH11.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH11.TabIndex = 11
        Me.LabelAnalogCH11.Text = "Channel 12:"
        Me.LabelAnalogCH11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH12
        '
        Me.LabelAnalogCH12.Location = New System.Drawing.Point(2, 298)
        Me.LabelAnalogCH12.Name = "LabelAnalogCH12"
        Me.LabelAnalogCH12.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH12.TabIndex = 12
        Me.LabelAnalogCH12.Text = "Channel 13:"
        Me.LabelAnalogCH12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH13
        '
        Me.LabelAnalogCH13.Location = New System.Drawing.Point(2, 322)
        Me.LabelAnalogCH13.Name = "LabelAnalogCH13"
        Me.LabelAnalogCH13.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH13.TabIndex = 13
        Me.LabelAnalogCH13.Text = "Channel 14:"
        Me.LabelAnalogCH13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH14
        '
        Me.LabelAnalogCH14.Location = New System.Drawing.Point(2, 346)
        Me.LabelAnalogCH14.Name = "LabelAnalogCH14"
        Me.LabelAnalogCH14.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH14.TabIndex = 14
        Me.LabelAnalogCH14.Text = "Channel 15:"
        Me.LabelAnalogCH14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH15
        '
        Me.LabelAnalogCH15.Location = New System.Drawing.Point(2, 370)
        Me.LabelAnalogCH15.Name = "LabelAnalogCH15"
        Me.LabelAnalogCH15.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH15.TabIndex = 15
        Me.LabelAnalogCH15.Text = "Channel 16:"
        Me.LabelAnalogCH15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH16
        '
        Me.LabelAnalogCH16.Location = New System.Drawing.Point(2, 394)
        Me.LabelAnalogCH16.Name = "LabelAnalogCH16"
        Me.LabelAnalogCH16.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH16.TabIndex = 382
        Me.LabelAnalogCH16.Text = "Channel 17:"
        Me.LabelAnalogCH16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH17
        '
        Me.LabelAnalogCH17.Location = New System.Drawing.Point(2, 418)
        Me.LabelAnalogCH17.Name = "LabelAnalogCH17"
        Me.LabelAnalogCH17.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH17.TabIndex = 382
        Me.LabelAnalogCH17.Text = "Channel 18:"
        Me.LabelAnalogCH17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH18
        '
        Me.LabelAnalogCH18.Location = New System.Drawing.Point(2, 442)
        Me.LabelAnalogCH18.Name = "LabelAnalogCH18"
        Me.LabelAnalogCH18.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH18.TabIndex = 382
        Me.LabelAnalogCH18.Text = "Channel 19:"
        Me.LabelAnalogCH18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH19
        '
        Me.LabelAnalogCH19.Location = New System.Drawing.Point(2, 466)
        Me.LabelAnalogCH19.Name = "LabelAnalogCH19"
        Me.LabelAnalogCH19.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH19.TabIndex = 382
        Me.LabelAnalogCH19.Text = "Channel 20:"
        Me.LabelAnalogCH19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH20
        '
        Me.LabelAnalogCH20.Location = New System.Drawing.Point(2, 490)
        Me.LabelAnalogCH20.Name = "LabelAnalogCH20"
        Me.LabelAnalogCH20.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH20.TabIndex = 377
        Me.LabelAnalogCH20.Text = "Channel 21:"
        Me.LabelAnalogCH20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH21
        '
        Me.LabelAnalogCH21.Location = New System.Drawing.Point(2, 514)
        Me.LabelAnalogCH21.Name = "LabelAnalogCH21"
        Me.LabelAnalogCH21.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH21.TabIndex = 378
        Me.LabelAnalogCH21.Text = "Channel 22:"
        Me.LabelAnalogCH21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH22
        '
        Me.LabelAnalogCH22.Location = New System.Drawing.Point(2, 538)
        Me.LabelAnalogCH22.Name = "LabelAnalogCH22"
        Me.LabelAnalogCH22.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH22.TabIndex = 379
        Me.LabelAnalogCH22.Text = "Channel 23:"
        Me.LabelAnalogCH22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH23
        '
        Me.LabelAnalogCH23.Location = New System.Drawing.Point(2, 562)
        Me.LabelAnalogCH23.Name = "LabelAnalogCH23"
        Me.LabelAnalogCH23.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH23.TabIndex = 380
        Me.LabelAnalogCH23.Text = "Channel 24:"
        Me.LabelAnalogCH23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH24
        '
        Me.LabelAnalogCH24.Location = New System.Drawing.Point(2, 586)
        Me.LabelAnalogCH24.Name = "LabelAnalogCH24"
        Me.LabelAnalogCH24.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH24.TabIndex = 381
        Me.LabelAnalogCH24.Text = "Channel 25:"
        Me.LabelAnalogCH24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH25
        '
        Me.LabelAnalogCH25.Location = New System.Drawing.Point(2, 610)
        Me.LabelAnalogCH25.Name = "LabelAnalogCH25"
        Me.LabelAnalogCH25.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH25.TabIndex = 382
        Me.LabelAnalogCH25.Text = "Channel 26:"
        Me.LabelAnalogCH25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH26
        '
        Me.LabelAnalogCH26.Location = New System.Drawing.Point(2, 634)
        Me.LabelAnalogCH26.Name = "LabelAnalogCH26"
        Me.LabelAnalogCH26.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH26.TabIndex = 382
        Me.LabelAnalogCH26.Text = "Channel 27:"
        Me.LabelAnalogCH26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH27
        '
        Me.LabelAnalogCH27.Location = New System.Drawing.Point(2, 658)
        Me.LabelAnalogCH27.Name = "LabelAnalogCH27"
        Me.LabelAnalogCH27.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH27.TabIndex = 382
        Me.LabelAnalogCH27.Text = "Channel 28:"
        Me.LabelAnalogCH27.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH28
        '
        Me.LabelAnalogCH28.Location = New System.Drawing.Point(2, 682)
        Me.LabelAnalogCH28.Name = "LabelAnalogCH28"
        Me.LabelAnalogCH28.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH28.TabIndex = 382
        Me.LabelAnalogCH28.Text = "Channel 29:"
        Me.LabelAnalogCH28.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH29
        '
        Me.LabelAnalogCH29.Location = New System.Drawing.Point(2, 706)
        Me.LabelAnalogCH29.Name = "LabelAnalogCH29"
        Me.LabelAnalogCH29.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH29.TabIndex = 382
        Me.LabelAnalogCH29.Text = "Channel 30:"
        Me.LabelAnalogCH29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH30
        '
        Me.LabelAnalogCH30.Location = New System.Drawing.Point(2, 730)
        Me.LabelAnalogCH30.Name = "LabelAnalogCH30"
        Me.LabelAnalogCH30.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH30.TabIndex = 377
        Me.LabelAnalogCH30.Text = "Channel 31:"
        Me.LabelAnalogCH30.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogCH31
        '
        Me.LabelAnalogCH31.Location = New System.Drawing.Point(2, 754)
        Me.LabelAnalogCH31.Name = "LabelAnalogCH31"
        Me.LabelAnalogCH31.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogCH31.TabIndex = 378
        Me.LabelAnalogCH31.Text = "Channel 32:"
        Me.LabelAnalogCH31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogRead0
        '
        Me.LabelAnalogRead0.Location = New System.Drawing.Point(72, 10)
        Me.LabelAnalogRead0.Name = "LabelAnalogRead0"
        Me.LabelAnalogRead0.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead0.TabIndex = 16
        Me.LabelAnalogRead0.Text = "LabelAnalogRead(0)"
        '
        'LabelAnalogRead1
        '
        Me.LabelAnalogRead1.Location = New System.Drawing.Point(72, 34)
        Me.LabelAnalogRead1.Name = "LabelAnalogRead1"
        Me.LabelAnalogRead1.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead1.TabIndex = 17
        Me.LabelAnalogRead1.Text = "LabelAnalogRead(1)"
        '
        'LabelAnalogRead2
        '
        Me.LabelAnalogRead2.Location = New System.Drawing.Point(72, 58)
        Me.LabelAnalogRead2.Name = "LabelAnalogRead2"
        Me.LabelAnalogRead2.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead2.TabIndex = 18
        Me.LabelAnalogRead2.Text = "LabelAnalogRead(2)"
        '
        'LabelAnalogRead3
        '
        Me.LabelAnalogRead3.Location = New System.Drawing.Point(72, 82)
        Me.LabelAnalogRead3.Name = "LabelAnalogRead3"
        Me.LabelAnalogRead3.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead3.TabIndex = 19
        Me.LabelAnalogRead3.Text = "LabelAnalogRead(3)"
        '
        'LabelAnalogRead4
        '
        Me.LabelAnalogRead4.Location = New System.Drawing.Point(72, 106)
        Me.LabelAnalogRead4.Name = "LabelAnalogRead4"
        Me.LabelAnalogRead4.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead4.TabIndex = 20
        Me.LabelAnalogRead4.Text = "LabelAnalogRead(4)"
        '
        'LabelAnalogRead5
        '
        Me.LabelAnalogRead5.Location = New System.Drawing.Point(72, 130)
        Me.LabelAnalogRead5.Name = "LabelAnalogRead5"
        Me.LabelAnalogRead5.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead5.TabIndex = 21
        Me.LabelAnalogRead5.Text = "LabelAnalogRead(5)"
        '
        'LabelAnalogRead6
        '
        Me.LabelAnalogRead6.Location = New System.Drawing.Point(72, 154)
        Me.LabelAnalogRead6.Name = "LabelAnalogRead6"
        Me.LabelAnalogRead6.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead6.TabIndex = 22
        Me.LabelAnalogRead6.Text = "LabelAnalogRead(6)"
        '
        'LabelAnalogRead7
        '
        Me.LabelAnalogRead7.Location = New System.Drawing.Point(72, 178)
        Me.LabelAnalogRead7.Name = "LabelAnalogRead7"
        Me.LabelAnalogRead7.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead7.TabIndex = 23
        Me.LabelAnalogRead7.Text = "LabelAnalogRead(7)"
        '
        'LabelAnalogRead8
        '
        Me.LabelAnalogRead8.Location = New System.Drawing.Point(72, 202)
        Me.LabelAnalogRead8.Name = "LabelAnalogRead8"
        Me.LabelAnalogRead8.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead8.TabIndex = 24
        Me.LabelAnalogRead8.Text = "LabelAnalogRead(8)"
        '
        'LabelAnalogRead9
        '
        Me.LabelAnalogRead9.Location = New System.Drawing.Point(72, 226)
        Me.LabelAnalogRead9.Name = "LabelAnalogRead9"
        Me.LabelAnalogRead9.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead9.TabIndex = 25
        Me.LabelAnalogRead9.Text = "LabelAnalogRead(9)"
        '
        'LabelAnalogRead10
        '
        Me.LabelAnalogRead10.Location = New System.Drawing.Point(72, 250)
        Me.LabelAnalogRead10.Name = "LabelAnalogRead10"
        Me.LabelAnalogRead10.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead10.TabIndex = 26
        Me.LabelAnalogRead10.Text = "LabelAnalogRead(10)"
        '
        'LabelAnalogRead11
        '
        Me.LabelAnalogRead11.Location = New System.Drawing.Point(72, 274)
        Me.LabelAnalogRead11.Name = "LabelAnalogRead11"
        Me.LabelAnalogRead11.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead11.TabIndex = 27
        Me.LabelAnalogRead11.Text = "LabelAnalogRead(11)"
        '
        'LabelAnalogRead12
        '
        Me.LabelAnalogRead12.Location = New System.Drawing.Point(72, 298)
        Me.LabelAnalogRead12.Name = "LabelAnalogRead12"
        Me.LabelAnalogRead12.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead12.TabIndex = 28
        Me.LabelAnalogRead12.Text = "LabelAnalogRead(12)"
        '
        'LabelAnalogRead13
        '
        Me.LabelAnalogRead13.Location = New System.Drawing.Point(72, 322)
        Me.LabelAnalogRead13.Name = "LabelAnalogRead13"
        Me.LabelAnalogRead13.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead13.TabIndex = 29
        Me.LabelAnalogRead13.Text = "LabelAnalogRead(13)"
        '
        'LabelAnalogRead14
        '
        Me.LabelAnalogRead14.Location = New System.Drawing.Point(72, 346)
        Me.LabelAnalogRead14.Name = "LabelAnalogRead14"
        Me.LabelAnalogRead14.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead14.TabIndex = 30
        Me.LabelAnalogRead14.Text = "LabelAnalogRead(14)"
        '
        'LabelAnalogRead15
        '
        Me.LabelAnalogRead15.Location = New System.Drawing.Point(72, 370)
        Me.LabelAnalogRead15.Name = "LabelAnalogRead15"
        Me.LabelAnalogRead15.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead15.TabIndex = 31
        Me.LabelAnalogRead15.Text = "LabelAnalogRead(15)"
        '
        'LabelAnalogRead16
        '
        Me.LabelAnalogRead16.Location = New System.Drawing.Point(72, 394)
        Me.LabelAnalogRead16.Name = "LabelAnalogRead16"
        Me.LabelAnalogRead16.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead16.TabIndex = 388
        Me.LabelAnalogRead16.Text = "LabelAnalogRead(16)"
        '
        'LabelAnalogRead17
        '
        Me.LabelAnalogRead17.Location = New System.Drawing.Point(72, 418)
        Me.LabelAnalogRead17.Name = "LabelAnalogRead17"
        Me.LabelAnalogRead17.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead17.TabIndex = 388
        Me.LabelAnalogRead17.Text = "LabelAnalogRead(17)"
        '
        'LabelAnalogRead18
        '
        Me.LabelAnalogRead18.Location = New System.Drawing.Point(72, 442)
        Me.LabelAnalogRead18.Name = "LabelAnalogRead18"
        Me.LabelAnalogRead18.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead18.TabIndex = 388
        Me.LabelAnalogRead18.Text = "LabelAnalogRead(18)"
        '
        'LabelAnalogRead19
        '
        Me.LabelAnalogRead19.Location = New System.Drawing.Point(72, 466)
        Me.LabelAnalogRead19.Name = "LabelAnalogRead19"
        Me.LabelAnalogRead19.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead19.TabIndex = 388
        Me.LabelAnalogRead19.Text = "LabelAnalogRead(19)"
        '
        'LabelAnalogRead20
        '
        Me.LabelAnalogRead20.Location = New System.Drawing.Point(72, 490)
        Me.LabelAnalogRead20.Name = "LabelAnalogRead20"
        Me.LabelAnalogRead20.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead20.TabIndex = 383
        Me.LabelAnalogRead20.Text = "LabelAnalogRead(20)"
        '
        'LabelAnalogRead21
        '
        Me.LabelAnalogRead21.Location = New System.Drawing.Point(72, 514)
        Me.LabelAnalogRead21.Name = "LabelAnalogRead21"
        Me.LabelAnalogRead21.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead21.TabIndex = 384
        Me.LabelAnalogRead21.Text = "LabelAnalogRead(21)"
        '
        'LabelAnalogRead22
        '
        Me.LabelAnalogRead22.Location = New System.Drawing.Point(72, 538)
        Me.LabelAnalogRead22.Name = "LabelAnalogRead22"
        Me.LabelAnalogRead22.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead22.TabIndex = 385
        Me.LabelAnalogRead22.Text = "LabelAnalogRead(22)"
        '
        'LabelAnalogRead23
        '
        Me.LabelAnalogRead23.Location = New System.Drawing.Point(72, 562)
        Me.LabelAnalogRead23.Name = "LabelAnalogRead23"
        Me.LabelAnalogRead23.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead23.TabIndex = 386
        Me.LabelAnalogRead23.Text = "LabelAnalogRead(23)"
        '
        'LabelAnalogRead24
        '
        Me.LabelAnalogRead24.Location = New System.Drawing.Point(72, 586)
        Me.LabelAnalogRead24.Name = "LabelAnalogRead24"
        Me.LabelAnalogRead24.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead24.TabIndex = 387
        Me.LabelAnalogRead24.Text = "LabelAnalogRead(24)"
        '
        'LabelAnalogRead25
        '
        Me.LabelAnalogRead25.Location = New System.Drawing.Point(72, 610)
        Me.LabelAnalogRead25.Name = "LabelAnalogRead25"
        Me.LabelAnalogRead25.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead25.TabIndex = 388
        Me.LabelAnalogRead25.Text = "LabelAnalogRead(25)"
        '
        'LabelAnalogRead26
        '
        Me.LabelAnalogRead26.Location = New System.Drawing.Point(72, 634)
        Me.LabelAnalogRead26.Name = "LabelAnalogRead26"
        Me.LabelAnalogRead26.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead26.TabIndex = 388
        Me.LabelAnalogRead26.Text = "LabelAnalogRead(26)"
        '
        'LabelAnalogRead27
        '
        Me.LabelAnalogRead27.Location = New System.Drawing.Point(72, 658)
        Me.LabelAnalogRead27.Name = "LabelAnalogRead27"
        Me.LabelAnalogRead27.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead27.TabIndex = 388
        Me.LabelAnalogRead27.Text = "LabelAnalogRead(27)"
        '
        'LabelAnalogRead28
        '
        Me.LabelAnalogRead28.Location = New System.Drawing.Point(72, 682)
        Me.LabelAnalogRead28.Name = "LabelAnalogRead28"
        Me.LabelAnalogRead28.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead28.TabIndex = 388
        Me.LabelAnalogRead28.Text = "LabelAnalogRead(28)"
        '
        'LabelAnalogRead29
        '
        Me.LabelAnalogRead29.Location = New System.Drawing.Point(72, 706)
        Me.LabelAnalogRead29.Name = "LabelAnalogRead29"
        Me.LabelAnalogRead29.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead29.TabIndex = 388
        Me.LabelAnalogRead29.Text = "LabelAnalogRead(29)"
        '
        'LabelAnalogRead30
        '
        Me.LabelAnalogRead30.Location = New System.Drawing.Point(72, 730)
        Me.LabelAnalogRead30.Name = "LabelAnalogRead30"
        Me.LabelAnalogRead30.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead30.TabIndex = 383
        Me.LabelAnalogRead30.Text = "LabelAnalogRead(30)"
        '
        'LabelAnalogRead31
        '
        Me.LabelAnalogRead31.Location = New System.Drawing.Point(72, 754)
        Me.LabelAnalogRead31.Name = "LabelAnalogRead31"
        Me.LabelAnalogRead31.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogRead31.TabIndex = 384
        Me.LabelAnalogRead31.Text = "LabelAnalogRead(31)"
        '
        'ProgressBarAnalog0
        '
        Me.ProgressBarAnalog0.Location = New System.Drawing.Point(150, 12)
        Me.ProgressBarAnalog0.Name = "ProgressBarAnalog0"
        Me.ProgressBarAnalog0.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog0.TabIndex = 32
        '
        'ProgressBarAnalog1
        '
        Me.ProgressBarAnalog1.Location = New System.Drawing.Point(150, 36)
        Me.ProgressBarAnalog1.Name = "ProgressBarAnalog1"
        Me.ProgressBarAnalog1.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog1.TabIndex = 33
        '
        'ProgressBarAnalog2
        '
        Me.ProgressBarAnalog2.Location = New System.Drawing.Point(150, 60)
        Me.ProgressBarAnalog2.Name = "ProgressBarAnalog2"
        Me.ProgressBarAnalog2.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog2.TabIndex = 34
        '
        'ProgressBarAnalog3
        '
        Me.ProgressBarAnalog3.Location = New System.Drawing.Point(150, 84)
        Me.ProgressBarAnalog3.Name = "ProgressBarAnalog3"
        Me.ProgressBarAnalog3.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog3.TabIndex = 35
        '
        'ProgressBarAnalog4
        '
        Me.ProgressBarAnalog4.Location = New System.Drawing.Point(150, 108)
        Me.ProgressBarAnalog4.Name = "ProgressBarAnalog4"
        Me.ProgressBarAnalog4.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog4.TabIndex = 36
        '
        'ProgressBarAnalog5
        '
        Me.ProgressBarAnalog5.Location = New System.Drawing.Point(150, 132)
        Me.ProgressBarAnalog5.Name = "ProgressBarAnalog5"
        Me.ProgressBarAnalog5.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog5.TabIndex = 37
        '
        'ProgressBarAnalog6
        '
        Me.ProgressBarAnalog6.Location = New System.Drawing.Point(150, 156)
        Me.ProgressBarAnalog6.Name = "ProgressBarAnalog6"
        Me.ProgressBarAnalog6.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog6.TabIndex = 38
        '
        'ProgressBarAnalog7
        '
        Me.ProgressBarAnalog7.Location = New System.Drawing.Point(150, 180)
        Me.ProgressBarAnalog7.Name = "ProgressBarAnalog7"
        Me.ProgressBarAnalog7.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog7.TabIndex = 39
        '
        'ProgressBarAnalog8
        '
        Me.ProgressBarAnalog8.Location = New System.Drawing.Point(150, 204)
        Me.ProgressBarAnalog8.Name = "ProgressBarAnalog8"
        Me.ProgressBarAnalog8.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog8.TabIndex = 40
        '
        'ProgressBarAnalog9
        '
        Me.ProgressBarAnalog9.Location = New System.Drawing.Point(150, 228)
        Me.ProgressBarAnalog9.Name = "ProgressBarAnalog9"
        Me.ProgressBarAnalog9.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog9.TabIndex = 41
        '
        'ProgressBarAnalog10
        '
        Me.ProgressBarAnalog10.Location = New System.Drawing.Point(150, 252)
        Me.ProgressBarAnalog10.Name = "ProgressBarAnalog10"
        Me.ProgressBarAnalog10.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog10.TabIndex = 42
        '
        'ProgressBarAnalog11
        '
        Me.ProgressBarAnalog11.Location = New System.Drawing.Point(150, 276)
        Me.ProgressBarAnalog11.Name = "ProgressBarAnalog11"
        Me.ProgressBarAnalog11.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog11.TabIndex = 43
        '
        'ProgressBarAnalog12
        '
        Me.ProgressBarAnalog12.Location = New System.Drawing.Point(150, 300)
        Me.ProgressBarAnalog12.Name = "ProgressBarAnalog12"
        Me.ProgressBarAnalog12.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog12.TabIndex = 44
        '
        'ProgressBarAnalog13
        '
        Me.ProgressBarAnalog13.Location = New System.Drawing.Point(150, 324)
        Me.ProgressBarAnalog13.Name = "ProgressBarAnalog13"
        Me.ProgressBarAnalog13.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog13.TabIndex = 45
        '
        'ProgressBarAnalog14
        '
        Me.ProgressBarAnalog14.Location = New System.Drawing.Point(150, 348)
        Me.ProgressBarAnalog14.Name = "ProgressBarAnalog14"
        Me.ProgressBarAnalog14.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog14.TabIndex = 46
        '
        'ProgressBarAnalog15
        '
        Me.ProgressBarAnalog15.Location = New System.Drawing.Point(150, 372)
        Me.ProgressBarAnalog15.Name = "ProgressBarAnalog15"
        Me.ProgressBarAnalog15.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog15.TabIndex = 47
        '
        'ProgressBarAnalog16
        '
        Me.ProgressBarAnalog16.Location = New System.Drawing.Point(150, 396)
        Me.ProgressBarAnalog16.Name = "ProgressBarAnalog16"
        Me.ProgressBarAnalog16.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog16.TabIndex = 376
        '
        'ProgressBarAnalog17
        '
        Me.ProgressBarAnalog17.Location = New System.Drawing.Point(150, 420)
        Me.ProgressBarAnalog17.Name = "ProgressBarAnalog17"
        Me.ProgressBarAnalog17.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog17.TabIndex = 376
        '
        'ProgressBarAnalog18
        '
        Me.ProgressBarAnalog18.Location = New System.Drawing.Point(150, 444)
        Me.ProgressBarAnalog18.Name = "ProgressBarAnalog18"
        Me.ProgressBarAnalog18.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog18.TabIndex = 376
        '
        'ProgressBarAnalog19
        '
        Me.ProgressBarAnalog19.Location = New System.Drawing.Point(150, 468)
        Me.ProgressBarAnalog19.Name = "ProgressBarAnalog19"
        Me.ProgressBarAnalog19.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog19.TabIndex = 376
        '
        'ProgressBarAnalog20
        '
        Me.ProgressBarAnalog20.Location = New System.Drawing.Point(150, 492)
        Me.ProgressBarAnalog20.Name = "ProgressBarAnalog20"
        Me.ProgressBarAnalog20.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog20.TabIndex = 371
        '
        'ProgressBarAnalog21
        '
        Me.ProgressBarAnalog21.Location = New System.Drawing.Point(150, 516)
        Me.ProgressBarAnalog21.Name = "ProgressBarAnalog21"
        Me.ProgressBarAnalog21.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog21.TabIndex = 372
        '
        'ProgressBarAnalog22
        '
        Me.ProgressBarAnalog22.Location = New System.Drawing.Point(150, 540)
        Me.ProgressBarAnalog22.Name = "ProgressBarAnalog22"
        Me.ProgressBarAnalog22.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog22.TabIndex = 373
        '
        'ProgressBarAnalog23
        '
        Me.ProgressBarAnalog23.Location = New System.Drawing.Point(150, 564)
        Me.ProgressBarAnalog23.Name = "ProgressBarAnalog23"
        Me.ProgressBarAnalog23.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog23.TabIndex = 374
        '
        'ProgressBarAnalog24
        '
        Me.ProgressBarAnalog24.Location = New System.Drawing.Point(150, 588)
        Me.ProgressBarAnalog24.Name = "ProgressBarAnalog24"
        Me.ProgressBarAnalog24.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog24.TabIndex = 375
        '
        'ProgressBarAnalog25
        '
        Me.ProgressBarAnalog25.Location = New System.Drawing.Point(150, 612)
        Me.ProgressBarAnalog25.Name = "ProgressBarAnalog25"
        Me.ProgressBarAnalog25.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog25.TabIndex = 376
        '
        'ProgressBarAnalog26
        '
        Me.ProgressBarAnalog26.Location = New System.Drawing.Point(150, 636)
        Me.ProgressBarAnalog26.Name = "ProgressBarAnalog26"
        Me.ProgressBarAnalog26.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog26.TabIndex = 376
        '
        'ProgressBarAnalog27
        '
        Me.ProgressBarAnalog27.Location = New System.Drawing.Point(150, 660)
        Me.ProgressBarAnalog27.Name = "ProgressBarAnalog27"
        Me.ProgressBarAnalog27.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog27.TabIndex = 376
        '
        'ProgressBarAnalog28
        '
        Me.ProgressBarAnalog28.Location = New System.Drawing.Point(150, 684)
        Me.ProgressBarAnalog28.Name = "ProgressBarAnalog28"
        Me.ProgressBarAnalog28.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog28.TabIndex = 376
        '
        'ProgressBarAnalog29
        '
        Me.ProgressBarAnalog29.Location = New System.Drawing.Point(150, 708)
        Me.ProgressBarAnalog29.Name = "ProgressBarAnalog29"
        Me.ProgressBarAnalog29.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog29.TabIndex = 376
        '
        'ProgressBarAnalog30
        '
        Me.ProgressBarAnalog30.Location = New System.Drawing.Point(150, 732)
        Me.ProgressBarAnalog30.Name = "ProgressBarAnalog30"
        Me.ProgressBarAnalog30.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog30.TabIndex = 371
        '
        'ProgressBarAnalog31
        '
        Me.ProgressBarAnalog31.Location = New System.Drawing.Point(150, 756)
        Me.ProgressBarAnalog31.Name = "ProgressBarAnalog31"
        Me.ProgressBarAnalog31.Size = New System.Drawing.Size(161, 9)
        Me.ProgressBarAnalog31.TabIndex = 372
        '
        'LabelAnalogLow1
        '
        Me.LabelAnalogLow1.Location = New System.Drawing.Point(156, 287)
        Me.LabelAnalogLow1.Name = "LabelAnalogLow1"
        Me.LabelAnalogLow1.Size = New System.Drawing.Size(43, 17)
        Me.LabelAnalogLow1.TabIndex = 398
        Me.LabelAnalogLow1.Text = "0v"
        '
        'LabelAnalogHigh1
        '
        Me.LabelAnalogHigh1.Location = New System.Drawing.Point(256, 287)
        Me.LabelAnalogHigh1.Name = "LabelAnalogHigh1"
        Me.LabelAnalogHigh1.Size = New System.Drawing.Size(61, 17)
        Me.LabelAnalogHigh1.TabIndex = 1
        Me.LabelAnalogHigh1.Text = "27v"
        Me.LabelAnalogHigh1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogHigh0
        '
        Me.LabelAnalogHigh0.Location = New System.Drawing.Point(253, 21)
        Me.LabelAnalogHigh0.Name = "LabelAnalogHigh0"
        Me.LabelAnalogHigh0.Size = New System.Drawing.Size(64, 17)
        Me.LabelAnalogHigh0.TabIndex = 4
        Me.LabelAnalogHigh0.Text = "27v"
        Me.LabelAnalogHigh0.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogLow0
        '
        Me.LabelAnalogLow0.Location = New System.Drawing.Point(156, 21)
        Me.LabelAnalogLow0.Name = "LabelAnalogLow0"
        Me.LabelAnalogLow0.Size = New System.Drawing.Size(43, 17)
        Me.LabelAnalogLow0.TabIndex = 3
        Me.LabelAnalogLow0.Text = "0v"
        '
        'LabelAnalogReading
        '
        Me.LabelAnalogReading.Location = New System.Drawing.Point(78, 21)
        Me.LabelAnalogReading.Name = "LabelAnalogReading"
        Me.LabelAnalogReading.Size = New System.Drawing.Size(72, 17)
        Me.LabelAnalogReading.TabIndex = 2
        Me.LabelAnalogReading.Text = "Reading:"
        '
        'GroupBoxBattVoltage
        '
        Me.GroupBoxBattVoltage.Controls.Add(Me.ButtonReadBatt)
        Me.GroupBoxBattVoltage.Controls.Add(Me.LabelBattRead)
        Me.GroupBoxBattVoltage.Location = New System.Drawing.Point(371, 252)
        Me.GroupBoxBattVoltage.Name = "GroupBoxBattVoltage"
        Me.GroupBoxBattVoltage.Size = New System.Drawing.Size(333, 73)
        Me.GroupBoxBattVoltage.TabIndex = 2
        Me.GroupBoxBattVoltage.TabStop = False
        Me.GroupBoxBattVoltage.Text = "Battery Voltage - OBDII pin 16"
        '
        'ButtonReadBatt
        '
        Me.ButtonReadBatt.Location = New System.Drawing.Point(32, 32)
        Me.ButtonReadBatt.Name = "ButtonReadBatt"
        Me.ButtonReadBatt.Size = New System.Drawing.Size(49, 25)
        Me.ButtonReadBatt.TabIndex = 0
        Me.ButtonReadBatt.Text = "Read"
        Me.ButtonReadBatt.UseVisualStyleBackColor = True
        '
        'LabelBattRead
        '
        Me.LabelBattRead.Location = New System.Drawing.Point(89, 40)
        Me.LabelBattRead.Name = "LabelBattRead"
        Me.LabelBattRead.Size = New System.Drawing.Size(113, 17)
        Me.LabelBattRead.TabIndex = 1
        Me.LabelBattRead.Text = "0v"
        '
        'GroupBoxProgVoltage
        '
        Me.GroupBoxProgVoltage.Controls.Add(Me.GroupBoxVolt)
        Me.GroupBoxProgVoltage.Controls.Add(Me.GroupBoxPin)
        Me.GroupBoxProgVoltage.Location = New System.Drawing.Point(371, 3)
        Me.GroupBoxProgVoltage.Name = "GroupBoxProgVoltage"
        Me.GroupBoxProgVoltage.Size = New System.Drawing.Size(333, 242)
        Me.GroupBoxProgVoltage.TabIndex = 1
        Me.GroupBoxProgVoltage.TabStop = False
        Me.GroupBoxProgVoltage.Text = "Programming Voltage"
        '
        'GroupBoxVolt
        '
        Me.GroupBoxVolt.Controls.Add(Me.LabelVoltWarning)
        Me.GroupBoxVolt.Controls.Add(Me.ButtonSetVoltage)
        Me.GroupBoxVolt.Controls.Add(Me.ButtonReadVolt)
        Me.GroupBoxVolt.Controls.Add(Me.TextBoxVoltSetting)
        Me.GroupBoxVolt.Controls.Add(Me.optSet)
        Me.GroupBoxVolt.Controls.Add(Me.optShortToGround)
        Me.GroupBoxVolt.Controls.Add(Me.optVoltOff)
        Me.GroupBoxVolt.Controls.Add(Me.LabelVoltRead)
        Me.GroupBoxVolt.Controls.Add(Me.LabelMV)
        Me.GroupBoxVolt.Location = New System.Drawing.Point(142, 19)
        Me.GroupBoxVolt.Name = "GroupBoxVolt"
        Me.GroupBoxVolt.Size = New System.Drawing.Size(185, 217)
        Me.GroupBoxVolt.TabIndex = 1
        Me.GroupBoxVolt.TabStop = False
        Me.GroupBoxVolt.Text = "Voltage:"
        '
        'LabelVoltWarning
        '
        Me.LabelVoltWarning.AutoSize = True
        Me.LabelVoltWarning.Location = New System.Drawing.Point(7, 172)
        Me.LabelVoltWarning.Name = "LabelVoltWarning"
        Me.LabelVoltWarning.Size = New System.Drawing.Size(172, 39)
        Me.LabelVoltWarning.TabIndex = 8
        Me.LabelVoltWarning.Text = "Caution: Do not apply programming" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "voltage to a pin that is in use by a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "J2534 pr" &
    "otocol."
        '
        'ButtonSetVoltage
        '
        Me.ButtonSetVoltage.Location = New System.Drawing.Point(17, 100)
        Me.ButtonSetVoltage.Name = "ButtonSetVoltage"
        Me.ButtonSetVoltage.Size = New System.Drawing.Size(49, 25)
        Me.ButtonSetVoltage.TabIndex = 5
        Me.ButtonSetVoltage.Text = "Set"
        Me.ButtonSetVoltage.UseVisualStyleBackColor = True
        '
        'ButtonReadVolt
        '
        Me.ButtonReadVolt.Location = New System.Drawing.Point(17, 131)
        Me.ButtonReadVolt.Name = "ButtonReadVolt"
        Me.ButtonReadVolt.Size = New System.Drawing.Size(49, 25)
        Me.ButtonReadVolt.TabIndex = 0
        Me.ButtonReadVolt.Text = "Read"
        Me.ButtonReadVolt.UseVisualStyleBackColor = True
        '
        'TextBoxVoltSetting
        '
        Me.TextBoxVoltSetting.AcceptsReturn = True
        Me.TextBoxVoltSetting.ContextMenuStrip = Me.ContextMenuStripTextBox
        Me.TextBoxVoltSetting.Location = New System.Drawing.Point(56, 69)
        Me.TextBoxVoltSetting.MaxLength = 0
        Me.TextBoxVoltSetting.Name = "TextBoxVoltSetting"
        Me.TextBoxVoltSetting.Size = New System.Drawing.Size(65, 20)
        Me.TextBoxVoltSetting.TabIndex = 4
        '
        'optSet
        '
        Me.optSet.AutoSize = True
        Me.optSet.Location = New System.Drawing.Point(17, 70)
        Me.optSet.Name = "optSet"
        Me.optSet.Size = New System.Drawing.Size(44, 17)
        Me.optSet.TabIndex = 3
        Me.optSet.TabStop = True
        Me.optSet.Text = "Set:"
        Me.optSet.UseVisualStyleBackColor = True
        '
        'optShortToGround
        '
        Me.optShortToGround.AutoSize = True
        Me.optShortToGround.Location = New System.Drawing.Point(17, 46)
        Me.optShortToGround.Name = "optShortToGround"
        Me.optShortToGround.Size = New System.Drawing.Size(104, 17)
        Me.optShortToGround.TabIndex = 2
        Me.optShortToGround.TabStop = True
        Me.optShortToGround.Text = "Short To Ground"
        Me.optShortToGround.UseVisualStyleBackColor = True
        '
        'optVoltOff
        '
        Me.optVoltOff.AutoSize = True
        Me.optVoltOff.Location = New System.Drawing.Point(17, 24)
        Me.optVoltOff.Name = "optVoltOff"
        Me.optVoltOff.Size = New System.Drawing.Size(78, 17)
        Me.optVoltOff.TabIndex = 1
        Me.optVoltOff.TabStop = True
        Me.optVoltOff.Text = "Voltage Off"
        Me.optVoltOff.UseVisualStyleBackColor = True
        '
        'LabelVoltRead
        '
        Me.LabelVoltRead.Location = New System.Drawing.Point(72, 137)
        Me.LabelVoltRead.Name = "LabelVoltRead"
        Me.LabelVoltRead.Size = New System.Drawing.Size(65, 17)
        Me.LabelVoltRead.TabIndex = 7
        Me.LabelVoltRead.Text = "0v"
        '
        'LabelMV
        '
        Me.LabelMV.Location = New System.Drawing.Point(127, 72)
        Me.LabelMV.Name = "LabelMV"
        Me.LabelMV.Size = New System.Drawing.Size(28, 17)
        Me.LabelMV.TabIndex = 6
        Me.LabelMV.Text = "mV"
        '
        'GroupBoxPin
        '
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin15)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin14)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin13)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin12)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin11)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin9)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin6)
        Me.GroupBoxPin.Controls.Add(Me.RadioButtonPin0)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin15)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin14)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin13)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin12)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin11)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin9)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin6)
        Me.GroupBoxPin.Controls.Add(Me.LabelPin0)
        Me.GroupBoxPin.Location = New System.Drawing.Point(9, 19)
        Me.GroupBoxPin.Name = "GroupBoxPin"
        Me.GroupBoxPin.Size = New System.Drawing.Size(127, 217)
        Me.GroupBoxPin.TabIndex = 0
        Me.GroupBoxPin.TabStop = False
        Me.GroupBoxPin.Text = "Pin:"
        '
        'RadioButtonPin15
        '
        Me.RadioButtonPin15.Location = New System.Drawing.Point(56, 185)
        Me.RadioButtonPin15.Name = "RadioButtonPin15"
        Me.RadioButtonPin15.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin15.TabIndex = 7
        Me.RadioButtonPin15.TabStop = True
        Me.RadioButtonPin15.Text = "Pin 15"
        Me.RadioButtonPin15.UseVisualStyleBackColor = True
        '
        'RadioButtonPin14
        '
        Me.RadioButtonPin14.Location = New System.Drawing.Point(56, 162)
        Me.RadioButtonPin14.Name = "RadioButtonPin14"
        Me.RadioButtonPin14.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin14.TabIndex = 6
        Me.RadioButtonPin14.TabStop = True
        Me.RadioButtonPin14.Text = "Pin 14"
        Me.RadioButtonPin14.UseVisualStyleBackColor = True
        '
        'RadioButtonPin13
        '
        Me.RadioButtonPin13.Location = New System.Drawing.Point(56, 139)
        Me.RadioButtonPin13.Name = "RadioButtonPin13"
        Me.RadioButtonPin13.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin13.TabIndex = 5
        Me.RadioButtonPin13.TabStop = True
        Me.RadioButtonPin13.Text = "Pin 13"
        Me.RadioButtonPin13.UseVisualStyleBackColor = True
        '
        'RadioButtonPin12
        '
        Me.RadioButtonPin12.Location = New System.Drawing.Point(56, 116)
        Me.RadioButtonPin12.Name = "RadioButtonPin12"
        Me.RadioButtonPin12.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin12.TabIndex = 4
        Me.RadioButtonPin12.TabStop = True
        Me.RadioButtonPin12.Text = "Pin 12"
        Me.RadioButtonPin12.UseVisualStyleBackColor = True
        '
        'RadioButtonPin11
        '
        Me.RadioButtonPin11.Location = New System.Drawing.Point(56, 93)
        Me.RadioButtonPin11.Name = "RadioButtonPin11"
        Me.RadioButtonPin11.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin11.TabIndex = 3
        Me.RadioButtonPin11.TabStop = True
        Me.RadioButtonPin11.Text = "Pin 11"
        Me.RadioButtonPin11.UseVisualStyleBackColor = True
        '
        'RadioButtonPin9
        '
        Me.RadioButtonPin9.Location = New System.Drawing.Point(56, 69)
        Me.RadioButtonPin9.Name = "RadioButtonPin9"
        Me.RadioButtonPin9.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin9.TabIndex = 2
        Me.RadioButtonPin9.TabStop = True
        Me.RadioButtonPin9.Text = "Pin 9"
        Me.RadioButtonPin9.UseVisualStyleBackColor = True
        '
        'RadioButtonPin6
        '
        Me.RadioButtonPin6.Location = New System.Drawing.Point(56, 47)
        Me.RadioButtonPin6.Name = "RadioButtonPin6"
        Me.RadioButtonPin6.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin6.TabIndex = 1
        Me.RadioButtonPin6.TabStop = True
        Me.RadioButtonPin6.Text = "Pin 6"
        Me.RadioButtonPin6.UseVisualStyleBackColor = True
        '
        'RadioButtonPin0
        '
        Me.RadioButtonPin0.Location = New System.Drawing.Point(56, 24)
        Me.RadioButtonPin0.Name = "RadioButtonPin0"
        Me.RadioButtonPin0.Size = New System.Drawing.Size(57, 17)
        Me.RadioButtonPin0.TabIndex = 0
        Me.RadioButtonPin0.TabStop = True
        Me.RadioButtonPin0.Text = "Aux"
        Me.RadioButtonPin0.UseVisualStyleBackColor = True
        '
        'LabelPin15
        '
        Me.LabelPin15.Location = New System.Drawing.Point(21, 187)
        Me.LabelPin15.Name = "LabelPin15"
        Me.LabelPin15.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin15.TabIndex = 15
        Me.LabelPin15.Text = "LabelPin(15)"
        Me.LabelPin15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin14
        '
        Me.LabelPin14.Location = New System.Drawing.Point(21, 164)
        Me.LabelPin14.Name = "LabelPin14"
        Me.LabelPin14.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin14.TabIndex = 14
        Me.LabelPin14.Text = "LabelPin(14)"
        Me.LabelPin14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin13
        '
        Me.LabelPin13.Location = New System.Drawing.Point(21, 141)
        Me.LabelPin13.Name = "LabelPin13"
        Me.LabelPin13.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin13.TabIndex = 13
        Me.LabelPin13.Text = "LabelPin(13)"
        Me.LabelPin13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin12
        '
        Me.LabelPin12.Location = New System.Drawing.Point(21, 118)
        Me.LabelPin12.Name = "LabelPin12"
        Me.LabelPin12.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin12.TabIndex = 12
        Me.LabelPin12.Text = "LabelPin(12)"
        Me.LabelPin12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin11
        '
        Me.LabelPin11.Location = New System.Drawing.Point(21, 95)
        Me.LabelPin11.Name = "LabelPin11"
        Me.LabelPin11.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin11.TabIndex = 11
        Me.LabelPin11.Text = "LabelPin(11)"
        Me.LabelPin11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin9
        '
        Me.LabelPin9.Location = New System.Drawing.Point(21, 72)
        Me.LabelPin9.Name = "LabelPin9"
        Me.LabelPin9.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin9.TabIndex = 10
        Me.LabelPin9.Text = "LabelPin(9)"
        Me.LabelPin9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin6
        '
        Me.LabelPin6.Location = New System.Drawing.Point(21, 49)
        Me.LabelPin6.Name = "LabelPin6"
        Me.LabelPin6.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin6.TabIndex = 9
        Me.LabelPin6.Text = "LabelPin(6)"
        Me.LabelPin6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelPin0
        '
        Me.LabelPin0.Location = New System.Drawing.Point(21, 26)
        Me.LabelPin0.Name = "LabelPin0"
        Me.LabelPin0.Size = New System.Drawing.Size(30, 17)
        Me.LabelPin0.TabIndex = 8
        Me.LabelPin0.Text = "LabelPin(0)"
        Me.LabelPin0.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelAnalogChannel
        '
        Me.LabelAnalogChannel.Location = New System.Drawing.Point(524, 336)
        Me.LabelAnalogChannel.Name = "LabelAnalogChannel"
        Me.LabelAnalogChannel.Size = New System.Drawing.Size(49, 17)
        Me.LabelAnalogChannel.TabIndex = 4
        Me.LabelAnalogChannel.Text = "Channel:"
        '
        'LabelDeviceCombo7
        '
        Me.LabelDeviceCombo7.Location = New System.Drawing.Point(395, 336)
        Me.LabelDeviceCombo7.Name = "LabelDeviceCombo7"
        Me.LabelDeviceCombo7.Size = New System.Drawing.Size(89, 17)
        Me.LabelDeviceCombo7.TabIndex = 3
        Me.LabelDeviceCombo7.Text = "Device:"
        '
        'TabPageResults
        '
        Me.TabPageResults.Controls.Add(Me.ListViewResults)
        Me.TabPageResults.Location = New System.Drawing.Point(4, 22)
        Me.TabPageResults.Name = "TabPageResults"
        Me.TabPageResults.Size = New System.Drawing.Size(714, 395)
        Me.TabPageResults.TabIndex = 8
        Me.TabPageResults.Text = "Results"
        '
        'ListViewResults
        '
        Me.ListViewResults.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ListViewResultsColumnHeader1, Me.ListViewResultsColumnHeader2})
        Me.ListViewResults.ContextMenuStrip = Me.ContextMenuStripResults
        Me.ListViewResults.FullRowSelect = True
        Me.ListViewResults.Location = New System.Drawing.Point(8, 15)
        Me.ListViewResults.Name = "ListViewResults"
        Me.ListViewResults.Size = New System.Drawing.Size(696, 361)
        Me.ListViewResults.TabIndex = 0
        Me.ListViewResults.UseCompatibleStateImageBehavior = False
        Me.ListViewResults.View = System.Windows.Forms.View.Details
        '
        'ListViewResultsColumnHeader1
        '
        Me.ListViewResultsColumnHeader1.Text = "Device"
        Me.ListViewResultsColumnHeader1.Width = 134
        '
        'ListViewResultsColumnHeader2
        '
        Me.ListViewResultsColumnHeader2.Text = "Result"
        Me.ListViewResultsColumnHeader2.Width = 527
        '
        'ContextMenuStripResults
        '
        Me.ContextMenuStripResults.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuResultsCopyLine, Me.MenuResultsDelete, Me.MenuResultsSelectAll, Me.MenuResultsSpace1, Me.MenuResultsClear})
        Me.ContextMenuStripResults.Name = "ContextMenuStripMsgIn"
        Me.ContextMenuStripResults.Size = New System.Drawing.Size(128, 98)
        '
        'MenuResultsCopyLine
        '
        Me.MenuResultsCopyLine.Enabled = False
        Me.MenuResultsCopyLine.Name = "MenuResultsCopyLine"
        Me.MenuResultsCopyLine.Size = New System.Drawing.Size(127, 22)
        Me.MenuResultsCopyLine.Text = "Copy Line"
        '
        'MenuResultsDelete
        '
        Me.MenuResultsDelete.Name = "MenuResultsDelete"
        Me.MenuResultsDelete.Size = New System.Drawing.Size(127, 22)
        Me.MenuResultsDelete.Text = "Delete"
        '
        'MenuResultsSelectAll
        '
        Me.MenuResultsSelectAll.Enabled = False
        Me.MenuResultsSelectAll.Name = "MenuResultsSelectAll"
        Me.MenuResultsSelectAll.Size = New System.Drawing.Size(127, 22)
        Me.MenuResultsSelectAll.Text = "Select All"
        '
        'MenuResultsSpace1
        '
        Me.MenuResultsSpace1.Name = "MenuResultsSpace1"
        Me.MenuResultsSpace1.Size = New System.Drawing.Size(124, 6)
        '
        'MenuResultsClear
        '
        Me.MenuResultsClear.Enabled = False
        Me.MenuResultsClear.Name = "MenuResultsClear"
        Me.MenuResultsClear.Size = New System.Drawing.Size(127, 22)
        Me.MenuResultsClear.Text = "Clear"
        '
        'ContextMenuStripMessageIn
        '
        Me.ContextMenuStripMessageIn.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuMsgInCopyLine, Me.MenuMsgInCopyData, Me.MenuMsgInMakeFilter, Me.MenuMsgInRxStatus, Me.MenuMsgInSelectAll, Me.MenuMsgInSpace1, Me.MenuMsgInSaveSelected, Me.MenuMsgInSaveAll, Me.MenuMsgInClear})
        Me.ContextMenuStripMessageIn.Name = "ContextMenuStripMsgIn"
        Me.ContextMenuStripMessageIn.Size = New System.Drawing.Size(146, 186)
        '
        'ContextMenuStripMessageOut
        '
        Me.ContextMenuStripMessageOut.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuMsgOutDelete, Me.MenuMsgOutEditMessage, Me.MenuMsgOutAddMessage, Me.MenuMsgOutCopyToScratchPad, Me.MenuMsgOutMakePeriodicMessage, Me.MenuMsgOutSpace1, Me.MenuMsgOutSave, Me.MenuMsgOutLoad, Me.MenuMsgOutClear})
        Me.ContextMenuStripMessageOut.Name = "ContextMenuStripMsgOut"
        Me.ContextMenuStripMessageOut.Size = New System.Drawing.Size(199, 186)
        '
        'FormJ2534
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(720, 466)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MenuStripMain)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(88, 149)
        Me.MaximizeBox = False
        Me.Name = "FormJ2534"
        Me.Text = "FormJ2534"
        Me.MenuStripMain.ResumeLayout(False)
        Me.MenuStripMain.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPageConnect.ResumeLayout(False)
        Me.GroupBoxDevices.ResumeLayout(False)
        Me.GroupBoxDevices.PerformLayout()
        Me.GroupBoxAPIs.ResumeLayout(False)
        Me.GroupBoxAPIs.PerformLayout()
        Me.ContextMenuStripTextBox.ResumeLayout(False)
        Me.GroupBoxConnect.ResumeLayout(False)
        Me.GroupBoxConnect.PerformLayout()
        Me.ContextMenuStripFlags.ResumeLayout(False)
        Me.GroupBoxJ2534Info.ResumeLayout(False)
        Me.GroupBoxJ2534Info.PerformLayout()
        Me.TabPageMessages.ResumeLayout(False)
        Me.TabPageMessages.PerformLayout()
        Me.ContextMenuStripScratchPad.ResumeLayout(False)
        Me.TabPagePeriodicMessages.ResumeLayout(False)
        Me.TabPagePeriodicMessages.PerformLayout()
        Me.TabPageFilters.ResumeLayout(False)
        Me.TabPageFilters.PerformLayout()
        Me.TabPageConfig.ResumeLayout(False)
        Me.TabPageConfig.PerformLayout()
        Me.GroupBoxAnalogConfig.ResumeLayout(False)
        Me.GroupBoxAnalogConfig.PerformLayout()
        Me.PanelAudioChannel.ResumeLayout(False)
        Me.PanelAudioChannel.PerformLayout()
        Me.TabPageInit.ResumeLayout(False)
        Me.GroupBoxFastInit.ResumeLayout(False)
        Me.GroupBoxFastInit.PerformLayout()
        Me.GroupBox5BInit.ResumeLayout(False)
        Me.GroupBox5BInit.PerformLayout()
        Me.TabPageFunctionalMessages.ResumeLayout(False)
        Me.TabPageFunctionalMessages.PerformLayout()
        Me.TabPageAnalog.ResumeLayout(False)
        Me.GroupBoxAnalog.ResumeLayout(False)
        Me.PanelAnalog.ResumeLayout(False)
        Me.GroupBoxBattVoltage.ResumeLayout(False)
        Me.GroupBoxProgVoltage.ResumeLayout(False)
        Me.GroupBoxVolt.ResumeLayout(False)
        Me.GroupBoxVolt.PerformLayout()
        Me.GroupBoxPin.ResumeLayout(False)
        Me.TabPageResults.ResumeLayout(False)
        Me.ContextMenuStripResults.ResumeLayout(False)
        Me.ContextMenuStripMessageIn.ResumeLayout(False)
        Me.ContextMenuStripMessageOut.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private Sub InitializeComboAvailableChannelLocator()
        ReDim ComboAvailableChannelLocator(7)
        Me.ComboAvailableChannelLocator(0) = ComboAvailableChannelLocator0
        Me.ComboAvailableChannelLocator(1) = ComboAvailableChannelLocator1
        Me.ComboAvailableChannelLocator(2) = ComboAvailableChannelLocator2
        Me.ComboAvailableChannelLocator(3) = ComboAvailableChannelLocator3
        Me.ComboAvailableChannelLocator(4) = ComboAvailableChannelLocator4
        Me.ComboAvailableChannelLocator(5) = ComboAvailableChannelLocator5
        Me.ComboAvailableChannelLocator(6) = ComboAvailableChannelLocator6
        Me.ComboAvailableChannelLocator(7) = ComboAvailableChannelLocator7
    End Sub

    Private Sub InitializeComboAvailableBoxLocator()
        ReDim ComboAvailableBoxLocator(7)
        Me.ComboAvailableBoxLocator(0) = ComboAvailableBoxLocator0
        Me.ComboAvailableBoxLocator(1) = ComboAvailableBoxLocator1
        Me.ComboAvailableBoxLocator(2) = ComboAvailableBoxLocator2
        Me.ComboAvailableBoxLocator(3) = ComboAvailableBoxLocator3
        Me.ComboAvailableBoxLocator(4) = ComboAvailableBoxLocator4
        Me.ComboAvailableBoxLocator(5) = ComboAvailableBoxLocator5
        Me.ComboAvailableBoxLocator(6) = ComboAvailableBoxLocator6
        Me.ComboAvailableBoxLocator(7) = ComboAvailableBoxLocator7
    End Sub

    Private Sub InitializeTextBoxPeriodicMessage()
        ReDim TextBoxPeriodicMessage(9)
        Me.TextBoxPeriodicMessage(0) = TextBoxPeriodicMessage0
        Me.TextBoxPeriodicMessage(1) = TextBoxPeriodicMessage1
        Me.TextBoxPeriodicMessage(2) = TextBoxPeriodicMessage2
        Me.TextBoxPeriodicMessage(3) = TextBoxPeriodicMessage3
        Me.TextBoxPeriodicMessage(4) = TextBoxPeriodicMessage4
        Me.TextBoxPeriodicMessage(5) = TextBoxPeriodicMessage5
        Me.TextBoxPeriodicMessage(6) = TextBoxPeriodicMessage6
        Me.TextBoxPeriodicMessage(7) = TextBoxPeriodicMessage7
        Me.TextBoxPeriodicMessage(8) = TextBoxPeriodicMessage8
        Me.TextBoxPeriodicMessage(9) = TextBoxPeriodicMessage9
    End Sub

    Private Sub InitializeTextBoxPeriodicMessageInterval()
        ReDim TextBoxPeriodicMessageInterval(9)
        Me.TextBoxPeriodicMessageInterval(0) = TextBoxPeriodicMessageInterval0
        Me.TextBoxPeriodicMessageInterval(1) = TextBoxPeriodicMessageInterval1
        Me.TextBoxPeriodicMessageInterval(2) = TextBoxPeriodicMessageInterval2
        Me.TextBoxPeriodicMessageInterval(3) = TextBoxPeriodicMessageInterval3
        Me.TextBoxPeriodicMessageInterval(4) = TextBoxPeriodicMessageInterval4
        Me.TextBoxPeriodicMessageInterval(5) = TextBoxPeriodicMessageInterval5
        Me.TextBoxPeriodicMessageInterval(6) = TextBoxPeriodicMessageInterval6
        Me.TextBoxPeriodicMessageInterval(7) = TextBoxPeriodicMessageInterval7
        Me.TextBoxPeriodicMessageInterval(8) = TextBoxPeriodicMessageInterval8
        Me.TextBoxPeriodicMessageInterval(9) = TextBoxPeriodicMessageInterval9
    End Sub

    Private Sub InitializeTextBoxPeriodicMessageFlags()
        ReDim TextBoxPeriodicMessageFlags(9)
        Me.TextBoxPeriodicMessageFlags(0) = TextBoxPeriodicMessageFlags0
        Me.TextBoxPeriodicMessageFlags(1) = TextBoxPeriodicMessageFlags1
        Me.TextBoxPeriodicMessageFlags(2) = TextBoxPeriodicMessageFlags2
        Me.TextBoxPeriodicMessageFlags(3) = TextBoxPeriodicMessageFlags3
        Me.TextBoxPeriodicMessageFlags(4) = TextBoxPeriodicMessageFlags4
        Me.TextBoxPeriodicMessageFlags(5) = TextBoxPeriodicMessageFlags5
        Me.TextBoxPeriodicMessageFlags(6) = TextBoxPeriodicMessageFlags6
        Me.TextBoxPeriodicMessageFlags(7) = TextBoxPeriodicMessageFlags7
        Me.TextBoxPeriodicMessageFlags(8) = TextBoxPeriodicMessageFlags8
        Me.TextBoxPeriodicMessageFlags(9) = TextBoxPeriodicMessageFlags9
    End Sub

    Private Sub InitializeTextBoxFunctionalMessage()
        ReDim TextBoxFunctionalMessage(31)
        Me.TextBoxFunctionalMessage(0) = TextBoxFunctionalMessage0
        Me.TextBoxFunctionalMessage(1) = TextBoxFunctionalMessage1
        Me.TextBoxFunctionalMessage(2) = TextBoxFunctionalMessage2
        Me.TextBoxFunctionalMessage(3) = TextBoxFunctionalMessage3
        Me.TextBoxFunctionalMessage(4) = TextBoxFunctionalMessage4
        Me.TextBoxFunctionalMessage(5) = TextBoxFunctionalMessage5
        Me.TextBoxFunctionalMessage(6) = TextBoxFunctionalMessage6
        Me.TextBoxFunctionalMessage(7) = TextBoxFunctionalMessage7
        Me.TextBoxFunctionalMessage(8) = TextBoxFunctionalMessage8
        Me.TextBoxFunctionalMessage(9) = TextBoxFunctionalMessage9
        Me.TextBoxFunctionalMessage(10) = TextBoxFunctionalMessage10
        Me.TextBoxFunctionalMessage(11) = TextBoxFunctionalMessage11
        Me.TextBoxFunctionalMessage(12) = TextBoxFunctionalMessage12
        Me.TextBoxFunctionalMessage(13) = TextBoxFunctionalMessage13
        Me.TextBoxFunctionalMessage(14) = TextBoxFunctionalMessage14
        Me.TextBoxFunctionalMessage(15) = TextBoxFunctionalMessage15
        Me.TextBoxFunctionalMessage(16) = TextBoxFunctionalMessage16
        Me.TextBoxFunctionalMessage(17) = TextBoxFunctionalMessage17
        Me.TextBoxFunctionalMessage(18) = TextBoxFunctionalMessage18
        Me.TextBoxFunctionalMessage(19) = TextBoxFunctionalMessage19
        Me.TextBoxFunctionalMessage(20) = TextBoxFunctionalMessage20
        Me.TextBoxFunctionalMessage(21) = TextBoxFunctionalMessage21
        Me.TextBoxFunctionalMessage(22) = TextBoxFunctionalMessage22
        Me.TextBoxFunctionalMessage(23) = TextBoxFunctionalMessage23
        Me.TextBoxFunctionalMessage(24) = TextBoxFunctionalMessage24
        Me.TextBoxFunctionalMessage(25) = TextBoxFunctionalMessage25
        Me.TextBoxFunctionalMessage(26) = TextBoxFunctionalMessage26
        Me.TextBoxFunctionalMessage(27) = TextBoxFunctionalMessage27
        Me.TextBoxFunctionalMessage(28) = TextBoxFunctionalMessage28
        Me.TextBoxFunctionalMessage(29) = TextBoxFunctionalMessage29
        Me.TextBoxFunctionalMessage(30) = TextBoxFunctionalMessage30
        Me.TextBoxFunctionalMessage(31) = TextBoxFunctionalMessage31
    End Sub

    Private Sub InitializeTextBoxFilterPatt()
        ReDim TextBoxFilterPatt(9)
        Me.TextBoxFilterPatt(0) = TextBoxFilterPatt0
        Me.TextBoxFilterPatt(1) = TextBoxFilterPatt1
        Me.TextBoxFilterPatt(2) = TextBoxFilterPatt2
        Me.TextBoxFilterPatt(3) = TextBoxFilterPatt3
        Me.TextBoxFilterPatt(4) = TextBoxFilterPatt4
        Me.TextBoxFilterPatt(5) = TextBoxFilterPatt5
        Me.TextBoxFilterPatt(6) = TextBoxFilterPatt6
        Me.TextBoxFilterPatt(7) = TextBoxFilterPatt7
        Me.TextBoxFilterPatt(8) = TextBoxFilterPatt8
        Me.TextBoxFilterPatt(9) = TextBoxFilterPatt9
    End Sub

    Private Sub InitializeTextBoxFilterMask()
        ReDim TextBoxFilterMask(9)
        Me.TextBoxFilterMask(9) = TextBoxFilterMask9
        Me.TextBoxFilterMask(8) = TextBoxFilterMask8
        Me.TextBoxFilterMask(7) = TextBoxFilterMask7
        Me.TextBoxFilterMask(6) = TextBoxFilterMask6
        Me.TextBoxFilterMask(5) = TextBoxFilterMask5
        Me.TextBoxFilterMask(4) = TextBoxFilterMask4
        Me.TextBoxFilterMask(3) = TextBoxFilterMask3
        Me.TextBoxFilterMask(2) = TextBoxFilterMask2
        Me.TextBoxFilterMask(1) = TextBoxFilterMask1
        Me.TextBoxFilterMask(0) = TextBoxFilterMask0
    End Sub

    Private Sub InitializeTextBoxFilterFlow()
        ReDim TextBoxFilterFlow(9)
        Me.TextBoxFilterFlow(0) = TextBoxFilterFlow0
        Me.TextBoxFilterFlow(1) = TextBoxFilterFlow1
        Me.TextBoxFilterFlow(2) = TextBoxFilterFlow2
        Me.TextBoxFilterFlow(3) = TextBoxFilterFlow3
        Me.TextBoxFilterFlow(4) = TextBoxFilterFlow4
        Me.TextBoxFilterFlow(5) = TextBoxFilterFlow5
        Me.TextBoxFilterFlow(6) = TextBoxFilterFlow6
        Me.TextBoxFilterFlow(7) = TextBoxFilterFlow7
        Me.TextBoxFilterFlow(8) = TextBoxFilterFlow8
        Me.TextBoxFilterFlow(9) = TextBoxFilterFlow9
    End Sub

    Private Sub InitializeTextBoxFilterFlags()
        ReDim TextBoxFilterFlags(9)
        Me.TextBoxFilterFlags(0) = TextBoxFilterFlags0
        Me.TextBoxFilterFlags(1) = TextBoxFilterFlags1
        Me.TextBoxFilterFlags(2) = TextBoxFilterFlags2
        Me.TextBoxFilterFlags(3) = TextBoxFilterFlags3
        Me.TextBoxFilterFlags(4) = TextBoxFilterFlags4
        Me.TextBoxFilterFlags(5) = TextBoxFilterFlags5
        Me.TextBoxFilterFlags(6) = TextBoxFilterFlags6
        Me.TextBoxFilterFlags(7) = TextBoxFilterFlags7
        Me.TextBoxFilterFlags(8) = TextBoxFilterFlags8
        Me.TextBoxFilterFlags(9) = TextBoxFilterFlags9
    End Sub

    Private Sub InitializeProgressBarAnalog()
        ReDim ProgressBarAnalog(31)
        Me.ProgressBarAnalog(0) = ProgressBarAnalog0
        Me.ProgressBarAnalog(1) = ProgressBarAnalog1
        Me.ProgressBarAnalog(2) = ProgressBarAnalog2
        Me.ProgressBarAnalog(3) = ProgressBarAnalog3
        Me.ProgressBarAnalog(4) = ProgressBarAnalog4
        Me.ProgressBarAnalog(5) = ProgressBarAnalog5
        Me.ProgressBarAnalog(6) = ProgressBarAnalog6
        Me.ProgressBarAnalog(7) = ProgressBarAnalog7
        Me.ProgressBarAnalog(8) = ProgressBarAnalog8
        Me.ProgressBarAnalog(9) = ProgressBarAnalog9
        Me.ProgressBarAnalog(10) = ProgressBarAnalog10
        Me.ProgressBarAnalog(11) = ProgressBarAnalog11
        Me.ProgressBarAnalog(12) = ProgressBarAnalog12
        Me.ProgressBarAnalog(13) = ProgressBarAnalog13
        Me.ProgressBarAnalog(14) = ProgressBarAnalog14
        Me.ProgressBarAnalog(15) = ProgressBarAnalog15
        Me.ProgressBarAnalog(16) = ProgressBarAnalog16
        Me.ProgressBarAnalog(17) = ProgressBarAnalog17
        Me.ProgressBarAnalog(18) = ProgressBarAnalog18
        Me.ProgressBarAnalog(19) = ProgressBarAnalog19
        Me.ProgressBarAnalog(20) = ProgressBarAnalog20
        Me.ProgressBarAnalog(21) = ProgressBarAnalog21
        Me.ProgressBarAnalog(22) = ProgressBarAnalog22
        Me.ProgressBarAnalog(23) = ProgressBarAnalog23
        Me.ProgressBarAnalog(24) = ProgressBarAnalog24
        Me.ProgressBarAnalog(25) = ProgressBarAnalog25
        Me.ProgressBarAnalog(26) = ProgressBarAnalog26
        Me.ProgressBarAnalog(27) = ProgressBarAnalog27
        Me.ProgressBarAnalog(28) = ProgressBarAnalog28
        Me.ProgressBarAnalog(29) = ProgressBarAnalog29
        Me.ProgressBarAnalog(30) = ProgressBarAnalog30
        Me.ProgressBarAnalog(31) = ProgressBarAnalog31
    End Sub

    Private Sub InitializeRadioButtonPin()
        ReDim RadioButtonPin(15)
        Me.RadioButtonPin(0) = RadioButtonPin0
        Me.RadioButtonPin(6) = RadioButtonPin6
        Me.RadioButtonPin(9) = RadioButtonPin9
        Me.RadioButtonPin(11) = RadioButtonPin11
        Me.RadioButtonPin(12) = RadioButtonPin12
        Me.RadioButtonPin(13) = RadioButtonPin13
        Me.RadioButtonPin(14) = RadioButtonPin14
        Me.RadioButtonPin(15) = RadioButtonPin15
    End Sub

    Private Sub InitializeLabelChannel()
        ReDim LabelChannel(6)
        Me.LabelChannel(0) = Nothing
        Me.LabelChannel(1) = LabelChannel1
        Me.LabelChannel(2) = LabelChannel2
        Me.LabelChannel(3) = LabelChannel3
        Me.LabelChannel(4) = LabelChannel4
        Me.LabelChannel(5) = LabelChannel5
        Me.LabelChannel(6) = LabelChannel6
    End Sub

    Private Sub InitializeLabelPin()
        ReDim LabelPin(15)
        Me.LabelPin(0) = LabelPin0
        Me.LabelPin(6) = LabelPin6
        Me.LabelPin(9) = LabelPin9
        Me.LabelPin(11) = LabelPin11
        Me.LabelPin(12) = LabelPin12
        Me.LabelPin(13) = LabelPin13
        Me.LabelPin(14) = LabelPin14
        Me.LabelPin(15) = LabelPin15
    End Sub

    Private Sub InitializeLabelParameterName()
        ReDim LabelParameterName(21)
        Me.LabelParameterName(0) = LabelParameterName0
        Me.LabelParameterName(1) = LabelParameterName1
        Me.LabelParameterName(2) = LabelParameterName2
        Me.LabelParameterName(3) = LabelParameterName3
        Me.LabelParameterName(4) = LabelParameterName4
        Me.LabelParameterName(5) = LabelParameterName5
        Me.LabelParameterName(6) = LabelParameterName6
        Me.LabelParameterName(7) = LabelParameterName7
        Me.LabelParameterName(8) = LabelParameterName8
        Me.LabelParameterName(9) = LabelParameterName9
        Me.LabelParameterName(10) = LabelParameterName10
        Me.LabelParameterName(11) = LabelParameterName11
        Me.LabelParameterName(12) = LabelParameterName12
        Me.LabelParameterName(13) = LabelParameterName13
        Me.LabelParameterName(14) = LabelParameterName14
        Me.LabelParameterName(15) = LabelParameterName15
        Me.LabelParameterName(16) = LabelParameterName16
        Me.LabelParameterName(17) = LabelParameterName17
        Me.LabelParameterName(18) = LabelParameterName18
        Me.LabelParameterName(19) = LabelParameterName19
        Me.LabelParameterName(20) = LabelParameterName20
        Me.LabelParameterName(21) = LabelParameterName21
    End Sub

    Private Sub InitializeLabelParamVal()
        ReDim LabelParamVal(21)
        Me.LabelParamVal(0) = LabelParamVal0
        Me.LabelParamVal(1) = LabelParamVal1
        Me.LabelParamVal(2) = LabelParamVal2
        Me.LabelParamVal(3) = LabelParamVal3
        Me.LabelParamVal(4) = LabelParamVal4
        Me.LabelParamVal(5) = LabelParamVal5
        Me.LabelParamVal(6) = LabelParamVal6
        Me.LabelParamVal(7) = LabelParamVal7
        Me.LabelParamVal(8) = LabelParamVal8
        Me.LabelParamVal(9) = LabelParamVal9
        Me.LabelParamVal(10) = LabelParamVal10
        Me.LabelParamVal(11) = LabelParamVal11
        Me.LabelParamVal(12) = LabelParamVal12
        Me.LabelParamVal(13) = LabelParamVal13
        Me.LabelParamVal(14) = LabelParamVal14
        Me.LabelParamVal(15) = LabelParamVal15
        Me.LabelParamVal(16) = LabelParamVal16
        Me.LabelParamVal(17) = LabelParamVal17
        Me.LabelParamVal(18) = LabelParamVal18
        Me.LabelParamVal(19) = LabelParamVal19
        Me.LabelParamVal(20) = LabelParamVal20
        Me.LabelParamVal(21) = LabelParamVal21
    End Sub

    Private Sub InitializeTextBoxParamVal()
        ReDim TextBoxParamVal(21)
        Me.TextBoxParamVal(0) = TextBoxParamVal0
        Me.TextBoxParamVal(1) = TextBoxParamVal1
        Me.TextBoxParamVal(2) = TextBoxParamVal2
        Me.TextBoxParamVal(3) = TextBoxParamVal3
        Me.TextBoxParamVal(4) = TextBoxParamVal4
        Me.TextBoxParamVal(5) = TextBoxParamVal5
        Me.TextBoxParamVal(6) = TextBoxParamVal6
        Me.TextBoxParamVal(7) = TextBoxParamVal7
        Me.TextBoxParamVal(8) = TextBoxParamVal8
        Me.TextBoxParamVal(9) = TextBoxParamVal9
        Me.TextBoxParamVal(10) = TextBoxParamVal10
        Me.TextBoxParamVal(11) = TextBoxParamVal11
        Me.TextBoxParamVal(12) = TextBoxParamVal12
        Me.TextBoxParamVal(13) = TextBoxParamVal13
        Me.TextBoxParamVal(14) = TextBoxParamVal14
        Me.TextBoxParamVal(15) = TextBoxParamVal15
        Me.TextBoxParamVal(16) = TextBoxParamVal16
        Me.TextBoxParamVal(17) = TextBoxParamVal17
        Me.TextBoxParamVal(18) = TextBoxParamVal18
        Me.TextBoxParamVal(19) = TextBoxParamVal19
        Me.TextBoxParamVal(20) = TextBoxParamVal20
        Me.TextBoxParamVal(21) = TextBoxParamVal21
    End Sub

    Private Sub InitializeLabelPeriodicMessageId()
        ReDim LabelPeriodicMessageId(9)
        Me.LabelPeriodicMessageId(0) = LabelPeriodicMessageId0
        Me.LabelPeriodicMessageId(1) = LabelPeriodicMessageId1
        Me.LabelPeriodicMessageId(2) = LabelPeriodicMessageId2
        Me.LabelPeriodicMessageId(3) = LabelPeriodicMessageId3
        Me.LabelPeriodicMessageId(4) = LabelPeriodicMessageId4
        Me.LabelPeriodicMessageId(5) = LabelPeriodicMessageId5
        Me.LabelPeriodicMessageId(6) = LabelPeriodicMessageId6
        Me.LabelPeriodicMessageId(7) = LabelPeriodicMessageId7
        Me.LabelPeriodicMessageId(8) = LabelPeriodicMessageId8
        Me.LabelPeriodicMessageId(9) = LabelPeriodicMessageId9
    End Sub

    Private Sub InitializeLabelPeriodicMessage()
        ReDim LabelPeriodicMessage(9)
        Me.LabelPeriodicMessage(0) = LabelPeriodicMessage0
        Me.LabelPeriodicMessage(1) = LabelPeriodicMessage1
        Me.LabelPeriodicMessage(2) = LabelPeriodicMessage2
        Me.LabelPeriodicMessage(3) = LabelPeriodicMessage3
        Me.LabelPeriodicMessage(4) = LabelPeriodicMessage4
        Me.LabelPeriodicMessage(5) = LabelPeriodicMessage5
        Me.LabelPeriodicMessage(6) = LabelPeriodicMessage6
        Me.LabelPeriodicMessage(7) = LabelPeriodicMessage7
        Me.LabelPeriodicMessage(8) = LabelPeriodicMessage8
        Me.LabelPeriodicMessage(9) = LabelPeriodicMessage9
    End Sub

    Private Sub InitializeLabelFunctionalMessage()
        ReDim LabelFunctionalMessage(31)
        Me.LabelFunctionalMessage(0) = LabelFunctionalMessage0
        Me.LabelFunctionalMessage(1) = LabelFunctionalMessage1
        Me.LabelFunctionalMessage(2) = LabelFunctionalMessage2
        Me.LabelFunctionalMessage(3) = LabelFunctionalMessage3
        Me.LabelFunctionalMessage(4) = LabelFunctionalMessage4
        Me.LabelFunctionalMessage(5) = LabelFunctionalMessage5
        Me.LabelFunctionalMessage(6) = LabelFunctionalMessage6
        Me.LabelFunctionalMessage(7) = LabelFunctionalMessage7
        Me.LabelFunctionalMessage(8) = LabelFunctionalMessage8
        Me.LabelFunctionalMessage(9) = LabelFunctionalMessage9
        Me.LabelFunctionalMessage(10) = LabelFunctionalMessage10
        Me.LabelFunctionalMessage(11) = LabelFunctionalMessage11
        Me.LabelFunctionalMessage(12) = LabelFunctionalMessage12
        Me.LabelFunctionalMessage(13) = LabelFunctionalMessage13
        Me.LabelFunctionalMessage(14) = LabelFunctionalMessage14
        Me.LabelFunctionalMessage(15) = LabelFunctionalMessage15
        Me.LabelFunctionalMessage(16) = LabelFunctionalMessage16
        Me.LabelFunctionalMessage(17) = LabelFunctionalMessage17
        Me.LabelFunctionalMessage(18) = LabelFunctionalMessage18
        Me.LabelFunctionalMessage(19) = LabelFunctionalMessage19
        Me.LabelFunctionalMessage(20) = LabelFunctionalMessage20
        Me.LabelFunctionalMessage(21) = LabelFunctionalMessage21
        Me.LabelFunctionalMessage(22) = LabelFunctionalMessage22
        Me.LabelFunctionalMessage(23) = LabelFunctionalMessage23
        Me.LabelFunctionalMessage(24) = LabelFunctionalMessage24
        Me.LabelFunctionalMessage(25) = LabelFunctionalMessage25
        Me.LabelFunctionalMessage(26) = LabelFunctionalMessage26
        Me.LabelFunctionalMessage(27) = LabelFunctionalMessage27
        Me.LabelFunctionalMessage(28) = LabelFunctionalMessage28
        Me.LabelFunctionalMessage(29) = LabelFunctionalMessage29
        Me.LabelFunctionalMessage(30) = LabelFunctionalMessage30
        Me.LabelFunctionalMessage(31) = LabelFunctionalMessage31
    End Sub

    Private Sub InitializeLabelFuncId()
        ReDim LabelFuncId(31)
        Me.LabelFuncId(0) = LabelFuncId0
        Me.LabelFuncId(1) = LabelFuncId1
        Me.LabelFuncId(2) = LabelFuncId2
        Me.LabelFuncId(3) = LabelFuncId3
        Me.LabelFuncId(4) = LabelFuncId4
        Me.LabelFuncId(5) = LabelFuncId5
        Me.LabelFuncId(6) = LabelFuncId6
        Me.LabelFuncId(7) = LabelFuncId7
        Me.LabelFuncId(8) = LabelFuncId8
        Me.LabelFuncId(9) = LabelFuncId9
        Me.LabelFuncId(10) = LabelFuncId10
        Me.LabelFuncId(11) = LabelFuncId11
        Me.LabelFuncId(12) = LabelFuncId12
        Me.LabelFuncId(13) = LabelFuncId13
        Me.LabelFuncId(14) = LabelFuncId14
        Me.LabelFuncId(15) = LabelFuncId15
        Me.LabelFuncId(16) = LabelFuncId16
        Me.LabelFuncId(17) = LabelFuncId17
        Me.LabelFuncId(18) = LabelFuncId18
        Me.LabelFuncId(19) = LabelFuncId19
        Me.LabelFuncId(20) = LabelFuncId20
        Me.LabelFuncId(21) = LabelFuncId21
        Me.LabelFuncId(22) = LabelFuncId22
        Me.LabelFuncId(23) = LabelFuncId23
        Me.LabelFuncId(24) = LabelFuncId24
        Me.LabelFuncId(25) = LabelFuncId25
        Me.LabelFuncId(26) = LabelFuncId26
        Me.LabelFuncId(27) = LabelFuncId27
        Me.LabelFuncId(28) = LabelFuncId28
        Me.LabelFuncId(29) = LabelFuncId29
        Me.LabelFuncId(30) = LabelFuncId30
        Me.LabelFuncId(31) = LabelFuncId31
    End Sub

    Private Sub InitializeLabelFilterId()
        ReDim LabelFilterId(9)
        Me.LabelFilterId(0) = LabelFilterId0
        Me.LabelFilterId(1) = LabelFilterId1
        Me.LabelFilterId(2) = LabelFilterId2
        Me.LabelFilterId(3) = LabelFilterId3
        Me.LabelFilterId(4) = LabelFilterId4
        Me.LabelFilterId(5) = LabelFilterId5
        Me.LabelFilterId(6) = LabelFilterId6
        Me.LabelFilterId(7) = LabelFilterId7
        Me.LabelFilterId(8) = LabelFilterId8
        Me.LabelFilterId(9) = LabelFilterId9
    End Sub

    Private Sub InitializeLabelFilter()
        ReDim LabelFilter(9)
        Me.LabelFilter(0) = LabelFilter0
        Me.LabelFilter(1) = LabelFilter1
        Me.LabelFilter(2) = LabelFilter2
        Me.LabelFilter(3) = LabelFilter3
        Me.LabelFilter(4) = LabelFilter4
        Me.LabelFilter(5) = LabelFilter5
        Me.LabelFilter(6) = LabelFilter6
        Me.LabelFilter(7) = LabelFilter7
        Me.LabelFilter(8) = LabelFilter8
        Me.LabelFilter(9) = LabelFilter9
    End Sub

    Private Sub InitializeLabelDeviceCombo()
        ReDim LabelDeviceCombo(7)
        Me.LabelDeviceCombo(0) = LabelDeviceCombo0
        Me.LabelDeviceCombo(1) = LabelDeviceCombo1
        Me.LabelDeviceCombo(2) = LabelDeviceCombo2
        Me.LabelDeviceCombo(3) = LabelDeviceCombo3
        Me.LabelDeviceCombo(4) = LabelDeviceCombo4
        Me.LabelDeviceCombo(5) = LabelDeviceCombo5
        Me.LabelDeviceCombo(6) = LabelDeviceCombo6
        Me.LabelDeviceCombo(7) = LabelDeviceCombo7
    End Sub

    Private Sub InitializeLabelAnalogRead()
        ReDim LabelAnalogRead(31)
        Me.LabelAnalogRead(0) = LabelAnalogRead0
        Me.LabelAnalogRead(1) = LabelAnalogRead1
        Me.LabelAnalogRead(2) = LabelAnalogRead2
        Me.LabelAnalogRead(3) = LabelAnalogRead3
        Me.LabelAnalogRead(4) = LabelAnalogRead4
        Me.LabelAnalogRead(5) = LabelAnalogRead5
        Me.LabelAnalogRead(6) = LabelAnalogRead6
        Me.LabelAnalogRead(7) = LabelAnalogRead7
        Me.LabelAnalogRead(8) = LabelAnalogRead8
        Me.LabelAnalogRead(9) = LabelAnalogRead9
        Me.LabelAnalogRead(10) = LabelAnalogRead10
        Me.LabelAnalogRead(11) = LabelAnalogRead11
        Me.LabelAnalogRead(12) = LabelAnalogRead12
        Me.LabelAnalogRead(13) = LabelAnalogRead13
        Me.LabelAnalogRead(14) = LabelAnalogRead14
        Me.LabelAnalogRead(15) = LabelAnalogRead15
        Me.LabelAnalogRead(16) = LabelAnalogRead16
        Me.LabelAnalogRead(17) = LabelAnalogRead17
        Me.LabelAnalogRead(18) = LabelAnalogRead18
        Me.LabelAnalogRead(19) = LabelAnalogRead19
        Me.LabelAnalogRead(20) = LabelAnalogRead20
        Me.LabelAnalogRead(21) = LabelAnalogRead21
        Me.LabelAnalogRead(22) = LabelAnalogRead22
        Me.LabelAnalogRead(23) = LabelAnalogRead23
        Me.LabelAnalogRead(24) = LabelAnalogRead24
        Me.LabelAnalogRead(25) = LabelAnalogRead25
        Me.LabelAnalogRead(26) = LabelAnalogRead26
        Me.LabelAnalogRead(27) = LabelAnalogRead27
        Me.LabelAnalogRead(28) = LabelAnalogRead28
        Me.LabelAnalogRead(29) = LabelAnalogRead29
        Me.LabelAnalogRead(30) = LabelAnalogRead30
        Me.LabelAnalogRead(31) = LabelAnalogRead31
    End Sub

    Private Sub InitializeLabelAnalogCH()
        ReDim LabelAnalogCH(31)
        Me.LabelAnalogCH(0) = LabelAnalogCH0
        Me.LabelAnalogCH(1) = LabelAnalogCH1
        Me.LabelAnalogCH(2) = LabelAnalogCH2
        Me.LabelAnalogCH(3) = LabelAnalogCH3
        Me.LabelAnalogCH(4) = LabelAnalogCH4
        Me.LabelAnalogCH(5) = LabelAnalogCH5
        Me.LabelAnalogCH(6) = LabelAnalogCH6
        Me.LabelAnalogCH(7) = LabelAnalogCH7
        Me.LabelAnalogCH(8) = LabelAnalogCH8
        Me.LabelAnalogCH(9) = LabelAnalogCH9
        Me.LabelAnalogCH(10) = LabelAnalogCH10
        Me.LabelAnalogCH(11) = LabelAnalogCH11
        Me.LabelAnalogCH(12) = LabelAnalogCH12
        Me.LabelAnalogCH(13) = LabelAnalogCH13
        Me.LabelAnalogCH(14) = LabelAnalogCH14
        Me.LabelAnalogCH(15) = LabelAnalogCH15
        Me.LabelAnalogCH(16) = LabelAnalogCH16
        Me.LabelAnalogCH(17) = LabelAnalogCH17
        Me.LabelAnalogCH(18) = LabelAnalogCH18
        Me.LabelAnalogCH(19) = LabelAnalogCH19
        Me.LabelAnalogCH(20) = LabelAnalogCH20
        Me.LabelAnalogCH(21) = LabelAnalogCH21
        Me.LabelAnalogCH(22) = LabelAnalogCH22
        Me.LabelAnalogCH(23) = LabelAnalogCH23
        Me.LabelAnalogCH(24) = LabelAnalogCH24
        Me.LabelAnalogCH(25) = LabelAnalogCH25
        Me.LabelAnalogCH(26) = LabelAnalogCH26
        Me.LabelAnalogCH(27) = LabelAnalogCH27
        Me.LabelAnalogCH(28) = LabelAnalogCH28
        Me.LabelAnalogCH(29) = LabelAnalogCH29
        Me.LabelAnalogCH(30) = LabelAnalogCH30
        Me.LabelAnalogCH(31) = LabelAnalogCH31
    End Sub

    Private Sub InitializeCheckBoxPeriodicMessageEnable()
        ReDim CheckBoxPeriodicMessageEnable(9)
        Me.CheckBoxPeriodicMessageEnable(0) = CheckBoxPeriodicMessageEnable0
        Me.CheckBoxPeriodicMessageEnable(1) = CheckBoxPeriodicMessageEnable1
        Me.CheckBoxPeriodicMessageEnable(2) = CheckBoxPeriodicMessageEnable2
        Me.CheckBoxPeriodicMessageEnable(3) = CheckBoxPeriodicMessageEnable3
        Me.CheckBoxPeriodicMessageEnable(4) = CheckBoxPeriodicMessageEnable4
        Me.CheckBoxPeriodicMessageEnable(5) = CheckBoxPeriodicMessageEnable5
        Me.CheckBoxPeriodicMessageEnable(6) = CheckBoxPeriodicMessageEnable6
        Me.CheckBoxPeriodicMessageEnable(7) = CheckBoxPeriodicMessageEnable7
        Me.CheckBoxPeriodicMessageEnable(8) = CheckBoxPeriodicMessageEnable8
        Me.CheckBoxPeriodicMessageEnable(9) = CheckBoxPeriodicMessageEnable9
    End Sub

    Private Sub InitializeCheckBoxFunctionalMessageDelete()
        ReDim CheckBoxFunctionalMessageDelete(31)
        Me.CheckBoxFunctionalMessageDelete(0) = CheckBoxFunctionalMessageDelete0
        Me.CheckBoxFunctionalMessageDelete(1) = CheckBoxFunctionalMessageDelete1
        Me.CheckBoxFunctionalMessageDelete(2) = CheckBoxFunctionalMessageDelete2
        Me.CheckBoxFunctionalMessageDelete(3) = CheckBoxFunctionalMessageDelete3
        Me.CheckBoxFunctionalMessageDelete(4) = CheckBoxFunctionalMessageDelete4
        Me.CheckBoxFunctionalMessageDelete(5) = CheckBoxFunctionalMessageDelete5
        Me.CheckBoxFunctionalMessageDelete(6) = CheckBoxFunctionalMessageDelete6
        Me.CheckBoxFunctionalMessageDelete(7) = CheckBoxFunctionalMessageDelete7
        Me.CheckBoxFunctionalMessageDelete(8) = CheckBoxFunctionalMessageDelete8
        Me.CheckBoxFunctionalMessageDelete(9) = CheckBoxFunctionalMessageDelete9
        Me.CheckBoxFunctionalMessageDelete(10) = CheckBoxFunctionalMessageDelete10
        Me.CheckBoxFunctionalMessageDelete(11) = CheckBoxFunctionalMessageDelete11
        Me.CheckBoxFunctionalMessageDelete(12) = CheckBoxFunctionalMessageDelete12
        Me.CheckBoxFunctionalMessageDelete(13) = CheckBoxFunctionalMessageDelete13
        Me.CheckBoxFunctionalMessageDelete(14) = CheckBoxFunctionalMessageDelete14
        Me.CheckBoxFunctionalMessageDelete(15) = CheckBoxFunctionalMessageDelete15
        Me.CheckBoxFunctionalMessageDelete(16) = CheckBoxFunctionalMessageDelete16
        Me.CheckBoxFunctionalMessageDelete(17) = CheckBoxFunctionalMessageDelete17
        Me.CheckBoxFunctionalMessageDelete(18) = CheckBoxFunctionalMessageDelete18
        Me.CheckBoxFunctionalMessageDelete(19) = CheckBoxFunctionalMessageDelete19
        Me.CheckBoxFunctionalMessageDelete(20) = CheckBoxFunctionalMessageDelete20
        Me.CheckBoxFunctionalMessageDelete(21) = CheckBoxFunctionalMessageDelete21
        Me.CheckBoxFunctionalMessageDelete(22) = CheckBoxFunctionalMessageDelete22
        Me.CheckBoxFunctionalMessageDelete(23) = CheckBoxFunctionalMessageDelete23
        Me.CheckBoxFunctionalMessageDelete(24) = CheckBoxFunctionalMessageDelete24
        Me.CheckBoxFunctionalMessageDelete(25) = CheckBoxFunctionalMessageDelete25
        Me.CheckBoxFunctionalMessageDelete(26) = CheckBoxFunctionalMessageDelete26
        Me.CheckBoxFunctionalMessageDelete(27) = CheckBoxFunctionalMessageDelete27
        Me.CheckBoxFunctionalMessageDelete(28) = CheckBoxFunctionalMessageDelete28
        Me.CheckBoxFunctionalMessageDelete(29) = CheckBoxFunctionalMessageDelete29
        Me.CheckBoxFunctionalMessageDelete(30) = CheckBoxFunctionalMessageDelete30
        Me.CheckBoxFunctionalMessageDelete(31) = CheckBoxFunctionalMessageDelete31
    End Sub

    Private Sub InitializeComboBoxPeriodicMessageChannel()
        ReDim ComboBoxPeriodicMessageChannel(9)
        Me.ComboBoxPeriodicMessageChannel(0) = ComboBoxPeriodicMessageChannel0
        Me.ComboBoxPeriodicMessageChannel(1) = ComboBoxPeriodicMessageChannel1
        Me.ComboBoxPeriodicMessageChannel(2) = ComboBoxPeriodicMessageChannel2
        Me.ComboBoxPeriodicMessageChannel(3) = ComboBoxPeriodicMessageChannel3
        Me.ComboBoxPeriodicMessageChannel(4) = ComboBoxPeriodicMessageChannel4
        Me.ComboBoxPeriodicMessageChannel(5) = ComboBoxPeriodicMessageChannel5
        Me.ComboBoxPeriodicMessageChannel(6) = ComboBoxPeriodicMessageChannel6
        Me.ComboBoxPeriodicMessageChannel(7) = ComboBoxPeriodicMessageChannel7
        Me.ComboBoxPeriodicMessageChannel(8) = ComboBoxPeriodicMessageChannel8
        Me.ComboBoxPeriodicMessageChannel(9) = ComboBoxPeriodicMessageChannel9
    End Sub

    Private Sub InitializeComboBoxFilterType()
        ReDim ComboBoxFilterType(9)
        Me.ComboBoxFilterType(0) = ComboBoxFilterType0
        Me.ComboBoxFilterType(1) = ComboBoxFilterType1
        Me.ComboBoxFilterType(2) = ComboBoxFilterType2
        Me.ComboBoxFilterType(3) = ComboBoxFilterType3
        Me.ComboBoxFilterType(4) = ComboBoxFilterType4
        Me.ComboBoxFilterType(5) = ComboBoxFilterType5
        Me.ComboBoxFilterType(6) = ComboBoxFilterType6
        Me.ComboBoxFilterType(7) = ComboBoxFilterType7
        Me.ComboBoxFilterType(8) = ComboBoxFilterType8
        Me.ComboBoxFilterType(9) = ComboBoxFilterType9
    End Sub

    Private Sub InitializeCheckBoxCH()
        ReDim CheckBoxCH(31)
        Me.CheckBoxCH(0) = CheckBoxCH0
        Me.CheckBoxCH(1) = CheckBoxCH1
        Me.CheckBoxCH(2) = CheckBoxCH2
        Me.CheckBoxCH(3) = CheckBoxCH3
        Me.CheckBoxCH(4) = CheckBoxCH4
        Me.CheckBoxCH(5) = CheckBoxCH5
        Me.CheckBoxCH(6) = CheckBoxCH6
        Me.CheckBoxCH(7) = CheckBoxCH7
        Me.CheckBoxCH(8) = CheckBoxCH8
        Me.CheckBoxCH(9) = CheckBoxCH9
        Me.CheckBoxCH(10) = CheckBoxCH10
        Me.CheckBoxCH(11) = CheckBoxCH11
        Me.CheckBoxCH(12) = CheckBoxCH12
        Me.CheckBoxCH(13) = CheckBoxCH13
        Me.CheckBoxCH(14) = CheckBoxCH14
        Me.CheckBoxCH(15) = CheckBoxCH15
        Me.CheckBoxCH(16) = CheckBoxCH16
        Me.CheckBoxCH(17) = CheckBoxCH17
        Me.CheckBoxCH(18) = CheckBoxCH18
        Me.CheckBoxCH(19) = CheckBoxCH19
        Me.CheckBoxCH(20) = CheckBoxCH20
        Me.CheckBoxCH(21) = CheckBoxCH21
        Me.CheckBoxCH(22) = CheckBoxCH22
        Me.CheckBoxCH(23) = CheckBoxCH23
        Me.CheckBoxCH(24) = CheckBoxCH24
        Me.CheckBoxCH(25) = CheckBoxCH25
        Me.CheckBoxCH(26) = CheckBoxCH26
        Me.CheckBoxCH(27) = CheckBoxCH27
        Me.CheckBoxCH(28) = CheckBoxCH28
        Me.CheckBoxCH(29) = CheckBoxCH29
        Me.CheckBoxCH(30) = CheckBoxCH30
        Me.CheckBoxCH(31) = CheckBoxCH31
    End Sub

    Private CheckBoxCH As CheckBox()
    Private CheckBoxCH0 As CheckBox
    Private CheckBoxCH1 As CheckBox
    Private CheckBoxCH2 As CheckBox
    Private CheckBoxCH3 As CheckBox
    Private CheckBoxCH4 As CheckBox
    Private CheckBoxCH5 As CheckBox
    Private CheckBoxCH6 As CheckBox
    Private CheckBoxCH7 As CheckBox
    Private CheckBoxCH8 As CheckBox
    Private CheckBoxCH9 As CheckBox
    Private CheckBoxCH10 As CheckBox
    Private CheckBoxCH11 As CheckBox
    Private CheckBoxCH12 As CheckBox
    Private CheckBoxCH13 As CheckBox
    Private CheckBoxCH14 As CheckBox
    Private CheckBoxCH15 As CheckBox
    Private CheckBoxCH16 As CheckBox
    Private CheckBoxCH17 As CheckBox
    Private CheckBoxCH18 As CheckBox
    Private CheckBoxCH19 As CheckBox
    Private CheckBoxCH20 As CheckBox
    Private CheckBoxCH21 As CheckBox
    Private CheckBoxCH22 As CheckBox
    Private CheckBoxCH23 As CheckBox
    Private CheckBoxCH24 As CheckBox
    Private CheckBoxCH25 As CheckBox
    Private CheckBoxCH26 As CheckBox
    Private CheckBoxCH27 As CheckBox
    Private CheckBoxCH28 As CheckBox
    Private CheckBoxCH29 As CheckBox
    Private CheckBoxCH30 As CheckBox
    Private CheckBoxCH31 As CheckBox

    Private CheckBoxFunctionalMessageDelete As CheckBox()
    Private CheckBoxFunctionalMessageDelete0 As CheckBox
    Private CheckBoxFunctionalMessageDelete1 As CheckBox
    Private CheckBoxFunctionalMessageDelete2 As CheckBox
    Private CheckBoxFunctionalMessageDelete3 As CheckBox
    Private CheckBoxFunctionalMessageDelete4 As CheckBox
    Private CheckBoxFunctionalMessageDelete5 As CheckBox
    Private CheckBoxFunctionalMessageDelete6 As CheckBox
    Private CheckBoxFunctionalMessageDelete7 As CheckBox
    Private CheckBoxFunctionalMessageDelete8 As CheckBox
    Private CheckBoxFunctionalMessageDelete9 As CheckBox
    Private CheckBoxFunctionalMessageDelete10 As CheckBox
    Private CheckBoxFunctionalMessageDelete11 As CheckBox
    Private CheckBoxFunctionalMessageDelete12 As CheckBox
    Private CheckBoxFunctionalMessageDelete13 As CheckBox
    Private CheckBoxFunctionalMessageDelete14 As CheckBox
    Private CheckBoxFunctionalMessageDelete15 As CheckBox
    Private CheckBoxFunctionalMessageDelete16 As CheckBox
    Private CheckBoxFunctionalMessageDelete17 As CheckBox
    Private CheckBoxFunctionalMessageDelete18 As CheckBox
    Private CheckBoxFunctionalMessageDelete19 As CheckBox
    Private CheckBoxFunctionalMessageDelete20 As CheckBox
    Private CheckBoxFunctionalMessageDelete21 As CheckBox
    Private CheckBoxFunctionalMessageDelete22 As CheckBox
    Private CheckBoxFunctionalMessageDelete23 As CheckBox
    Private CheckBoxFunctionalMessageDelete24 As CheckBox
    Private CheckBoxFunctionalMessageDelete25 As CheckBox
    Private CheckBoxFunctionalMessageDelete26 As CheckBox
    Private CheckBoxFunctionalMessageDelete27 As CheckBox
    Private CheckBoxFunctionalMessageDelete28 As CheckBox
    Private CheckBoxFunctionalMessageDelete29 As CheckBox
    Private CheckBoxFunctionalMessageDelete30 As CheckBox
    Private CheckBoxFunctionalMessageDelete31 As CheckBox

    Private CheckBoxPeriodicMessageEnable As CheckBox()
    Private CheckBoxPeriodicMessageEnable0 As CheckBox
    Private CheckBoxPeriodicMessageEnable1 As CheckBox
    Private CheckBoxPeriodicMessageEnable2 As CheckBox
    Private CheckBoxPeriodicMessageEnable3 As CheckBox
    Private CheckBoxPeriodicMessageEnable4 As CheckBox
    Private CheckBoxPeriodicMessageEnable5 As CheckBox
    Private CheckBoxPeriodicMessageEnable6 As CheckBox
    Private CheckBoxPeriodicMessageEnable7 As CheckBox
    Private CheckBoxPeriodicMessageEnable8 As CheckBox
    Private CheckBoxPeriodicMessageEnable9 As CheckBox

    Private ComboAvailableBoxLocator As Label()
    Private ComboAvailableChannelLocator As Label()
    Private ComboAvailableBoxLocator0 As Label
    Private ComboAvailableChannelLocator0 As Label
    Private ComboAvailableBoxLocator1 As Label
    Private ComboAvailableChannelLocator1 As Label
    Private ComboAvailableBoxLocator2 As Label
    Private ComboAvailableChannelLocator2 As Label
    Private ComboAvailableBoxLocator3 As Label
    Private ComboAvailableChannelLocator3 As Label
    Private ComboAvailableBoxLocator4 As Label
    Private ComboAvailableChannelLocator4 As Label
    Private ComboAvailableBoxLocator5 As Label
    Private ComboAvailableChannelLocator5 As Label
    Private ComboAvailableBoxLocator6 As Label
    Private ComboAvailableChannelLocator6 As Label
    Private ComboAvailableBoxLocator7 As Label
    Private ComboAvailableChannelLocator7 As Label

    Private ComboBoxFilterType As ComboBox()
    Private ComboBoxFilterType0 As ComboBox
    Private ComboBoxFilterType1 As ComboBox
    Private ComboBoxFilterType2 As ComboBox
    Private ComboBoxFilterType3 As ComboBox
    Private ComboBoxFilterType4 As ComboBox
    Private ComboBoxFilterType5 As ComboBox
    Private ComboBoxFilterType6 As ComboBox
    Private ComboBoxFilterType7 As ComboBox
    Private ComboBoxFilterType8 As ComboBox
    Private ComboBoxFilterType9 As ComboBox

    Private ComboBoxPeriodicMessageChannel As ComboBox()
    Private ComboBoxPeriodicMessageChannel0 As ComboBox
    Private ComboBoxPeriodicMessageChannel1 As ComboBox
    Private ComboBoxPeriodicMessageChannel2 As ComboBox
    Private ComboBoxPeriodicMessageChannel3 As ComboBox
    Private ComboBoxPeriodicMessageChannel4 As ComboBox
    Private ComboBoxPeriodicMessageChannel5 As ComboBox
    Private ComboBoxPeriodicMessageChannel6 As ComboBox
    Private ComboBoxPeriodicMessageChannel7 As ComboBox
    Private ComboBoxPeriodicMessageChannel8 As ComboBox
    Private ComboBoxPeriodicMessageChannel9 As ComboBox

    Private LabelAnalogCH As Label()
    Private LabelAnalogCH0 As Label
    Private LabelAnalogCH1 As Label
    Private LabelAnalogCH2 As Label
    Private LabelAnalogCH3 As Label
    Private LabelAnalogCH4 As Label
    Private LabelAnalogCH5 As Label
    Private LabelAnalogCH6 As Label
    Private LabelAnalogCH7 As Label
    Private LabelAnalogCH8 As Label
    Private LabelAnalogCH9 As Label
    Private LabelAnalogCH10 As Label
    Private LabelAnalogCH11 As Label
    Private LabelAnalogCH12 As Label
    Private LabelAnalogCH13 As Label
    Private LabelAnalogCH14 As Label
    Private LabelAnalogCH15 As Label
    Private LabelAnalogCH16 As Label
    Private LabelAnalogCH17 As Label
    Private LabelAnalogCH18 As Label
    Private LabelAnalogCH19 As Label
    Private LabelAnalogCH20 As Label
    Private LabelAnalogCH21 As Label
    Private LabelAnalogCH22 As Label
    Private LabelAnalogCH23 As Label
    Private LabelAnalogCH24 As Label
    Private LabelAnalogCH25 As Label
    Private LabelAnalogCH26 As Label
    Private LabelAnalogCH27 As Label
    Private LabelAnalogCH28 As Label
    Private LabelAnalogCH29 As Label
    Private LabelAnalogCH30 As Label
    Private LabelAnalogCH31 As Label

    Private LabelAnalogRead As Label()
    Private LabelAnalogRead0 As Label
    Private LabelAnalogRead1 As Label
    Private LabelAnalogRead2 As Label
    Private LabelAnalogRead3 As Label
    Private LabelAnalogRead4 As Label
    Private LabelAnalogRead5 As Label
    Private LabelAnalogRead6 As Label
    Private LabelAnalogRead7 As Label
    Private LabelAnalogRead8 As Label
    Private LabelAnalogRead9 As Label
    Private LabelAnalogRead10 As Label
    Private LabelAnalogRead11 As Label
    Private LabelAnalogRead12 As Label
    Private LabelAnalogRead13 As Label
    Private LabelAnalogRead14 As Label
    Private LabelAnalogRead15 As Label
    Private LabelAnalogRead16 As Label
    Private LabelAnalogRead17 As Label
    Private LabelAnalogRead18 As Label
    Private LabelAnalogRead19 As Label
    Private LabelAnalogRead20 As Label
    Private LabelAnalogRead21 As Label
    Private LabelAnalogRead22 As Label
    Private LabelAnalogRead23 As Label
    Private LabelAnalogRead24 As Label
    Private LabelAnalogRead25 As Label
    Private LabelAnalogRead26 As Label
    Private LabelAnalogRead27 As Label
    Private LabelAnalogRead28 As Label
    Private LabelAnalogRead29 As Label
    Private LabelAnalogRead30 As Label
    Private LabelAnalogRead31 As Label

    Private LabelDeviceCombo As Label()
    Private LabelDeviceCombo0 As Label
    Private LabelDeviceCombo1 As Label
    Private LabelDeviceCombo2 As Label
    Private LabelDeviceCombo3 As Label
    Private LabelDeviceCombo4 As Label
    Private LabelDeviceCombo5 As Label
    Private LabelDeviceCombo6 As Label
    Private LabelDeviceCombo7 As Label

    Private LabelFilter As Label()
    Private LabelFilter0 As Label
    Private LabelFilter1 As Label
    Private LabelFilter2 As Label
    Private LabelFilter3 As Label
    Private LabelFilter4 As Label
    Private LabelFilter5 As Label
    Private LabelFilter6 As Label
    Private LabelFilter7 As Label
    Private LabelFilter8 As Label
    Private LabelFilter9 As Label

    Private LabelFilterId As Label()
    Private LabelFilterId0 As Label
    Private LabelFilterId1 As Label
    Private LabelFilterId2 As Label
    Private LabelFilterId3 As Label
    Private LabelFilterId4 As Label
    Private LabelFilterId5 As Label
    Private LabelFilterId6 As Label
    Private LabelFilterId7 As Label
    Private LabelFilterId8 As Label
    Private LabelFilterId9 As Label

    Private LabelFuncId As Label()
    Private LabelFuncId0 As Label
    Private LabelFuncId1 As Label
    Private LabelFuncId2 As Label
    Private LabelFuncId3 As Label
    Private LabelFuncId4 As Label
    Private LabelFuncId5 As Label
    Private LabelFuncId6 As Label
    Private LabelFuncId7 As Label
    Private LabelFuncId8 As Label
    Private LabelFuncId9 As Label
    Private LabelFuncId10 As Label
    Private LabelFuncId11 As Label
    Private LabelFuncId12 As Label
    Private LabelFuncId13 As Label
    Private LabelFuncId14 As Label
    Private LabelFuncId15 As Label
    Private LabelFuncId16 As Label
    Private LabelFuncId17 As Label
    Private LabelFuncId18 As Label
    Private LabelFuncId19 As Label
    Private LabelFuncId20 As Label
    Private LabelFuncId21 As Label
    Private LabelFuncId22 As Label
    Private LabelFuncId23 As Label
    Private LabelFuncId24 As Label
    Private LabelFuncId25 As Label
    Private LabelFuncId26 As Label
    Private LabelFuncId27 As Label
    Private LabelFuncId28 As Label
    Private LabelFuncId29 As Label
    Private LabelFuncId30 As Label
    Private LabelFuncId31 As Label

    Private LabelFunctionalMessage As Label()
    Private LabelFunctionalMessage0 As Label
    Private LabelFunctionalMessage1 As Label
    Private LabelFunctionalMessage2 As Label
    Private LabelFunctionalMessage3 As Label
    Private LabelFunctionalMessage4 As Label
    Private LabelFunctionalMessage5 As Label
    Private LabelFunctionalMessage6 As Label
    Private LabelFunctionalMessage7 As Label
    Private LabelFunctionalMessage8 As Label
    Private LabelFunctionalMessage9 As Label
    Private LabelFunctionalMessage10 As Label
    Private LabelFunctionalMessage11 As Label
    Private LabelFunctionalMessage12 As Label
    Private LabelFunctionalMessage13 As Label
    Private LabelFunctionalMessage14 As Label
    Private LabelFunctionalMessage15 As Label
    Private LabelFunctionalMessage16 As Label
    Private LabelFunctionalMessage17 As Label
    Private LabelFunctionalMessage18 As Label
    Private LabelFunctionalMessage19 As Label
    Private LabelFunctionalMessage20 As Label
    Private LabelFunctionalMessage21 As Label
    Private LabelFunctionalMessage22 As Label
    Private LabelFunctionalMessage23 As Label
    Private LabelFunctionalMessage24 As Label
    Private LabelFunctionalMessage25 As Label
    Private LabelFunctionalMessage26 As Label
    Private LabelFunctionalMessage27 As Label
    Private LabelFunctionalMessage28 As Label
    Private LabelFunctionalMessage29 As Label
    Private LabelFunctionalMessage30 As Label
    Private LabelFunctionalMessage31 As Label

    Private LabelParameterName As Label()
    Private LabelParameterName0 As Label
    Private LabelParameterName1 As Label
    Private LabelParameterName2 As Label
    Private LabelParameterName3 As Label
    Private LabelParameterName4 As Label
    Private LabelParameterName5 As Label
    Private LabelParameterName6 As Label
    Private LabelParameterName7 As Label
    Private LabelParameterName8 As Label
    Private LabelParameterName9 As Label
    Private LabelParameterName10 As Label
    Private LabelParameterName11 As Label
    Private LabelParameterName12 As Label
    Private LabelParameterName13 As Label
    Private LabelParameterName14 As Label
    Private LabelParameterName15 As Label
    Private LabelParameterName16 As Label
    Private LabelParameterName17 As Label
    Private LabelParameterName18 As Label
    Private LabelParameterName19 As Label
    Private LabelParameterName20 As Label
    Private LabelParameterName21 As Label

    Private LabelParamVal As Label()
    Private LabelParamVal0 As Label
    Private LabelParamVal1 As Label
    Private LabelParamVal2 As Label
    Private LabelParamVal3 As Label
    Private LabelParamVal4 As Label
    Private LabelParamVal5 As Label
    Private LabelParamVal6 As Label
    Private LabelParamVal7 As Label
    Private LabelParamVal8 As Label
    Private LabelParamVal9 As Label
    Private LabelParamVal10 As Label
    Private LabelParamVal11 As Label
    Private LabelParamVal12 As Label
    Private LabelParamVal13 As Label
    Private LabelParamVal14 As Label
    Private LabelParamVal15 As Label
    Private LabelParamVal16 As Label
    Private LabelParamVal17 As Label
    Private LabelParamVal18 As Label
    Private LabelParamVal19 As Label
    Private LabelParamVal20 As Label
    Private LabelParamVal21 As Label

    Private LabelPin As Label()
    Private LabelPin0 As Label
    Private LabelPin6 As Label
    Private LabelPin9 As Label
    Private LabelPin11 As Label
    Private LabelPin12 As Label
    Private LabelPin13 As Label
    Private LabelPin14 As Label
    Private LabelPin15 As Label

    Private LabelPeriodicMessageId As Label()
    Private LabelPeriodicMessageId0 As Label
    Private LabelPeriodicMessageId1 As Label
    Private LabelPeriodicMessageId2 As Label
    Private LabelPeriodicMessageId3 As Label
    Private LabelPeriodicMessageId4 As Label
    Private LabelPeriodicMessageId5 As Label
    Private LabelPeriodicMessageId6 As Label
    Private LabelPeriodicMessageId7 As Label
    Private LabelPeriodicMessageId8 As Label
    Private LabelPeriodicMessageId9 As Label

    Private LabelPeriodicMessage As Label()
    Private LabelPeriodicMessage0 As Label
    Private LabelPeriodicMessage1 As Label
    Private LabelPeriodicMessage2 As Label
    Private LabelPeriodicMessage3 As Label
    Private LabelPeriodicMessage4 As Label
    Private LabelPeriodicMessage5 As Label
    Private LabelPeriodicMessage6 As Label
    Private LabelPeriodicMessage7 As Label
    Private LabelPeriodicMessage8 As Label
    Private LabelPeriodicMessage9 As Label

    Private LabelChannel As Label()
    Private LabelChannel1 As Label
    Private LabelChannel2 As Label
    Private LabelChannel3 As Label
    Private LabelChannel4 As Label
    Private LabelChannel5 As Label
    Private LabelChannel6 As Label

    Private ProgressBarAnalog As ProgressBar()
    Private ProgressBarAnalog0 As ProgressBar
    Private ProgressBarAnalog1 As ProgressBar
    Private ProgressBarAnalog2 As ProgressBar
    Private ProgressBarAnalog3 As ProgressBar
    Private ProgressBarAnalog4 As ProgressBar
    Private ProgressBarAnalog5 As ProgressBar
    Private ProgressBarAnalog6 As ProgressBar
    Private ProgressBarAnalog7 As ProgressBar
    Private ProgressBarAnalog8 As ProgressBar
    Private ProgressBarAnalog9 As ProgressBar
    Private ProgressBarAnalog10 As ProgressBar
    Private ProgressBarAnalog11 As ProgressBar
    Private ProgressBarAnalog12 As ProgressBar
    Private ProgressBarAnalog13 As ProgressBar
    Private ProgressBarAnalog14 As ProgressBar
    Private ProgressBarAnalog15 As ProgressBar
    Private ProgressBarAnalog16 As ProgressBar
    Private ProgressBarAnalog17 As ProgressBar
    Private ProgressBarAnalog18 As ProgressBar
    Private ProgressBarAnalog19 As ProgressBar
    Private ProgressBarAnalog20 As ProgressBar
    Private ProgressBarAnalog21 As ProgressBar
    Private ProgressBarAnalog22 As ProgressBar
    Private ProgressBarAnalog23 As ProgressBar
    Private ProgressBarAnalog24 As ProgressBar
    Private ProgressBarAnalog25 As ProgressBar
    Private ProgressBarAnalog26 As ProgressBar
    Private ProgressBarAnalog27 As ProgressBar
    Private ProgressBarAnalog28 As ProgressBar
    Private ProgressBarAnalog29 As ProgressBar
    Private ProgressBarAnalog30 As ProgressBar
    Private ProgressBarAnalog31 As ProgressBar

    Private RadioButtonPin As RadioButton()
    Private RadioButtonPin0 As RadioButton
    Private RadioButtonPin6 As RadioButton
    Private RadioButtonPin9 As RadioButton
    Private RadioButtonPin11 As RadioButton
    Private RadioButtonPin12 As RadioButton
    Private RadioButtonPin13 As RadioButton
    Private RadioButtonPin14 As RadioButton
    Private RadioButtonPin15 As RadioButton

    Private TextBoxFilterFlow As TextBox()
    Private TextBoxFilterFlow0 As TextBox
    Private TextBoxFilterFlow1 As TextBox
    Private TextBoxFilterFlow2 As TextBox
    Private TextBoxFilterFlow3 As TextBox
    Private TextBoxFilterFlow4 As TextBox
    Private TextBoxFilterFlow5 As TextBox
    Private TextBoxFilterFlow6 As TextBox
    Private TextBoxFilterFlow7 As TextBox
    Private TextBoxFilterFlow8 As TextBox
    Private TextBoxFilterFlow9 As TextBox

    Private TextBoxFilterFlags As TextBox()
    Private TextBoxFilterFlags0 As TextBox
    Private TextBoxFilterFlags1 As TextBox
    Private TextBoxFilterFlags2 As TextBox
    Private TextBoxFilterFlags3 As TextBox
    Private TextBoxFilterFlags4 As TextBox
    Private TextBoxFilterFlags5 As TextBox
    Private TextBoxFilterFlags6 As TextBox
    Private TextBoxFilterFlags7 As TextBox
    Private TextBoxFilterFlags8 As TextBox
    Private TextBoxFilterFlags9 As TextBox

    Private TextBoxFilterPatt As TextBox()
    Private TextBoxFilterPatt0 As TextBox
    Private TextBoxFilterPatt1 As TextBox
    Private TextBoxFilterPatt2 As TextBox
    Private TextBoxFilterPatt3 As TextBox
    Private TextBoxFilterPatt4 As TextBox
    Private TextBoxFilterPatt5 As TextBox
    Private TextBoxFilterPatt6 As TextBox
    Private TextBoxFilterPatt7 As TextBox
    Private TextBoxFilterPatt8 As TextBox
    Private TextBoxFilterPatt9 As TextBox

    Private TextBoxFilterMask As TextBox()
    Private TextBoxFilterMask0 As TextBox
    Private TextBoxFilterMask1 As TextBox
    Private TextBoxFilterMask2 As TextBox
    Private TextBoxFilterMask3 As TextBox
    Private TextBoxFilterMask4 As TextBox
    Private TextBoxFilterMask5 As TextBox
    Private TextBoxFilterMask6 As TextBox
    Private TextBoxFilterMask7 As TextBox
    Private TextBoxFilterMask8 As TextBox
    Private TextBoxFilterMask9 As TextBox

    Private TextBoxFunctionalMessage As TextBox()
    Private TextBoxFunctionalMessage0 As TextBox
    Private TextBoxFunctionalMessage1 As TextBox
    Private TextBoxFunctionalMessage2 As TextBox
    Private TextBoxFunctionalMessage3 As TextBox
    Private TextBoxFunctionalMessage4 As TextBox
    Private TextBoxFunctionalMessage5 As TextBox
    Private TextBoxFunctionalMessage6 As TextBox
    Private TextBoxFunctionalMessage7 As TextBox
    Private TextBoxFunctionalMessage8 As TextBox
    Private TextBoxFunctionalMessage9 As TextBox
    Private TextBoxFunctionalMessage10 As TextBox
    Private TextBoxFunctionalMessage11 As TextBox
    Private TextBoxFunctionalMessage12 As TextBox
    Private TextBoxFunctionalMessage13 As TextBox
    Private TextBoxFunctionalMessage14 As TextBox
    Private TextBoxFunctionalMessage15 As TextBox
    Private TextBoxFunctionalMessage16 As TextBox
    Private TextBoxFunctionalMessage17 As TextBox
    Private TextBoxFunctionalMessage18 As TextBox
    Private TextBoxFunctionalMessage19 As TextBox
    Private TextBoxFunctionalMessage20 As TextBox
    Private TextBoxFunctionalMessage21 As TextBox
    Private TextBoxFunctionalMessage22 As TextBox
    Private TextBoxFunctionalMessage23 As TextBox
    Private TextBoxFunctionalMessage24 As TextBox
    Private TextBoxFunctionalMessage25 As TextBox
    Private TextBoxFunctionalMessage26 As TextBox
    Private TextBoxFunctionalMessage27 As TextBox
    Private TextBoxFunctionalMessage28 As TextBox
    Private TextBoxFunctionalMessage29 As TextBox
    Private TextBoxFunctionalMessage30 As TextBox
    Private TextBoxFunctionalMessage31 As TextBox

    Private TextBoxParamVal As TextBox()
    Private TextBoxParamVal0 As TextBox
    Private TextBoxParamVal1 As TextBox
    Private TextBoxParamVal2 As TextBox
    Private TextBoxParamVal3 As TextBox
    Private TextBoxParamVal4 As TextBox
    Private TextBoxParamVal5 As TextBox
    Private TextBoxParamVal6 As TextBox
    Private TextBoxParamVal7 As TextBox
    Private TextBoxParamVal8 As TextBox
    Private TextBoxParamVal9 As TextBox
    Private TextBoxParamVal10 As TextBox
    Private TextBoxParamVal11 As TextBox
    Private TextBoxParamVal12 As TextBox
    Private TextBoxParamVal13 As TextBox
    Private TextBoxParamVal14 As TextBox
    Private TextBoxParamVal15 As TextBox
    Private TextBoxParamVal16 As TextBox
    Private TextBoxParamVal17 As TextBox
    Private TextBoxParamVal18 As TextBox
    Private TextBoxParamVal19 As TextBox
    Private TextBoxParamVal20 As TextBox
    Private TextBoxParamVal21 As TextBox

    Private TextBoxPeriodicMessageFlags As TextBox()
    Private TextBoxPeriodicMessageFlags0 As TextBox
    Private TextBoxPeriodicMessageFlags1 As TextBox
    Private TextBoxPeriodicMessageFlags2 As TextBox
    Private TextBoxPeriodicMessageFlags3 As TextBox
    Private TextBoxPeriodicMessageFlags4 As TextBox
    Private TextBoxPeriodicMessageFlags5 As TextBox
    Private TextBoxPeriodicMessageFlags6 As TextBox
    Private TextBoxPeriodicMessageFlags7 As TextBox
    Private TextBoxPeriodicMessageFlags8 As TextBox
    Private TextBoxPeriodicMessageFlags9 As TextBox

    Private TextBoxPeriodicMessage As TextBox()
    Private TextBoxPeriodicMessage0 As TextBox
    Private TextBoxPeriodicMessage1 As TextBox
    Private TextBoxPeriodicMessage2 As TextBox
    Private TextBoxPeriodicMessage3 As TextBox
    Private TextBoxPeriodicMessage4 As TextBox
    Private TextBoxPeriodicMessage5 As TextBox
    Private TextBoxPeriodicMessage6 As TextBox
    Private TextBoxPeriodicMessage7 As TextBox
    Private TextBoxPeriodicMessage8 As TextBox
    Private TextBoxPeriodicMessage9 As TextBox

    Private TextBoxPeriodicMessageInterval As TextBox()
    Private TextBoxPeriodicMessageInterval9 As TextBox
    Private TextBoxPeriodicMessageInterval8 As TextBox
    Private TextBoxPeriodicMessageInterval7 As TextBox
    Private TextBoxPeriodicMessageInterval6 As TextBox
    Private TextBoxPeriodicMessageInterval5 As TextBox
    Private TextBoxPeriodicMessageInterval4 As TextBox
    Private TextBoxPeriodicMessageInterval3 As TextBox
    Private TextBoxPeriodicMessageInterval2 As TextBox
    Private TextBoxPeriodicMessageInterval1 As TextBox
    Private TextBoxPeriodicMessageInterval0 As TextBox

    Private WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Private WithEvents ContextMenuStripScratchPad As ContextMenuStrip
    Private WithEvents MenuScratchPadUndo As ToolStripMenuItem
    Private WithEvents MenuScratchPadSeparator1 As ToolStripSeparator
    Private WithEvents MenuScratchPadCut As ToolStripMenuItem
    Private WithEvents MenuScratchPadCopy As ToolStripMenuItem
    Private WithEvents MenuScratchPadPaste As ToolStripMenuItem
    Private WithEvents MenuScratchPadSeparator2 As ToolStripSeparator
    Private WithEvents MenuScratchPadSelectAll As ToolStripMenuItem
    Private WithEvents MenuScratchPadSeparator3 As ToolStripSeparator
    Private WithEvents MenuFileMsgOutLoad As ToolStripMenuItem
    Private WithEvents MenuScratchPadAddToOutgoingMessageSet As ToolStripMenuItem
    Private WithEvents MenuFlagsSetDefault As ToolStripMenuItem
    Private WithEvents MenuEditScratchAddToOutgoingMessageSet As ToolStripMenuItem
    Private WithEvents MenuFlagsDelete As ToolStripMenuItem
    Private WithEvents MenuScratchPadDelete As ToolStripMenuItem
    Private WithEvents ContextMenuStripTextBox As ContextMenuStrip
    Private WithEvents MenuTextBoxUndo As ToolStripMenuItem
    Private WithEvents MenuTextBoxSeparator1 As ToolStripSeparator
    Private WithEvents MenuTextBoxCut As ToolStripMenuItem
    Private WithEvents MenuTextBoxCopy As ToolStripMenuItem
    Private WithEvents MenuTextBoxPaste As ToolStripMenuItem
    Private WithEvents MenuTextBoxDelete As ToolStripMenuItem
    Private WithEvents MenuTextBoxSeparator2 As ToolStripSeparator
    Private WithEvents MenuTextBoxSelectAll As ToolStripMenuItem
    Private WithEvents ComboBoxAnalogChannel As ComboBox
    Private WithEvents ToolTipMessageIn As ToolTip
    Private WithEvents MenuFileFilterLoad As ToolStripMenuItem
    Private WithEvents MenuFileFilterSave As ToolStripMenuItem
    Private WithEvents MenuFilePeriodicMessageSave As ToolStripMenuItem
    Private WithEvents PanelAnalog As Panel
    Private WithEvents ContextMenuStripResults As ContextMenuStrip
    Private WithEvents MenuResultsCopyLine As ToolStripMenuItem
    Private WithEvents MenuResultsDelete As ToolStripMenuItem
    Private WithEvents MenuResultsSelectAll As ToolStripMenuItem
    Private WithEvents MenuResultsSpace1 As ToolStripSeparator
    Private WithEvents MenuResultsClear As ToolStripMenuItem
    Private WithEvents GroupBoxAnalogConfig As GroupBox
    Private WithEvents TextBoxAnalogRate As TextBox
    Private WithEvents PanelAudioChannel As Panel
    Private WithEvents ComboBoxAnalogRateMode As ComboBox
    Private WithEvents ButtonClaimJ1939Address As Button
    Private WithEvents MenuFilePeriodicMessageLoad As ToolStripMenuItem
    Private WithEvents MenuFileSpace1 As ToolStripSeparator
    Private WithEvents MenuFileMsgInSaveSelected As ToolStripMenuItem
    Private WithEvents MenuFileMsgInSaveAll As ToolStripMenuItem
    Private WithEvents MenuFileMsgOutSave As ToolStripMenuItem
    Private WithEvents MenuFileSpace2 As ToolStripSeparator
    Private WithEvents MenuFileExit As ToolStripMenuItem
    Private WithEvents MenuFile As ToolStripMenuItem
    Private WithEvents MenuEditUndo As ToolStripMenuItem
    Private WithEvents MenuEditSpace1 As ToolStripSeparator
    Private WithEvents MenuEditCut As ToolStripMenuItem
    Private WithEvents MenuEditCopy As ToolStripMenuItem
    Private WithEvents MenuEditCopyLine As ToolStripMenuItem
    Private WithEvents MenuEditMsgInCopyData As ToolStripMenuItem
    Private WithEvents MenuEditPaste As ToolStripMenuItem
    Private WithEvents MenuEditDelete As ToolStripMenuItem
    Private WithEvents MenuEditSpace2 As ToolStripSeparator
    Private WithEvents MenuEditMsgInMakeFilter As ToolStripMenuItem
    Private WithEvents MenuEditFlagsEdit As ToolStripMenuItem
    Private WithEvents MenuEditRxStatus As ToolStripMenuItem
    Private WithEvents MenuEditFlagsClear As ToolStripMenuItem
    Private WithEvents MenuEditFlagsSetDefault As ToolStripMenuItem
    Private WithEvents MenuEditMsgOutEditMessage As ToolStripMenuItem
    Private WithEvents MenuEditMsgOutAddMessage As ToolStripMenuItem
    Private WithEvents MenuEditMsgOutCopyToScratchPad As ToolStripMenuItem
    Private WithEvents MenuEditMsgOutMakePeriodicMessage As ToolStripMenuItem
    Private WithEvents MenuEditSelectAll As ToolStripMenuItem
    Private WithEvents MenuEditClear As ToolStripMenuItem
    Private WithEvents MenuEdit As ToolStripMenuItem
    Private WithEvents MenuHelpContents As ToolStripMenuItem
    Private WithEvents MenuHelpAbout As ToolStripMenuItem
    Private WithEvents MenuHelp As ToolStripMenuItem
    Private WithEvents MenuMsgInCopyLine As ToolStripMenuItem
    Private WithEvents MenuMsgInCopyData As ToolStripMenuItem
    Private WithEvents MenuMsgInMakeFilter As ToolStripMenuItem
    Private WithEvents MenuMsgInRxStatus As ToolStripMenuItem
    Private WithEvents MenuMsgInSelectAll As ToolStripMenuItem
    Private WithEvents MenuMsgInSpace1 As ToolStripSeparator
    Private WithEvents MenuMsgInSaveSelected As ToolStripMenuItem
    Private WithEvents MenuMsgInSaveAll As ToolStripMenuItem
    Private WithEvents MenuMsgInClear As ToolStripMenuItem

    Private WithEvents MenuMsgOutDelete As ToolStripMenuItem
    Private WithEvents MenuMsgOutEditMessage As ToolStripMenuItem
    Private WithEvents MenuMsgOutAddMessage As ToolStripMenuItem
    Private WithEvents MenuMsgOutCopyToScratchPad As ToolStripMenuItem
    Private WithEvents MenuMsgOutMakePeriodicMessage As ToolStripMenuItem
    Private WithEvents MenuMsgOutSpace1 As ToolStripSeparator
    Private WithEvents MenuMsgOutSave As ToolStripMenuItem
    Private WithEvents MenuMsgOutLoad As ToolStripMenuItem
    Private WithEvents MenuMsgOutClear As ToolStripMenuItem

    Private WithEvents MenuFlagsUndo As ToolStripMenuItem
    Private WithEvents MenuFlagsSpace1 As ToolStripSeparator
    Private WithEvents MenuFlagsCut As ToolStripMenuItem
    Private WithEvents MenuFlagsCopy As ToolStripMenuItem
    Private WithEvents MenuFlagsPaste As ToolStripMenuItem
    Private WithEvents MenuFlagsSpace2 As ToolStripSeparator
    Private WithEvents MenuFlagsSelectAll As ToolStripMenuItem
    Private WithEvents MenuFlagsSpace3 As ToolStripSeparator
    Private WithEvents MenuFlagsEdit As ToolStripMenuItem
    Private WithEvents MenuFlagsClear As ToolStripMenuItem

    Private WithEvents MenuStripMain As MenuStrip
    Private WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Private WithEvents StatusStrip1 As StatusStrip
    Private WithEvents ComboBoxDevice As ComboBox
    Private WithEvents ButtonDiscover As Button
    Private WithEvents ButtonCloseBox As Button
    Private WithEvents ButtonOpenBox As Button
    Private WithEvents GroupBoxDevices As GroupBox
    Private WithEvents ComboBoxAPI As ComboBox
    Private WithEvents ButtonLoadDLL As Button
    Private WithEvents TextBoxDllPath As TextBox

    Private WithEvents GroupBoxAPIs As GroupBox
    Private WithEvents ComboBoxPins As ComboBox
    Private WithEvents ComboBoxConnector As ComboBox
    Private WithEvents CheckBoxMixedMode As CheckBox
    Private WithEvents TextBoxConnectFlags As TextBox
    Private WithEvents ComboBoxBaudRate As ComboBox
    Private WithEvents ButtonConnect As Button
    Private WithEvents ButtonDisconnect As Button
    Private WithEvents ComboBoxConnectChannel As ComboBox

    Private WithEvents GroupBoxConnect As GroupBox

    Private WithEvents GroupBoxJ2534Info As GroupBox
    Private WithEvents ComboBoxMessageChannel As ComboBox
    Private WithEvents CheckBoxPadMessage As CheckBox
    Private WithEvents TextBoxTimeOut As TextBox
    Private DialogFileOpen As OpenFileDialog
    Private DialogFileSave As SaveFileDialog
    Private WithEvents TextBoxOutFlags As TextBox
    Private WithEvents ButtonClearRx As Button
    Private WithEvents ButtonClearTx As Button
    Private WithEvents TextBoxMessageOut As TextBox
    Private WithEvents ButtonSend As Button
    Private WithEvents ButtonReceive As Button
    Private WithEvents TextBoxReadRate As TextBox
    Private WithEvents ButtonClearList As Button
    Private WithEvents ButtonClearAllPeriodicMessages As Button
    Private WithEvents ButtonCancelPeriodicMessages As Button
    Private WithEvents ButtonApplyPeriodicMessages As Button

    Private LabelPeriodicMessageChannel As Label
    Private LabelPeriodicMessageIds As Label
    Private LabelPeriodicMessageFlags As Label
    Private LabelPMMessage As Label
    Private LvInLocator As Label
    Private LvOutLocator As Label
    Private LabelPeriodicMessageInterval As Label
    Private LabelPeriodicMessageDelete As Label
    Private LabelScratchPad As Label
    Private LabelTO As Label
    Private LabelReadRate As Label
    Private LabelProtSupport As Label
    Private LabelJ2534Info As Label
    Private LabelPins As Label
    Private LabelConn As Label
    Private LabelConnectFlags As Label
    Private LabelComboDevice As Label
    Private LabelAPI As Label
    Private LabelVendor As Label
    Private LabelDevice As Label
    Private LabelDllPath As Label
    Private LabelDllName As Label
    Private LabelConnectChannel As Label
    Private LabelAnalogChannel As Label
    Private LabelBaud As Label
    Private LabelDeviceInfo As Label
    Private LabelMsgFlags As Label
    Private LabelVoltWarning As Label
    Private LabelMaxSample As Label
    Private LabelAnalogRate As Label

    Private WithEvents ButtonCreatePassFilter As Button
    Private WithEvents ButtonClearAllFilter As Button
    Private WithEvents ButtonCancelFilter As Button
    Private WithEvents ButtonApplyFilter As Button

    Private LabelFilterIds As Label
    Private LabelFilterType As Label
    Private LabelFilterFlags As Label
    Private LabelFilterFlow As Label
    Private LabelFilterPatt As Label
    Private LabelFilterMask As Label

    Private WithEvents ComboBoxAvailableDevice As ComboBox
    Private WithEvents ComboBoxAvailableChannel As ComboBox
    Private WithEvents ButtonSetConfig As Button
    Private WithEvents ButtonClearConfig As Button

    Private LabelParameters0 As Label
    Private LabelValues0 As Label
    Private LabelMod0 As Label
    Private LabelParameters1 As Label
    Private LabelValues1 As Label
    Private LabelMod1 As Label
    Private WithEvents ButtonExecuteFastInit As Button
    Private WithEvents TextBoxFIMessage As TextBox
    Private WithEvents TextBoxFIFlags As TextBox
    Private LabelFIRXTitle As Label
    Private LabelFITXFlags As Label
    Private LabelFIRxStatus As Label
    Private LabelFIResponse As Label
    Private LabelFIMsgResp As Label
    Private LabelFIMsgData As Label
    Private WithEvents GroupBoxFastInit As GroupBox
    Private WithEvents TextBox5BInitECU As TextBox
    Private WithEvents ButtonExecute5BInit As Button
    Private LabelKWlabel1 As Label
    Private LabelKWlabel0 As Label
    Private Label5BECU As Label
    Private Label5BKeyWord0 As Label
    Private Label5BKeyWord1 As Label
    Private WithEvents GroupBox5BInit As GroupBox
    Private WithEvents ButtonApplyFunctionalMessages As Button
    Private WithEvents ButtonCancelFunctionalMessages As Button
    Private WithEvents ButtonClearAllFunctionalMessages As Button
    Private LabelFunctionalMessageModify As Label
    Private LabelFunctionalMessageValues As Label
    Private LabelAnalogLow1 As Label
    Private LabelAnalogHigh1 As Label
    Private LabelAnalogHigh0 As Label
    Private LabelAnalogLow0 As Label
    Private LabelAnalogReading As Label
    Private WithEvents GroupBoxAnalog As GroupBox
    Private WithEvents ButtonReadBatt As Button
    Private LabelBattRead As Label
    Private WithEvents GroupBoxBattVoltage As GroupBox
    Private WithEvents ButtonSetVoltage As Button
    Private WithEvents ButtonReadVolt As Button
    Private WithEvents TextBoxVoltSetting As TextBox
    Private WithEvents optSet As RadioButton
    Private WithEvents optShortToGround As RadioButton
    Private WithEvents optVoltOff As RadioButton
    Private LabelVoltRead As Label
    Private LabelMV As Label
    Private WithEvents GroupBoxVolt As GroupBox

    Private WithEvents GroupBoxPin As GroupBox
    Private WithEvents GroupBoxProgVoltage As GroupBox

    Private WithEvents ListViewResultsColumnHeader1 As ColumnHeader
    Private WithEvents ListViewResultsColumnHeader2 As ColumnHeader
    Private WithEvents ListViewResults As ListView

    Private WithEvents TabControl1 As TabControl
    Private WithEvents TabPageConnect As TabPage
    Private WithEvents TabPageMessages As TabPage
    Private WithEvents TabPagePeriodicMessages As TabPage
    Private WithEvents TabPageFilters As TabPage
    Private WithEvents TabPageConfig As TabPage
    Private WithEvents TabPageInit As TabPage
    Private WithEvents TabPageFunctionalMessages As TabPage
    Private WithEvents TabPageAnalog As TabPage
    Private WithEvents TabPageResults As TabPage

    Private WithEvents ContextMenuStripMessageIn As ContextMenuStrip
    Private WithEvents ContextMenuStripMessageOut As ContextMenuStrip
    Private WithEvents ContextMenuStripFlags As ContextMenuStrip

End Class